package br.com.uesc.colcic.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

import br.com.uesc.colcic.R;
import br.com.uesc.colcic.adapter.HorarioAdapter;
import br.com.uesc.colcic.adapter.HorarioDescCodMatAdapter;
import br.com.uesc.colcic.auxiliares.DialogInformacoesMaterias;
import br.com.uesc.colcic.domain.Acesso;
import br.com.uesc.colcic.domain.Comandos;
import br.com.uesc.colcic.fragment.SelecionarMateriaExluirFragment;
import br.com.uesc.colcic.modelo.Materia;

public class Meu_HorarioActivity extends AppCompatActivity {
    protected RecyclerView rc_myhorario, rc_mydescCodMat;
    Bundle savedInstanceState;
    boolean portrait = true;


    Meu_HorarioActivity m;
    SelecionarMateriaExluirFragment fragMenuMaterias;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meu__horario);
        this.savedInstanceState = savedInstanceState;
        this.m = this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);

        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Meu Horário");


        rc_myhorario = (RecyclerView) findViewById(R.id.rc_myhorarioLetivo);
        rc_myhorario.setItemAnimator(new DefaultItemAnimator());
        rc_myhorario.setLayoutManager(new LinearLayoutManager(this));
        rc_myhorario.setHasFixedSize(true);

        rc_mydescCodMat = (RecyclerView) findViewById(R.id.rc_mydescCodMat);
        rc_mydescCodMat.setItemAnimator(new DefaultItemAnimator());
        rc_mydescCodMat.setLayoutManager(new LinearLayoutManager(this));
        rc_mydescCodMat.setHasFixedSize(true);
        desenhaHorario(portrait);

    }


    public ArrayList<String[]> removeLinhasVazias(ArrayList<String[]> tabelaHorario) {
        ArrayList<String[]> th = new ArrayList<>();
        for (int i = 0; i < tabelaHorario.size(); i++) {
            String[] linha = tabelaHorario.get(i);
            if (verificaSeLinhaNaoNula(linha)) {
                th.add(linha);
            }
        }
        return th;
    }

    public boolean verificaSeLinhaNaoNula(String[] linha) {
        for (int i = 0; i < linha.length; i++) {
            if (linha[i] != null) {
                return true;
            }
        }
        return false;
    }


    public static ArrayList<String[]> agrupaNomeMateriasLinhasColunas() {
        ArrayList<String[]> tabelaHoraAula = new ArrayList<>();
        ArrayList<String> listHoras = Comandos.returnTodasHorasAulas();
        for (String hora : listHoras) {
            String[] linha = Comandos.returnDisciplinaLinhasMyHorario(hora);
            tabelaHoraAula.add(linha);
        }
        return tabelaHoraAula;
    }

    public void chamaActivityInformacoesMateria(String nome) {
        Intent intent = new Intent(getBaseContext(), DialogInformacoesMaterias.class);
        intent.putExtra("tipo", nome);
        startActivityForResult(intent, 1);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_materias, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.addMat) {
            if (savedInstanceState == null) {
                Bundle args = new Bundle();
                args.putInt("tipo", 1);
                SelecionarMateriaExluirFragment frag_add = new SelecionarMateriaExluirFragment();
                frag_add.setArguments(args);
                this.fragMenuMaterias = frag_add;
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.slide_in_button, R.anim.slide_out_button);
                transaction.replace(R.id.selecionarMateria_meu_horario, this.fragMenuMaterias);
                transaction.commit();

            }
        } else if (id == R.id.delMat) {


            if (savedInstanceState == null) {
                //ação do menu que chama o fragmento para excluir materia cursando
                Bundle args = new Bundle();
                args.putInt("tipo", 0);
                SelecionarMateriaExluirFragment frag_add = new SelecionarMateriaExluirFragment();
                frag_add.setArguments(args);
                this.fragMenuMaterias = frag_add;
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.slide_in_button, R.anim.slide_out_button);
                transaction.replace(R.id.selecionarMateria_meu_horario, this.fragMenuMaterias);
                transaction.commit();

            }
        }
        return super.onOptionsItemSelected(item);
    }

    //fecha o fragmento que escolhe multiplas materias para excluir
    public void fechaFragmentoSelecionarMateria() {

        getSupportFragmentManager().beginTransaction().remove(fragMenuMaterias).commit();
        getSupportFragmentManager().beginTransaction().setCustomAnimations(0, R.anim.slide_in_button).commit();
        getSupportFragmentManager().beginTransaction().setCustomAnimations(0, R.anim.slide_out_button).commit();

    }

    // metodo usado no adapter da lista de materias
    private HorarioDescCodMatAdapter.MateriasOnClickListener onClickMaterias() {
        return new HorarioDescCodMatAdapter.MateriasOnClickListener() {
            @Override
            public void onClickMaterias(View view, String nome) {
                chamaActivityInformacoesMateria(nome);
            }

        };
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            portrait = false;
            desenhaHorario(portrait);

        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            portrait = true;
            desenhaHorario(portrait);

        }
    }


    public void desenhaHorario(boolean portrait) {


        TypedArray cores = getResources().obtainTypedArray(R.array.cores);//pegar as cores das materias

        ArrayList<String> lha = Comandos.returnListaHorasAulasMeuHorario();//pega uma lista com todas as aulas que irei ter
        if (lha != null) {

            if(portrait) {
                rc_myhorario.setAdapter(new HorarioAdapter(this, getBaseContext(), lha, cores, portrait));
                ArrayList<Materia> listMatSemestre = Comandos.listaMateriaHorarioCursando();
                rc_mydescCodMat.setAdapter(new HorarioDescCodMatAdapter(this, listMatSemestre, cores, onClickMaterias()));
            }else {
                rc_myhorario.setAdapter(new HorarioAdapter(this, getBaseContext(), lha, cores, portrait));
            }
        }

    }
}
