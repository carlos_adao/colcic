package br.com.uesc.colcic.modelo;

/**
 * Created by estagio-nit on 01/12/17.
 */

public class Credito {
    String id;
    String nomeMateria;
    int numCredito;
    float notaCredito;

    public Credito(String id, String nomeMateria, int numCredito, float notaCredito) {
        this.id = id;
        this.nomeMateria = nomeMateria;
        this.numCredito = numCredito;
        this.notaCredito = notaCredito;
    }

    public Credito() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNomeMateria() {
        return nomeMateria;
    }

    public void setNomeMateria(String nomeMateria) {
        this.nomeMateria = nomeMateria;
    }

    public int getNumCredito() {
        return numCredito;
    }

    public void setNumCredito(int numCredito) {
        this.numCredito = numCredito;
    }

    public float getNotaCredito() {
        return notaCredito;
    }

    public void setNotaCredito(float notaCredito) {
        this.notaCredito = notaCredito;
    }
}
