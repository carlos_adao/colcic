package br.com.uesc.colcic.fragment.QuadroDocente;
public class CarregaDados {

    private static int counterForLocation;
    private static int counterForName;
    private static int counterForTimePassed;

    static {
        counterForLocation = 0;
        counterForName = 0;
        counterForTimePassed = 0;
    }

    public static String dummyLocation(){

        String locations[] = new String[] {
                "dryoliveira@yahoo.com.br",
                "alanpocoes@yahoo.com.br",
                "prof.alexisteixeira@yahoo.com.br",
                "amandaf.mendes@gmail.com",
                "gabipoll@hotmail.com",
                "arismargr@yahoo.com.br",
                "brunarehem@ifba.edu.br",
                "brunojinfo@gmail.com",
                "cacio.costa@ifba.edu.br",
                "celinaarosasantos@gmail.com",
                "christian@ifba.edu.br",
                "claudiaribeiroef@gmail.com",
                "cad10ba@hotmail.com",
                "danilofisico@gmail.com",
                "deborasantafe@hotmail.com",
                "enex1@hotmail.com",
                "esequias.arquiteto@gmail.com",
                "fabriciolonguinhos@ifba.edu.br",
                "gabyfreitascosta@hotmail.com",
                "ninckgdm@gmail.com",
                "jacyara@ifba.edu.br",
                "belsilva@yahoo.com",
                "jgustavoa@yahoo.com.br",
                "jumenezes2@hotmail",
                "kftvneves@yahoo.com.br",
                "leandroassis@agronomo.eng.br",
                "marciamaiah@yahoo.com.br",
                "marco.goes@ifs.edu.br",
                "ma.isabel.oliveira@gmail.com",
                "oliviafranco2000@yahoo.com.br",
                "brancaselva@yahoo.com.br",
                "agroleandra@yahoo.com.br",
                "philipe@ifba.edu.br",
                "regilan@gmail.com",
                "roripessoa@ig.com.br",
                "roseanesbatista@ifba.edu.br",
                "sscunha.eng@hotmail.com",
                "suzana.oliveirabrito@hotmail.com",
                "thiagofisico@yahoo.com.br",
                "uildo10@gmail.com",
                "urbanocavalcante@yahoo.com.br",
                ""
        };
        return locations[counterForLocation++];
    }

    public static String dummyTimePassed(){

        String timePassed[] = new String[] {
                "História",
                "Seg. Trabalho",
                "Matemática",
                "Biologia",
                "Espanhol",
                "SMS",
                "Biologia",
                "Informática",
                "Educação Física",
                "Sociologia",
                "Química",
                "Ed. Física",
                "Ed. Física",
                "Fisíca",
                "Arquitetura e Urbanismo",
                "Matematica",
                "Desenho e Topografia",
                "Administração",
                "Segurança do Trabalho",
                "Filosofia",
                "Inglês",
                "ARTES",
                "Informática",
                "Português",
                "Geografia",
                "Topografia",
                "Língua Portuguesa",
                "Geografia",
                "Física",
                "Inglês",
                "Matemática",
                "Segurança do Trabalho",
                "História",
                "Lógica",
                "Filosofia",
                "Saúde Ocupacional",
                "Introdução a contrução civil",
                "Português",
                "Física",
                "Geografia",
                "Português",
                ""};
        return timePassed[counterForTimePassed++];
    }

    public static String dummyName(){

        String names[] = new String[] {
                "Adriana de Oliveira Silva",
                "Alan Oliveira dos Santos",
                "Alexis Martins Teixeira",
                "Amanda Ferreira da Silva Mendes",
                "Ana Gabriela Poll",
                "Arismar Estevão Guedes Ramos",
                "Bruna Carmo Rehem",
                "Bruno de Jesus Santos",
                "Cácio Costa da Silva",
                "Celina Rosa dos Santos",
                "Christian Ricardo Silva Passos",
                "Claudia Ribeiro",
                "Cristiano Araujo Dias",
                "Danilo Almeida Souza",
                "Débora Santa Fé Monteiro de Almeida",
                "Enexandro Nobre Dutra",
                "Esequias Souza de Freitas",
                "Fabrício Longuinhos Silva",
                "Gabriela Freitas Costa",
                "Graziela Ninck Dias Manezes",
                "Jacyara Nô dos Santos",
                "Isabel de Fatima Rodrigues Silva",
                "José Gustavo Cordeiro de Araujo",
                "Juliana Santos Menezes",
                "Karina Fernanda Travagim Viturino Neves",
                "Leandro Silva de Assis",
                "Márcia Souza Maia e Araujo",
                "Marco Antônio Tavares Góes",
                "Maria Isabel Almeida de Oliveira",
                "Maria Olívia Berbert da Silva Franco",
                "Mariluce de Oliveira Silva",
                "Mayana Leandra Souza Santos",
                "Phillipe Murillo Santana de Carvalho",
                "Regilan Meira Silva",
                "Rodrigo Rizerio de Almeida e Pessoa",
                "Roseane Santos Batista ",
                "Sandra Cunha Gonçalves",
                "Suzana Oliveira Brito",
                "Thiago Nascimento Barbosa",
                "Uildo Batista Oliveira",
                "Urbano Cavalcante da Silva Filho",
                ""};

        return names[counterForName++];
    }

    public static void resetAllCounters(){
        counterForTimePassed = 0;
        counterForLocation = 0;
        counterForName = 0;
    }

    private static void checkCounterForOverflow(){

        if(counterForName >= 40){
            counterForName = 0;
        } else if(counterForTimePassed >= 40){
            counterForTimePassed = 0;
        } else if(counterForLocation >= 40){
            counterForLocation = 0;
        }
    }

}
