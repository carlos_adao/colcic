package br.com.uesc.colcic.domain.Objetos;

import java.util.ArrayList;

/**
 * Created by carlos on 14/06/2017.
 */

public class InformacoesProfessor {
    String nome, email, tel, urlLates;
    ArrayList<String> listDisciplinaProfessor = new ArrayList<>();

    public InformacoesProfessor() {

    }

    public ArrayList<String> getListDisciplinaProfessor() {
        return listDisciplinaProfessor;
    }

    public void setListDisciplinaProfessor(ArrayList<String> listDisciplinaProfessor) {
        this.listDisciplinaProfessor = listDisciplinaProfessor;
    }

    public InformacoesProfessor(String nome, String email, String tel, String urlLates, ArrayList<String> listDisciplinaProfessor) {

        this.nome = nome;
        this.email = email;
        this.tel = tel;
        this.urlLates = urlLates;
        this.listDisciplinaProfessor = listDisciplinaProfessor;
    }

    public InformacoesProfessor(String nome, String email, String tel, String urlLates) {
        this.nome = nome;
        this.email = email;
        this.tel = tel;
        this.urlLates = urlLates;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getUrlLates() {
        return urlLates;
    }

    public void setUrlLates(String urlLates) {
        this.urlLates = urlLates;
    }

}
