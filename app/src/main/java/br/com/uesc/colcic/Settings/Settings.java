package br.com.uesc.colcic.Settings;

/**
 * Created by estagio-nit on 22/09/17.
 */

public class Settings {

    String cod, att_funcionario, att_professor,att_sala,att_semestre_virgente,att_disciplina,att_ementa,att_pre_requisito,att_disciplina_dia_hora, att_horario_onibus;

    public Settings() { }

    public Settings(String cod, String att_funcionario, String att_professor, String att_sala, String att_semestre_virgente, String att_disciplina, String att_ementa, String att_pre_requisito, String att_disciplina_dia_hora) {
        this.cod = cod;
        this.att_funcionario = att_funcionario;
        this.att_professor = att_professor;
        this.att_sala = att_sala;
        this.att_semestre_virgente = att_semestre_virgente;
        this.att_disciplina = att_disciplina;
        this.att_ementa = att_ementa;
        this.att_pre_requisito = att_pre_requisito;
        this.att_disciplina_dia_hora = att_disciplina_dia_hora;
    }

    public Settings(String cod, String att_funcionario, String att_professor, String att_sala, String att_semestre_virgente, String att_disciplina, String att_ementa, String att_pre_requisito, String att_disciplina_dia_hora, String att_horario_onibus) {
        this.cod = cod;
        this.att_funcionario = att_funcionario;
        this.att_professor = att_professor;
        this.att_sala = att_sala;
        this.att_semestre_virgente = att_semestre_virgente;
        this.att_disciplina = att_disciplina;
        this.att_ementa = att_ementa;
        this.att_pre_requisito = att_pre_requisito;
        this.att_disciplina_dia_hora = att_disciplina_dia_hora;
        this.att_horario_onibus = att_horario_onibus;
    }

    public String getAtt_horario_onibus() {
        return att_horario_onibus;
    }

    public void setAtt_horario_onibus(String att_horario_onibus) {
        this.att_horario_onibus = att_horario_onibus;
    }
    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public String getAtt_funcionario() {
        return att_funcionario;
    }

    public void setAtt_funcionario(String att_funcionario) {
        this.att_funcionario = att_funcionario;
    }

    public String getAtt_professor() {

        return att_professor;
    }

    public void setAtt_professor(String att_professor) {
        this.att_professor = att_professor;
    }

    public String getAtt_sala()
    {
        return att_sala;
    }

    public void setAtt_sala(String att_sala) {

        this.att_sala = att_sala;
    }

    public String getAtt_semestre_virgente() {

        return att_semestre_virgente;
    }

    public void setAtt_semestre_virgente(String att_semestre_virgente) {
        this.att_semestre_virgente = att_semestre_virgente;
    }

    public String getAtt_disciplina() {
        return att_disciplina;
    }

    public void setAtt_disciplina(String att_disciplina) {
        this.att_disciplina = att_disciplina;
    }

    public String getAtt_ementa() {
        return att_ementa;
    }

    public void setAtt_ementa(String att_ementa) {
        this.att_ementa = att_ementa;
    }

    public String getAtt_pre_requisito() {
        return att_pre_requisito;
    }

    public void setAtt_pre_requisito(String att_pre_requisito) {
        this.att_pre_requisito = att_pre_requisito;
    }

    public String getAtt_disciplina_dia_hora() {
        return att_disciplina_dia_hora;
    }

    public void setAtt_disciplina_dia_hora(String att_disciplina_dia_hora) {
        this.att_disciplina_dia_hora = att_disciplina_dia_hora;
    }

    @Override
    public String toString() {
        return "Settings{" +
                "cod='" + cod + '\'' +
                ", att_funcionario='" + att_funcionario + '\'' +
                ", att_professor='" + att_professor + '\'' +
                ", att_sala='" + att_sala + '\'' +
                ", att_semestre_virgente='" + att_semestre_virgente + '\'' +
                ", att_disciplina='" + att_disciplina + '\'' +
                ", att_ementa='" + att_ementa + '\'' +
                ", att_pre_requisito='" + att_pre_requisito + '\'' +
                ", att_disciplina_dia_hora='" + att_disciplina_dia_hora + '\'' +
                ", att_horario_onibus='" + att_horario_onibus + '\'' +
                '}';
    }
}
