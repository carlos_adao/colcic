package br.com.uesc.colcic.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;

import br.com.uesc.colcic.R;
import br.com.uesc.colcic.adapter.CalculadoraUescAdapter;
import br.com.uesc.colcic.auxiliares.DecimalDigitsInputFilter;
import br.com.uesc.colcic.auxiliares.MyDigitsKeyListener;
import br.com.uesc.colcic.fragment.ResultadoCalculadoraUescFragment;

public class CalculadoraUescActivity extends AppCompatActivity {

    Bundle savedInstanceState;
    Animation shake, animation_click;
    Vibrator vibrator;
    ImageView iv_btn_reset, iv_btn_calculate, iv_icon_mais, iv_icon_menos;
    LinearLayout ll_iv_mais_calculadora_uesc, llprincipal_cal_uesc;
    static FrameLayout fl_frag_resultado_cal_uesc;
    private static RecyclerView recyclerView;
    private static Context context;
    public static ArrayList<String> ltest = new ArrayList<>();
    public EditText ed_credito1;
    public static ResultadoCalculadoraUescFragment frag_result;
    public static boolean fg_resultado_aberto = false;
    CalculadoraUescActivity calculadoraUescActivity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora_uesc);
        this.savedInstanceState = savedInstanceState;
        this.context = getBaseContext();
        this.calculadoraUescActivity = this;

        iv_btn_reset = (ImageView) findViewById(R.id.icon_btn_reset);
        iv_btn_calculate = (ImageView) findViewById(R.id.icon_btn_calculator);
        ed_credito1 = (EditText) findViewById(R.id.ed_credito1);
        iv_icon_mais = (ImageView) findViewById(R.id.icon_btn_mais);
        iv_icon_menos = (ImageView) findViewById(R.id.icon_btn_menos);
        ll_iv_mais_calculadora_uesc = (LinearLayout) findViewById(R.id.ll_iv_mais_calculadora_uesc);
        fl_frag_resultado_cal_uesc = (FrameLayout) findViewById(R.id.frag_resultado_cal_uesc);
        llprincipal_cal_uesc = (LinearLayout) findViewById(R.id.llprincipal_cal_uesc);
        ed_credito1.setOnFocusChangeListener(focoTexto);
        ed_credito1.setOnTouchListener(toque);
        ed_credito1.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(2, 2)});
        ed_credito1.setKeyListener(new MyDigitsKeyListener(false, true));



        /*Evento usado para adicionar mascara de notas ao campo*/
        ed_credito1.addTextChangedListener(new TextWatcher() {
            boolean isUpdating;

            public void afterTextChanged(Editable s) {

            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {

            }

            @Override
            public void onTextChanged(
                    CharSequence s, int start, int before, int after) {

                if (isUpdating) {
                    isUpdating = false;
                    return;
                }
                boolean hasMask = s.toString().indexOf('.') > -1 || s.toString().indexOf('-') > -1 || s.toString().indexOf(',') > -1;

                // Remove o '.' e '-' da String
                String str = s.toString().replaceAll("[.]", "").replaceAll("[-]", "").replaceAll("[,]", "");

                if (after > before) {

                    if (str.equals("100")) {//verifica se a nota digita é dez
                        str = str.substring(0, 2) + '.' + str.substring(2);
                        isUpdating = true;
                        ed_credito1.setText(str);
                        ed_credito1.setSelection(Math.max(0, Math.min(hasMask ? start - (before) : start, str.length())));
                    } else if (str.length() > 1) {

                        str = str.substring(0, 1) + '.' + str.substring(1);
                    }
                    // Seta a flag pra evitar chamada infinita
                    isUpdating = true;
                    // seta o novo texto
                    ed_credito1.setText(str);
                    // seta a posição do cursor
                    ed_credito1.setSelection(ed_credito1.getText().length());

                } else {
                    if (str.length() == 2) {
                        str = str.substring(0, 1) + '.' + str.substring(1);
                        isUpdating = true;
                        ed_credito1.setText(str);
                        ed_credito1.setSelection(Math.max(0, Math.min(hasMask ? start - (before - 1) : start, str.length())));
                    } else {
                        isUpdating = true;
                        ed_credito1.setText(str);

                        ed_credito1.setSelection(Math.max(0, Math.min(hasMask ? start - (before) : start, str.length())));
                    }
                }
            }
        });
        ed_credito1.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (keyEvent != null && keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    if (keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                        desenhaLinhasBranco();
                        return false;
                    }

                }
                return false;
            }
        });


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);

        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Calculadora UESC");


        shake = AnimationUtils.loadAnimation(context, R.anim.animation_swing);
        animation_click = AnimationUtils.loadAnimation(context, R.anim.animation_click);
        vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);

        iv_btn_reset.setOnClickListener(clickInIcones);
        iv_btn_calculate.setOnClickListener(clickInIcones);
        iv_icon_mais.setOnClickListener(clickInIcones);
        ll_iv_mais_calculadora_uesc.setOnClickListener(clickInIcones);
        llprincipal_cal_uesc.setOnClickListener(clickInIcones);


        recyclerView = (RecyclerView) findViewById(R.id.rc_linhas_calculadora_uesc);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new CalculadoraUescAdapter(this, ltest));
        recyclerView.setNestedScrollingEnabled(false);
        restartLinhasCalculadora();

    }

    //Método usuado para evento de click nos icone
    private View.OnClickListener clickInIcones = new View.OnClickListener() {
        @SuppressLint("WrongConstant")
        public void onClick(View v) {
            Intent intent;

            switch (v.getId()) {
                /*clicando no icon btn reset*/
                case R.id.icon_btn_reset:
                    reset();

                    break;

                   /*clicando no icon btn calculate*/
                case R.id.icon_btn_calculator:
                    if (!fg_resultado_aberto) {
                        float somaNotas = 0;
                        try {
                            somaNotas += Float.parseFloat(ed_credito1.getText().toString());
                            for (String s_notas : calculadoraUescActivity.ltest) {
                                somaNotas += Float.parseFloat(s_notas);
                            }

                            Bundle args = new Bundle();
                            args.putFloat("soma", somaNotas);
                            ResultadoCalculadoraUescFragment frag_resultado_cal_uesc = new ResultadoCalculadoraUescFragment();
                            frag_resultado_cal_uesc.setArguments(args);
                            frag_result = frag_resultado_cal_uesc;
                            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                            transaction.setCustomAnimations(R.anim.slide_in_top, R.anim.slide_out_button);
                            transaction.replace(R.id.frag_resultado_cal_uesc, frag_result);
                            transaction.commit();
                            setVibrator(iv_btn_calculate);


                            RotateAnimation r = new RotateAnimation(0, 180, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                            r.setDuration(0);
                            r.setFillAfter(true);

                            fl_frag_resultado_cal_uesc.setVisibility(1);
                            fg_resultado_aberto = true;

                            ((InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(iv_btn_calculate.getWindowToken(), 0);

                            llprincipal_cal_uesc.clearFocus();
                        }catch (NumberFormatException e) {

                            Toast.makeText(CalculadoraUescActivity.this, "Preencha todos os campos!", Toast.LENGTH_SHORT).show();

                        }

                        }

                        break;

                        case R.id.icon_btn_mais:

                            desenhaLinhasBranco();//chama metodo para desenhar linhas em branco
                            break;

                        case R.id.ll_iv_mais_calculadora_uesc:
                            desenhaLinhasBranco();
                            break;

                        case R.id.llprincipal_cal_uesc:

                            if (fg_resultado_aberto) {
                                fechaResultado();
                                fechaFragmentoResultado();
                            }

                            break;
                        default:
                            break;
                    }
            }
        }

        ;


        public void setVibrator(ImageView iv) {

            // iv.startAnimation(animation_click);
            vibrator.vibrate(30);
        }

        public void desenhaLinhas() {

            recyclerView.setAdapter(new CalculadoraUescAdapter(this, ltest));

        }

        public void desenhaLinhasBranco() {

            if (ltest.size() < 6) {
                ltest.add("");
                desenhaLinhas();
            }
        }

        public void restartLinhasCalculadora() {
            ltest = new ArrayList<>();
            desenhaLinhas();//chama o metodo para desenhar linhas em branco
            ed_credito1.setText("");
            fl_frag_resultado_cal_uesc.setVisibility(View.GONE);

        }


        //fecha o fragmento que escolhe multiplas materias para excluir
        public void fechaFragmentoResultado() {
            getSupportFragmentManager().beginTransaction().remove(frag_result).commit();
            getSupportFragmentManager().beginTransaction().setCustomAnimations(0, R.anim.slide_in_button).commit();
            getSupportFragmentManager().beginTransaction().setCustomAnimations(0, R.anim.slide_out_button).commit();

        }

        public static void fechaResultado() {
            fl_frag_resultado_cal_uesc.setVisibility(View.GONE);
            fg_resultado_aberto = false;

        }


        //Método usuado para evento de FOCO nos campo de texto
        private View.OnFocusChangeListener focoTexto = new View.OnFocusChangeListener()

        {

            @Override
            public void onFocusChange(View view, boolean hasFocus) {

                switch (view.getId()) {
                /*clicando no icon btn reset*/
                    case R.id.ed_credito1:
                        if (hasFocus) {

                        } else {
                            if (CalculadoraUescActivity.fg_resultado_aberto) {
                                // CalculadoraUescActivity.fechaResultado();
                            }
                        }

                        break;

                    default:
                        break;


                }
            }
        };

        private View.OnTouchListener toque = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (view.getId()) {
                /*clicando no icon btn reset*/
                    case R.id.ed_credito1:
                        if (CalculadoraUescActivity.fg_resultado_aberto) {
                            fechaResultado();
                            fechaFragmentoResultado();
                        }
                        break;

                    default:
                        break;


                }


                return false;
            }
        };

        public void reset() {
            restartLinhasCalculadora();
            fechaFragmentoResultado();
            setVibrator(iv_btn_reset);
            fg_resultado_aberto = false;
        }

        public void resetPacial() {

            fechaFragmentoResultado();
            setVibrator(iv_btn_reset);
            fg_resultado_aberto = false;
        }


    }
