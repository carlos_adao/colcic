package br.com.uesc.colcic.fragment.QuadroDocente;

/**
 * Created by carlos on 09/06/2017.
 */

public class Docente {
    public Docente(String codigo, String nome, String mail, String titulacao, String clas) {
        this.codigo = codigo;
        this.nome = nome;
        this.mail = mail;
        this.titulacao = titulacao;
        this.clas = clas;
    }

    String codigo, nome ,mail,titulacao,clas;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getTitulacao() {
        return titulacao;
    }

    public void setTitulacao(String titulacao) {
        this.titulacao = titulacao;
    }

    public String getClas() {
        return clas;
    }

    public void setClas(String clas) {
        this.clas = clas;
    }
}
