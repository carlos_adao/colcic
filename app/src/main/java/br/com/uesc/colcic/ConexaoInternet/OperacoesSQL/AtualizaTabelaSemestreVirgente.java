package br.com.uesc.colcic.ConexaoInternet.OperacoesSQL;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import br.com.uesc.colcic.ConexaoInternet.Objetos.Professor;
import br.com.uesc.colcic.ConexaoInternet.Objetos.Sala;
import br.com.uesc.colcic.ConexaoInternet.Objetos.SemestreVirgente;
import br.com.uesc.colcic.domain.Comandos;

/**
 * Created by estagio-nit on 23/10/17.
 */

public class AtualizaTabelaSemestreVirgente {

    public static void run(SemestreVirgente svsw , SQLiteDatabase bd) {
        ArrayList<SemestreVirgente> lSemestreVirgente = new ArrayList<>();
        SemestreVirgente svdb;

        Log.i("Atualizando","Tabela Semestre Virgente");

        String query = "SELECT * FROM semestre_virgente";

        Cursor cursor = bd.rawQuery(query, null);

        while (cursor.moveToNext()) {

            int a = cursor.getColumnIndex("codigo");
            int b = cursor.getColumnIndex("ano");
            int c = cursor.getColumnIndex("periodo");


            svdb = new SemestreVirgente();
            svdb.setCodigo(cursor.getString(a));
            svdb.setAno(cursor.getString(b));
            svdb.setPeriodo(cursor.getString(c));
            lSemestreVirgente.add(svdb);
        }


        if(lSemestreVirgente.size() == 0){

            insertSemestreViergente(svsw);

        }else{
            for(SemestreVirgente sv: lSemestreVirgente){
                if (!(sv.getAno().equalsIgnoreCase(svsw.getAno()))) {

                    updateSemestreViergente(svsw);

                } else if (!(sv.getPeriodo().equalsIgnoreCase(svsw.getPeriodo()))) {

                    updateSemestreViergente(svsw);
                }
            }
        }

    }

    public static void updateSemestreViergente(SemestreVirgente svsw) {

        String  ud ="UPDATE semestre_virgente SET codigo = '"+svsw.getCodigo()+ "'" +
                ", ano = '"+svsw.getAno()+ "'" +
                ", periodo = '"+svsw.getPeriodo()+ "'" +
                "  WHERE codigo = '"+svsw.getCodigo()+"' ;";

        String[] sql = {ud};
        Comandos.executeSQL(sql);
    }


    public static void  insertSemestreViergente(SemestreVirgente svsw){

        String ins = "INSERT INTO semestre_virgente VALUES ('"+ svsw.getCodigo() +"','"+ svsw.getAno() +"','"+ svsw.getPeriodo() +"')";
        String[] sql = {ins};
        Comandos.executeSQL(sql);

    }

}
