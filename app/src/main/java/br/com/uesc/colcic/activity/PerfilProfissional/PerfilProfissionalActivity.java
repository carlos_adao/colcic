package br.com.uesc.colcic.activity.PerfilProfissional;

import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import br.com.uesc.colcic.R;
import br.com.uesc.colcic.views.JustifiedTextView;

public class PerfilProfissionalActivity extends AppCompatActivity {
    JustifiedTextView tv_conteudo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil_profissional);

        tv_conteudo = (JustifiedTextView)findViewById(R.id.tv_conteudo);
        tv_conteudo.setText(getString(R.string.p1_perfil_profissional) + getString(R.string.p2_perfil_profissional) +getString(R.string.p3_perfil_profissional) +getString(R.string.p4_perfil_profissional) +getString(R.string.p5_perfil_profissional));

        final Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        CollapsingToolbarLayout collapsingToolbar = findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle("Profissional");

        loadBackdrop();
    }

    private void loadBackdrop() {
        final ImageView imageView = findViewById(R.id.backdrop);
        Glide.with(this).load(R.drawable.banner_perfil_profi).into(imageView);
    }

}
