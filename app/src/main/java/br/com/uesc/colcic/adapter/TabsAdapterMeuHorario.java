package br.com.uesc.colcic.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import br.com.uesc.colcic.R;
import br.com.uesc.colcic.fragment.ColcicSobreFragment;
import br.com.uesc.colcic.fragment.MeuHorarioFragment;

public class TabsAdapterMeuHorario extends FragmentPagerAdapter {
    private Context context;

    public TabsAdapterMeuHorario(Context context, FragmentManager fm) {
        super(fm);

        this.context = context;


    }

    @Override
    public int getCount() {
        return 5;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        if (position == 0) {
            return context.getString(R.string.tbh1);
        } else if (position == 1) {
            return context.getString(R.string.tbh2);
        }else if (position == 2) {
            return context.getString(R.string.tbh3);
        }else if (position == 3) {
            return context.getString(R.string.tbh4);
        }
        return context.getString(R.string.tbh5);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment f = null;
        Log.i("get item","chegando");
        if (position == 0) {
            f = MeuHorarioFragment.newInstance(2);
        } else if (position == 1) {
            f = MeuHorarioFragment.newInstance(3);
        }else if (position == 2) {
            f = MeuHorarioFragment.newInstance(4);
        }else if (position == 3) {
            f = MeuHorarioFragment.newInstance(5);
        }else{
            f = MeuHorarioFragment.newInstance(6);
        }
        return f;
    }
}
