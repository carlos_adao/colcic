package br.com.uesc.colcic.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import br.com.uesc.colcic.R;
import br.com.uesc.colcic.activity.CicActivity;
import br.com.uesc.colcic.auxiliares.DialogInformacoesProfessores;
import br.com.uesc.colcic.domain.Comandos;
import br.com.uesc.colcic.fragment.QuadroDocente.Docente;
import br.com.uesc.colcic.fragment.QuadroDocente.MyRecyclerViewAdapter;
import br.com.uesc.colcic.fragment.QuadroDocente.ObjectProfessor;


public class ColcicRecursosFragment extends BaseFragment {
    RecyclerView mRecyclerView;
    LinearLayoutManager mLayoutManager;
    RecyclerView.Adapter mAdapter;
    LinearLayout llimg_pes, ll_edt_pesquisar;
    EditText edt_pesquisar;
    Context context;
    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_colcic_recursos, container, false);
        this.context = getContext();

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView = (RecyclerView) view.findViewById(R.id.rc_listaDocentes);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        atualizaLista("");


        return view;
    }

    // Método para instanciar esse fragment pelo tipo.
    public static ColcicRecursosFragment newInstance(String tipo) {
        Bundle args = new Bundle();
        args.putString("tipo", tipo);
        ColcicRecursosFragment f = new ColcicRecursosFragment();
        f.setArguments(args);
        return f;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    //Metodo usado para retornar a lista de professores docurso
    public ArrayList<Docente> returnListDocentes(String consulta) {
        ArrayList<Docente> listDocentes;
        if (consulta.equalsIgnoreCase("")) {
            listDocentes = Comandos.returnListProfessores();
        } else {
            listDocentes = Comandos.returnListProfessoresLike(consulta);
        }
        return listDocentes;
    }


    // metodo usado para pegar a linha que foi clicada do recicler professor
    private MyRecyclerViewAdapter.ProfOnClickListener onClickProfessor() {
        return new MyRecyclerViewAdapter.ProfOnClickListener() {
            @Override
            public void onClickProfessor(View view, String nome) {
                Intent intent = new Intent(getContext(), DialogInformacoesProfessores.class);
                intent.putExtra("tipo", nome);
                startActivityForResult(intent, 1);
            }
        };
    }
    public void atualizaLista(String nome){

        ArrayList<Docente> listDocentes = returnListDocentes(nome);
        mAdapter = new MyRecyclerViewAdapter(context, listDocentes, onClickProfessor());
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

    }
}