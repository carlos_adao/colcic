package br.com.uesc.colcic.auxiliares;

import android.app.ListActivity;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import java.util.ArrayList;

import br.com.uesc.colcic.R;
import br.com.uesc.colcic.activity.MeuHorarioActivity;
import br.com.uesc.colcic.adapter.ListaMateriaAdapter;
import br.com.uesc.colcic.domain.Comandos;
import br.com.uesc.colcic.domain.Item;
import br.com.uesc.colcic.domain.Tipo;

public class DialogListasGenericas extends ListActivity {
    ArrayList<String> listaNomeMaterias;
    ArrayList<Item> ListaItens;
    private TypedArray imgs;
    String tipo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tipo = getIntent().getStringExtra("tipo");

        if (tipo.equalsIgnoreCase("listaMaterias")) {
            setTitle("Selecione a materia");
        }

        listaNomeMaterias = returnListMateriasBd();
        final ArrayList<Tipo> listaTipos = returnListTips(listaNomeMaterias);
        //ArrayAdapter<Tipo> adapter = new ListaMateriaAdapter(this, listaTipos);
        //setListAdapter(adapter);

        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Tipo item = listaTipos.get(i);
                Intent returnIntent = new Intent();
                returnIntent.putExtra("tipo", item.getName());
                setResult(RESULT_OK, returnIntent);
                imgs.recycle();
                finish();
            }
        });

    }

   //retorna a lista de materia do banco de dados
    public ArrayList<String> returnListMateriasBd() {

        return Comandos.listaNomeMaterias();

    }


    //metodo usado para retornar uma lista de tipos
    public ArrayList<Tipo> returnListTips(ArrayList<String> listaNomeMaterias) {

        ArrayList<Tipo> listaTipos = new ArrayList<>();
        imgs = getResources().obtainTypedArray(R.array.img_materia);
        for (int i = 0; i < listaNomeMaterias.size(); i++) {
            listaTipos.add(new Tipo(listaNomeMaterias.get(i).toString(), imgs.getDrawable(0)));
        }
        return listaTipos;
    }
}


