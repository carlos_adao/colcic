package br.com.uesc.colcic.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import br.com.uesc.colcic.R;
import br.com.uesc.colcic.activity.CicActivity;
import br.com.uesc.colcic.auxiliares.DialogHasInternet;
import br.com.uesc.colcic.auxiliares.MetodosPublicos;
import br.com.uesc.colcic.views.JustifiedTextView;

public class ColcicSobreFragment extends BaseFragment {
    TextView tv_sobre_titulo;
    JustifiedTextView tv_conteudo;
    ImageView iv_home, iv_phone, iv_fc, iv_localizacao;
    Animation shake, animation_click;
    Context context;
    Vibrator vibrator;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_colcic_sobre, container, false);

        this.context = getContext();

        /*Vinculação da variaveis com o xml*/
        iv_home = (ImageView) view.findViewById(R.id.iv_home);
        iv_phone = (ImageView) view.findViewById(R.id.iv_phone);
        iv_fc = (ImageView) view.findViewById(R.id.iv_fc);
        iv_localizacao = (ImageView) view.findViewById(R.id.iv_localizacao);

        /*Atribuido eventos ao icons*/
        iv_home.setOnClickListener(clickInIcones);
        iv_phone.setOnClickListener(clickInIcones);
        iv_fc.setOnClickListener(clickInIcones);
        iv_localizacao.setOnClickListener(clickInIcones);



        tv_sobre_titulo = (TextView) view.findViewById(R.id.tv_titulo_sobre);
        tv_sobre_titulo.setText(getString(R.string.titulo));
        tv_conteudo = (JustifiedTextView)view.findViewById(R.id.tv_conteudo);
        tv_conteudo.setText(getString(R.string.p1_colcic_sobre) + getString(R.string.p2_colcic_sobre) + getString(R.string.p3_colcic_sobre) + getString(R.string.p4_colcic_sobre));

        shake = AnimationUtils.loadAnimation(context, R.anim.animation_swing);
        animation_click = AnimationUtils.loadAnimation(context, R.anim.animation_click);
        vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        return view;

    }

    // Método para instanciar esse fragment pelo tipo.
    public static ColcicSobreFragment newInstance(String tipo) {
        Bundle args = new Bundle();
        args.putString("tipo", tipo);
        ColcicSobreFragment f = new ColcicSobreFragment();
        f.setArguments(args);

        return f;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    //Método usuado para evento de click nos icone
    private View.OnClickListener clickInIcones = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent;

            switch (v.getId()) {
                /*clicando no icon home*/
                case R.id.iv_home:

                    setVibrator(iv_home);

                    if (!MetodosPublicos.verificaConexao(context)) {
                        intent = new Intent(getContext(), DialogHasInternet.class);
                        startActivityForResult(intent, 1);
                    } else {

                        intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.web_site_colcic)));
                        startActivity(intent);

                    }

                    break;

                   /*clicando no icon phone*/
                case R.id.iv_phone:

                    intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", getString(R.string.phone_colcic), null));
                    startActivity(intent);
                    setVibrator(iv_phone);
                    break;

                   /*clicando no icon facebook*/
                case R.id.iv_fc:

                    setVibrator(iv_fc);

                    if (!MetodosPublicos.verificaConexao(context)) {
                        intent = new Intent(getContext(), DialogHasInternet.class);
                        startActivityForResult(intent, 1);
                    } else {

                        intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.fb_colcic)));
                        startActivity(intent);

                    }
                    break;

                  /*clicando no icon localização*/
                case R.id.iv_localizacao:
                    //setVibrator(iv_localizacao);

                    //iv_localizacao.startAnimation(animation_click);


                    vibrator.vibrate(30);
                    Toast.makeText(getActivity(), "Click in localização", Toast.LENGTH_LONG).show();
                    break;

                default:
                    break;
            }
        }
    };

    public void setVibrator(ImageView iv) {

        iv.startAnimation(animation_click);
        vibrator.vibrate(30);
    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser) {

        }
    }
}
