package br.com.uesc.colcic.activity;


import android.app.SearchManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import br.com.uesc.colcic.R;
import br.com.uesc.colcic.adapter.TabsAdapter;
import br.com.uesc.colcic.fragment.ColcicRecursosFragment;
import br.com.uesc.colcic.fragment.ColcicTabFragment;

public class CicActivity extends BaseActivity {
    SearchView searchView = null;
    ColcicTabFragment frag;
    Toolbar toolbar;
    Menu m;
    MenuItem mSearchMenuItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cic);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        iniciaTabFragmentoColcic(savedInstanceState, "Colcic");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.m = menu;
        getMenuInflater().inflate(R.menu._menu_icon_text_tabs, m);
        SearchManager searchManager = (SearchManager) getSystemService(this.SEARCH_SERVICE);
        mSearchMenuItem = m.findItem(R.id.action_search);
        searchView = (SearchView) m.findItem(R.id.action_search)
                .getActionView();

        if (mSearchMenuItem != null) {
            searchView = (SearchView) mSearchMenuItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(this
                    .getComponentName()));
        }
        searchView.setIconifiedByDefault(true);
        MenuItemCompat.expandActionView(mSearchMenuItem);

        SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
            public boolean onQueryTextChange(String query) {
                // this is your adapter that will be filtered
                //frag.enviaMensagem(query);//Envia mensagem para pesquisar no fragmento
                frag.comunicacaoColcicRecursosFragment(query);
                return false;

            }

            public boolean onQueryTextSubmit(String query) {
                // Here u can get the value "query" which is entered in the

                return false;

            }
        };
        searchView.setOnQueryTextListener(queryTextListener);

        return super.onCreateOptionsMenu(menu);
    }

    //Iniicia a tab fragment para o colcic
    public void iniciaTabFragmentoColcic(Bundle savedInstanceState, String titulo) {
        // Título da toolbar e botão up navigation
        getSupportActionBar().setTitle(titulo);
        // Adiciona o fragment no layout
        if (savedInstanceState == null) {
            // Cria o fragment com o mesmo Bundle (args) da intent
            ColcicTabFragment frag = new ColcicTabFragment();
            this.frag = frag;
            frag.setArguments(getIntent().getExtras());

            // Adiciona o fragment no layout
            getSupportFragmentManager().beginTransaction().add(R.id.alternavelFragment, this.frag).commit();
        }
    }

    public void mudaTitulo(String titulo) {

        getSupportActionBar().setTitle(titulo);

    }
    public void invisibleMenu(){
        this.mSearchMenuItem.setVisible(false);
        Log.i("Chegando", "invisible");
    }

    public void visibleMenu(){
        this.mSearchMenuItem.setVisible(true);
        Log.i("Chegando", "visible");
    }




}
