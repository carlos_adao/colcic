package br.com.uesc.colcic.Settings;

import android.content.Context;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import br.com.uesc.colcic.R;

public class SettingsModel {
    private String BASE_URL, URL_SETTINGS;

    public JSONObject getJsonSettings(Context context) throws IOException, JSONException {
        BASE_URL = context.getResources().getString(R.string.URL);
        URL_SETTINGS = context.getResources().getString(R.string.URL_settings);
        Log.i("MSG", "Chegando");

        String url = BASE_URL + URL_SETTINGS;
        int resposta = connectarSettings(url).getResponseCode();
        if (resposta == HttpURLConnection.HTTP_OK) {
            InputStream is = connectarSettings(url).getInputStream();
            JSONObject json = new JSONObject(bytesParaString(is));
            return json;
        }

        return null;
    }

    private HttpURLConnection connectarSettings(String urlArquivo) throws IOException {

        final int SEGUNDOS = 1000;
        URL url = new URL(urlArquivo.toString());
        HttpURLConnection conexao = (HttpURLConnection) url.openConnection();
        conexao.setReadTimeout(10 * SEGUNDOS);
        conexao.setConnectTimeout(15 * SEGUNDOS);
        conexao.setRequestMethod("GET");
        conexao.setDoInput(true);
        conexao.setDoOutput(false);
        conexao.connect();
        return conexao;
    }

    /*Utilizado para fazer a bufferização dos dados vindo pela rede*/
    private String bytesParaString(InputStream is) throws IOException {
        byte[] buffer = new byte[1024];
        // O bufferzao vai armazenar todos os bytes lidos
        ByteArrayOutputStream bufferzao = new ByteArrayOutputStream();
        // precisamos saber quantos bytes foram lidos
        int bytesLidos;
        // Vamos lendo de 1KB por vez...
        while ((bytesLidos = is.read(buffer)) != -1) {
            // copiando a quantidade de bytes lidos do buffer para o bufferzão
            bufferzao.write(buffer, 0, bytesLidos);
        }
        return new String(bufferzao.toByteArray(), "UTF-8");
    }
}

