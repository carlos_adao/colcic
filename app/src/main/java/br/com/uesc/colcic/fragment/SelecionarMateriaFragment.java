package br.com.uesc.colcic.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import br.com.uesc.colcic.R;
import br.com.uesc.colcic.activity.MeuHorarioActivity;
import br.com.uesc.colcic.auxiliares.DialogListasGenericas;
import br.com.uesc.colcic.domain.Comandos;
import br.com.uesc.colcic.modelo.Materia;


public class SelecionarMateriaFragment extends Fragment {
    EditText ed_nomeMateria;
    ImageView imgSalvar, imgCancelar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_selecionar_mateia, container, false);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar_selecionarMateria);
        toolbar.setTitle("Adcionar matéria");
        imgSalvar = (ImageView) view.findViewById(R.id.imv_salvar);
        imgCancelar = (ImageView) view.findViewById(R.id.imv_excluir);

        ed_nomeMateria = (EditText) view.findViewById(R.id.ed_nomeMateria);
        ed_nomeMateria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getContext(), DialogListasGenericas.class);
                intent.putExtra("tipo", "listaMaterias");
                startActivityForResult(intent, 1);


            }

        });

        imgSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String nomeDisciplina = ed_nomeMateria.getText().toString();
                if(nomeDisciplina.equalsIgnoreCase("")){

                    Toast.makeText(getContext(), "Selecione a Matéria", Toast.LENGTH_SHORT).show();

                }else {

                    String cod = Comandos.returnCodDisciplina(nomeDisciplina);
                    String turma = Comandos.returnTurmaDisciplina(nomeDisciplina);
                    Materia mat = Comandos.listaMateriaMeuHorario(nomeDisciplina);
                    Comandos.insertIntoMeuHorario(cod,turma, mat.getHora_inicio(), mat.getHora_fim());
                    fechaFragmento();
                    atualizaActivity();
                    Toast.makeText(getContext(), "Matéria adicionada ao horário!", Toast.LENGTH_LONG).show();

                }
            }

        });
        imgCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                fechaFragmento();

            }

        });


        return view;
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String retorno;

        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {

            retorno = data.getStringExtra("tipo");

            ed_nomeMateria.setText(String.valueOf(retorno));


        }
    }

    public void fechaFragmento() {

        ((MeuHorarioActivity) getActivity()).fechaFragmento();
    }

    //atualia o horário na activiti de meu horario
    public void atualizaHorarioInMeuHorarioActivity(){

    }

    public void atualizaActivity(){

        ((MeuHorarioActivity) getActivity()).iniciaTabFragmentoMeuHorarioExterno();
    }

}
