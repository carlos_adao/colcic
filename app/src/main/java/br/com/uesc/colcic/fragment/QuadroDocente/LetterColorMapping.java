package br.com.uesc.colcic.fragment.QuadroDocente;
import java.util.HashMap;
import br.com.uesc.colcic.R;

public class LetterColorMapping {


    public static HashMap<String, Integer> letterToColorIdMapping;

    static {
        letterToColorIdMapping = new HashMap<String, Integer>(){{
            put("A",R.color.c1);
            put("B",R.color.c2);
            put("C",R.color.c3);
            put("D",R.color.c4);
            put("E",R.color.c5);
            put("F",R.color.c6);
            put("G",R.color.c7);
            put("H",R.color.c8);
            put("I",R.color.c9);
            put("J",R.color.c10);
            put("K",R.color.c11);
            put("L",R.color.c12);
            put("M",R.color.c13);
            put("N",R.color.c14);
            put("O",R.color.c15);
            put("P",R.color.c16);
            put("Q",R.color.c17);
            put("R",R.color.c18);
            put("S",R.color.c19);
            put("T",R.color.c20);
            put("U",R.color.c21);
            put("V",R.color.c22);
            put("W",R.color.c23);
            put("X",R.color.c24);
            put("Y",R.color.c25);
            put("Z",R.color.c26);
        }};
    }
}
