package br.com.uesc.colcic.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.uesc.colcic.R;
import br.com.uesc.colcic.domain.Tipo;

public class SelecionarMateriaAdapter extends RecyclerView.Adapter<SelecionarMateriaAdapter.ViewHolder> {

    private List<Tipo> listaMaterias;
    private MateriaClick materiaclick;


    public SelecionarMateriaAdapter(List<Tipo> listaMaterias, MateriaClick materiaclick) {
        this.listaMaterias = listaMaterias;
        this.materiaclick = materiaclick;
    }




    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_selecionar_materia, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.bindData(listaMaterias.get(position));
        if (materiaclick != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    materiaclick.onclick(holder.itemView,listaMaterias.get(position).getName());
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return listaMaterias.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivMateriaSelecionarExluir;
        private TextView tvMateriaSelecionarExluir;


        public ViewHolder(View v) {
            super(v);
            ivMateriaSelecionarExluir = (ImageView) v.findViewById(R.id.ivMateriaSelecionarExcluir);
            tvMateriaSelecionarExluir = (TextView) v.findViewById(R.id.tvMateriaSelecionarExcluir);

        }

        public void bindData(Tipo materia) {
            ivMateriaSelecionarExluir.setImageDrawable(materia.getimg());
            tvMateriaSelecionarExluir.setText(materia.getName());
        }
    }

    public interface MateriaClick {
        public void onclick(View view, String idx);
    }

}
