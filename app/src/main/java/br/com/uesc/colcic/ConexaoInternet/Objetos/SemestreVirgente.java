package br.com.uesc.colcic.ConexaoInternet.Objetos;

/**
 * Created by estagio-nit on 23/10/17.
 */

public class SemestreVirgente {

    String codigo;
    String ano;
    String periodo;

    public SemestreVirgente() {

    }

    public SemestreVirgente(String codigo, String ano, String periodo) {
        this.codigo = codigo;
        this.ano = ano;
        this.periodo = periodo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }
}
