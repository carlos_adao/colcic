package br.com.uesc.colcic.fragment.QuadroDocente;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;

public class CircleDrawable extends Drawable {

    Paint mPaint;
    RectF mRectF;

    public CircleDrawable(int backgroundColor) {

        this.mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setColor(backgroundColor);

        this.mRectF = new RectF();
    }

    @Override
    public void draw(Canvas canvas) {

        int height = getBounds().height();
        int width = getBounds().width();

        if (width != height) {
            mRectF.left = 0.0f;
            mRectF.top = 0.0f;
            mRectF.right = width;
            mRectF.bottom = height;
            canvas.drawOval(mRectF, mPaint);
        }

        else {
            canvas.drawCircle(height / 2, width / 2, width / 2, mPaint);
        }
    }

    @Override
    public void setAlpha(int alpha) {
        mPaint.setAlpha(alpha);
    }


    @Override
    public void setColorFilter(ColorFilter colorFilter) {
        mPaint.setColorFilter(colorFilter);
    }

    @Override
    public int getOpacity() {
        return PixelFormat.OPAQUE;
    }
}
