package br.com.uesc.colcic.fragment.QuadroDocente;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import br.com.uesc.colcic.R;


public class MyRecyclerOnScrollListener extends RecyclerView.OnScrollListener{

    private int positionDelta = -1;

    private LinearLayoutManager mLinearLayoutManager;

    private RecyclerView mRecyclerView;

    private MaskedTileDataObjectHolder mMaskedTileDataObjectHolder;

    private Context mContext;

    public MyRecyclerOnScrollListener(LinearLayoutManager linearLayoutManager, RecyclerView recyclerView, Context context){
        this.mLinearLayoutManager = linearLayoutManager;
        this.mRecyclerView = recyclerView;
        this.mMaskedTileDataObjectHolder = new MaskedTileDataObjectHolder(context);
        this.mContext = context;
    }

    public static class MaskedTileDataObjectHolder {

        TextView maskedNameTextView, maskedLocationTextView, maskedTimePassedTextView, maskedNamePlateTextView;

        View maskedItemView;

        public MaskedTileDataObjectHolder(Context context){
            maskedItemView = ((Activity)context).findViewById(R.id.masked_tile);
            maskedNameTextView = (TextView)((Activity)context).findViewById(R.id.masked_name_text_view);
            maskedLocationTextView = (TextView)((Activity)context).findViewById(R.id.masked_place);
            maskedTimePassedTextView = (TextView) ((Activity)context).findViewById(R.id.masked_time_passed_out);
            maskedNamePlateTextView = (TextView) ((Activity)context).findViewById(R.id.masked_name_plate_text_view);
        }
    }

    @Override
    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);
    }


    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        if(Math.abs(positionDelta - mLinearLayoutManager.findFirstVisibleItemPosition()) > 0) {

            if (mLinearLayoutManager.findFirstVisibleItemPosition() < positionDelta) {

                View v = mRecyclerView.getChildAt(1);
                v.setAlpha(1);
            }

        }

        }


    public void imitateMaskedTileViewWithTileView(final MyRecyclerViewAdapter.DataObjectHolder dataObjectHolder){

        mMaskedTileDataObjectHolder.maskedNameTextView.setText(dataObjectHolder.nameTextView.getText());
        mMaskedTileDataObjectHolder.maskedNamePlateTextView.setText(dataObjectHolder.namePlateTextView.getText());
        mMaskedTileDataObjectHolder.maskedTimePassedTextView.setText(dataObjectHolder.areaAtuacaoTextView.getText());
        mMaskedTileDataObjectHolder.maskedLocationTextView.setText(dataObjectHolder.emailTextView.getText());


        String namePlateLetter = mMaskedTileDataObjectHolder.maskedNamePlateTextView.getText().toString().toUpperCase().substring(0,1);


        CircleDrawable circleDrawable = new CircleDrawable(mContext.getResources().getColor(LetterColorMapping.letterToColorIdMapping.get(namePlateLetter)));


        mMaskedTileDataObjectHolder.maskedNamePlateTextView.setBackgroundDrawable(circleDrawable);

    }
}
