package br.com.uesc.colcic.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import br.com.uesc.colcic.R;
import br.com.uesc.colcic.activity.CicActivity;
import br.com.uesc.colcic.adapter.ListaFuncionariosAdapter;
import br.com.uesc.colcic.domain.Comandos;
import br.com.uesc.colcic.fragment.QuadroDocente.Docente;
import br.com.uesc.colcic.fragment.QuadroDocente.MyRecyclerViewAdapter;
import br.com.uesc.colcic.modelo.Funcionario;
import br.com.uesc.colcic.views.JustifiedTextView;

public class ColcicEstruturaFragment extends BaseFragment {
    RecyclerView rc_listaFuncionarios, rc_listaLaboratorios;
    LinearLayoutManager layoutManager;
    private Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_colcic_estrutura, container, false);
        layoutManager = new LinearLayoutManager(getActivity());
        context = getContext();

        rc_listaFuncionarios = (RecyclerView) view.findViewById(R.id.rc_listaFuncionarios);
        rc_listaFuncionarios.setHasFixedSize(true);
        rc_listaFuncionarios.setLayoutManager(layoutManager);
        rc_listaFuncionarios.setItemAnimator(new DefaultItemAnimator());
        carregaListaFuncionarios();
        return view;
    }

    //metodo para preencher a lista de funcionarios no recyclerview
    public void carregaListaFuncionarios() {
        ArrayList<Funcionario> listaFuncionarios = Comandos.retornaListaFuncionarios();
        listaFuncionarios = obtemIconesFuncionarios(listaFuncionarios);
        ListaFuncionariosAdapter ListaFuncionariosAdapter = new ListaFuncionariosAdapter(context, listaFuncionarios, onClickFuncionario());
        rc_listaFuncionarios.setAdapter(ListaFuncionariosAdapter);
    }
    //metodo para preencher a lista de funcionarios no recyclerview
    /*public void  carregaListaLaboratorios(){
        ArrayList<Docente> listDocentes = returnListDocentes("");
        mAdapter = new MyRecyclerViewAdapter(context, listDocentes, onClickProfessor());
        mRecyclerView.setAdapter(mAdapter);

    }*/

    // Método para instanciar esse fragment pelo tipo.
    public static ColcicEstruturaFragment newInstance(String tipo) {
        Bundle args = new Bundle();
        args.putString("tipo", tipo);
        ColcicEstruturaFragment f = new ColcicEstruturaFragment();
        f.setArguments(args);
        return f;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    //Evento de click na lista de funcionarios
    private ListaFuncionariosAdapter.FuncionarioOnClickListener onClickFuncionario() {
        return new ListaFuncionariosAdapter.FuncionarioOnClickListener() {

            @Override
            public void onClickFuncionario(View view, int idx) {
                //Toast.makeText(getContext(), "Clicou na lista de funcionarios", Toast.LENGTH_LONG).show();
            }
        };
    }

    //Obtenhos o icones para o fragmento COLCIC_ESTRUTURA
    public ArrayList<Funcionario> obtemIconesFuncionarios(ArrayList<Funcionario> listaFuncionarios) {
        Funcionario auxFuncionario;
        int id;
        ArrayList<Funcionario> listaAuxFuncionarios = new ArrayList<>();
        TypedArray imgs = getResources().obtainTypedArray(R.array.icons_fragmento_colcic_estrutura);

        for (int i = 0; i < listaFuncionarios.size(); i++) {
            auxFuncionario = listaFuncionarios.get(i);
            if (auxFuncionario.getSexo().equalsIgnoreCase("masculino") && auxFuncionario.getCargo().equalsIgnoreCase("Coordenador")) {
                id = 0;
                auxFuncionario.setImg(imgs.getDrawable(id));
            } else if (auxFuncionario.getSexo().equalsIgnoreCase("feminino")) {
                 id = 1;
                auxFuncionario.setImg(imgs.getDrawable(id));
            } else if (auxFuncionario.getSexo().equalsIgnoreCase("masculino")) {
                id = 2;
                auxFuncionario.setImg(imgs.getDrawable(id));
            }
            listaAuxFuncionarios.add(auxFuncionario);
        }
        return listaAuxFuncionarios;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);


    }
}
