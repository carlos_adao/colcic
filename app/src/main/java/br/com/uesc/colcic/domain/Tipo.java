package br.com.uesc.colcic.domain;

import android.graphics.drawable.Drawable;

/**
 * Created by carlos on 22/05/2017.
 */

public class Tipo {

    private String name;
    private Drawable img;
    private boolean isSelected;

    public Tipo(String name, Drawable img) {
        this.name = name;

        this.img = img;
    }

    public String getName() {
        return name;
    }

    public Drawable getimg() {
        return img;
    }

    public boolean isSelected() {
        return isSelected;
    }
    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}

