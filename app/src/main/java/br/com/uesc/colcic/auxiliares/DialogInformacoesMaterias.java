package br.com.uesc.colcic.auxiliares;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import br.com.uesc.colcic.R;
import br.com.uesc.colcic.domain.Comandos;
import br.com.uesc.colcic.modelo.InformacoesMateria;

public class DialogInformacoesMaterias extends Activity {
    TextView tv_nomeMateria, tv_localizacao,tv_sala,tv_professor;
    private String nome;
    private String emailProf;
    LinearLayout li;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog_informacoes_materias);

        nome = getIntent().getStringExtra("tipo");

        tv_nomeMateria = (TextView)findViewById(R.id.tv_nomeMateria);
        tv_localizacao = (TextView)findViewById(R.id.tv_localizacao);
        tv_sala = (TextView)findViewById(R.id.tv_sala);
        li = (LinearLayout)findViewById(R.id.li_email);
        tv_professor = (TextView)findViewById(R.id.tv_professor);


        final InformacoesMateria info = Comandos.returnInformacoesDisciplina(nome);


        tv_localizacao.setText(info.getLocalizacao());
        tv_sala.setText("Sala: "+info.getSala());
        SpannableString content = new SpannableString(info.getNomeMat());
        content.setSpan(new UnderlineSpan(), 0, info.getNomeMat().length(), 0);
        tv_nomeMateria.setText(content);
        tv_nomeMateria.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), DialogEmentaMateria.class);
                intent.putExtra("tipo", nome);
                startActivityForResult(intent, 1);
            }

        });

        content = new SpannableString(info.getProf());
        content.setSpan(new UnderlineSpan(), 0, info.getProf().length(), 0);
        tv_professor.setText(content);
        tv_professor.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
            Intent intent = new Intent(getBaseContext(), DialogInformacoesProfessores.class);
                intent.putExtra("tipo", info.getProf());
                startActivityForResult(intent, 1);
            }

        });

    }
}
