package br.com.uesc.colcic.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import br.com.uesc.colcic.R;
import br.com.uesc.colcic.domain.Tipo;

public class SelecionarMateriaExcluirAdapter extends RecyclerView.Adapter<SelecionarMateriaExcluirAdapter.ViewHolder> {

    private List<Tipo> listaMaterias;

    public SelecionarMateriaExcluirAdapter(List<Tipo> listaMaterias) {
        this.listaMaterias = listaMaterias;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_selecionar_materia_excluir, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        holder.bindData(listaMaterias.get(position));
        holder.checkboxMateriaSelecionarExluir.setOnCheckedChangeListener(null);
        holder.checkboxMateriaSelecionarExluir.setChecked(listaMaterias.get(position).isSelected());
        holder.checkboxMateriaSelecionarExluir.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                listaMaterias.get(holder.getAdapterPosition()).setSelected(isChecked);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listaMaterias.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivMateriaSelecionarExluir;
        private TextView tvMateriaSelecionarExluir;
        private CheckBox checkboxMateriaSelecionarExluir;

        public ViewHolder(View v) {
            super(v);
            ivMateriaSelecionarExluir = (ImageView) v.findViewById(R.id.ivMateriaSelecionarExcluir);
            tvMateriaSelecionarExluir = (TextView) v.findViewById(R.id.tvMateriaSelecionarExcluir);
            checkboxMateriaSelecionarExluir = (CheckBox) v.findViewById(R.id.checkbox);
        }

        public void bindData(Tipo materia) {
            ivMateriaSelecionarExluir.setImageDrawable(materia.getimg());
            tvMateriaSelecionarExluir.setText(materia.getName());
        }
    }
}
