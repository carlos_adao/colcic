package br.com.uesc.colcic.activity;

import android.content.res.TypedArray;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.ArrayList;

import br.com.uesc.colcic.R;
import br.com.uesc.colcic.adapter.SelecionarMateriaAdapter;
import br.com.uesc.colcic.domain.Comandos;
import br.com.uesc.colcic.domain.Tipo;
import br.com.uesc.colcic.fragment.MinhasTurmas.CreditosMinhaTurmaFragment;

public class MinhasTurmasActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    ArrayList<String> listaNomeMaterias;
    ArrayList<Tipo> listaTipos;
    public CreditosMinhaTurmaFragment f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_minhas_turmas);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);

        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Minhas Turmas");

        recyclerView = (RecyclerView) findViewById(R.id.rv_minhas_turmas);
        recyclerView.setLayoutManager(new LinearLayoutManager(MinhasTurmasActivity.this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);
        listaNomeMaterias = Comandos.listaNomeMateriasCursando();
        listaTipos = returnListTips(listaNomeMaterias);
        recyclerView.setAdapter(new SelecionarMateriaAdapter(listaTipos,materiaClick()));
    }

    public ArrayList<Tipo> returnListTips(ArrayList<String> listaNomeMaterias) {
        TypedArray imgs;
        ArrayList<Tipo> listaTipos = new ArrayList<>();
        imgs = getResources().obtainTypedArray(R.array.img_materia);
        for (int i = 0; i < listaNomeMaterias.size(); i++) {
            listaTipos.add(new Tipo(listaNomeMaterias.get(i).toString(), imgs.getDrawable(0)));
        }
        return listaTipos;
    }

    // metodo usado no adapter da lista de materias
    private SelecionarMateriaAdapter.MateriaClick materiaClick() {
        return new SelecionarMateriaAdapter.MateriaClick() {
            @Override
            public void onclick(View view, String nome) {

                Bundle args = new Bundle();
                args.putString("nomeDisciplina", nome);
                CreditosMinhaTurmaFragment frag = new CreditosMinhaTurmaFragment();
                frag.setArguments(args);
                f = frag;

                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_in_right);
                transaction.replace(R.id.FragmentMinhasTurmas, f);
                transaction.commit();
            }

        };
    }

    public void fechaFragmento() {

        getSupportFragmentManager().beginTransaction().remove(f).commit();
        getSupportFragmentManager().beginTransaction().setCustomAnimations(0, R.anim.slide_in_button).commit();
        getSupportFragmentManager().beginTransaction().setCustomAnimations(0, R.anim.slide_out_button).commit();

    }
}
