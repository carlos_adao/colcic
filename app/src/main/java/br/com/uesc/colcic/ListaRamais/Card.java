package br.com.uesc.colcic.ListaRamais;


public class Card {
    private long id;
    private String name;
    private String ramal;
    private int color_resource;

    public String getRamal(){return this.ramal;}

    public void setRamal(String ramal){this.ramal = ramal;}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getColorResource() {
        return color_resource;
    }

    public void setColorResource(int color_resource) {
        this.color_resource = color_resource;
    }
}
