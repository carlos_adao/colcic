package br.com.uesc.colcic.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import br.com.uesc.colcic.R;
import br.com.uesc.colcic.fragment.ResultadoCalculadoraUescFragment;

public class OpcaoCalculadoraUescActivity extends AppCompatActivity {
    ImageView icon_calculadora_uesc, icon_minhas_turmas;
    Vibrator vibrator;
    Animation shake, animation_click;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opcao_calculadora_uesc);

        context = getBaseContext();
        icon_calculadora_uesc = (ImageView) findViewById(R.id.icon_calculadora_uesc);
        icon_minhas_turmas = (ImageView) findViewById(R.id.icon_minhas_turmas);
        icon_calculadora_uesc.setOnClickListener(clickInIcones);
        icon_minhas_turmas.setOnClickListener(clickInIcones);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);

        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Calculos de Notas");

        shake = AnimationUtils.loadAnimation(context, R.anim.animation_swing);
        animation_click = AnimationUtils.loadAnimation(context, R.anim.animation_click);
        vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
    }

    //Método usuado para evento de click nos icone
    private View.OnClickListener clickInIcones = new View.OnClickListener() {
        public void onClick(View v) {
            Intent intent;

            switch (v.getId()) {
                /*clicando no icon btn reset*/
                case R.id.icon_calculadora_uesc:
                    intent = new Intent(OpcaoCalculadoraUescActivity.this, CalculadoraUescActivity.class);
                    startActivity(intent);
                    setVibrator(icon_calculadora_uesc);

                    break;
                case R.id.icon_minhas_turmas:
                    intent = new Intent(OpcaoCalculadoraUescActivity.this, MinhasTurmasActivity.class);
                    startActivity(intent);
                    setVibrator(icon_calculadora_uesc);

                    break;

                default:
                    break;
            }
        }
    };


    public void setVibrator(ImageView iv) {

        //iv.startAnimation(animation_click);
        vibrator.vibrate(30);
    }


}
