package br.com.uesc.colcic.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import br.com.uesc.colcic.ConexaoInternet.ConexaoHttp;
import br.com.uesc.colcic.R;
import br.com.uesc.colcic.adapter.HorarioAdapter;
import br.com.uesc.colcic.adapter.HorarioDescCodMatAdapter;
import br.com.uesc.colcic.adapter.ListaMateriaAdapter;
import br.com.uesc.colcic.auxiliares.DialogEmentaMateria;
import br.com.uesc.colcic.auxiliares.DialogInformacoesMaterias;
import br.com.uesc.colcic.domain.Acesso;
import br.com.uesc.colcic.domain.AuxiliarComando;
import br.com.uesc.colcic.domain.Comandos;
import br.com.uesc.colcic.fragment.HorariosOnibus.IlheusSalobrinhoFragment;
import br.com.uesc.colcic.modelo.AuxDisciplina;
import br.com.uesc.colcic.modelo.Materia;

public class HorarioLetivoActivity extends BaseActivity {
    protected RecyclerView rc_horarioLetivo, rc_descCodMat;
    public TextView tv_semestre;
    ImageView iv_frente, iv_volta;
    LinearLayout ll_corpo, ll_voltar, ll_frente;
    Acesso a;
    float x1, x2;
    float y1, y2;
    ArrayList<Integer> listaMaterias = new ArrayList<>();
    Vibrator vibrator;
    Animation shake, animation_click;
    Context context;
    int trocSem;
    boolean portrait = true;

    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_horario_letivo);
        setUpToolbar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        String hl = Comandos.returnSemestreVirgente().getPeriodo();
        if(hl != null){
           getSupportActionBar().setTitle("Horário " + hl);
        }else{
            hl = "Horário Letivo";
            getSupportActionBar().setTitle(hl);
        }
        context = getBaseContext();
        ll_corpo = (LinearLayout) findViewById(R.id.corpo);
        ll_corpo.setOnTouchListener(swip);


        ll_frente = (LinearLayout) findViewById(R.id.ll_frente);
        ll_voltar = (LinearLayout) findViewById(R.id.ll_volta);


        iv_frente = (ImageView) findViewById(R.id.iv_frente);
        tv_semestre = (TextView) findViewById(R.id.tv_semestre);
        iv_volta = (ImageView) findViewById(R.id.iv_volta);


        iv_volta.setVisibility(0);

        iv_volta.setOnClickListener(clickSetaFrente);
        ll_voltar.setOnClickListener(clickSetaFrente);
        iv_frente.setOnClickListener(clickSetavoltar);
        ll_frente.setOnClickListener(clickSetavoltar);

        rc_horarioLetivo = (RecyclerView) findViewById(R.id.rc_horarioLetivo);
        rc_horarioLetivo.setItemAnimator(new DefaultItemAnimator());
        rc_horarioLetivo.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        rc_horarioLetivo.setHasFixedSize(true);

        rc_descCodMat = (RecyclerView) findViewById(R.id.rc_descCodMat);
        rc_descCodMat.setItemAnimator(new DefaultItemAnimator());
        rc_descCodMat.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        rc_descCodMat.setHasFixedSize(true);
        leMateria();

        //rc_descCodMat.setOnTouchListener(swip);
        //rc_horarioLetivo.setOnTouchListener(swip);
        shake = AnimationUtils.loadAnimation(context, R.anim.animation_swing);
        animation_click = AnimationUtils.loadAnimation(context, R.anim.animation_click);
        vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_selecionar_turma, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.smT1) {
            chamaTurmasSemestre(tv_semestre.getText().toString(), "T1");
        } else if (id == R.id.smT2) {
            chamaTurmasSemestre(tv_semestre.getText().toString(), "T2");
        }else if (id == R.id.smT3) {
            chamaTurmasSemestre(tv_semestre.getText().toString(), "T3");
        }
        return super.onOptionsItemSelected(item);

    }


    private void leMateria() {

        listaMaterias.add(0);
        listaMaterias.add(1);
        listaMaterias.add(2);
        a = new Acesso(getBaseContext());
        a.criaBD(getBaseContext());
        trocaSemestre(1);


    }



    public View.OnClickListener clickSetaFrente = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            setVibrator();
            frente();
        }
    };
    private View.OnClickListener clickSetavoltar = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            setVibrator();
            traz();
        }
    };

    private View.OnTouchListener swip = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent event) {
            switch (event.getAction()) {
                // when user first touches the screen we get x and y coordinate
                case MotionEvent.ACTION_DOWN: {
                    x1 = event.getX();
                    y1 = event.getY();
                    break;
                }
                case MotionEvent.ACTION_UP: {
                    x2 = event.getX();
                    y2 = event.getY();

                    //if left to right sweep event on screen
                    if (x1 < x2) {

                        traz();
                    }
                    // if right to left sweep event on screen
                    if (x1 > x2) {

                        frente();
                    }
                    // if UP to Down sweep event on screen
                    if (y1 < y2) {

                    }
                    //if Down to UP sweep event on screen
                    if (y1 > y2) {
                    }
                    break;
                }
            }
            return false;
        }
    };

    public void frente() {
        String sm = tv_semestre.getText().toString();
        String turma = "T1";
        if(sm.equalsIgnoreCase(getString(R.string.s1c))){
            tv_semestre.setText(getString(R.string.s1v));
            trocaSemestre(12);
        }else if(sm.equalsIgnoreCase(getString(R.string.s1v))) {
            tv_semestre.setText(getString(R.string.s2));
            trocaSemestre(2);
        } else if (sm.equalsIgnoreCase(getString(R.string.s2))) {
            tv_semestre.setText(getString(R.string.s3));
            trocaSemestre(3);
        } else if (sm.equalsIgnoreCase(getString(R.string.s3))) {
            tv_semestre.setText(getString(R.string.s4));
            trocaSemestre(4);
        } else if (sm.equalsIgnoreCase(getString(R.string.s4))) {
            tv_semestre.setText(getString(R.string.s5));
            trocaSemestre(5);
        } else if (sm.equalsIgnoreCase(getString(R.string.s5))) {
            tv_semestre.setText(getString(R.string.s6));
            trocaSemestre(6);
        } else if (sm.equalsIgnoreCase(getString(R.string.s6))) {
            tv_semestre.setText(getString(R.string.s7));
            trocaSemestre(7);
        } else if (sm.equalsIgnoreCase(getString(R.string.s7))) {
            tv_semestre.setText(getString(R.string.s8));
            trocaSemestre(8);
        } else if (sm.equalsIgnoreCase(getString(R.string.s8))) {

        }
    }

    public void traz() {
        String turma = "T1";
        String sm = tv_semestre.getText().toString();
        if (sm.equalsIgnoreCase(getString(R.string.s8))) {
            tv_semestre.setText(getString(R.string.s7));
            trocaSemestre(7);
        } else if (sm.equalsIgnoreCase(getString(R.string.s7))) {
            tv_semestre.setText(getString(R.string.s6));
            trocaSemestre(6);
        } else if (sm.equalsIgnoreCase(getString(R.string.s6))) {
            tv_semestre.setText(getString(R.string.s5));
            trocaSemestre(5);
        } else if (sm.equalsIgnoreCase(getString(R.string.s5))) {
            tv_semestre.setText(getString(R.string.s4));
            trocaSemestre(4);
        } else if (sm.equalsIgnoreCase(getString(R.string.s4))) {
            tv_semestre.setText(getString(R.string.s3));
            trocaSemestre(3);
        } else if (sm.equalsIgnoreCase(getString(R.string.s3))) {
            tv_semestre.setText(getString(R.string.s2));
            trocaSemestre(2);
        } else if (sm.equalsIgnoreCase(getString(R.string.s2))) {
            tv_semestre.setText(getString(R.string.s1v));
            trocaSemestre(12);
        }else if (sm.equalsIgnoreCase(getString(R.string.s1v))) {
            tv_semestre.setText(getString(R.string.s1c));
            trocaSemestre(1);
        }

    }

    public void trocaSemestre(int sem) {

        trocSem = sem;
        desenhaHorario(portrait);

    }

    public void chamaActivityInformacoesMateria(String nome) {
        Intent intent = new Intent(getContext(), DialogInformacoesMaterias.class);
        intent.putExtra("tipo", nome);
        startActivityForResult(intent, 1);

    }
    /*Atraves do menu chama as turmas T1, T2, T3 do semestre selecionado*/
    public void chamaTurmasSemestre(String semestre, String turma){

        if (semestre.equalsIgnoreCase(getString(R.string.s1c))) {
            tv_semestre.setText(getString(R.string.s1c));
            trocaSemestre(1);
        }else if (semestre.equalsIgnoreCase(getString(R.string.s1v))) {
            tv_semestre.setText(getString(R.string.s1v));
            trocaSemestre(12);
        }else if (semestre.equalsIgnoreCase(getString(R.string.s2))) {
            tv_semestre.setText(getString(R.string.s2));
            trocaSemestre(2);
        } else if (semestre.equalsIgnoreCase(getString(R.string.s3))) {
            tv_semestre.setText(getString(R.string.s3));
            trocaSemestre(3);
        } else if (semestre.equalsIgnoreCase(getString(R.string.s4))) {
            tv_semestre.setText(getString(R.string.s4));
            trocaSemestre(4);
        } else if (semestre.equalsIgnoreCase(getString(R.string.s5))) {
            tv_semestre.setText(getString(R.string.s5));
            trocaSemestre(5);
        } else if (semestre.equalsIgnoreCase(getString(R.string.s6))) {
            tv_semestre.setText(getString(R.string.s6));
            trocaSemestre(6);
        } else if (semestre.equalsIgnoreCase(getString(R.string.s7))) {
            tv_semestre.setText(getString(R.string.s7));
            trocaSemestre(7);
        } else if (semestre.equalsIgnoreCase(getString(R.string.s8))) {

        }
    }

    // metodo usado no adapter da lista de materias
    private HorarioDescCodMatAdapter.MateriasOnClickListener onClickMaterias() {
        return new HorarioDescCodMatAdapter.MateriasOnClickListener() {
            @Override
            public void onClickMaterias(View view, String nome) {
                chamaActivityInformacoesMateria(nome);
            }

        };
    }

    public void setVibrator() {
        vibrator.vibrate(30);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            portrait = false;
            desenhaHorario(portrait);


        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            portrait = true;
            desenhaHorario(portrait);


        }
    }

    public void desenhaHorario(boolean portrait){


        TypedArray cores = getResources().obtainTypedArray(R.array.cores);//pegar as cores das materias

        ArrayList<String>lha = Comandos.returnListaHorasAulas(trocSem);//obtem uma lista com tadas as horas no semestre que tem aula
        if(lha != null) {

            rc_horarioLetivo.setAdapter(new HorarioAdapter(this,getBaseContext(), lha, cores, trocSem, portrait));
            ArrayList<Materia> listMatSemestre = Comandos.listaMateriaHorarioPorSemestre(trocSem);
            rc_descCodMat.setAdapter(new HorarioDescCodMatAdapter(getContext(), listMatSemestre, cores, onClickMaterias()));

        }
    }

}
