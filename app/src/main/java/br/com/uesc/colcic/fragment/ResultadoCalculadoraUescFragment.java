package br.com.uesc.colcic.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import br.com.uesc.colcic.R;
import br.com.uesc.colcic.activity.CalculadoraUescActivity;


public class ResultadoCalculadoraUescFragment extends Fragment {

    TextView tv_situacao, tv_media, tv_quanto_precisa;

    CalculadoraUescActivity calculadoraUescActivity;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_resultado_cal_uesc, container, false);

            float soma = getArguments().getFloat("soma");

            this.calculadoraUescActivity = (CalculadoraUescActivity) getActivity();

            tv_media = (TextView) view.findViewById(R.id.tv_media);
            tv_situacao = (TextView) view.findViewById(R.id.tv_situacao);
            tv_quanto_precisa = (TextView) view.findViewById(R.id.tv_quanto_precisa);
            realizaCalculo(soma);

        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {


        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String retorno;

        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {

            retorno = data.getStringExtra("tipo");


        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    //METODO USADO PARA VERIFICAR SE O ALUNO FOI APROVADO
    public void realizaCalculo(float soma) {
        float  mediaParcial = 0, PrecisaFinal = 0;
        int qtdCreditos = calculadoraUescActivity.ltest.size() + 1;
        float end;
        float c[] = new float[6];

        float media = soma / qtdCreditos;
        NumberFormat formatter = NumberFormat.getInstance(Locale.US);
        formatter.setMaximumFractionDigits(2);
        formatter.setMinimumFractionDigits(2);
        formatter.setRoundingMode(RoundingMode.HALF_UP);
        media = new Float(formatter.format(media));
        if (media >= 7) {

            tv_situacao.setText("Aprovado!!!");
            tv_media.setText(String.valueOf(media));
            tv_quanto_precisa.setText("Parabens! :)");

        } else if (media <= 1.6) {
            tv_situacao.setText("Reprovado");
            tv_media.setText(String.valueOf(media));
            tv_quanto_precisa.setText("Não há como passar");


        } else {
            tv_situacao.setText("Final");
            end = ((50 - media * 6) / 4);
            formatter = NumberFormat.getInstance(Locale.US);
            formatter.setMaximumFractionDigits(2);
            formatter.setMinimumFractionDigits(2);
            formatter.setRoundingMode(RoundingMode.HALF_UP);
            end = new Float(formatter.format(end));
            tv_media.setText(String.valueOf(media));

            tv_quanto_precisa.setText("Precisa de " + String.valueOf(end));

        }
    }

}
