package br.com.uesc.colcic.adapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import br.com.uesc.colcic.R;
import br.com.uesc.colcic.domain.Tipo;
import br.com.uesc.colcic.modelo.Funcionario;
import br.com.uesc.colcic.modelo.Materia;

public class ListaFuncionariosAdapter extends RecyclerView.Adapter<ListaFuncionariosAdapter.MyViewHolder> {

    private Context mContext;
    private List<Funcionario> listaFuncionario;
    private ListaFuncionariosAdapter.FuncionarioOnClickListener funcionarioOnClickListener;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView iv_funcionario;
        private TextView tv_name, tv_email, tv_cargo;

        public MyViewHolder(View v) {
            super(v);
            iv_funcionario = (ImageView) v.findViewById(R.id.iv_funcionario);
            tv_name = (TextView) v.findViewById(R.id.tv_name);
            tv_email = (TextView) v.findViewById(R.id.tv_email);
            tv_cargo = (TextView) v.findViewById(R.id.tv_cargo);
        }
    }

    public ListaFuncionariosAdapter(Context mContext, List<Funcionario> listaFuncionario, ListaFuncionariosAdapter.FuncionarioOnClickListener funcionarioOnClickListener) {
        this.mContext = mContext;
        this.listaFuncionario = listaFuncionario;
        this.funcionarioOnClickListener = funcionarioOnClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_lista_funcionarios, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        Funcionario funcionario = listaFuncionario.get(position);

        holder.iv_funcionario.setImageDrawable(funcionario.getImg());
        holder.tv_name.setText(funcionario.getNome());
        holder.tv_email.setText(funcionario.getEmail());
        holder.tv_cargo.setText(funcionario.getCargo());


        if (funcionarioOnClickListener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    funcionarioOnClickListener.onClickFuncionario(holder.itemView, position);
                }
            });
        }

    }
    public interface FuncionarioOnClickListener {
        public void onClickFuncionario(View view, int idx);
    }

    @Override
    public int getItemCount() {
        return listaFuncionario.size();
    }
}
