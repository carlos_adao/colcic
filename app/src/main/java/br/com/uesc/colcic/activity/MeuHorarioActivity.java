package br.com.uesc.colcic.activity;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import br.com.uesc.colcic.R;
import br.com.uesc.colcic.fragment.MeuHorarioTabFragment;
import br.com.uesc.colcic.fragment.SelecionarMateriaExluirFragment;
import br.com.uesc.colcic.fragment.SelecionarMateriaFragment;

public class MeuHorarioActivity extends AppCompatActivity {
    Bundle savedInstanceState;
    SelecionarMateriaFragment fragAdd;
    SelecionarMateriaExluirFragment fragExluir;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private com.google.android.gms.common.api.GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meu_horario);
        this.savedInstanceState = savedInstanceState;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);

        }

        iniciaTabFragmentoMeuHorario(savedInstanceState, "Meu Horario");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_materias, menu);
        return true;
    }

    //Iniicia a tab fragment para o colcic
    public void iniciaTabFragmentoMeuHorario(Bundle savedInstanceState, String titulo) {
        getSupportActionBar().setTitle(titulo);
        // Adiciona o fragment no layout
        // Título da toolbar e botão up navigation
        if (savedInstanceState == null) {
            // Cria o fragment com o mesmo Bundle (args) da intent
            MeuHorarioTabFragment frag = new MeuHorarioTabFragment();
            frag.setArguments(getIntent().getExtras());
            // Adiciona o fragment no layout
            getSupportFragmentManager().beginTransaction().add(R.id.alternavelFragment, frag).commit();
        }
    }
    public void iniciaTabFragmentoMeuHorarioExterno() {
        // Adiciona o fragment no layout
        // Título da toolbar e botão up navigation
        if (savedInstanceState == null) {
            // Cria o fragment com o mesmo Bundle (args) da intent
            MeuHorarioTabFragment frag = new MeuHorarioTabFragment();
            frag.setArguments(getIntent().getExtras());
            // Adiciona o fragment no layout
            getSupportFragmentManager().beginTransaction().add(R.id.alternavelFragment, frag).commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.addMat) {
            if (savedInstanceState == null) {
                // Cria o fragment com o mesmo Bundle (args) da intent
                SelecionarMateriaFragment frag = new SelecionarMateriaFragment();
                this.fragAdd = frag;
                this.fragAdd.setArguments(getIntent().getExtras());
                // Adiciona o fragment no layout
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.slide_in_button, R.anim.slide_out_button);
                transaction.replace(R.id.selecionarMateriaFragment, this.fragAdd);

                transaction.commit();
            }
        } else if (id == R.id.delMat) {


            if (savedInstanceState == null) {
                // Cria o fragment com o mesmo Bundle (args) da intent
                SelecionarMateriaExluirFragment fragExcluir = new SelecionarMateriaExluirFragment();
                this.fragExluir = fragExcluir;
                this.fragExluir.setArguments(getIntent().getExtras());
                // Adiciona o fragment no layout
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.slide_in_button, R.anim.slide_out_button);
                transaction.replace(R.id.selecionarMateriaFragment, this.fragExluir);

                transaction.commit();

            }
        }
        return super.onOptionsItemSelected(item);
    }


    public void fechaFragmento() {

        getSupportFragmentManager().beginTransaction().remove(fragAdd).commit();
        getSupportFragmentManager().beginTransaction().setCustomAnimations(0, R.anim.slide_in_button).commit();
        getSupportFragmentManager().beginTransaction().setCustomAnimations(0, R.anim.slide_out_button).commit();

    }

    //fecha o fragmento que escolhe multiplas materias para excluir
    public void fechaFragmentoSelecionarMateria() {

        getSupportFragmentManager().beginTransaction().remove(fragExluir).commit();
        getSupportFragmentManager().beginTransaction().setCustomAnimations(0, R.anim.slide_in_button).commit();
        getSupportFragmentManager().beginTransaction().setCustomAnimations(0, R.anim.slide_out_button).commit();

    }

}
