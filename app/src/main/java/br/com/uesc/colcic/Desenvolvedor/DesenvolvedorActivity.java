package br.com.uesc.colcic.Desenvolvedor;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import br.com.uesc.colcic.R;
import br.com.uesc.colcic.views.JustifiedTextView;

public class DesenvolvedorActivity extends AppCompatActivity {
    TextView tv_titulo, tv_titulo2, tv_conteudo, tv_conteudo2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_desenvolvedor);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Colcic");

        tv_titulo = (TextView) findViewById(R.id.tv_titulo);
        tv_conteudo = (TextView) findViewById(R.id.tv_conteudo);
        tv_titulo.setText(getString(R.string.sobre_app));
        tv_conteudo.setText(getString(R.string.p1info_sobre_app));

        tv_titulo2 = (TextView) findViewById(R.id.tv_titulo2);
        tv_conteudo2 = (TextView) findViewById(R.id.tv_conteudo2);
        tv_titulo2.setText(getString(R.string.desenvolvedor_app));
        //tv_conteudo2.setText(getString(R.string.p1info_sobre_app) + getString(R.string.p2info_sobre_app));
    }
}
