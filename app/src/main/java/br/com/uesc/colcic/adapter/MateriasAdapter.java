package br.com.uesc.colcic.adapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import br.com.uesc.colcic.R;
import br.com.uesc.colcic.domain.Comandos;
import br.com.uesc.colcic.modelo.Materia;


public class MateriasAdapter extends RecyclerView.Adapter<MateriasAdapter.MyViewHolder> {

    private Context mContext;
    private List<Materia> materiasList;
    private MateriasOnClickListener materiaOnClickListener;
    TypedArray cores;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_cod, tv_nome, tv_hora;
        CardView cv;

        public MyViewHolder(View view) {
            super(view);
            tv_hora = (TextView) view.findViewById(R.id.tv_hora);
            tv_cod = (TextView) view.findViewById(R.id.tv_cod);
            tv_nome = (TextView) view.findViewById(R.id.tv_nome);
            cv = (CardView)view.findViewById(R.id.card_view);
        }
    }

    public MateriasAdapter(Context mContext, List<Materia> materiasList, MateriasOnClickListener materiaOnClickListener,TypedArray cores) {
        this.mContext = mContext;
        this.cores = cores;
        this.materiasList = materiasList;
        this.materiaOnClickListener = materiaOnClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_materia, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        Materia materia = materiasList.get(position);
        holder.tv_hora.setText(materia.getHora_inicio() + " - " + materia.getHora_fim());
        holder.tv_cod.setText(materia.getCodigo());
        holder.tv_nome.setText(materia.getNome());
        int id = Comandos.returnIdMateria(materia.getNome());
        holder.cv.setBackgroundColor(cores.getColor(id,id));
        if (materiaOnClickListener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    materiaOnClickListener.onClickMaterias(holder.itemView, position);
                }
            });
        }

    }

    public interface MateriasOnClickListener {
        public void onClickMaterias(View view, int idx);
    }

    @Override
    public int getItemCount() {
        return materiasList.size();
    }
}
