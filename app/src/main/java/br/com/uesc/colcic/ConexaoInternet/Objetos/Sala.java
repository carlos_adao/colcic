package br.com.uesc.colcic.ConexaoInternet.Objetos;

/**
 * Created by estagio-nit on 21/09/17.
 */

public class Sala {
    String codigo, localizacao, caracteristica;

    public Sala(String codigo, String localizacao, String caracteristica) {
        this.codigo = codigo;
        this.localizacao = localizacao;
        this.caracteristica = caracteristica;
    }

    public Sala() {

    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getLocalizacao() {
        return localizacao;
    }

    public void setLocalizacao(String localizacao) {
        this.localizacao = localizacao;
    }

    public String getCaracteristica() {
        return caracteristica;
    }

    public void setCaracteristica(String caracteristica) {
        this.caracteristica = caracteristica;
    }
}
