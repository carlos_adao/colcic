package br.com.uesc.colcic.ConexaoInternet.Objetos;

/**
 * Created by estagio-nit on 21/09/17.
 */

public class Ementa {

    String fk_disicplina, fk_pre_requisito, ementa,objetivo,metodologia,avaliacao,conteudo_programatico,referencia;

    public Ementa(String fk_disicplina, String fk_pre_requisito, String ementa, String objetivo, String metodologia, String avaliacao, String conteudo_programatico, String referencia) {
        this.fk_disicplina = fk_disicplina;
        this.fk_pre_requisito = fk_pre_requisito;
        this.ementa = ementa;
        this.objetivo = objetivo;
        this.metodologia = metodologia;
        this.avaliacao = avaliacao;
        this.conteudo_programatico = conteudo_programatico;
        this.referencia = referencia;
    }

    public String getFk_disicplina() {
        return fk_disicplina;
    }

    public void setFk_disicplina(String fk_disicplina) {
        this.fk_disicplina = fk_disicplina;
    }

    public String getFk_pre_requisito() {
        return fk_pre_requisito;
    }

    public void setFk_pre_requisito(String fk_pre_requisito) {
        this.fk_pre_requisito = fk_pre_requisito;
    }

    public String getEmenta() {
        return ementa;
    }

    public void setEmenta(String ementa) {
        this.ementa = ementa;
    }

    public String getObjetivo() {
        return objetivo;
    }

    public void setObjetivo(String objetivo) {
        this.objetivo = objetivo;
    }

    public String getMetodologia() {
        return metodologia;
    }

    public void setMetodologia(String metodologia) {
        this.metodologia = metodologia;
    }

    public String getAvaliacao() {
        return avaliacao;
    }

    public void setAvaliacao(String avaliacao) {
        this.avaliacao = avaliacao;
    }

    public String getConteudo_programatico() {
        return conteudo_programatico;
    }

    public void setConteudo_programatico(String conteudo_programatico) {
        this.conteudo_programatico = conteudo_programatico;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }
}
