package br.com.uesc.colcic.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import br.com.uesc.colcic.Desenvolvedor.DesenvolvedorActivity;
import br.com.uesc.colcic.DeveloperTools.verificationActivity;
import br.com.uesc.colcic.ListaRamais.ListaRamaisActivity;
import br.com.uesc.colcic.R;
import br.com.uesc.colcic.activity.Historia.HistoriaActivity;
import br.com.uesc.colcic.activity.Objetivo.ObjetivoActivity;
import br.com.uesc.colcic.activity.PerfilProfissional.PerfilProfissionalActivity;
import br.com.uesc.colcic.auxiliares.DialogHasInternet;
import br.com.uesc.colcic.auxiliares.MetodosPublicos;


public class BaseActivity extends livroandroid.lib.activity.BaseActivity {
    protected DrawerLayout drawerLayout;
    private Context context;

    // Configura a Toolbar
    protected void setUpToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            toolbar.inflateMenu(R.menu.menu_materias);
        }
    }

    protected void setUpToolbarSearch() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            toolbar.inflateMenu(R.menu.menu_materias);
        }
    }

    // Configura o Nav Drawer
    protected void setupNavDrawer() {
        // Drawer Layout
        final ActionBar actionBar = getSupportActionBar();
        // Ícone do menu do nav drawer
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
        actionBar.setDisplayHomeAsUpEnabled(true);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        if (navigationView != null && drawerLayout != null) {

            // Trata o evento de clique no menu.
            navigationView.setNavigationItemSelectedListener(
                    new NavigationView.OnNavigationItemSelectedListener() {
                        @Override
                        public boolean onNavigationItemSelected(MenuItem menuItem) {
                            // Seleciona a linha
                            menuItem.setChecked(true);
                            // Fecha o menu
                            drawerLayout.closeDrawers();
                            // Trata o evento do menu
                            onNavDrawerItemSelected(menuItem);
                            return true;
                        }
                    }
            );
        }
    }

    protected void setContext(Context context) {
        this.context = context;
    }


    // Trata o evento do menu lateral
    private void onNavDrawerItemSelected(MenuItem menuItem) {

        switch (menuItem.getItemId()) {

            case R.id.nav_historia:
                abrirActivityHistoria();
                break;
            case R.id.nav_objetivo:
                abrirActivityObjetivo();
                break;
            case R.id.nav_perfil:
                abrirActivityPerfilProfissional();
                break;
            case R.id.nav_colcic:
                abrirActivityColcic();
                break;
            case R.id.nav_meu_horario:
                abrirActivityMeuHorario();
                break;
            case R.id.nav_calculadora_uesc:
                abrirActivityCalculadoraUesc();
                break;
            case R.id.nav_horario:
                abrirActivityHorario();
                break;
            case R.id.nav_orientacao_m:
                abrirActivityOrientacaoMatricula();
                break;
            case R.id.nav_orientacao_r:
                abrirActivityOrientacaoRequerimento();
                break;
            case R.id.nav_fluxograma:
                abrirActivityFluxograma();
                break;
            case R.id.nav_lis_ramais:
                abrirActivityListaRamais();
                break;
            case R.id.nav_portal:
                abrirActivityPortalAcademico();
                break;
            case R.id.nav_biblioteca:
                abrirActivityBiblioteca();
                break;
            case R.id.nav_calendario_g:
                abrirActivityCalendario();
                break;
            case R.id.nav_guia:
                abrirActivityGuiaEstudante();
                break;
            case R.id.nav_regulamento:
                abrirActivityRegulamentos();
                break;
            case R.id.nav_regimento_c:
               abrirActivityRegCurso();
                break;
            case R.id.nav_regimento_uesc:
                abrirActivityRegGeral();
                break;
            case R.id.nav_hor_bus_ilheus:
              abrirActivityHorarioOnibusIlheus();
                break;
            case R.id.nav_hor_bus_itabuna:
                abrirActivityHorarioOnibusItabuna();
                break;
            case R.id.nav_sobre:
                abrirActivitySobre();
                break;
        }
    }

    // Adiciona o fragment no centro da tela
    protected void replaceFragment(Fragment frag) {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, frag, "TAG").commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // Trata o clique no botão que abre o menu.
                if (drawerLayout != null) {
                    openDrawer();
                    return true;
                }
        }
        return super.onOptionsItemSelected(item);
    }

    // Abre o menu lateral
    protected void openDrawer() {
        if (drawerLayout != null) {
            drawerLayout.openDrawer(GravityCompat.START);
        }
    }

    // Fecha o menu lateral
    protected void closeDrawer() {
        if (drawerLayout != null) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    protected void abrirActivityHistoria() {
        Intent intent;
        context = getBaseContext();
        intent = new Intent(getContext(), HistoriaActivity.class);
        intent.putExtra("texto", "historia");
        startActivity(intent);
        Log.i("menu ", "historia");
    }

    protected void abrirActivityObjetivo() {
        Intent intent;
        context = getBaseContext();
        intent = new Intent(getContext(), ObjetivoActivity.class);
        intent.putExtra("texto", "objetivo");
        startActivity(intent);
    }

    protected void abrirActivityPerfilProfissional() {
        Intent intent;
        context = getBaseContext();
        intent = new Intent(getContext(), PerfilProfissionalActivity.class);
        intent.putExtra("texto", "Perfil do Profissional");
        startActivity(intent);
    }

    protected void abrirActivityColcic() {
        Intent intent;
        context = getBaseContext();
        intent = new Intent(getContext(), CicActivity.class);
        startActivity(intent);
    }

    protected void abrirActivityMeuHorario() {
        Intent intent;
        context = getBaseContext();
        intent = new Intent(getContext(), Meu_HorarioActivity.class);
        intent.putExtra("texto", "Meu horário");
        startActivity(intent);
    }

    protected void abrirActivityCalculadoraUesc() {
        Intent intent;
        context = getBaseContext();
        intent = new Intent(getContext(), OpcaoCalculadoraUescActivity.class);
        startActivity(intent);
    }

    protected void abrirActivityHorario() {
        Intent intent;
        context = getBaseContext();
        intent = new Intent(getContext(), HorarioLetivoActivity.class);
        intent.putExtra("texto", "Horário");
        startActivity(intent);
    }

    protected void abrirActivityOrientacaoMatricula() {
        Intent intent;
        context = getBaseContext();
        intent = new Intent(getContext(), PrincipalActivity.class);
        intent.putExtra("texto", "om");
        startActivity(intent);
    }

    protected void abrirActivityOrientacaoRequerimento() {
        Intent intent;
        context = getBaseContext();
        intent = new Intent(getContext(), PrincipalActivity.class);
        intent.putExtra("texto", "or");
        startActivity(intent);
    }

    protected void abrirActivityFluxograma() {
        Intent intent;
        context = getBaseContext();
        if (!MetodosPublicos.verificaConexao(getBaseContext())) {
            intent = new Intent(getContext(), DialogHasInternet.class);
            startActivityForResult(intent, 1);
        } else {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.urlFluxograma)));
            startActivity(intent);
        }
    }

    protected void abrirActivityListaRamais() {
        Intent intent;
        context = getBaseContext();
        intent = new Intent(getContext(), ListaRamaisActivity.class);
        startActivity(intent);
    }

    protected void abrirActivityPortalAcademico() {
        Intent intent;
        context = getBaseContext();
        if (!MetodosPublicos.verificaConexao(context)) {
            intent = new Intent(getContext(), DialogHasInternet.class);
            startActivityForResult(intent, 1);
        } else {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.urlPortalAcademico)));
            startActivity(intent);
        }
    }

    protected void abrirActivityBiblioteca() {
        Intent intent;
        context = getBaseContext();
        if (!MetodosPublicos.verificaConexao(context)) {
            intent = new Intent(getContext(), DialogHasInternet.class);
            startActivityForResult(intent, 1);
        } else {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.urlBiblioteca)));
            startActivity(intent);
        }
    }
    protected void abrirActivityCalendario() {
        Intent intent;
        context = getBaseContext();
        if (!MetodosPublicos.verificaConexao(context)) {
            intent = new Intent(getContext(), DialogHasInternet.class);
            startActivityForResult(intent, 1);
        } else {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.urlCalendarioAcademico)));
            startActivity(intent);
        }
    }
    protected void abrirActivityGuiaEstudante() {
        Intent intent;
        context = getBaseContext();
        if (!MetodosPublicos.verificaConexao(context)) {
            intent = new Intent(getContext(), DialogHasInternet.class);
            startActivityForResult(intent, 1);
        } else {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.urlGuiaEstudante)));
            startActivity(intent);
        }
    }
    protected void abrirActivityRegulamentos() {
        Intent intent;
        context = getBaseContext();

        if (!MetodosPublicos.verificaConexao(context)) {
            intent = new Intent(getContext(), DialogHasInternet.class);
            startActivityForResult(intent, 1);
        } else {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.urlRegulamentoEstagio)));
            startActivity(intent);
        }
    }
    protected void abrirActivityRegCurso() {
        Intent intent;
        context = getBaseContext();

        if (!MetodosPublicos.verificaConexao(context)) {
            intent = new Intent(getContext(), DialogHasInternet.class);
            startActivityForResult(intent, 1);
        } else {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.urlRegulamentoColegiado)));
            startActivity(intent);
        }
    }

    protected void abrirActivityRegGeral(){
        Intent intent;
        context = getBaseContext();

        if (!MetodosPublicos.verificaConexao(context)) {
            intent = new Intent(getContext(), DialogHasInternet.class);
            startActivityForResult(intent, 1);
        } else {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.urlRegulamentoEstagio)));
            startActivity(intent);
        }
    }
    protected void abrirActivityHorarioOnibusIlheus(){
        Intent intent;
        context = getBaseContext();
        intent = new Intent(getContext(), HorarioOnibusActivity.class);
        intent.putExtra("titulo", "Horario Ônibus");
        intent.putExtra("local", 0);
        startActivity(intent);
    }
    protected void abrirActivityHorarioOnibusItabuna(){
        Intent intent;
        context = getBaseContext();
        Log.i("onibus ", "itabuna");
        intent = new Intent(getContext(), HorarioOnibusActivity.class);
        intent.putExtra("titulo", "Horario Ônibus");
        intent.putExtra("local", 1);
        startActivity(intent);
    }
    protected void abrirActivitySobre(){
        Intent intent;
        context = getBaseContext();
        intent = new Intent(getContext(), DesenvolvedorActivity.class);
        startActivity(intent);
    }


}
