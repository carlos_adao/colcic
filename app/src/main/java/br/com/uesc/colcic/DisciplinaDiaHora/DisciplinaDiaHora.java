package br.com.uesc.colcic.DisciplinaDiaHora;

/**
 * Created by estagio-nit on 26/02/18.
 */

public class DisciplinaDiaHora {

    String nomeDisciplina,horaAula, diaSemana;

    public String getNomeDisciplina() {
        return nomeDisciplina;
    }

    public void setNomeDisciplina(String nomeDisciplina) {
        this.nomeDisciplina = nomeDisciplina;
    }

    public String getHoraAula() {
        return horaAula;
    }

    public void setHoraAula(String horaAula) {
        this.horaAula = horaAula;
    }

    public String getDiaSemana() {
        return diaSemana;
    }

    public void setDiaSemana(String diaSemana) {
        this.diaSemana = diaSemana;
    }

    public DisciplinaDiaHora(String nomeDisciplina, String horaAula, String diaSemana) {

        this.nomeDisciplina = nomeDisciplina;
        this.horaAula = horaAula;
        this.diaSemana = diaSemana;
    }
}
