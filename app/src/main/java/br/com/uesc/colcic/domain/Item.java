package br.com.uesc.colcic.domain;

import android.graphics.drawable.Drawable;

/**
 * Created by Convidado on 01/02/2017.
 */

public class Item {

        private String name;
        private String code;
        private Drawable img;

        public Item(String name, String code, Drawable img) {
            this.name = name;
            this.code = code;
            this.img = img;
        }

        public Item() {

        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public Drawable getImg() {
            return img;
        }

        public void setImg(Drawable img) {
            this.img = img;
        }

        @Override
        public String toString() {
            return "Produto{" +
                    "name='" + name + '\'' +
                    ", code='" + code + '\'' +
                    ", img=" + img +
                    '}';
        }
}
