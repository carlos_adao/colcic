package br.com.uesc.colcic.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Vibrator;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import br.com.uesc.colcic.R;
import br.com.uesc.colcic.activity.HorarioLetivoActivity;
import br.com.uesc.colcic.activity.Meu_HorarioActivity;
import br.com.uesc.colcic.domain.Comandos;
import br.com.uesc.colcic.modelo.AuxDisciplina;


public class HorarioAdapter extends RecyclerView.Adapter<HorarioAdapter.MyViewHolder> {

    TypedArray cores;
    private ArrayList<String[]> tabelaHorario;
    private MateriasOnClickListener materiaOnClickListener;
    MyViewHolder holder;
    HorarioLetivoActivity hl = null;
    Meu_HorarioActivity mh = null;
    ArrayList<String> lha;
    int semestre = 0;
    Vibrator vibrator;
    Animation shake, animation_click;
    Context context;
    boolean portrait;

    public HorarioAdapter(HorarioLetivoActivity hl, Context context, ArrayList<String> lha, TypedArray cores, int semestre, boolean portrait) {
        this.hl = hl;
        this.lha = lha;
        this.cores = cores;
        this.context = context;
        this.portrait = portrait;
        this.semestre = semestre;


    }

    public HorarioAdapter(Meu_HorarioActivity mh,Context context, ArrayList<String> lha, TypedArray cores, boolean portrait) {
        this.mh = mh;
        this.lha = lha;
        this.cores = cores;
        this.context = context;
        this.portrait = portrait;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_hora;
        public TextView tv_seg_mat, tv_seg_mat1, tv_seg_mat2;
        public TextView tv_ter_mat, tv_ter_mat1, tv_ter_mat2;
        public TextView tv_qua_mat, tv_qua_mat1, tv_qua_mat2;
        public TextView tv_qui_mat, tv_qui_mat1, tv_qui_mat2;
        public TextView tv_sex_mat, tv_sex_mat1, tv_sex_mat2;
        public TextView tv_seg_tur, tv_ter_tur, tv_qua_tur, tv_qui_tur, tv_sex_tur;
        public LinearLayout ll_seg_full, ll_seg_comp;
        public LinearLayout ll_ter_full, ll_ter_comp;
        public LinearLayout ll_qua_full, ll_qua_comp;
        public LinearLayout ll_qui_full, ll_qui_comp;
        public LinearLayout ll_sex_full, ll_sex_comp;

        public MyViewHolder(View view) {
            super(view);
            tv_hora = (TextView) view.findViewById(R.id.tv_hora);

            ll_seg_full = (LinearLayout) view.findViewById(R.id.ll_seg_full);
            ll_seg_comp = (LinearLayout) view.findViewById(R.id.ll_seg_comp);
            tv_seg_mat = (TextView) view.findViewById(R.id.tv_seg_mat);
            tv_seg_mat1 = (TextView) view.findViewById(R.id.tv_seg_mat1);
            tv_seg_mat2 = (TextView) view.findViewById(R.id.tv_seg_mat2);

            ll_ter_full = (LinearLayout) view.findViewById(R.id.ll_ter_full);
            ll_ter_comp = (LinearLayout) view.findViewById(R.id.ll_ter_comp);
            tv_ter_mat = (TextView) view.findViewById(R.id.tv_ter_mat);
            tv_ter_mat1 = (TextView) view.findViewById(R.id.tv_ter_mat1);
            tv_ter_mat2 = (TextView) view.findViewById(R.id.tv_ter_mat2);

            ll_qua_full = (LinearLayout) view.findViewById(R.id.ll_qua_full);
            ll_qua_comp = (LinearLayout) view.findViewById(R.id.ll_qua_comp);
            tv_qua_mat = (TextView) view.findViewById(R.id.tv_qua_mat);
            tv_qua_mat1 = (TextView) view.findViewById(R.id.tv_qua_mat1);
            tv_qua_mat2 = (TextView) view.findViewById(R.id.tv_qua_mat2);

            ll_qui_full = (LinearLayout) view.findViewById(R.id.ll_qui_full);
            ll_qui_comp = (LinearLayout) view.findViewById(R.id.ll_qui_comp);
            tv_qui_mat = (TextView) view.findViewById(R.id.tv_qui_mat);
            tv_qui_mat1 = (TextView) view.findViewById(R.id.tv_qui_mat1);
            tv_qui_mat2 = (TextView) view.findViewById(R.id.tv_qui_mat2);

            ll_sex_full = (LinearLayout) view.findViewById(R.id.ll_sex_full);
            ll_sex_comp = (LinearLayout) view.findViewById(R.id.ll_sex_comp);
            tv_sex_mat = (TextView) view.findViewById(R.id.tv_sex_mat);
            tv_sex_mat1 = (TextView) view.findViewById(R.id.tv_sex_mat1);
            tv_sex_mat2 = (TextView) view.findViewById(R.id.tv_sex_mat2);

            tv_seg_tur = (TextView) view.findViewById(R.id.tv_seg_tur);
            tv_ter_tur = (TextView) view.findViewById(R.id.tv_ter_tur);
            tv_qua_tur = (TextView) view.findViewById(R.id.tv_qua_tur);
            tv_qui_tur = (TextView) view.findViewById(R.id.tv_qui_tur);
            tv_sex_tur = (TextView) view.findViewById(R.id.tv_sex_tur);


            shake = AnimationUtils.loadAnimation(context, R.anim.animation_swing);
            animation_click = AnimationUtils.loadAnimation(context, R.anim.animation_click);
            vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);

        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_horario_letivo, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        this.holder = holder;

        desenhaHorario(holder,position);

    }


    @SuppressLint("WrongConstant")
    public void desenhaHorario(final MyViewHolder holder, int position) {
        int id;//pega o indice da disciplina para pintar o background
        String hora = lha.get(position);
        ArrayList<AuxDisciplina> listAuxDisc = null;
        holder.tv_hora.setText(hora);

        String cod;
        String tur;
        String abr;

        /*verifica é meu horario ou horario do semestre*/
        if (semestre == 0) {
            /*Pega as aulas para segunda-feira*/
            listAuxDisc = Comandos.returnListAulaMeuHorario(hora, 2);

        } else {
            /*Pega as aulas para segunda-feira*/
            listAuxDisc = Comandos.returnListAula(hora, 2, semestre);
        }

        if (listAuxDisc != null) {

            if (listAuxDisc.size() == 1) {//verifica se tem mais de uma aula no horario

                holder.ll_seg_comp.setVisibility(View.GONE);
                holder.ll_seg_full.setVisibility(1);

                cod = listAuxDisc.get(0).getCod();
                tur = listAuxDisc.get(0).getTurma();
                abr = listAuxDisc.get(0).getAbreviacao();

                id = Comandos.returnIdMateria(cod, tur);
                if(portrait) {
                    holder.tv_seg_mat.setText(cod);
                }else {
                    holder.tv_seg_mat.setText(abr);
                }
                holder.tv_seg_tur.setText(tur);
                holder.ll_seg_full.setBackgroundColor(cores.getColor(id, id));

                final String finalCod14 = cod;
                final String finalTur14 = tur;
                holder.ll_seg_full.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(mh != null) {
                            setVibrator(holder.ll_seg_full);
                            mh.chamaActivityInformacoesMateria(Comandos.getNameDisciplina(finalCod14, finalTur14));
                        }else{
                            setVibrator(holder.ll_seg_full);
                            hl.chamaActivityInformacoesMateria(Comandos.getNameDisciplina(finalCod14, finalTur14));
                        }
                    }
                });

            } else if (listAuxDisc.size() == 2) {

                holder.ll_seg_full.setVisibility(View.GONE);
                holder.ll_seg_comp.setVisibility(1);

                cod = listAuxDisc.get(0).getCod();
                tur = listAuxDisc.get(0).getTurma();
                abr = listAuxDisc.get(0).getAbreviacao();


                id = Comandos.returnIdMateria(cod, tur);
                holder.tv_seg_mat1.setBackgroundColor(cores.getColor(id, id));
                if(portrait) {
                    holder.tv_seg_mat1.setText(cod + " " + tur);

                }else {
                    holder.tv_seg_mat1.setText(abr+ " " + tur);
                }

                final String finalCod = cod;
                final String finalTur = tur;
                abr = listAuxDisc.get(0).getAbreviacao();
                holder.tv_seg_mat1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(mh != null) {
                            setVibrator(holder.tv_seg_mat1);
                            mh.chamaActivityInformacoesMateria(Comandos.getNameDisciplina(finalCod, finalTur));
                        }else{
                            setVibrator(holder.tv_seg_mat1);
                            hl.chamaActivityInformacoesMateria(Comandos.getNameDisciplina(finalCod, finalTur));
                        }

                    }
                });


                cod = listAuxDisc.get(1).getCod();
                tur = listAuxDisc.get(1).getTurma();
                abr = listAuxDisc.get(1).getAbreviacao();

                id = Comandos.returnIdMateria(cod, tur);
                holder.tv_seg_mat2.setBackgroundColor(cores.getColor(id, id));
                if(portrait) {
                    holder.tv_seg_mat2.setText(cod + " " + tur);

                }else {
                    holder.tv_seg_mat2.setText(abr+ " " + tur);
                }


                final String finalCod1 = cod;
                final String finalTur1 = tur;
                holder.tv_seg_mat2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if(mh != null) {
                            setVibrator(holder.tv_seg_mat2);
                            mh.chamaActivityInformacoesMateria(Comandos.getNameDisciplina(finalCod1, finalTur1));
                        }else{
                            setVibrator(holder.tv_seg_mat2);
                            hl.chamaActivityInformacoesMateria(Comandos.getNameDisciplina(finalCod1, finalTur1));
                        }
                    }
                });
            } else {

                holder.ll_seg_full.setVisibility(1);
                holder.ll_seg_comp.setVisibility(View.GONE);
            }

        }


         /*verifica é meu horario ou horario do semestre*/
        if (semestre == 0) {
            /*Pega as aulas para terca-feira*/
            listAuxDisc = Comandos.returnListAulaMeuHorario(hora, 3);
        } else {
             /*Pega as aulas para terca-feira*/
            listAuxDisc = Comandos.returnListAula(hora, 3, semestre);
        }

        if (listAuxDisc != null) {

            if (listAuxDisc.size() == 1) {//verifica se tem mais de uma aula no horario

                holder.ll_ter_full.setVisibility(1);
                holder.ll_ter_comp.setVisibility(View.GONE);

                cod = listAuxDisc.get(0).getCod();
                tur = listAuxDisc.get(0).getTurma();
                abr = listAuxDisc.get(0).getAbreviacao();

                id = Comandos.returnIdMateria(cod, tur);
                if(portrait) {
                    holder.tv_ter_mat.setText(cod);
                }else {
                    holder.tv_ter_mat.setText(abr);
                }

                holder.tv_ter_tur.setText(tur);
                holder.ll_ter_full.setBackgroundColor(cores.getColor(id, id));
                final String finalCod2 = cod;
                final String finalTur2 = tur;
                holder.ll_ter_full.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if(mh != null) {
                            setVibrator(holder.ll_ter_full);
                            mh.chamaActivityInformacoesMateria(Comandos.getNameDisciplina(finalCod2, finalTur2));
                        }else{
                            setVibrator(holder.ll_ter_full);
                            hl.chamaActivityInformacoesMateria(Comandos.getNameDisciplina(finalCod2, finalTur2));
                        }

                    }
                });
            } else if (listAuxDisc.size() == 2) {

                holder.ll_ter_comp.setVisibility(1);
                holder.ll_ter_full.setVisibility(View.GONE);

                cod = listAuxDisc.get(0).getCod();
                tur = listAuxDisc.get(0).getTurma();
                abr = listAuxDisc.get(0).getAbreviacao();

                id = Comandos.returnIdMateria(cod, tur);
                holder.tv_ter_mat1.setBackgroundColor(cores.getColor(id, id));

                if(portrait) {
                    holder.tv_ter_mat1.setText(cod + " " + tur);
                }else {
                    holder.tv_ter_mat1.setText(abr+ " " + tur);
                }

                final String finalTur3 = tur;
                final String finalCod3 = cod;
                holder.tv_ter_mat1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if(mh != null) {
                            setVibrator(holder.tv_ter_mat1);
                            mh.chamaActivityInformacoesMateria(Comandos.getNameDisciplina(finalCod3, finalTur3));
                        }else{
                            setVibrator(holder.tv_ter_mat1);
                            hl.chamaActivityInformacoesMateria(Comandos.getNameDisciplina(finalCod3, finalTur3));
                        }
                    }
                });
                cod = listAuxDisc.get(1).getCod();
                tur = listAuxDisc.get(1).getTurma();
                abr = listAuxDisc.get(1).getAbreviacao();
                id = Comandos.returnIdMateria(cod, tur);
                holder.tv_ter_mat2.setBackgroundColor(cores.getColor(id, id));

                if(portrait) {
                    holder.tv_ter_mat2.setText(cod + " " + tur);
                }else {
                    holder.tv_ter_mat2.setText(abr+ " " + tur);
                }

                final String finalCod4 = cod;
                final String finalTur4 = tur;
                holder.tv_ter_mat2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if(mh != null) {
                            setVibrator(holder.tv_ter_mat2);
                            mh.chamaActivityInformacoesMateria(Comandos.getNameDisciplina(finalCod4, finalTur4));
                        }else{
                            setVibrator(holder.tv_ter_mat2);
                            hl.chamaActivityInformacoesMateria(Comandos.getNameDisciplina(finalCod4, finalTur4));
                        }
                    }
                });
            } else {
                holder.ll_ter_full.setVisibility(1);
                holder.ll_ter_comp.setVisibility(View.GONE);
            }

        }


         /*verifica é meu horario ou horario do semestre*/
        if (semestre == 0) {
            /*Pega as aulas para quarta-feira*/
            listAuxDisc = Comandos.returnListAulaMeuHorario(hora, 4);
        } else {
             /*Pega as aulas para quarta-feira*/
            listAuxDisc = Comandos.returnListAula(hora, 4, semestre);
        }

        if (listAuxDisc != null) {
            if (listAuxDisc.size() == 1) {//verifica se tem mais de uma aula no horario

                holder.ll_qua_full.setVisibility(1);
                holder.ll_qua_comp.setVisibility(View.GONE);

                cod = listAuxDisc.get(0).getCod();
                tur = listAuxDisc.get(0).getTurma();
                abr = listAuxDisc.get(0).getAbreviacao();
                id = Comandos.returnIdMateria(cod, tur);


                if(portrait) {
                    holder.tv_qua_mat.setText(cod);
                }else {
                    holder.tv_qua_mat.setText(abr);
                }

                holder.tv_qua_tur.setText(tur);
                holder.ll_qua_full.setBackgroundColor(cores.getColor(id, id));
                final String finalCod5 = cod;
                final String finalTur5 = tur;
                holder.ll_qua_full.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if(mh != null) {
                            setVibrator(holder.ll_qua_full);
                            mh.chamaActivityInformacoesMateria(Comandos.getNameDisciplina(finalCod5, finalTur5));
                        }else{
                            setVibrator(holder.ll_qua_full);
                            hl.chamaActivityInformacoesMateria(Comandos.getNameDisciplina(finalCod5, finalTur5));
                        }
                    }
                });
            } else if (listAuxDisc.size() == 2) {

                holder.ll_qua_comp.setVisibility(1);
                holder.ll_qua_full.setVisibility(View.GONE);

                cod = listAuxDisc.get(0).getCod();
                tur = listAuxDisc.get(0).getTurma();
                abr = listAuxDisc.get(1).getAbreviacao();
                id = Comandos.returnIdMateria(cod, tur);

                holder.tv_qua_mat1.setBackgroundColor(cores.getColor(id, id));


                if(portrait) {
                    holder.tv_qua_mat1.setText(cod + " " + tur);
                }else {
                    holder.tv_qua_mat1.setText(abr+ " " + tur);
                }

                final String finalCod6 = cod;
                final String finalTur6 = tur;
                holder.tv_qua_mat1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        if(mh != null) {
                            setVibrator(holder.tv_qua_mat1);
                            mh.chamaActivityInformacoesMateria(Comandos.getNameDisciplina(finalCod6, finalTur6));
                        }else{
                            setVibrator(holder.tv_qua_mat1);
                            hl.chamaActivityInformacoesMateria(Comandos.getNameDisciplina(finalCod6, finalTur6));
                        }
                    }
                });

                cod = listAuxDisc.get(1).getCod();
                tur = listAuxDisc.get(1).getTurma();
                abr = listAuxDisc.get(1).getAbreviacao();

                id = Comandos.returnIdMateria(cod, tur);
                holder.tv_qua_mat2.setBackgroundColor(cores.getColor(id, id));


                if(portrait) {
                    holder.tv_qua_mat2.setText(cod + " " + tur);
                }else {
                    holder.tv_qua_mat2.setText(abr+ " " + tur);
                }

                final String finalCod7 = cod;
                final String finalTur7 = tur;
                holder.tv_qua_mat2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        if(mh != null) {
                            setVibrator(holder.tv_qua_mat2);
                            mh.chamaActivityInformacoesMateria(Comandos.getNameDisciplina(finalCod7, finalTur7));
                        }else{
                            setVibrator(holder.tv_qua_mat2);
                            hl.chamaActivityInformacoesMateria(Comandos.getNameDisciplina(finalCod7, finalTur7));
                        }
                    }
                });
            } else {
                holder.ll_qua_full.setVisibility(1);
                holder.ll_qua_comp.setVisibility(View.GONE);
            }

        }

           /*verifica é meu horario ou horario do semestre*/
        if (semestre == 0) {
             /*Pega as aulas para quint-feira*/
            listAuxDisc = Comandos.returnListAulaMeuHorario(hora, 5);
        } else {
           /*Pega as aulas para quint-feira*/
            listAuxDisc = Comandos.returnListAula(hora, 5, semestre);
        }


        if (listAuxDisc != null) {
            if (listAuxDisc.size() == 1) {//verifica se tem mais de uma aula no horario

                holder.ll_qui_full.setVisibility(1);
                holder.ll_qui_comp.setVisibility(View.GONE);

                cod = listAuxDisc.get(0).getCod();
                tur = listAuxDisc.get(0).getTurma();
                abr = listAuxDisc.get(0).getAbreviacao();
                id = Comandos.returnIdMateria(cod, tur);

                if(portrait) {
                    holder.tv_qui_mat.setText(cod);
                }else {
                    holder.tv_qui_mat.setText(abr);
                }

                holder.tv_qui_tur.setText(tur);
                holder.ll_qui_full.setBackgroundColor(cores.getColor(id, id));
                final String finalCod8 = cod;
                final String finalTur8 = tur;
                holder.ll_qui_full.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        if(mh != null) {
                            setVibrator(holder.ll_qui_full);
                            mh.chamaActivityInformacoesMateria(Comandos.getNameDisciplina(finalCod8, finalTur8));
                        }else{
                            setVibrator(holder.ll_qui_full);
                            hl.chamaActivityInformacoesMateria(Comandos.getNameDisciplina(finalCod8, finalTur8));
                        }
                    }
                });

            } else if (listAuxDisc.size() == 2) {

                holder.ll_qui_comp.setVisibility(1);
                holder.ll_qui_full.setVisibility(View.GONE);

                cod = listAuxDisc.get(0).getCod();
                tur = listAuxDisc.get(0).getTurma();
                abr = listAuxDisc.get(0).getAbreviacao();

                id = Comandos.returnIdMateria(cod, tur);
                holder.tv_qui_mat1.setBackgroundColor(cores.getColor(id, id));


                if(portrait) {
                    holder.tv_qui_mat1.setText(cod + " " + tur);
                }else {
                    holder.tv_qui_mat1.setText(abr+ " " + tur);
                }
                final String finalCod9 = cod;
                final String finalTur9 = tur;
                holder.tv_qui_mat1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if(mh != null) {
                            setVibrator(holder.tv_qui_mat1);
                            mh.chamaActivityInformacoesMateria(Comandos.getNameDisciplina(finalCod9, finalTur9));
                        }else{
                            setVibrator(holder.tv_qui_mat1);
                            hl.chamaActivityInformacoesMateria(Comandos.getNameDisciplina(finalCod9, finalTur9));
                        }
                    }
                });

                cod = listAuxDisc.get(1).getCod();
                tur = listAuxDisc.get(1).getTurma();
                abr = listAuxDisc.get(1).getAbreviacao();

                id = Comandos.returnIdMateria(cod, tur);
                holder.tv_qui_mat2.setBackgroundColor(cores.getColor(id, id));


                if(portrait) {
                    holder.tv_qui_mat2.setText(cod + " " + tur);
                }else {
                    holder.tv_qui_mat2.setText(abr+ " " + tur);
                }

                final String finalCod10 = cod;
                final String finalTur10 = tur;
                holder.tv_qui_mat2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        if(mh != null) {
                            setVibrator(holder.tv_qui_mat2);
                            mh.chamaActivityInformacoesMateria(Comandos.getNameDisciplina(finalCod10, finalTur10));
                        }else{
                            setVibrator(holder.tv_qui_mat2);
                            hl.chamaActivityInformacoesMateria(Comandos.getNameDisciplina(finalCod10, finalTur10));
                        }
                    }
                });
            } else {
                holder.ll_qui_full.setVisibility(1);
                holder.ll_qui_comp.setVisibility(View.GONE);
            }

        }

             /*verifica é meu horario ou horario do semestre*/
        if (semestre == 0) {
              /*Pega as aulas para sexta-feira*/
            listAuxDisc = Comandos.returnListAulaMeuHorario(hora, 6);
        } else {
             /*Pega as aulas para sexta-feira*/
            listAuxDisc = Comandos.returnListAula(hora, 6, semestre);
        }


        if (listAuxDisc != null) {
            if (listAuxDisc.size() == 1) {//verifica se tem mais de uma aula no horario

                holder.ll_sex_full.setVisibility(1);
                holder.ll_sex_comp.setVisibility(View.GONE);

                cod = listAuxDisc.get(0).getCod();
                tur = listAuxDisc.get(0).getTurma();
                abr = listAuxDisc.get(0).getAbreviacao();
                id = Comandos.returnIdMateria(cod, tur);


                if(portrait) {
                    holder.tv_sex_mat.setText(cod);
                }else {
                    holder.tv_sex_mat.setText(abr);
                }
                holder.tv_sex_tur.setText(tur);
                holder.ll_sex_full.setBackgroundColor(cores.getColor(id, id));
                final String finalCod11 = cod;
                final String finalTur11 = tur;
                holder.ll_sex_full.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        if(mh != null) {
                            setVibrator(holder.ll_sex_full);
                            mh.chamaActivityInformacoesMateria(Comandos.getNameDisciplina(finalCod11, finalTur11));
                        }else{
                            setVibrator(holder.ll_sex_full);
                            hl.chamaActivityInformacoesMateria(Comandos.getNameDisciplina(finalCod11, finalTur11));
                        }
                    }
                });

            } else if (listAuxDisc.size() == 2) {
                holder.ll_sex_comp.setVisibility(1);
                holder.ll_sex_full.setVisibility(View.GONE);

                cod = listAuxDisc.get(0).getCod();
                tur = listAuxDisc.get(0).getTurma();
                abr = listAuxDisc.get(0).getAbreviacao();

                id = Comandos.returnIdMateria(cod, tur);
                holder.tv_sex_mat1.setBackgroundColor(cores.getColor(id, id));


                if(portrait) {
                    holder.tv_sex_mat1.setText(cod + " " + tur);
                }else {
                    holder.tv_sex_mat1.setText(abr+ " " + tur);
                }

                final String finalCod12 = cod;
                final String finalTur12 = tur;
                holder.tv_sex_mat1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if(mh != null) {
                            setVibrator(holder.tv_sex_mat1);
                            mh.chamaActivityInformacoesMateria(Comandos.getNameDisciplina(finalCod12, finalTur12));
                        }else{
                            setVibrator(holder.tv_sex_mat1);
                            hl.chamaActivityInformacoesMateria(Comandos.getNameDisciplina(finalCod12, finalTur12));
                        }
                    }
                });
                cod = listAuxDisc.get(1).getCod();
                tur = listAuxDisc.get(1).getTurma();
                abr = listAuxDisc.get(1).getAbreviacao();

                id = Comandos.returnIdMateria(cod, tur);
                holder.tv_sex_mat2.setBackgroundColor(cores.getColor(id, id));


                if(portrait) {
                    holder.tv_sex_mat2.setText(cod+ " " + tur);
                }else {
                    holder.tv_sex_mat2.setText(abr+ " " + tur);
                }

                final String finalCod13 = cod;
                final String finalTur13 = tur;
                holder.tv_sex_mat2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if(mh != null) {
                            setVibrator(holder.tv_sex_mat2);
                            mh.chamaActivityInformacoesMateria(Comandos.getNameDisciplina(finalCod13, finalTur13));
                        }else{
                            setVibrator(holder.tv_sex_mat2);
                            hl.chamaActivityInformacoesMateria(Comandos.getNameDisciplina(finalCod13, finalTur13));
                        }
                    }
                });
            } else {
                holder.ll_sex_full.setVisibility(1);
                holder.ll_sex_comp.setVisibility(View.GONE);
            }


        }

    }

    public interface MateriasOnClickListener {
        public void onClickMaterias(View view, int idx);
    }


    @Override
    public int getItemCount() {
        return lha.size();
    }


    public void setVibrator(TextView tv) {
        tv.startAnimation(animation_click);
        vibrator.vibrate(30);
    }

    public void setVibrator(LinearLayout ll) {
        ll.startAnimation(animation_click);
        vibrator.vibrate(30);
    }


}
