package br.com.uesc.colcic.DisciplinaDiaHora;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import br.com.uesc.colcic.ConexaoInternet.Objetos.DisciplinaDiaHora;
import br.com.uesc.colcic.domain.Comandos;

public class DiscDiaHoraDB {
    SQLiteDatabase db;

    public DiscDiaHoraDB(SQLiteDatabase db) {
        this.db = db;
    }

    /*Recebe uma disc dia hora e insere no banco de dados local*/
    public static void insertDisciplinaDiaHora(DisciplinaDiaHora ddh) {

        String ins = "INSERT INTO disciplina_dia_hora VALUES (" + ddh.getId() + ",'" + ddh.getFk_codigo() + "'" +
                ",'" + ddh.getFk_turma() + "','" + ddh.getFk_dia_hora() + "'," +
                "'" + ddh.getFk_sala() + "')";

        String[] sql = {ins};
        Comandos.executeSQL(sql);
    }

    /*Atualiza Disciplinas dia hora de acordo com ID*/
    public static void updateDisciplinaDiaHora(DisciplinaDiaHora ddh) {

        String ud = "UPDATE disciplina_dia_hora SET fk_codigo = '" + ddh.getFk_codigo() + "'" +
                ", fk_turma = '" + ddh.getFk_turma() + "'" +
                ", fk_dia_hora = '" + ddh.getFk_dia_hora() + "'" +
                ", fk_sala = '" + ddh.getFk_sala() + "'" +
                "  WHERE id = '" + ddh.getId() + "' ;";

        String[] sql = {ud};
        Comandos.executeSQL(sql);
    }

    /*Deleta uma Disciplinas dia hora de acordo com ID*/
    public static void deleteDisciplinaDiaHora(DisciplinaDiaHora ddh) {

        String del = "DELETE FROM disciplina_dia_hora  WHERE id = '" + ddh.getId() + "';";
        String[] sql = {del};
        Comandos.executeSQL(sql);
    }

    /*Deleta todas Disciplinas dia hora*/
    public void deleteAllDisciplinaDiaHora() {

        String del = "DELETE FROM disciplina_dia_hora;";
        String[] sql = {del};
        Comandos.executeSQL(sql);
    }

    /*Retorna uma lista com todas as disciplina dia hora */
    public ArrayList<DisciplinaDiaHora> selectAllDDH() {
        String query = "SELECT id, fk_codigo, fk_turma, fk_dia_hora, fk_sala FROM disciplina_dia_hora";
        ArrayList<DisciplinaDiaHora> listDDH = new ArrayList<>();

        Cursor cursor = db.rawQuery(query, null);
        while (cursor.moveToNext()) {
            listDDH.add(returnObjDDH(cursor));
        }
        return listDDH;
    }

    /*Metodos de atualização da tabela discplina dia hora do banco de dados local*/
    //recebe duas listas de DDH uma do WS e outra do BD e verifica se estão atualizadas entre si
    public void atualizaDDH(ArrayList<DisciplinaDiaHora> listDDHSW, ArrayList<DisciplinaDiaHora> listDDHBD) {

        boolean insert;//caso a variavel permaneca falsa insere um novo professor coso contrario atualiza
        boolean delete;// caso a varial fique true deleta o professor
        int contInsert = 0;
        int contUpdate = 0;
        int contDelete = 0;

        //verifica se a lista de ddh do banco local está vazia
        if (listDDHBD.isEmpty()) {

            //verifica se a lista de ddh do servidor não está vazia
            if (!listDDHSW.isEmpty()) {
                for (DisciplinaDiaHora dsw : listDDHSW) {
                    insertDisciplinaDiaHora(dsw);
                    contInsert++;
                }
                Log.i("MSG", "Foram inseridos "+contInsert+" novos registros.");
                contInsert = 0;
            } else {
                Log.i("ERRO", "Não é possivel atualizar lista DDH , server emptry");
            }

        } else {

            if (listDDHSW.size() >= listDDHBD.size()) {

                for (DisciplinaDiaHora ddhsw : listDDHSW) {
                    insert = true;
                    for (DisciplinaDiaHora ddhdb : listDDHBD) {

                        if ((ddhsw.getId() == ddhdb.getId())) {
                            contUpdate++;
                            insert = false;
                            if (!(ddhdb.getFk_codigo().equalsIgnoreCase(ddhsw.getFk_codigo()))) {

                                updateDisciplinaDiaHora(ddhsw);

                            } else if (!(ddhdb.getFk_turma().equalsIgnoreCase(ddhsw.getFk_turma()))) {

                                updateDisciplinaDiaHora(ddhsw);

                            } else if (!(ddhdb.getFk_sala().equalsIgnoreCase(ddhsw.getFk_sala()))) {

                                updateDisciplinaDiaHora(ddhsw);

                            } else if (!(ddhdb.getFk_dia_hora().equalsIgnoreCase(ddhsw.getFk_dia_hora()))) {

                                updateDisciplinaDiaHora(ddhsw);

                            }
                        }
                    }
                    if (insert) {
                        contInsert++;
                        insertDisciplinaDiaHora(ddhsw);

                    }
                }
                if(contInsert > 0){
                    Log.i("MSG", "Foram inseridos "+contInsert+"novos registros.");
                    contInsert = 0;

                }else{
                    Log.i("MSG", "Nenhum novo registro inserido.");
                }
                if(contUpdate > 0){
                    Log.i("MSG", "Foram atualizados "+contInsert+" registros.");
                    contUpdate = 0;
                }else{
                    Log.i("MSG", "Nenhum registro atualizado.");
                }
                //Opção usada para executar o delete
            } else {

                for (DisciplinaDiaHora ddhdb : listDDHBD) {

                    delete = true;
                    for (DisciplinaDiaHora ddhsw : listDDHSW) {

                        if ((ddhsw.getId() == ddhdb.getId())) {
                            delete = false;
                        }
                    }
                    if (delete) {

                        deleteDisciplinaDiaHora(ddhdb);
                    }

                }
            }
        }
    }

    private DisciplinaDiaHora returnObjDDH(Cursor cursor) {
        int e = cursor.getColumnIndex("id");
        int a = cursor.getColumnIndex("fk_codigo");
        int b = cursor.getColumnIndex("fk_turma");
        int c = cursor.getColumnIndex("fk_dia_hora");
        int d = cursor.getColumnIndex("fk_sala");
        int id = Integer.parseInt(cursor.getString(e));
        String fk_cod = cursor.getString(a);
        String fk_tur = cursor.getString(b);
        String fk_dia_hor = cursor.getString(c);
        String fk_sala = cursor.getString(d);
        return new DisciplinaDiaHora(id, fk_cod, fk_tur, fk_dia_hor, fk_sala);
    }

}
