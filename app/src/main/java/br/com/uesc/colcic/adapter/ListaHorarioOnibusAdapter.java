package br.com.uesc.colcic.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import br.com.uesc.colcic.ConexaoInternet.Objetos.HorarioOnibus;
import br.com.uesc.colcic.R;
import br.com.uesc.colcic.domain.Tipo;
import br.com.uesc.colcic.modelo.LinhaOnibus;

public class ListaHorarioOnibusAdapter extends RecyclerView.Adapter<ListaHorarioOnibusAdapter.ViewHolder> {

    private List<HorarioOnibus> listaHorarioOnibus;
    // OnibusOnClickListener onibusOnClickListener;OnibusOnClickListener onibusOnClickListener
    Context context;

    public ListaHorarioOnibusAdapter(List<HorarioOnibus> listaHorarioOnibus, Context context) {
        this.listaHorarioOnibus = listaHorarioOnibus;
        this.context = context;
        //this.matOnClickListener = onibusOnClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_horario_onibus, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final HorarioOnibus linha = listaHorarioOnibus.get(position);

        if (linha.getCidade().equals("Ilhéus")) {

            holder.tv_num_linha.setText(linha.getNum_linha());
            holder.tv_nome_linha.setText(linha.getLoc_chegada());
            holder.tv_via.setText(linha.getVia());
            holder.tv_hora_saida.setText(linha.getHora_saida());
            holder.loc_saida.setText(linha.getLoc_saida());
            holder.loc_chegada.setText(linha.getLoc_chegada());

        } else if (linha.getCidade().equals("Itabuna")) {

            holder.tv_nome_linha.setText(linha.getLoc_saida());
            holder.tv_via.setText(linha.getLoc_chegada());
            holder.tv_hora_saida.setText(linha.getHora_saida());
            holder.loc_saida.setText(linha.getLoc_saida());
            holder.loc_chegada.setText(linha.getLoc_chegada());
        }

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        Date hora = Calendar.getInstance().getTime(); // Ou qualquer outra forma que tem
        String dataFormatada = sdf.format(hora);

        int result = linha.getHora_saida().compareTo(dataFormatada);

        if (result < 0) {

            holder.tv_hora_saida.setTextColor(ContextCompat.getColor(context, R.color.red));
            holder.tv_legenda_hora_saida.setTextColor(ContextCompat.getColor(context, R.color.red));


        } else if (result > 0) {

            holder.tv_hora_saida.setTextColor(ContextCompat.getColor(context, R.color.c38));
            holder.tv_legenda_hora_saida.setTextColor(ContextCompat.getColor(context, R.color.c38));
        }

    }

    public interface MatOnClickListener {
        public void onClickMat(View view, String nome);
    }

    @Override
    public int getItemCount() {
        return listaHorarioOnibus.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_num_linha, tv_nome_linha, tv_via, tv_hora_saida, tv_legenda_hora_saida, loc_saida, loc_chegada;

        View mItemView;

        public ViewHolder(View v) {
            super(v);
            mItemView = v;
            // img = (ImageView) v.findViewById(R.id.imgMateria);
            tv_num_linha = (TextView) v.findViewById(R.id.tv_num_linha);
            tv_nome_linha = (TextView) v.findViewById(R.id.tv_nome_linha);
            tv_via = (TextView) v.findViewById(R.id.tv_via);
            tv_hora_saida = (TextView) v.findViewById(R.id.tv_hora_saida);
            tv_legenda_hora_saida = (TextView) v.findViewById(R.id.tv_legenda_hora_saida);
            loc_saida = (TextView) v.findViewById(R.id.loc_saida);
            loc_chegada = (TextView) v.findViewById(R.id.loc_chegada);
            //tv_status = (TextView) v.findViewById(R.id.tv_status);


        }
    }
}