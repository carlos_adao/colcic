package br.com.uesc.colcic.activity.Objetivo;

import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import br.com.uesc.colcic.R;
import br.com.uesc.colcic.views.JustifiedTextView;

public class ObjetivoActivity extends AppCompatActivity {
    JustifiedTextView tv_conteudo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_objetivo);

        final Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tv_conteudo = (JustifiedTextView)findViewById(R.id.tv_conteudo);
        tv_conteudo.setText(getString(R.string.p1_objetivo));

        CollapsingToolbarLayout collapsingToolbar = findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle("Objetivo");

        loadBackdrop();
    }

    private void loadBackdrop() {
        final ImageView imageView = findViewById(R.id.backdrop);
        Glide.with(this).load(R.drawable.banner_objetivo).into(imageView);
    }
}
