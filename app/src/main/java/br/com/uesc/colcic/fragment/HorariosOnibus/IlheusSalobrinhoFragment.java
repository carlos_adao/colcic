package br.com.uesc.colcic.fragment.HorariosOnibus;

import android.content.Context;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import br.com.uesc.colcic.R;
import br.com.uesc.colcic.adapter.ListaHorarioOnibusAdapter;
import br.com.uesc.colcic.domain.Comandos;
import br.com.uesc.colcic.fragment.BaseFragment;

public class IlheusSalobrinhoFragment extends BaseFragment {
    RecyclerView mRecyclerViewHoraioOnibus;
    LinearLayoutManager mLayoutManager;
    int cidade;
    int tipo_exibicao;
    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
    Date hora;
    String horaFormatada;
    int tipo_viagem = 0; //ida 0 e volta 1
    ImageView iv_trocar;
    TextView tv_loc_saida, tv_loc_chegada;
    Animation shake, animation_click;
    Context context;
    Vibrator vibrator;
    LinearLayout ll_trocar;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_ilheus_salobrinho, container, false);


        this.context = getContext();
        hora = Calendar.getInstance().getTime();
        horaFormatada = sdf.format(hora);

        iv_trocar = (ImageView) view.findViewById(R.id.iv_trocar);
        iv_trocar.setOnClickListener(clickInIcones);
        tv_loc_chegada = (TextView) view.findViewById(R.id.tv_loc_chegada);
        tv_loc_saida = (TextView) view.findViewById(R.id.tv_loc_saida);


        ll_trocar = (LinearLayout) view.findViewById(R.id.ll_trocar);
        ll_trocar.setOnClickListener(clickInIcones);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerViewHoraioOnibus = (RecyclerView) view.findViewById(R.id.recycler_view_horario_onibus);
        mRecyclerViewHoraioOnibus.setHasFixedSize(true);

        setHorariosLocal(cidade, 0);


        shake = AnimationUtils.loadAnimation(context, R.anim.animation_swing);
        animation_click = AnimationUtils.loadAnimation(context, R.anim.animation_click);
        vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);

        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            // Lê o tipo dos argumentos.
            this.tipo_exibicao = getArguments().getInt("tipo");
            this.cidade = getArguments().getInt("local");
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    //Método usuado para evento de click nos icone
    private View.OnClickListener clickInIcones = new View.OnClickListener() {
        public void onClick(View v) {

            switch (v.getId()) {

                case R.id.iv_trocar:
                    executa();

                case R.id.ll_trocar:
                    executa();

                default:
                    break;
            }
        }
    };

    /*executa é usado nos botões de troca de ida para volta, não interfere no tipo de visualização(próximos, já sairam e todos)*/
    public void executa() {
        String cid, query = null;
        hora = Calendar.getInstance().getTime();
        horaFormatada = sdf.format(hora);

        switch (tipo_viagem) {//switch que verifica o tipo de viagem se e de ida ou de volta
            case 0:// 0 - configura ida
                cid = defineCidade(cidade, tipo_viagem);
                query = defineTipoVisualizacao(cid,tipo_viagem,tipo_exibicao);
                tipo_viagem = 1;
                break;

            case 1:// 1 - configura volta
                cid = defineCidade(cidade, tipo_viagem);
                query = defineTipoVisualizacao(cid,tipo_viagem,tipo_exibicao);
                tipo_viagem = 0;
                break;

            default:
        }

        mRecyclerViewHoraioOnibus.setAdapter(new ListaHorarioOnibusAdapter(Comandos.retornaListaHorarioOnibus(query), getContext()));
    }

    public void setHorariosLocal(int cidade, int tipo_exibicao) {
        this.tipo_exibicao = tipo_exibicao;
        String cid, query;
        hora = Calendar.getInstance().getTime();

        horaFormatada = sdf.format(hora);//pego a hora do sistema para fazer as verificações se os ônibus já sairam
        cid = defineCidade(cidade, tipo_viagem);//método que escreve nome da cidade no textviel e retorna o nome da cidade string
        query = defineTipoVisualizacao(cid, tipo_viagem, tipo_exibicao);//define o tipo de visualização e compoe o select para consulta

        mRecyclerViewHoraioOnibus.setLayoutManager(mLayoutManager);
        mRecyclerViewHoraioOnibus.setItemAnimator(new DefaultItemAnimator());
        mRecyclerViewHoraioOnibus.setAdapter(new ListaHorarioOnibusAdapter(Comandos.retornaListaHorarioOnibus(query), getContext()));

    }

    /*metodo usado para verificar de qual cidade é o horario solicitado */
    public String defineCidade(int cidade, int tipo_viagem) {
        String cid = null;
        switch (cidade) {
            case 0://cidade de Ilhéus
                setTipoViagemIlheus(tipo_viagem);//método usado para escrever nos campos se é ida ou volta de Ilhéus
                return cid = "Ilhéus";
            case 1://cidade de Itabuna
                setTipoViagemItabuna(tipo_viagem);//método usado para escrever nos campos se é ida ou volta de Itabuna
                return cid = "Itabuna";
            default:
                return cid;
        }
    }

    /*método usado para verificar se é viagem de ida ou volta para cidade de Ilhéus*/
    public void setTipoViagemIlheus(int tipo_viagem) {

        switch (tipo_viagem) {
            case 0:
                tv_loc_saida.setText("Ilhéus");
                tv_loc_chegada.setText("UESC");
                break;
            case 1:
                tv_loc_saida.setText("UESC");
                tv_loc_chegada.setText("Ilhéus");
                break;
            default:
        }
    }

    /*método usado para verificar se é viagem de ida ou volta para cidade de Itabuna*/
    public void setTipoViagemItabuna(int tipo_viagem) {

        switch (tipo_viagem) {
            case 0:
                tv_loc_saida.setText("Itabuna");
                tv_loc_chegada.setText("UESC");
                break;
            case 1:
                tv_loc_saida.setText("UESC");
                tv_loc_chegada.setText("Itabuna");
                break;
            default:
        }
    }

    /*método usado para verificar se é pra mostrar todos horarios, somente os que ja sairam ou os proximos*/
    public String defineTipoVisualizacao(String cidade, int tipo_viagem, int tipo_exibicao) {
        String query = null;

        switch (tipo_exibicao) {
            case 0://proximos horarios
                return query = "SELECT * FROM horario_onibus where status = '" + tipo_viagem + "' and hora_saida > '" + horaFormatada + "' and cidade = '" + cidade + "'  ORDER BY hora_saida";
            case 1://já sairam
                return query = "SELECT * FROM horario_onibus where status = '" + tipo_viagem + "' and hora_saida < '" + horaFormatada + "' and cidade = '" + cidade + "'  ORDER BY hora_saida desc";
            case 2://todos horários
                                                                                                                                                                        return query = "SELECT * FROM horario_onibus where status = '" + tipo_viagem + "' and cidade = '" + cidade + "'   ORDER BY hora_saida";
            default:
                return query;
        }
    }
}


