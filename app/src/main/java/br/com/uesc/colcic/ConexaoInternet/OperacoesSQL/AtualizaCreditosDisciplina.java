package br.com.uesc.colcic.ConexaoInternet.OperacoesSQL;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.ArrayList;

import br.com.uesc.colcic.ConexaoInternet.Objetos.Disciplina;
import br.com.uesc.colcic.domain.Comandos;
import br.com.uesc.colcic.modelo.Credito;

/**
 * Created by estagio-nit on 26/09/17.
 */

public class AtualizaCreditosDisciplina {

    public static ArrayList<Credito> selectCreditos(String nome, SQLiteDatabase bd) {
        Credito credito;
        ArrayList<Credito> listaCreditos = new ArrayList<>();


        final String query = "SELECT id, num_credito, nota_credito, fk_nome_disciplina FROM creditos where fk_nome_disciplina = '" + nome + "' ";

        Cursor cursor = bd.rawQuery(query, null);

        while (cursor.moveToNext()) {

            int a = cursor.getColumnIndex("id");
            int b = cursor.getColumnIndex("num_credito");
            int c = cursor.getColumnIndex("nota_credito");
            int d = cursor.getColumnIndex("fk_nome_disciplina");


            credito = new Credito();
            credito.setId(cursor.getString(a));
            credito.setNumCredito(Integer.parseInt(cursor.getString(b)));
            credito.setNotaCredito(Float.parseFloat(cursor.getString(c)));
            credito.setNomeMateria(cursor.getString(d));
            listaCreditos.add(credito);

        }

        return listaCreditos;
    }
    /*verifica se o credito já tem uma nota cadastrada*/
    public static Boolean verificaNotaEmCredito(Credito credito, SQLiteDatabase bd) {


        final String query = "SELECT id, num_credito, nota_credito, fk_nome_disciplina FROM creditos where num_credito = '" + credito.getNumCredito() + "' and  fk_nome_disciplina = '"+credito.getNomeMateria()+"'";

        Cursor cursor = bd.rawQuery(query, null);

        while (cursor.moveToNext()) {
            return true;
        }
        return false;
    }

    public static void updateCredito(Credito crt) {
        String ud = "UPDATE creditos SET  num_credito = '" + crt.getNumCredito() + "'" +
                ", nota_credito = '" + crt.getNotaCredito() + "'" +
                ", fk_nome_disciplina = '" + crt.getNomeMateria() + "'" +
                "  where num_credito = '" + crt.getNumCredito() + "' and  fk_nome_disciplina = '"+crt.getNomeMateria()+"'";

        String[] sql = {ud};
        Comandos.executeSQL(sql);
    }

    public static void insertCredito(Credito crt) {
        Log.i("inserindo", crt.getNomeMateria());

        String ins = "INSERT INTO creditos(num_credito, nota_credito,fk_nome_disciplina) VALUES ('" + crt.getNumCredito() + "','" + crt.getNotaCredito() + "','" + crt.getNomeMateria() + "')";
        String[] sql = {ins};
        Comandos.executeSQL(sql);

    }

    public static void deleteCredito(Credito crt) {
        Log.i("Deletando", crt.getNomeMateria());

        String del = "DELETE FROM creditos   where num_credito = '" + crt.getNumCredito() + "' and  fk_nome_disciplina = '"+crt.getNomeMateria()+"'";
        String[] sql = {del};
        Comandos.executeSQL(sql);
    }

    public static void deleteCredito(String nomeMateria) {

        String del = "DELETE FROM creditos   where  fk_nome_disciplina = '"+nomeMateria+"'";
        String[] sql = {del};
        Comandos.executeSQL(sql);
    }

    /*método usado para retornar a media de creditos de uma disciplina*/
    public static Float returnMediaCreditos(String nomeDisciplina, SQLiteDatabase bd) {
        int quantidade = 0;
        float soma = 0;

        final String query = "SELECT nota_credito FROM creditos where fk_nome_disciplina = '" + nomeDisciplina + "'";

        Cursor cursor = bd.rawQuery(query, null);

        while (cursor.moveToNext()) {
            int c = cursor.getColumnIndex("nota_credito");
            soma = soma + (Float.parseFloat(cursor.getString(c)));
            quantidade++;
        }

        float media = (soma/quantidade);


        NumberFormat format = NumberFormat.getInstance();
        format.setMaximumFractionDigits(2);
        format.setMinimumFractionDigits(2);
        format.setMaximumIntegerDigits(2);
        format.setRoundingMode(RoundingMode.HALF_UP);
        media = Float.valueOf(format.format(media).replace(",", "."));


        return media;
    }

    /*método usado para retornar a soma de creditos de uma disciplina*/
    public static Float returnSomaCreditos(String nomeDisciplina, SQLiteDatabase bd) {

        float soma = 0;

        final String query = "SELECT nota_credito FROM creditos where fk_nome_disciplina = '" + nomeDisciplina + "'";

        Cursor cursor = bd.rawQuery(query, null);

        while (cursor.moveToNext()) {
            int c = cursor.getColumnIndex("nota_credito");
            soma = soma + (Float.parseFloat(cursor.getString(c)));
        }

        NumberFormat format = NumberFormat.getInstance();
        format.setMaximumFractionDigits(2);
        format.setMinimumFractionDigits(2);
        format.setMaximumIntegerDigits(2);
        format.setRoundingMode(RoundingMode.HALF_UP);
        soma = Float.valueOf(format.format(soma).replace(",", "."));


        return soma;
    }

    /*método usado para retornar a quantidade de creditos de uma disciplina*/
    public static Integer returnQuantidadeCreditos(String nomeDisciplina, SQLiteDatabase bd) {
        int quantidade = 0;


        final String query = "SELECT nota_credito FROM creditos where fk_nome_disciplina = '" + nomeDisciplina + "'";

        Cursor cursor = bd.rawQuery(query, null);

        while (cursor.moveToNext()) {
           quantidade++;
        }

        return quantidade;
    }

    /*verifica se tem algun credito cadastrado para materia*/
    public static boolean temCreditos(String nomeDisciplina, SQLiteDatabase bd) {

        final String query = "SELECT * FROM creditos where fk_nome_disciplina = '" + nomeDisciplina + "'";

        Cursor cursor = bd.rawQuery(query, null);

        while (cursor.moveToNext()) {
           return  true;
        }

        return false;
    }
/************************************************************************************************************/

}
