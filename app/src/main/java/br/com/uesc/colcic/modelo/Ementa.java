package br.com.uesc.colcic.modelo;

/**
 * Created by carlos on 25/06/2017.
 */

public class Ementa {


    String cod;
    String nomeDisc;
    String preReq;
    String qtdCreitos;
    String cargaHoraria;

    public String getCargaHoraria() {
        return cargaHoraria;
    }

    public void setCargaHoraria(String cargaHoraria) {
        this.cargaHoraria = cargaHoraria;
    }

    public Ementa(String cod, String nomeDisc, String preReq, String qtdCreitos, String cargaHoraria, String ementa, String objetivo, String metodologia, String avalicao, String contProg, String referencia) {

        this.cod = cod;
        this.nomeDisc = nomeDisc;
        this.preReq = preReq;
        this.qtdCreitos = qtdCreitos;
        this.cargaHoraria = cargaHoraria;
        this.ementa = ementa;
        this.objetivo = objetivo;
        this.metodologia = metodologia;
        this.avalicao = avalicao;
        this.contProg = contProg;
        this.referencia = referencia;
    }

    String ementa;
    String objetivo;
    String metodologia;
    String avalicao;
    String contProg;
    String referencia;



    public String getQtdCreitos() {
        return qtdCreitos;
    }

    public void setQtdCreitos(String qtdCreitos) {
        this.qtdCreitos = qtdCreitos;
    }

    public Ementa(String cod, String nomeDisc, String preReq, String qtdCreitos, String ementa, String objetivo, String metodologia, String avalicao, String contProg, String referencia) {

        this.cod = cod;
        this.nomeDisc = nomeDisc;
        this.preReq = preReq;
        this.qtdCreitos = qtdCreitos;
        this.ementa = ementa;
        this.objetivo = objetivo;
        this.metodologia = metodologia;
        this.avalicao = avalicao;
        this.contProg = contProg;
        this.referencia = referencia;
    }


    public Ementa(String cod, String nomeDisc, String preReq, String ementa, String objetivo, String metodologia, String avalicao, String contProg, String referencia) {
        this.cod = cod;
        this.nomeDisc = nomeDisc;
        this.preReq = preReq;
        this.ementa = ementa;
        this.objetivo = objetivo;
        this.metodologia = metodologia;
        this.avalicao = avalicao;
        this.contProg = contProg;
        this.referencia = referencia;
    }

    public Ementa() {

    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public String getNomeDisc() {
        return nomeDisc;
    }

    @Override
    public String toString() {
        return "Ementa{" +
                "cod='" + cod + '\'' +
                ", nomeDisc='" + nomeDisc + '\'' +
                ", preReq='" + preReq + '\'' +
                ", ementa='" + ementa + '\'' +
                ", objetivo='" + objetivo + '\'' +
                ", metodologia='" + metodologia + '\'' +
                ", avalicao='" + avalicao + '\'' +
                ", contProg='" + contProg + '\'' +
                ", referencia='" + referencia + '\'' +
                '}';
    }

    public void setNomeDisc(String nomeDisc) {
        this.nomeDisc = nomeDisc;
    }

    public String getPreReq() {
        return preReq;
    }

    public void setPreReq(String preReq) {
        this.preReq = preReq;
    }

    public String getEmenta() {
        return ementa;
    }

    public void setEmenta(String ementa) {
        this.ementa = ementa;
    }

    public String getObjetivo() {
        return objetivo;
    }

    public void setObjetivo(String objetivo) {
        this.objetivo = objetivo;
    }

    public String getMetodologia() {
        return metodologia;
    }

    public void setMetodologia(String metodologia) {
        this.metodologia = metodologia;
    }

    public String getAvalicao() {
        return avalicao;
    }

    public void setAvalicao(String avalicao) {
        this.avalicao = avalicao;
    }

    public String getContProg() {
        return contProg;
    }

    public void setContProg(String contProg) {
        this.contProg = contProg;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }
}
