package br.com.uesc.colcic.modelo;

/**
 * Created by carlos on 27/04/2017.
 */

public class Materia {
    String codigo;
    String nome;
    String turma;
    String dia_semana;

    public String getTurma() {
        return turma;
    }

    public void setTurma(String turma) {
        this.turma = turma;
    }

    public Materia(String codigo, String nome, String turma, String dia_semana, String hora_inicio, String hora_fim, String professor) {

        this.codigo = codigo;
        this.nome = nome;
        this.turma = turma;
        this.dia_semana = dia_semana;
        this.hora_inicio = hora_inicio;
        this.hora_fim = hora_fim;
        this.professor = professor;
    }

    String hora_inicio;
    String hora_fim;
    String professor ;



    public Materia(){

    }

    public Materia(String codigo, String nome, String dia_semana, String hora_inicio, String hora_fim, String professor) {
        this.codigo = codigo;
        this.nome = nome;
        this.dia_semana = dia_semana;
        this.hora_inicio = hora_inicio;
        this.hora_fim = hora_fim;
        this.professor = professor;
    }

    public String getDia_semana() {
        return dia_semana;
    }

    public void setDia_semana(String dia_semana) {
        this.dia_semana = dia_semana;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getHora_inicio() {
        return hora_inicio;
    }

    public void setHora_inicio(String hora_inicio) {
        this.hora_inicio = hora_inicio;
    }

    public String getHora_fim() {
        return hora_fim;
    }

    public void setHora_fim(String hora_fim) {
        this.hora_fim = hora_fim;
    }

    public String getProfessor() {
        return professor;
    }

    public void setProfessor(String professor) {
        this.professor = professor;
    }



    @Override
    public String toString() {
        return "Materia{" +
                "codigo='" + codigo + '\'' +
                ", nome='" + nome + '\'' +
                ", dia_semana='" + dia_semana + '\'' +
                ", hora_inicio='" + hora_inicio + '\'' +
                ", hora_fim='" + hora_fim + '\'' +
                ", professor='" + professor + '\'' +
                '}';
    }
}
