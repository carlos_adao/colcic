package br.com.uesc.colcic.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import br.com.uesc.colcic.R;
import br.com.uesc.colcic.domain.Tipo;

public class ListaMateriaAdapter extends RecyclerView.Adapter<ListaMateriaAdapter.ViewHolder> {

    private List<Tipo> listaTipos;
    MatOnClickListener matOnClickListener;

    public ListaMateriaAdapter(List<Tipo> list, MatOnClickListener matOnClickListener) {
        this.listaTipos = list;
        this.matOnClickListener = matOnClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.materia_layout_linha_lista_escolha, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Tipo tipo = listaTipos.get(position);

        holder.name.setText(tipo.getName());
        holder.img.setImageDrawable(tipo.getimg());


        if (matOnClickListener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    matOnClickListener.onClickMat(holder.itemView, tipo.getName());
                }
            });
        }
    }

    public interface MatOnClickListener {
        public void onClickMat(View view, String nome);
    }

    @Override
    public int getItemCount() {
        return listaTipos.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private ImageView img;
        View mItemView;

        public ViewHolder(View v) {
            super(v);
            mItemView = v;
            img = (ImageView) v.findViewById(R.id.imgMateria);
            name = (TextView) v.findViewById(R.id.nameMateria);
        }
    }
}