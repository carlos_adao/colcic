package br.com.uesc.colcic.ConexaoInternet.Objetos;

/**
 * Created by estagio-nit on 22/09/17.
 */

public class Func {
    String codigo, nome, email, tel, cargo, hierarquia, sexo;

    public Func(String codigo, String nome, String email, String tel, String cargo, String hierarquia, String sexo) {
        this.codigo = codigo;
        this.nome = nome;
        this.email = email;
        this.tel = tel;
        this.cargo = cargo;
        this.hierarquia = hierarquia;
        this.sexo = sexo;
    }

    public Func() {

    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getHierarquia() {
        return hierarquia;
    }

    public void setHierarquia(String hierarquia) {
        this.hierarquia = hierarquia;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }
}
