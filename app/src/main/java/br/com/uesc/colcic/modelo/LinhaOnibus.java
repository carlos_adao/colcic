package br.com.uesc.colcic.modelo;

/**
 * Created by estagio-nit on 31/10/17.
 */

public class LinhaOnibus {
    String nome_linha;
    String num_linha;
    String loc_saida;
    String loc_chegada;
    String hora_saida;
    String hora_chegada;
    String via;

    public LinhaOnibus(String nome_linha, String num_linha, String loc_saida, String loc_chegada, String hora_saida, String hora_chegada, String via) {
        this.nome_linha = nome_linha;
        this.num_linha = num_linha;
        this.loc_saida = loc_saida;
        this.loc_chegada = loc_chegada;
        this.hora_saida = hora_saida;
        this.hora_chegada = hora_chegada;
        this.via = via;
    }

    public String getNome_linha() {
        return nome_linha;
    }

    public void setNome_linha(String nome_linha) {
        this.nome_linha = nome_linha;
    }

    public String getNum_linha() {
        return num_linha;
    }

    public void setNum_linha(String num_linha) {
        this.num_linha = num_linha;
    }

    public String getLoc_saida() {
        return loc_saida;
    }

    public void setLoc_saida(String loc_saida) {
        this.loc_saida = loc_saida;
    }

    public String getLoc_chegada() {
        return loc_chegada;
    }

    public void setLoc_chegada(String loc_chegada) {
        this.loc_chegada = loc_chegada;
    }

    public String getHora_saida() {
        return hora_saida;
    }

    public void setHora_saida(String hora_saida) {
        this.hora_saida = hora_saida;
    }

    public String getHora_chegada() {
        return hora_chegada;
    }

    public void setHora_chegada(String hora_chegada) {
        this.hora_chegada = hora_chegada;
    }

    public String getVia() {
        return via;
    }

    public void setVia(String via) {
        this.via = via;
    }

    public LinhaOnibus() {

    }
}
