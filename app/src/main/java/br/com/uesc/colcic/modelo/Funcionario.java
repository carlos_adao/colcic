package br.com.uesc.colcic.modelo;

import android.graphics.drawable.Drawable;

/**
 * Created by carlos on 02/07/2017.
 */

public class Funcionario {

    String codigo;
    String nome;
    String email;
    String tel;
    String cargo;
    String hierarquia;
    String sexo;
    private Drawable img;


    public Funcionario(String nome, String email, String tel, String cargo, String hierarquia, String sexo, Drawable img) {
        this.nome = nome;
        this.email = email;
        this.tel = tel;
        this.cargo = cargo;
        this.hierarquia = hierarquia;
        this.sexo = sexo;
        this.img = img;
    }

    public Funcionario() {

    }

    public Drawable getImg() {

        return img;
    }


    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setImg(Drawable img) {
        this.img = img;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getHierarquia() {
        return hierarquia;
    }

    public void setHierarquia(String hierarquia) {
        this.hierarquia = hierarquia;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public Funcionario(String nome, String email, String tel, String cargo, String hierarquia, String sexo) {

        this.nome = nome;
        this.email = email;
        this.tel = tel;
        this.cargo = cargo;
        this.hierarquia = hierarquia;
        this.sexo = sexo;
    }

    @Override
    public String toString() {
        return "Func{" +
                "nome='" + nome + '\'' +
                ", email='" + email + '\'' +
                ", tel='" + tel + '\'' +
                ", cargo='" + cargo + '\'' +
                ", hierarquia='" + hierarquia + '\'' +
                ", sexo='" + sexo + '\'' +
                ", img=" + img +
                '}';
    }


}
