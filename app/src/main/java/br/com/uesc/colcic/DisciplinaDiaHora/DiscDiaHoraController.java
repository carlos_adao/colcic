package br.com.uesc.colcic.DisciplinaDiaHora;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.util.ArrayList;
import br.com.uesc.colcic.ConexaoInternet.Objetos.DisciplinaDiaHora;

public class DiscDiaHoraController extends AsyncTask<Void, Void, JSONObject> {
    private DiscDiaHoraModel ddhModel;
    private Context context;
    private DiscDiaHoraDB ddhBD;

    public void setContext(Context context, DiscDiaHoraDB ddhBD) {
        this.context = context;
        this.ddhBD = ddhBD;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected JSONObject doInBackground(Void... strings) {

        try {
            ddhModel = new DiscDiaHoraModel();
            return ddhModel.getJsonDDH(context);
        } catch (Exception e) {
            Log.i("ERRO", "Falha ao fazer downlod Json DDH");
        }
        return null;
    }

    protected void onPostExecute(JSONObject arquivoJson) {
        String data = String.valueOf(arquivoJson);

        if (arquivoJson != null) {
            try {

                ArrayList<DisciplinaDiaHora> listDDHSW = jsonForDisciplinaDiaHora(data);

                ddhBD.deleteAllDisciplinaDiaHora();
                ArrayList<DisciplinaDiaHora> listDDHDB = ddhBD.selectAllDDH();
                ddhBD.atualizaDDH(listDDHSW, listDDHDB);

            } catch (IOException e) {
                Log.i("ERRO", "Falha ao converter JSON");
                e.printStackTrace();
            }
        }
    }

    /*Convert json uma lista de objetos disciplina dia Hora*/
    private ArrayList<DisciplinaDiaHora> jsonForDisciplinaDiaHora(String json) throws IOException {

        ArrayList<DisciplinaDiaHora> listdisciplina_dia_hora = new ArrayList<>();
        DisciplinaDiaHora disciplinaDiaHora = null;

        try {

            JSONObject object = new JSONObject(json);
            JSONArray jsonDisDiaHora  = object.getJSONArray("Disciplina_dia_hora");

            for (int i = 0; i < jsonDisDiaHora.length(); i++) {
                JSONObject jsonset = jsonDisDiaHora.getJSONObject(i);

                disciplinaDiaHora = new DisciplinaDiaHora();
                disciplinaDiaHora.setId(Integer.parseInt(jsonset.optString("id")));
                disciplinaDiaHora.setFk_codigo(jsonset.optString("fk_codigo"));
                disciplinaDiaHora.setFk_turma(jsonset.optString("fk_turma"));
                disciplinaDiaHora.setFk_dia_hora(jsonset.optString("fk_dia_hora"));
                disciplinaDiaHora.setFk_sala(jsonset.optString("fk_sala"));

                listdisciplina_dia_hora.add(disciplinaDiaHora);
            }

        } catch (JSONException e) {
            throw new IOException(e.getMessage(), e);
        }

        return listdisciplina_dia_hora;
    }

}


