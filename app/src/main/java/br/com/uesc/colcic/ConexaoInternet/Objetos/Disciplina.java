package br.com.uesc.colcic.ConexaoInternet.Objetos;

/**
 * Created by estagio-nit on 21/09/17.
 */

public class Disciplina {

    String codigo, nome, abreviacao,  carga_horaria, fk_semestre_virgente, semestre_diciplina, fk_curso, fk_professor, turma, CHS;

    public Disciplina(String codigo, String nome, String abreviacao, String carga_horaria, String fk_semestre_virgente, String semestre_diciplina, String fk_curso, String fk_professor, String turma, String CHS) {
        this.codigo = codigo;
        this.nome = nome;
        this.abreviacao = abreviacao;
        this.carga_horaria = carga_horaria;
        this.fk_semestre_virgente = fk_semestre_virgente;
        this.semestre_diciplina = semestre_diciplina;
        this.fk_curso = fk_curso;
        this.fk_professor = fk_professor;
        this.turma = turma;
        this.CHS = CHS;
    }

    public Disciplina(String codigo, String nome, String carga_horaria, String fk_semestre_virgente, String semestre_diciplina, String fk_curso, String fk_professor, String turma, String CHS) {
        this.codigo = codigo;
        this.nome = nome;
        this.carga_horaria = carga_horaria;
        this.fk_semestre_virgente = fk_semestre_virgente;
        this.semestre_diciplina = semestre_diciplina;
        this.fk_curso = fk_curso;
        this.fk_professor = fk_professor;
        this.turma = turma;
        this.CHS = CHS;
    }

    public Disciplina() {

    }

    public String getAbreviacao() {
        return abreviacao;
    }

    public void setAbreviacao(String abreviacao) {
        this.abreviacao = abreviacao;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCarga_horaria() {
        return carga_horaria;
    }

    public void setCarga_horaria(String carga_horaria) {
        this.carga_horaria = carga_horaria;
    }

    public String getFk_semestre_virgente() {
        return fk_semestre_virgente;
    }

    public void setFk_semestre_virgente(String fk_semestre_virgente) {
        this.fk_semestre_virgente = fk_semestre_virgente;
    }

    public String getSemestre_diciplina() {
        return semestre_diciplina;
    }

    public void setSemestre_diciplina(String semestre_diciplina) {
        this.semestre_diciplina = semestre_diciplina;
    }

    public String getFk_curso() {
        return fk_curso;
    }

    public void setFk_curso(String fk_curso) {
        this.fk_curso = fk_curso;
    }

    public String getFk_professor() {
        return fk_professor;
    }

    public void setFk_professor(String fk_professor) {
        this.fk_professor = fk_professor;
    }

    public String getTurma() {
        return turma;
    }

    public void setTurma(String turma) {
        this.turma = turma;
    }

    public String getCHS() {
        return CHS;
    }

    public void setCHS(String CHS) {
        this.CHS = CHS;
    }
}
