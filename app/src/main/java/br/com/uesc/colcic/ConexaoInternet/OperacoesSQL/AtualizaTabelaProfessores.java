package br.com.uesc.colcic.ConexaoInternet.OperacoesSQL;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import br.com.uesc.colcic.ConexaoInternet.Objetos.Func;
import br.com.uesc.colcic.ConexaoInternet.Objetos.Professor;
import br.com.uesc.colcic.domain.Comandos;

/**
 * Created by estagio-nit on 23/10/17.
 */

public class AtualizaTabelaProfessores {

    public static void run(ArrayList<Professor> lProf , SQLiteDatabase bd) {
        ArrayList<Professor> lprofDB = new ArrayList<>();
        Professor prof;
        boolean insert;//caso a variavel permaneca falsa insere um novo professor coso contrario atualiza
        boolean delete;// caso a varial fique true deleta o professor

        Log.i("Atualizando","Tabela Professor");

        final String query = "SELECT * FROM professor ORDER BY codigo";

        Cursor cursor = bd.rawQuery(query, null);
        while (cursor.moveToNext()) {
            int cd = cursor.getColumnIndex("codigo");
            int n = cursor.getColumnIndex("nome");
            int e = cursor.getColumnIndex("email");
            int t = cursor.getColumnIndex("tel");
            int tt = cursor.getColumnIndex("titulacao");
            int c = cursor.getColumnIndex("classe");
            int ul = cursor.getColumnIndex("url_lates");

            String co =  cursor.getString(cd);
            String no =  cursor.getString(n);
            String ma =  cursor.getString(e);
            String te =  cursor.getString(t);
            String ti =  cursor.getString(tt);
            String cl = cursor.getString(c);
            String ur = cursor.getString(ul);


            prof = new Professor(co, no, ma, te, ti, cl, ur);
            lprofDB.add(prof);



        }

        if (lprofDB.size() == 0) {


            /*Caso não tenha dados na tabela insere os dados vindos do servidor*/
            for (Professor psw : lProf) {

                insertProfessor(psw);

            }

        }

        else {

            if (lProf.size() >= lprofDB.size()) {
                for (Professor psw : lProf) {
                    insert = true;
                    for (Professor pdb : lprofDB) {
                        if (psw.getCodigo().equals(pdb.getCodigo())) {


                                insert = false;
                                if (!(pdb.getNome().equalsIgnoreCase(psw.getNome()))) {


                                    updateProfessor(psw);

                                } else if (!(pdb.getEmail().equalsIgnoreCase(psw.getEmail()))) {

                                    updateProfessor(psw);
                                } else if (!(pdb.getTel().equalsIgnoreCase(psw.getTel()))) {

                                    updateProfessor(psw);

                                } else if (!(pdb.getTitulacao().equalsIgnoreCase(psw.getTitulacao()))) {

                                    updateProfessor(psw);

                                }else if (!(pdb.getClasse().equalsIgnoreCase(psw.getClasse()))) {
                                    Log.i("Professor mudou", "Classe");
                                    updateProfessor(psw);

                                }else if (!(pdb.getUrl_lattes().equalsIgnoreCase(psw.getUrl_lattes()))) {
                                    Log.i("Professor mudou", "Classes");
                                    updateProfessor(psw);
                                }
                        }
                    }
                    if (insert) {

                        insertProfessor(psw);
                    }

                }
            } else {
                for (Professor pdb : lprofDB) {
                    delete = true;
                    for (Professor psw : lProf) {
                        if (psw.getCodigo().equals(pdb.getCodigo())) {

                            delete = false;
                        }
                    }
                    if (delete) {

                        deleteProfessor(pdb);
                    }

                }
            }
        }

    }


    public static void updateProfessor(Professor prof) {

        String  ud ="UPDATE professor SET codigo = '"+prof.getCodigo()+ "'" +
                ", nome = '"+prof.getNome()+ "'" +
                ", email = '"+prof.getEmail()+ "'" +
                ", tel = '"+prof.getTel()+ "'" +
                ", titulacao = '"+prof.getTitulacao()+ "'" +
                ", classe = '"+prof.getClasse()+ "'" +
                ", url_lates = '"+prof.getUrl_lattes()+ "'" +
                "  WHERE codigo = '"+prof.getCodigo()+"' ;";

        String[] sql = {ud};
        Comandos.executeSQL(sql);
    }

    public static void insertProfessor(Professor prof) {

        String ins = "INSERT INTO professor(codigo, nome, email, tel, titulacao, classe, url_lates) VALUES ('"+ prof.getCodigo() +"','"+ prof.getNome() +"','"+ prof.getEmail() +"','','"+ prof.getTitulacao() +"','"+ prof.getClasse() +"','"+ prof.getUrl_lattes() +"')";
        String[] sql = {ins};

        Comandos.executeSQL(sql);

    }
    public static void deleteProfessor(Professor prof) {

        String del = "DELETE FROM professor  WHERE codigo = '" + prof.getCodigo() + "' ;";
        String[] sql = {del};
        Comandos.executeSQL(sql);
    }

}
