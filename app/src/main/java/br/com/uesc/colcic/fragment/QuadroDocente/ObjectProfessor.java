package br.com.uesc.colcic.fragment.QuadroDocente;

public class ObjectProfessor {

    private String name;

    private String email;

    private String areaAtuacao;

    public ObjectProfessor(String name, String location, String timePassed){
        setName(name);
        setLocation(location);
        setTimePassed(timePassed);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return email;
    }

    public void setLocation(String location) {this.email = location;}

    public String getTimePassed() {
        return areaAtuacao;
    }

    public void setTimePassed(String timePassed) {
        this.areaAtuacao = timePassed;
    }
}
