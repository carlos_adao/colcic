package br.com.uesc.colcic.adapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import br.com.uesc.colcic.R;
import br.com.uesc.colcic.domain.Comandos;
import br.com.uesc.colcic.modelo.InformacoesMateria;
import br.com.uesc.colcic.modelo.Materia;


public class HorarioDescCodMatAdapter extends RecyclerView.Adapter<HorarioDescCodMatAdapter.MyViewHolder> {

    TypedArray cores;
    private ArrayList<Materia> listMatSemestre;
    private MateriasOnClickListener materiaOnClickListener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_codMat, tv_nomeMat;

        public MyViewHolder(View view) {
            super(view);
            tv_codMat = (TextView) view.findViewById(R.id.tv_codMat);
            tv_nomeMat = (TextView) view.findViewById(R.id.tv_nomeMat);
        }
    }

    public HorarioDescCodMatAdapter(Context mContext, ArrayList<Materia> listMatSemestre, TypedArray cores, MateriasOnClickListener materiaOnClickListener) {
        this.cores = cores;
        this.listMatSemestre = listMatSemestre;
        this.materiaOnClickListener = materiaOnClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_horario_letivo_desc_cod_mat, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Materia ms = listMatSemestre.get(position);
        int id = id = Comandos.returnIdMateria(ms.getCodigo(), ms.getTurma());
        holder.tv_codMat.setText(ms.getCodigo());

        holder.tv_codMat.setBackgroundColor(cores.getColor(id,id));
        holder.tv_nomeMat.setText(ms.getNome());

        if (materiaOnClickListener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    materiaOnClickListener.onClickMaterias(holder.itemView, ms.getNome());
                }
            });
        }

    }

    public interface MateriasOnClickListener {
        public void onClickMaterias(View view, String idx);
    }


    @Override
    public int getItemCount() {
        return listMatSemestre.size();
    }
}
