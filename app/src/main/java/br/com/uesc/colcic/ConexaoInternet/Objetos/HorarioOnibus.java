package br.com.uesc.colcic.ConexaoInternet.Objetos;

/**
 * Created by estagio-nit on 06/11/17.
 */

public class HorarioOnibus {

    String cod;
    String num_linha;
    String via;
    String loc_saida;
    String loc_chegada;
    String cidade;
    String empresa;
    String hora_saida;
    String status;

    public HorarioOnibus() {}

    public HorarioOnibus(String cod, String num_linha, String via, String loc_saida, String loc_chegada, String cidade, String empresa, String hora_saida, String status) {
        this.cod = cod;
        this.num_linha = num_linha;
        this.via = via;
        this.loc_saida = loc_saida;
        this.loc_chegada = loc_chegada;
        this.cidade = cidade;
        this.empresa = empresa;
        this.hora_saida = hora_saida;
        this.status = status;
    }
    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public String getNum_linha() {
        return num_linha;
    }

    public void setNum_linha(String num_linha) {
        this.num_linha = num_linha;
    }

    public String getVia() {
        return via;
    }

    public void setVia(String via) {
        this.via = via;
    }

    public String getLoc_saida() {
        return loc_saida;
    }

    public void setLoc_saida(String loc_saida) {
        this.loc_saida = loc_saida;
    }

    public String getLoc_chegada() {
        return loc_chegada;
    }

    public void setLoc_chegada(String loc_chegada) {
        this.loc_chegada = loc_chegada;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getHora_saida() {
        return hora_saida;
    }

    public void setHora_saida(String hora_saida) {
        this.hora_saida = hora_saida;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "HorarioOnibus{" +
                "cod='" + cod + '\'' +
                ", num_linha='" + num_linha + '\'' +
                ", via='" + via + '\'' +
                ", loc_saida='" + loc_saida + '\'' +
                ", loc_chegada='" + loc_chegada + '\'' +
                ", cidade='" + cidade + '\'' +
                ", empresa='" + empresa + '\'' +
                ", hora_saida='" + hora_saida + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
