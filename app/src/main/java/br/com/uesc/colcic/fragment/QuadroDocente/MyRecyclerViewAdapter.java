package br.com.uesc.colcic.fragment.QuadroDocente;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import br.com.uesc.colcic.R;


public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.DataObjectHolder> {

    private ArrayList<Docente> listLDocentes;
    private Context mContext;
    private ProfOnClickListener profOnClickListener;


    public MyRecyclerViewAdapter(Context context, ArrayList<Docente> listLDocentes, ProfOnClickListener profOnClickListener) {
        mContext = context;
        this.listLDocentes = listLDocentes;
        this.profOnClickListener = profOnClickListener;
    }


    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tile, parent, false);
        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(final DataObjectHolder holder, final int position) {

        final Docente docente;
        String namePlateLetter = listLDocentes.get(position).getNome().substring(0, 1).toUpperCase();
        namePlateLetter = LetrasComAcento(namePlateLetter);
        CircleDrawable circleDrawable = new CircleDrawable(mContext.getResources().getColor(LetterColorMapping.letterToColorIdMapping.get(namePlateLetter)));
        docente = listLDocentes.get(position);
        holder.namePlateTextView.setText(namePlateLetter);
        holder.nameTextView.setText(docente.getNome());
        holder.emailTextView.setText(docente.mail);
        holder.areaAtuacaoTextView.setText(docente.clas);
        holder.namePlateTextView.setBackgroundDrawable(circleDrawable);

        if (profOnClickListener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    profOnClickListener.onClickProfessor(holder.itemView, docente.getNome());
                }
            });
        }
    }

    public interface ProfOnClickListener {
        public void onClickProfessor(View view, String nome);
    }

    public static class DataObjectHolder extends RecyclerView.ViewHolder {

        TextView nameTextView, emailTextView, areaAtuacaoTextView, namePlateTextView;

        View mItemView;

        public DataObjectHolder(View itemView) {
            super(itemView);

            mItemView = itemView;
            nameTextView = (TextView) itemView.findViewById(R.id.name_text_view);
            emailTextView = (TextView) itemView.findViewById(R.id.email);
            areaAtuacaoTextView = (TextView) itemView.findViewById(R.id.area_atuacao);
            namePlateTextView = (TextView) itemView.findViewById(R.id.name_plate_text_view);
        }

    }

    @Override
    public int getItemCount() {
        return listLDocentes.size();
    }


    public String LetrasComAcento(String letra) {
        if (letra.equalsIgnoreCase("Á"))
            return "A";
        else if (letra.equalsIgnoreCase("É"))
            return "E";
        else if (letra.equalsIgnoreCase("Í"))
            return "I";
        else if (letra.equalsIgnoreCase("Ó"))
            return "O";
        else if (letra.equalsIgnoreCase("Ú"))
            return "U";
        else return letra;
    }

}
