package br.com.uesc.colcic.Settings;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import br.com.uesc.colcic.ConexaoInternet.Objetos.DisciplinaDiaHora;
import br.com.uesc.colcic.DisciplinaDiaHora.DiscDiaHoraDB;
import br.com.uesc.colcic.DisciplinaDiaHora.DiscDiaHoraModel;

public class SettingsController extends AsyncTask<Void, Void, JSONObject> {
    private Context context;
    private SettingsDB settingsDB;
    private SettingsModel settingsModel;

    public void setContext(Context context, SettingsDB settingsDB) {
        this.context = context;
        this.settingsDB = settingsDB;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected JSONObject doInBackground(Void... strings) {

        try {
            settingsModel = new SettingsModel();
            return settingsModel.getJsonSettings(context);
        } catch (Exception e) {

            Log.i("ERRO", "Falha ao fazer downlod settings model");
        }
        return null;
    }

    protected void onPostExecute(JSONObject arquivoJson) {
        String data = String.valueOf(arquivoJson);

        if (arquivoJson != null) {
            try {

                Settings setSW = jsonForSettings(data);
                Settings setDB = settingsDB.retornaTabelaAtualizacoes();
                settingsDB.atualizaSettings(setSW, setDB);

            } catch (IOException e) {
                Log.i("ERRO", "Falha ao converter JSON");
                e.printStackTrace();
            }
        }
    }

    /*Convert Converte um arquivo json em um objeto settings*/
    private Settings jsonForSettings(String json) throws IOException {

        Settings settings = null;
        try {
            JSONObject object = new JSONObject(json);
            JSONArray jsonSettings = object.getJSONArray("Setting");

            JSONObject jsonset = jsonSettings.getJSONObject(0);

            // Lê as informações de settings
            settings = new Settings();
            settings.setCod(jsonset.optString("cod"));
            settings.setAtt_funcionario(jsonset.optString("att_funcionario"));
            settings.setAtt_professor(jsonset.optString("att_professor"));
            settings.setAtt_sala(jsonset.optString("att_sala"));
            settings.setAtt_semestre_virgente(jsonset.optString("att_semestre_virgente"));
            settings.setAtt_disciplina(jsonset.optString("att_disciplina"));
            settings.setAtt_ementa(jsonset.optString("att_ementa"));
            settings.setAtt_pre_requisito(jsonset.optString("att_pre_requisito"));
            settings.setAtt_disciplina_dia_hora(jsonset.optString("att_disciplina_dia_hora"));
            settings.setAtt_horario_onibus(jsonset.optString("att_horario_onibus"));

        } catch (JSONException e) {
            throw new IOException(e.getMessage(), e);
        }

        return settings;
    }

}


