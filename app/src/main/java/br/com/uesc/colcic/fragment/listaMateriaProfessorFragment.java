package br.com.uesc.colcic.fragment;

import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import br.com.uesc.colcic.R;
import br.com.uesc.colcic.adapter.ListaMateriaAdapter;
import br.com.uesc.colcic.auxiliares.DialogEmentaMateria;
import br.com.uesc.colcic.domain.Comandos;
import br.com.uesc.colcic.domain.Tipo;

public class listaMateriaProfessorFragment extends BaseFragment {
    private RecyclerView rv_informacoesProfessorMateria;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_lista_materia_professor, container, false);
        rv_informacoesProfessorMateria = (RecyclerView) view.findViewById(R.id.rv_informacoes);
        rv_informacoesProfessorMateria.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv_informacoesProfessorMateria.setItemAnimator(new DefaultItemAnimator());
        rv_informacoesProfessorMateria.setHasFixedSize(true);

        new Thread(new Runnable() {
            public void run() {

                String nome = getArguments().getString("tipo");
                task(nome);

            }

        }).start();

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    public void task(String nomeProfessor) {
        ArrayList<Tipo> listTipo = obtemMateriasMinistadasSemestre(nomeProfessor);

        rv_informacoesProfessorMateria.setAdapter(new ListaMateriaAdapter(listTipo, onClickMat()));
    }

    //metodo usado para obter as materia que o professor está ministrando no semestre
    public ArrayList<Tipo> obtemMateriasMinistadasSemestre(String nomeProfessor) {
        ArrayList<String> listNome = Comandos.returnListDisciplinasMinistradasSem(nomeProfessor);
        return returnListTips(listNome);
    }

    //metodo usado para retornar uma lista de tipos
    public ArrayList<Tipo> returnListTips(ArrayList<String> listaNomeMaterias) {

        ArrayList<Tipo> listaTipos = new ArrayList<>();
        TypedArray imgs = getResources().obtainTypedArray(R.array.img_materia);
        for (int i = 0; i < listaNomeMaterias.size(); i++) {
            listaTipos.add(new Tipo(listaNomeMaterias.get(i).toString(), imgs.getDrawable(0)));
        }
        return listaTipos;
    }

    // metodo usado no adapter da lista de materias
    private ListaMateriaAdapter.MatOnClickListener onClickMat() {
        return new ListaMateriaAdapter.MatOnClickListener() {
            @Override
            public void onClickMat(View view, String nome) {

                Intent intent = new Intent(getContext(), DialogEmentaMateria.class);
                intent.putExtra("tipo", nome);
                startActivityForResult(intent, 1);

            }

        };
    }

}
