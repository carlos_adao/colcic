package br.com.uesc.colcic.modelo;

/**
 * Created by carlos on 31/05/2017.
 */

public class InformacoesMateria {
    String nomeMat;
    int qtdCod;
    String localizacao;
    String sala;
    String prof;
    String emailProf;

    public InformacoesMateria(String nomeMat, int qtdCod, String localizacao, String sala, String prof, String emailProf) {
        this.nomeMat = nomeMat;
        this.qtdCod = qtdCod;
        this.localizacao = localizacao;
        this.sala = sala;
        this.prof = prof;
        this.emailProf = emailProf;
    }

    @Override
    public String toString() {
        return "InformacoesMateria{" +
                "nomeMat='" + nomeMat + '\'' +
                ", qtdCod=" + qtdCod +
                ", localizacao='" + localizacao + '\'' +
                ", sala='" + sala + '\'' +
                ", prof='" + prof + '\'' +
                ", emailProf='" + emailProf + '\'' +
                '}';
    }

    public InformacoesMateria() {

    }

    public String getNomeMat() {
        return nomeMat;
    }

    public void setNomeMat(String nomeMat) {
        this.nomeMat = nomeMat;
    }

    public int getQtdCod() {
        return qtdCod;
    }

    public void setQtdCod(int qtdCod) {
        this.qtdCod = qtdCod;
    }

    public String getLocalizacao() {
        return localizacao;
    }

    public void setLocalizacao(String localizacao) {
        this.localizacao = localizacao;
    }

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }

    public String getProf() {
        return prof;
    }

    public void setProf(String prof) {
        this.prof = prof;
    }

    public String getEmailProf() {
        return emailProf;
    }

    public void setEmailProf(String emailProf) {
        this.emailProf = emailProf;
    }
}
