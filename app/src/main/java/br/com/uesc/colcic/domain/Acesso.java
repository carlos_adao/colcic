package br.com.uesc.colcic.domain;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class Acesso extends Comandos {

    private static final String TAG = "sql";
    public static final String NOME_BANCO = "COLCIC.sqlite";
    private static final int VERSAO_BANCO = 1;
    private SQLiteBanco bdHelper;


    public Acesso(Context context) {

        bdHelper = new SQLiteBanco(context, NOME_BANCO, VERSAO_BANCO, Comandos.createTable());
        //Abre o banco no modo leitura para poder alterar também
        bd = bdHelper.getWritableDatabase();

    }

    public void criaBD(Context context){
        bdHelper = new SQLiteBanco(context, NOME_BANCO, VERSAO_BANCO, Comandos.createTable());
        //Abre o banco no modo leitura para poder alterar também
        bd = bdHelper.getWritableDatabase();

    }
    public SQLiteDatabase getDB(){
        return bd;
    }

    public void fecha() {
        super.fechar();
        if (bdHelper != null) {
            bdHelper.close();
        }
    }

}
