package br.com.uesc.colcic.fragment;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.uesc.colcic.R;
import br.com.uesc.colcic.adapter.TabsAdapterMeuHorario;

/**
 * Created by Convidado on 04/05/2017.
 */

public class MeuHorarioTabFragment extends BaseFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_fragment_meu_horario, container, false);
        // ViewPager
        ViewPager viewPager = (ViewPager) view.findViewById(R.id.vp_meu_horario);
        viewPager.setOffscreenPageLimit(5);

        viewPager.setAdapter(new TabsAdapterMeuHorario(getContext(), getChildFragmentManager()));
        // Tabs

        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tl_meu_horario);
        // Cria as tabs com o mesmo adapter utilizado pelo ViewPager
        tabLayout.setupWithViewPager(viewPager);
        int cor = ContextCompat.getColor(getContext(),R.color.white);
        // Cor branca no texto (o fundo azul foi definido no layout)
        tabLayout.setTabTextColors(cor, cor);
        return view;
    }
}
