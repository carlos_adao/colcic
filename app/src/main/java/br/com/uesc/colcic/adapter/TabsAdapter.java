package br.com.uesc.colcic.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import br.com.uesc.colcic.R;
import br.com.uesc.colcic.fragment.ColcicEstruturaFragment;
import br.com.uesc.colcic.fragment.ColcicRecursosFragment;
import br.com.uesc.colcic.fragment.ColcicSobreFragment;

public class TabsAdapter extends FragmentPagerAdapter {
    private Context context;
    ColcicRecursosFragment crf;

    public TabsAdapter(Context context, FragmentManager fm) {
        super(fm);
        this.context = context;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) {
            return context.getString(R.string.tb1);
        } else if (position == 1) {
            return context.getString(R.string.tb2);
        }
        return context.getString(R.string.tb3);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment f = null;

        if (position == 0) {
            f = ColcicSobreFragment.newInstance("classicos");
        } else if (position == 1) {
            f = ColcicRecursosFragment.newInstance("classicos");
            this.crf = (ColcicRecursosFragment) f;
        } else {
            f = ColcicEstruturaFragment.newInstance("classicos");
        }
        return f;
    }

    public ColcicRecursosFragment getCRF() {
        return crf;
    }
}