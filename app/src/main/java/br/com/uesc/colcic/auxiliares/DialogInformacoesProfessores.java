package br.com.uesc.colcic.auxiliares;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import br.com.uesc.colcic.R;
import br.com.uesc.colcic.domain.Comandos;
import br.com.uesc.colcic.domain.Objetos.InformacoesProfessor;
import br.com.uesc.colcic.fragment.listaMateriaProfessorFragment;

public class DialogInformacoesProfessores extends FragmentActivity {
    TextView tv_nomeProfessor, tv_emailProfessor, tv_phoneProfessor, tv_disciplinas_ministradas;
    private InformacoesProfessor informacoesProfessor;
    private String nome;
    private SpannableString content;

    Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog_informacoes_professores);
        context = getBaseContext();
        nome = getIntent().getStringExtra("tipo");
        informacoesProfessor = returnInformacoesProfessor(nome);


        tv_nomeProfessor = (TextView) findViewById(R.id.tv_nomeProfessor);
        tv_emailProfessor = (TextView) findViewById(R.id.tv_emailProfessor);
        tv_phoneProfessor = (TextView) findViewById(R.id.tv_phoneProfessor);
        tv_disciplinas_ministradas = (TextView) findViewById(R.id.tv_disciplinas_ministradas);

        String sem = Comandos.returnSemestreVirgente().getPeriodo();//legenda do semestre
        String leg = "Disciplinas ministradas ";
        if(sem != null){
            tv_disciplinas_ministradas.setText(leg+sem);
        }else{
            tv_disciplinas_ministradas.setText(leg);
        }

        content = new SpannableString(informacoesProfessor.getNome());
        content.setSpan(new UnderlineSpan(), 0, informacoesProfessor.getNome().length(), 0);
        tv_nomeProfessor.setText(content);
        tv_nomeProfessor.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent in = new Intent(Intent.ACTION_VIEW, Uri.parse(informacoesProfessor.getUrlLates()));
                startActivity(in);
            }

        });

        content = new SpannableString(informacoesProfessor.getEmail());
        content.setSpan(new UnderlineSpan(), 0, informacoesProfessor.getEmail().length(), 0);
        tv_emailProfessor.setText(content);
        tv_emailProfessor.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                enviarEmail(informacoesProfessor.getEmail());
            }

        });

        iniciaFragmentoListaMateriaProfessor(savedInstanceState,informacoesProfessor.getNome());

    }

    public InformacoesProfessor returnInformacoesProfessor(final String nome) {
        InformacoesProfessor informacoesProfessor = null;
        informacoesProfessor = Comandos.returnInformacoesProfessor(nome);

        return informacoesProfessor;
    }

    public void enviarEmail(String emailProf) {

        Intent intent = new Intent(Intent.ACTION_SEND);
        String[] strTo = {emailProf};
        intent.putExtra(Intent.EXTRA_EMAIL, strTo);
        intent.setType("message/rfc822");
        intent.setPackage("com.google.android.gm");
        startActivity(intent);
    }

    public void iniciaFragmentoListaMateriaProfessor(Bundle savedInstanceState,String nomeProfessor) {

        listaMateriaProfessorFragment frag = new listaMateriaProfessorFragment();
        Bundle args = new Bundle();
        args.putString("tipo", nomeProfessor);
        frag.setArguments(args);
        getSupportFragmentManager().beginTransaction().add(R.id.fg_informacoesProfessorMateria, frag).commit();

    }


}
