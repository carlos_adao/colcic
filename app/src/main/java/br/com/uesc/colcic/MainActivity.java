package br.com.uesc.colcic;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Locale;
import br.com.uesc.colcic.Settings.SettingsController;
import br.com.uesc.colcic.Settings.SettingsDB;
import br.com.uesc.colcic.activity.BaseActivity;
import br.com.uesc.colcic.domain.Acesso;
import br.com.uesc.colcic.domain.Comandos;
import br.com.uesc.colcic.domain.Objetos.StatusTabelas;
import br.com.uesc.colcic.fragment.AboutDialog;

public class MainActivity extends BaseActivity {

    Acesso a;
    StatusTabelas tabelaSettings;
    String URL = "http://18.188.123.7/colcicapi/settings";
    private LinearLayout ll_capa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUpToolbar();
        setupNavDrawer();
        Comandos.matarBandoDados(getContext());

        /*indica que a tabela de configuração está preenchida*/
        tabelaSettings = new StatusTabelas("settings", true);
        ll_capa = (LinearLayout) findViewById(R.id.ll_colcic_capa);

        ll_capa.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                getSpeechInput();
                return true;
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            AboutDialog.showAbout(getSupportFragmentManager());
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getDataBase() {
        a = new Acesso(getBaseContext());
        a.criaBD(getBaseContext());
    }

    public void verificaSeBancoEstaCriado() {
        verificaTabelasPreenchidas();
    }

    /*Verifica se as tabelas estão preenchidas e preenche caso não estejam [preenche com dados locais]*/
    public void verificaTabelasPreenchidas() {
        ArrayList<StatusTabelas> listStatusTabelas;

        listStatusTabelas = Comandos.verificaTabelasEstaoPreenchidas();

        for (StatusTabelas stb : listStatusTabelas) {


            if (stb.getNomeTabela().equalsIgnoreCase("hora_aula") && stb.isEstaPreenchida() == false) {

                Comandos.insertHoraAula();

            } else if (stb.getNomeTabela().equalsIgnoreCase("dia_semana") && stb.isEstaPreenchida() == false) {

                Comandos.insertDiaSemana();

            } else if (stb.getNomeTabela().equalsIgnoreCase("ementa_disciplina") && stb.isEstaPreenchida() == false) {

                Comandos.insertEmenta();

            } else if (stb.getNomeTabela().equalsIgnoreCase("dia_hora") && stb.isEstaPreenchida() == false) {

                Comandos.insertDiaHora();

            } else if (stb.getNomeTabela().equalsIgnoreCase("settings") && stb.isEstaPreenchida() == false) {

                tabelaSettings = stb;

            } else if (stb.getNomeTabela().equalsIgnoreCase("ramal") && stb.isEstaPreenchida() == false) {

                Comandos.insertRamais();

            }

        }

        verificaTabelasAtualizada();
    }

    /*verifica de as tabelas estão atualizadas de acordo com a tabela de configurações*/
    public void verificaTabelasAtualizada() {

        getDataBase();
        SettingsController settingsController = new SettingsController();
        settingsController.setContext(getBaseContext(), new SettingsDB(getApplicationContext(), a.getDB()));
        settingsController.execute();

    }

    /*Método usado para iniciar o comando de voz*/
    public void getSpeechInput() {

        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, 10);
        } else {
            Toast.makeText(getBaseContext(), "Erro ", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 10:
                if (resultCode == RESULT_OK && data != null) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    //Toast.makeText(getBaseContext(),result.get(0), Toast.LENGTH_SHORT).show();

                    executaComandoVoz(result.get(0));
                }
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        /*Pega a base de dados*/
        getDataBase();

        /*Verifica se todas tabelas necessarias estão criadas*/
        verificaSeBancoEstaCriado();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /*Metodo usado para executar as ações de acordo com o comando de voz*/
    public void executaComandoVoz(String comando) {
        Context context = getBaseContext();
        if (comando.equalsIgnoreCase(context.getResources().getString(R.string.H1))
                || comando.equalsIgnoreCase(context.getResources().getString(R.string.H2))
                || comando.equalsIgnoreCase(context.getResources().getString(R.string.H3))) {
            abrirActivityHistoria();

        } else if (comando.equalsIgnoreCase(context.getResources().getString(R.string.O1))
                || comando.equalsIgnoreCase(context.getResources().getString(R.string.O2))) {
            abrirActivityObjetivo();

        } else if (comando.equalsIgnoreCase(context.getResources().getString(R.string.P1))
                || comando.equalsIgnoreCase(context.getResources().getString(R.string.P2))
                || comando.equalsIgnoreCase(context.getResources().getString(R.string.P3))) {
            abrirActivityPerfilProfissional();

        } else if (comando.equalsIgnoreCase(context.getResources().getString(R.string.C1))
                || comando.equalsIgnoreCase(context.getResources().getString(R.string.C2))) {
            abrirActivityColcic();

        } else if (comando.equalsIgnoreCase(context.getResources().getString(R.string.MH))) {
            abrirActivityMeuHorario();

        } else if (comando.equalsIgnoreCase(context.getResources().getString(R.string.CU))) {
            abrirActivityCalculadoraUesc();

        } else if (comando.equalsIgnoreCase(context.getResources().getString(R.string.HL1))
                || comando.equalsIgnoreCase(context.getResources().getString(R.string.HL2))) {
            abrirActivityHorario();

        } else if (comando.equalsIgnoreCase(context.getResources().getString(R.string.OM1))
                || comando.equalsIgnoreCase(context.getResources().getString(R.string.OM2))
                || comando.equalsIgnoreCase(context.getResources().getString(R.string.OM23))
                || comando.equalsIgnoreCase(context.getResources().getString(R.string.OM24))) {
            abrirActivityOrientacaoMatricula();

        } else if (comando.equalsIgnoreCase(context.getResources().getString(R.string.OR1))
                || comando.equalsIgnoreCase(context.getResources().getString(R.string.OR2))) {
            abrirActivityOrientacaoRequerimento();

        } else if (comando.equalsIgnoreCase(context.getResources().getString(R.string.F1))
                || comando.equalsIgnoreCase(context.getResources().getString(R.string.F2))) {
            abrirActivityFluxograma();

        } else if (comando.equalsIgnoreCase(context.getResources().getString(R.string.LR1))
                || comando.equalsIgnoreCase(context.getResources().getString(R.string.LR2))) {
            abrirActivityListaRamais();

        } else if (comando.equalsIgnoreCase(context.getResources().getString(R.string.PA1))
                || comando.equalsIgnoreCase(context.getResources().getString(R.string.PA2))) {
            abrirActivityPortalAcademico();

        } else if (comando.equalsIgnoreCase(context.getResources().getString(R.string.B1))) {
            abrirActivityBiblioteca();

        } else if (comando.equalsIgnoreCase(context.getResources().getString(R.string.CA1))
                || comando.equalsIgnoreCase(context.getResources().getString(R.string.CA2))) {
            abrirActivityCalendario();

        } else if (comando.equalsIgnoreCase(context.getResources().getString(R.string.R1))) {
            abrirActivityRegulamentos();

        } else if (comando.equalsIgnoreCase(context.getResources().getString(R.string.RC1))) {
            abrirActivityRegCurso();
        } else if (comando.equalsIgnoreCase(context.getResources().getString(R.string.RU1))) {
            abrirActivityRegGeral();

        } else if (comando.equalsIgnoreCase(context.getResources().getString(R.string.HOI1))
                || comando.equalsIgnoreCase(context.getResources().getString(R.string.HOI2))
                || comando.equalsIgnoreCase(context.getResources().getString(R.string.HOI3))
                || comando.equalsIgnoreCase(context.getResources().getString(R.string.HOI4))) {
            abrirActivityHorarioOnibusIlheus();

        } else if (comando.equalsIgnoreCase(context.getResources().getString(R.string.HOIT1))
                || comando.equalsIgnoreCase(context.getResources().getString(R.string.HOIT2))
                || comando.equalsIgnoreCase(context.getResources().getString(R.string.HOIT3))
                || comando.equalsIgnoreCase(context.getResources().getString(R.string.HOIT4))) {
            abrirActivityHorarioOnibusItabuna();
        } else {
            Toast.makeText(getApplicationContext(), "Não entendi >>" + comando, Toast.LENGTH_SHORT).show();
        }
    }

}
