package br.com.uesc.colcic.adapter;

import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import br.com.uesc.colcic.ConexaoInternet.Objetos.Disciplina;
import br.com.uesc.colcic.ConexaoInternet.OperacoesSQL.AtualizaCreditosDisciplina;
import br.com.uesc.colcic.R;
import br.com.uesc.colcic.auxiliares.DecimalDigitsInputFilter;
import br.com.uesc.colcic.auxiliares.MyDigitsKeyListener;
import br.com.uesc.colcic.domain.Comandos;
import br.com.uesc.colcic.modelo.Credito;

public class ListaCreditoDisciplinaAdapter extends RecyclerView.Adapter<ListaCreditoDisciplinaAdapter.ViewHolder> {

    private List<Credito> listaCreditosDisciplina;
    private Disciplina disciplina;
    private ImageViewClick iconsClick;
    private Credito credito;
    String hint;
    int numCreditos;
    float notaCredito;


    public ListaCreditoDisciplinaAdapter(Disciplina disciplina, ArrayList<Credito> listaCreditosDisciplina, ImageViewClick iconsClick) {
        this.listaCreditosDisciplina = listaCreditosDisciplina;
        this.disciplina = disciplina;
        this.iconsClick = iconsClick;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_linha_materia_credito_uesc, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.ll_msg.setVisibility(View.GONE);
        holder.etCredito.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(2, 2)});
        holder.etCredito.setKeyListener(new MyDigitsKeyListener(false, true));
        holder.etCredito.addTextChangedListener(new TextWatcher() {
            boolean isUpdating;

            public void afterTextChanged(Editable s) {

            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {

            }

            @Override
            public void onTextChanged(
                    CharSequence s, int start, int before, int after) {

                if (isUpdating) {
                    isUpdating = false;
                    return;
                }
                boolean hasMask = s.toString().indexOf('.') > -1 || s.toString().indexOf('-') > -1 || s.toString().indexOf(',') > -1;

                // Remove o '.' e '-' da String
                String str = s.toString().replaceAll("[.]", "").replaceAll("[-]", "").replaceAll("[,]", "");

                if (after > before) {

                    if (str.equals("100")) {//verifica se a nota digita é dez
                        str = str.substring(0, 2) + '.' + str.substring(2);
                        isUpdating = true;
                        holder.etCredito.setText(str);
                        holder.etCredito.setSelection(Math.max(0, Math.min(hasMask ? start - (before) : start, str.length())));
                    } else if (str.length() > 0) {

                        str = str.substring(0, 1) + '.' + str.substring(1);
                    }
                    // Seta a flag pra evitar chamada infinita
                    isUpdating = true;
                    // seta o novo texto
                    holder.etCredito.setText(str);
                    // seta a posição do cursor
                    holder.etCredito.setSelection(holder.etCredito.getText().length());

                } else {
                    if (str.length() == 2) {
                        str = str.substring(0, 1) + '.' + str.substring(1);
                        isUpdating = true;
                        holder.etCredito.setText(str);
                        holder.etCredito.setSelection(Math.max(0, Math.min(hasMask ? start - (before - 1) : start, str.length())));
                    } else {
                        isUpdating = true;
                        holder.etCredito.setText(str);

                        holder.etCredito.setSelection(Math.max(0, Math.min(hasMask ? start - (before) : start, str.length())));
                    }
                }
            }
        });


        holder.etCredito.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {

                }
            }
        });


        String nota = returnNota(listaCreditosDisciplina, position);
        if (nota != null) {

            holder.etCredito.setText(nota);
            holder.ll_quanto_falta.setVisibility(View.GONE);

        } else {
            hint = (String) holder.etCredito.getHint();
            holder.etCredito.setHint((position + 1) + " º Crédito");

            if (returnQantoPrescisa() > 10.0) {
                holder.ll_msg.setVisibility(View.VISIBLE);
                holder.ll_quanto_falta.setVisibility(View.GONE);

            } else {
                holder.ll_msg.setVisibility(View.GONE);
                holder.ll_quanto_falta.setVisibility(View.VISIBLE);
            }
            holder.tv_quanto_falta.setText(String.valueOf(returnQantoPrescisa()));

        }
        holder.ivSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isEmpty(holder.etCredito)) {

                    notaCredito = Float.parseFloat(holder.etCredito.getText().toString());
                    Credito credito = returnCredito(disciplina.getNome(), notaCredito, position);
                    iconsClick.onclick(holder.itemView, credito, 0, holder.ivSalvar);
                    holder.etCredito.setCursorVisible(false);
                }
            }
        });
        holder.ivEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isEmpty(holder.etCredito)){

                    notaCredito = Float.parseFloat(holder.etCredito.getText().toString());
                    Credito credito = returnCredito(disciplina.getNome(), notaCredito, position);

                    iconsClick.onclick(holder.itemView, credito, 1, holder.ivSalvar);
                }
            }
        });
        holder.etCredito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.etCredito.setCursorVisible(true);
            }
        });


    }

    @Override
    public int getItemCount() {
        return Integer.parseInt(disciplina.getCHS());
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivEditar, ivSalvar;
        private EditText etCredito;
        private LinearLayout ll_quanto_falta, ll_msg;
        TextView tv_quanto_falta;

        public ViewHolder(View v) {
            super(v);
            ivEditar = (ImageView) v.findViewById(R.id.ivEditar);
            ivSalvar = (ImageView) v.findViewById(R.id.ivSalvar);
            etCredito = (EditText) v.findViewById(R.id.etCredito);
            tv_quanto_falta = (TextView) v.findViewById(R.id.tv_quanto_precisa);
            ll_quanto_falta = (LinearLayout) v.findViewById(R.id.ll_quanto_falta);
            ll_msg = (LinearLayout) v.findViewById(R.id.ll_msg);
        }

    }

    public interface ImageViewClick {
        public void onclick(View view, Credito credito, int comando, ImageView iv_salvar);
    }


    /*Médoto para criar e retornar um credito*/
    private Credito returnCredito(String nomeDisciplina, float notaCredito, int numCredito) {
        Credito credito = new Credito();
        credito.setNomeMateria(nomeDisciplina);
        credito.setNumCredito(numCredito);
        credito.setNotaCredito(notaCredito);
        return credito;
    }

    /*retorna nota para colocar no edText*/
    private String returnNota(List<Credito> listaCreditosDisciplina, int position) {

        for (Credito crt : listaCreditosDisciplina) {

            if (crt.getNumCredito() == position) {
                return String.valueOf(crt.getNotaCredito());
            }

        }

        return null;
    }

    private Float returnQantoPrescisa() {

        float mininaAprovacao = (float) ((Integer.parseInt(disciplina.getCHS())) * 7.0);
        float somaParcial = AtualizaCreditosDisciplina.returnSomaCreditos(disciplina.getNome(), Comandos.bd);
        int qtdParcial = AtualizaCreditosDisciplina.returnQuantidadeCreditos(disciplina.getNome(), Comandos.bd);
        int qtdTot = Integer.parseInt(disciplina.getCHS());
        float quantoFalta = (mininaAprovacao - somaParcial);

        float result = quantoFalta / (qtdTot - qtdParcial);
        NumberFormat format = NumberFormat.getInstance();
        format.setMaximumFractionDigits(2);
        format.setMinimumFractionDigits(2);
        format.setMaximumIntegerDigits(2);
        format.setRoundingMode(RoundingMode.HALF_UP);
        result = Float.valueOf(format.format(result).replace(",", "."));


        return result;
    }

    private boolean isEmpty(EditText myeditText) {
        return myeditText.getText().toString().trim().length() == 0;
    }


}
