package br.com.uesc.colcic.ConexaoInternet;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import br.com.uesc.colcic.ConexaoInternet.Objetos.Disciplina;
import br.com.uesc.colcic.ConexaoInternet.Objetos.DisciplinaDiaHora;
import br.com.uesc.colcic.ConexaoInternet.Objetos.Func;
import br.com.uesc.colcic.ConexaoInternet.Objetos.HorarioOnibus;
import br.com.uesc.colcic.ConexaoInternet.Objetos.Professor;
import br.com.uesc.colcic.ConexaoInternet.Objetos.Sala;
import br.com.uesc.colcic.ConexaoInternet.Objetos.SemestreVirgente;
import br.com.uesc.colcic.domain.Comandos;
import br.com.uesc.colcic.modelo.Ementa;

/**
 * Created by estagio-nit on 22/09/17.
 */

public class AtualizaTabelas extends AsyncTask<Void, Void, JSONObject> {
    int tipo;
    String ip_serve = "http://18.188.123.7/";

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected JSONObject doInBackground(Void... strings) {

        JSONObject j = null;
        String URL;
        try {

            switch (tipo) {
                case 1:

                    URL = ip_serve+"colcicapi/funcionarios";

                    return (ConexaoHttp.carregarArquivoJson(1,URL));

                case 2:
                    //passa a url para professor
                   // ConexaoHttp.setURL("https://pastebin.com/raw/ndZQUkC1");
                    URL = ip_serve+"colcicapi/professores";
                    return (ConexaoHttp.carregarArquivoJson(2, URL));

                case 3:
                    //passa a url para salas
                    URL = ip_serve+"colcicapi/salas";

                    return (ConexaoHttp.carregarArquivoJson(3, URL));

                case 4:

                    //passa a url para semestre virgente
                    URL = ip_serve+"colcicapi/semestre_virgente";
                    return (ConexaoHttp.carregarArquivoJson(4, URL));

                case 5:
                    //passa a url para disciplina
                    URL = ip_serve+"colcicapi/disciplina";
                    return (ConexaoHttp.carregarArquivoJson(5, URL));

                case 6:
                    //passa a url para ementa
                    //ConexaoHttp.setURL("");
                    break;
                case 7:
                    //passa a url para pre requisitos
                    //ConexaoHttp.setURL("");
                    break;
                case 8:
                    URL = ip_serve+"colcicapi/disciplina_dia_hora";
                    return (ConexaoHttp.carregarArquivoJson(8, URL));
                case 9:
                    /*Atualizar o horario dos onibus*/
                    URL = ip_serve+"colcicapi/horario_onibus";
                    return (ConexaoHttp.carregarArquivoJson(9,URL));


                default:
                    break;
            }




        } catch (Exception e) {
            Log.e("Erro Download", e.getMessage(), e);
        }

        return j;

    }

    protected void onPostExecute(JSONObject arquivoJson) {

        String data = String.valueOf(arquivoJson);

        if (arquivoJson != null) {
            try {

                switch (tipo) {
                    case 1:

                        ArrayList<Func> lFunc =  jsonForFuncionario(data);
                        Comandos.atualizaTabelaFuncionario(lFunc);
                        break;

                    case 2:

                        ArrayList<Professor> lProf =  jsonForProfessor(data);
                        Comandos.atualizaTabelaProfessores(lProf);
                        break;

                    case 3:

                        ArrayList<Sala> lSalasSw = jsonForSala(data);
                        Comandos.atualizaTabelaSalas(lSalasSw);
                        break;

                    case 4:

                        SemestreVirgente svsw = jsonForSemestreVirgente(data);
                        Comandos.atualizaTabelaSemestreVirgente(svsw);
                        break;

                    case 5:

                        ArrayList<Disciplina>lDisciplina = jsonForDisciplina(data);
                        Comandos.atualizaTabelaDisciplina(lDisciplina);
                        break;

                    case 6:

                        break;
                    case 7:

                        break;
                    case 8:

                        ArrayList<DisciplinaDiaHora>lDisDiaH = jsonForDisciplinaDiaHora(data);
                        Comandos.atualizaTabelaDisciplinaDiaHora(lDisDiaH);
                        break;

                    case 9:

                        ArrayList<HorarioOnibus>lHorOnibus = jsonForHorarioOnibus(data);
                        Comandos.atualizaTabelaHorarioOnibus(lHorOnibus);
                        break;

                    default:
                        break;
                }


            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }


    private ArrayList<Func> jsonForFuncionario(String json) throws IOException {
        ArrayList<Func> listFuncionario = new ArrayList<>();
        Func func = null;
        try {

            JSONObject object = new JSONObject(json);
            JSONArray jsonFuncionario  = object.getJSONArray("Funcionario");

            for (int i = 0; i < jsonFuncionario.length(); i++) {
                JSONObject jsonset = jsonFuncionario.getJSONObject(i);

                // Lê as informações de settings
                func = new Func();
                func.setCodigo(jsonset.optString("codigo"));
                func.setNome(jsonset.optString("nome"));
                func.setEmail(jsonset.optString("email"));
                func.setTel(jsonset.optString("tel"));
                func.setCargo(jsonset.optString("cargo"));
                func.setHierarquia(jsonset.optString("hierarquia"));
                func.setSexo(jsonset.optString("sexo"));

                listFuncionario.add(func);
            }

        } catch (JSONException e) {
            throw new IOException(e.getMessage(), e);
        }
        return listFuncionario;
    }

    private ArrayList<Professor> jsonForProfessor(String json) throws IOException {

        ArrayList<Professor> listprofessor = new ArrayList<>();
        Professor professor = null;
        try {

            JSONObject object = new JSONObject(json);
            JSONArray jsonProfessores  = object.getJSONArray("Professor");


            // Insere os professores na lista
            for (int i = 0; i < jsonProfessores.length(); i++) {
                JSONObject jsonset = jsonProfessores.getJSONObject(i);

                // Lê as informações dos professores
                professor = new Professor();
                professor.setCodigo(jsonset.optString("codigo"));
                professor.setNome(jsonset.optString("nome"));
                professor.setEmail(jsonset.optString("email"));
                professor.setTel(jsonset.optString("tel"));
                professor.setTitulacao(jsonset.optString("titulacao"));
                professor.setClasse(jsonset.optString("classe"));
                professor.setUrl_lattes(jsonset.optString("url_lates"));

                listprofessor.add(professor);
            }

        } catch (JSONException e) {
            throw new IOException(e.getMessage(), e);
        }

        return listprofessor;
    }

    private ArrayList<Disciplina> jsonForDisciplina(String json) throws IOException {

        ArrayList<Disciplina> listdisciplina = new ArrayList<>();
        Disciplina disciplina = null;

        try {

            JSONObject object = new JSONObject(json);
            JSONArray jsonDisciplina  = object.getJSONArray("Disciplina");

            for (int i = 0; i < jsonDisciplina.length(); i++) {
                JSONObject jsonset = jsonDisciplina.getJSONObject(i);

                disciplina = new Disciplina();
                disciplina.setCodigo(jsonset.optString("codigo"));
                disciplina.setNome(jsonset.optString("nome"));
                disciplina.setAbreviacao(jsonset.optString("abreviacao"));
                disciplina.setCarga_horaria(jsonset.optString("carga_horaria"));
                disciplina.setFk_semestre_virgente(jsonset.optString("fk_semestre_virgente"));
                disciplina.setSemestre_diciplina(jsonset.optString("semestre_diciplina"));
                disciplina.setFk_curso(jsonset.optString("fk_curso"));
                disciplina.setFk_professor(jsonset.optString("fk_professor"));
                disciplina.setTurma(jsonset.optString("turma"));
                disciplina.setCHS(jsonset.optString("CHS"));

                listdisciplina.add(disciplina);
            }

        } catch (JSONException e) {
            throw new IOException(e.getMessage(), e);
        }
        return listdisciplina;
    }

    private ArrayList<DisciplinaDiaHora> jsonForDisciplinaDiaHora(String json) throws IOException {


        ArrayList<DisciplinaDiaHora> listdisciplina_dia_hora = new ArrayList<>();
        DisciplinaDiaHora disciplinaDiaHora = null;


        try {

            JSONObject object = new JSONObject(json);
            JSONArray jsonDisDiaHora  = object.getJSONArray("Disciplina_dia_hora");

            for (int i = 0; i < jsonDisDiaHora.length(); i++) {
                JSONObject jsonset = jsonDisDiaHora.getJSONObject(i);

                disciplinaDiaHora = new DisciplinaDiaHora();
                disciplinaDiaHora.setId(Integer.parseInt(jsonset.optString("id")));
                disciplinaDiaHora.setFk_codigo(jsonset.optString("fk_codigo"));
                disciplinaDiaHora.setFk_turma(jsonset.optString("fk_turma"));
                disciplinaDiaHora.setFk_dia_hora(jsonset.optString("fk_dia_hora"));
                disciplinaDiaHora.setFk_sala(jsonset.optString("fk_sala"));


                listdisciplina_dia_hora.add(disciplinaDiaHora);
            }

        } catch (JSONException e) {
            throw new IOException(e.getMessage(), e);
        }

        return listdisciplina_dia_hora;
    }

    private ArrayList<Sala> jsonForSala(String json) throws IOException {
        ArrayList<Sala> listSala = new ArrayList<>();

        Sala sala = null;

        try {

            JSONObject object = new JSONObject(json);
            JSONArray jsonSala  = object.getJSONArray("Sala");

            for (int i = 0; i < jsonSala.length(); i++) {
                JSONObject jsonset = jsonSala.getJSONObject(i);

                sala = new Sala();
                sala.setCodigo(jsonset.optString("codigo"));
                sala.setCaracteristica(jsonset.optString("caracteristica"));
                sala.setLocalizacao(jsonset.optString("localizacao"));
                listSala.add(sala);
            }

        } catch (JSONException e) {
            throw new IOException(e.getMessage(), e);
        }
        return listSala;
    }

    private ArrayList<Ementa> jsonForEmenta(String json) throws IOException {
        ArrayList<Ementa> listEmenta = new ArrayList<>();
        Ementa ementa = null;
        try {
            JSONObject root = new JSONObject(json);
            JSONObject obj = root.getJSONObject("Settings");
            JSONArray jsonSettings = obj.getJSONArray("setings");

            for (int i = 0; i < jsonSettings.length(); i++) {
                JSONObject jsonset = jsonSettings.getJSONObject(i);

                // Lê as informações de settings
                ementa = new Ementa();
                ementa.setCod(jsonset.optString("cod"));
                ementa.setAvalicao(jsonset.optString("cod"));
                ementa.setCargaHoraria(jsonset.optString("cod"));
                ementa.setContProg(jsonset.optString("cod"));
                ementa.setEmenta(jsonset.optString("cod"));
                ementa.setMetodologia(jsonset.optString("cod"));
                ementa.setNomeDisc(jsonset.optString("cod"));
                ementa.setObjetivo(jsonset.optString("cod"));
                ementa.setPreReq(jsonset.optString("cod"));
                ementa.setQtdCreitos(jsonset.optString("cod"));
                ementa.setReferencia(jsonset.optString("cod"));


                listEmenta.add(ementa);
            }

        } catch (JSONException e) {
            throw new IOException(e.getMessage(), e);
        }
        return listEmenta;
    }

    private ArrayList<HorarioOnibus> jsonForHorarioOnibus(String json) throws IOException {
        ArrayList<HorarioOnibus> listHorarioOnibus = new ArrayList<>();
        HorarioOnibus ho = null;
        try {

            JSONObject object = new JSONObject(json);
            JSONArray jsonHorarioOnibus  = object.getJSONArray("Horario_onibus");


            for (int i = 0; i < jsonHorarioOnibus.length(); i++) {
                JSONObject jsonset = jsonHorarioOnibus.getJSONObject(i);

                // Lê as informações da lista de horarios dos onibus
                ho = new HorarioOnibus();
                ho.setCod(jsonset.optString("cod"));
                ho.setNum_linha(jsonset.optString("num_linha"));
                ho.setVia(jsonset.optString("via"));
                ho.setLoc_saida(jsonset.optString("loc_saida"));
                ho.setLoc_chegada(jsonset.optString("loc_chegada"));
                ho.setCidade(jsonset.optString("cidade"));
                ho.setEmpresa(jsonset.optString("empresa"));
                ho.setHora_saida(jsonset.optString("hora_saida"));
                ho.setStatus(jsonset.optString("status"));

                listHorarioOnibus.add(ho);
            }

        } catch (JSONException e) {
            throw new IOException(e.getMessage(), e);
        }

        return listHorarioOnibus;
    }

    private SemestreVirgente jsonForSemestreVirgente(String json) throws IOException {

        SemestreVirgente svsw = null;

        try {

            JSONObject object = new JSONObject(json);
            JSONArray jsonSemestreVirgente  = object.getJSONArray("Semestre Virgente");

            for (int i = 0; i < jsonSemestreVirgente.length(); i++) {
                JSONObject jsonset = jsonSemestreVirgente.getJSONObject(i);

                svsw = new SemestreVirgente();

                svsw.setCodigo(jsonset.optString("codigo"));
                svsw.setAno(jsonset.optString("ano"));
                svsw.setPeriodo(jsonset.optString("periodo"));


            }

        } catch (JSONException e) {
            throw new IOException(e.getMessage(), e);
        }
        return svsw;
    }



}
