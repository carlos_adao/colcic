package br.com.uesc.colcic.domain;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import br.com.uesc.colcic.R;

public class AuxiliarComando extends AppCompatActivity {
    Button create, drop, insert, delete;
    Acesso a;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auxiliar_comando);

        create = (Button) findViewById(R.id.create);
        drop = (Button) findViewById(R.id.drop);
        insert = (Button) findViewById(R.id.inset);
        delete = (Button) findViewById(R.id.delete);

        create.setOnClickListener(click_create);
        drop.setOnClickListener(click_drop);
        insert.setOnClickListener(click_insert);
        delete.setOnClickListener(click_delete);

        a = new Acesso(getBaseContext());
        a.criaBD(getBaseContext());
    }

    // create
    private View.OnClickListener click_create = new View.OnClickListener() {
        public void onClick(View v) {


            Comandos.createTable();
        }
    };
    private View.OnClickListener click_drop = new View.OnClickListener() {
        public void onClick(View v) {
            Comandos.dropTable();
        }
    };
    private View.OnClickListener click_insert = new View.OnClickListener() {
        public void onClick(View v) {
         //   Comandos.insertAllData();
        }
    };
    private View.OnClickListener click_delete = new View.OnClickListener() {
        public void onClick(View v) {

        }
    };

}
