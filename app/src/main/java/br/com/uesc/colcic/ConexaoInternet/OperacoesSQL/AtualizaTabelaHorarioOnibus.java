package br.com.uesc.colcic.ConexaoInternet.OperacoesSQL;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import br.com.uesc.colcic.ConexaoInternet.Objetos.HorarioOnibus;
import br.com.uesc.colcic.ConexaoInternet.Objetos.Professor;
import br.com.uesc.colcic.domain.Comandos;

/**
 * Created by estagio-nit on 23/10/17.
 */

public class AtualizaTabelaHorarioOnibus {

    public static void run(ArrayList<HorarioOnibus> lHoSw , SQLiteDatabase bd) {
        ArrayList<HorarioOnibus> lHoDB = new ArrayList<>();
        HorarioOnibus ho;
        boolean insert;//caso a variavel permaneca falsa insere um novo professor coso contrario atualiza
        boolean delete;// caso a varial fique true deleta o professor

        Log.i("Atualizando","Tabela Horario Onibus");

        final String query = "SELECT * FROM horario_onibus";

        Cursor cursor = bd.rawQuery(query, null);

        while (cursor.moveToNext()) {
            int a = cursor.getColumnIndex("cod");
            int b = cursor.getColumnIndex("num_linha");
            int c = cursor.getColumnIndex("via");
            int d = cursor.getColumnIndex("loc_saida");
            int e = cursor.getColumnIndex("loc_chegada");
            int f = cursor.getColumnIndex("cidade");
            int g = cursor.getColumnIndex("empresa");
            int h = cursor.getColumnIndex("hora_saida");
            int i = cursor.getColumnIndex("status");

            ho = new HorarioOnibus();
            ho.setCod(cursor.getString(a));
            ho.setNum_linha(cursor.getString(b));
            ho.setVia(cursor.getString(c));
            ho.setLoc_saida(cursor.getString(d));
            ho.setLoc_chegada(cursor.getString(e));
            ho.setCidade(cursor.getString(f));
            ho.setEmpresa(cursor.getString(g));
            ho.setHora_saida(cursor.getString(h));
            ho.setStatus(cursor.getString(i));

            lHoDB.add(ho);
        }

        if (lHoDB.size() == 0) {


            /*Caso não tenha dados na tabela insere os dados vindos do servidor*/
            for (HorarioOnibus hosw : lHoSw) {

                insertHorarioOnibus(hosw);

            }

        }

        else {


            if (lHoSw.size() >= lHoDB.size()) {

                for (HorarioOnibus hosw : lHoSw) {
                    insert = true;

                    for (HorarioOnibus hodb : lHoDB) {

                        if (hosw.getCod().equals(hodb.getCod())) {


                                insert = false;
                                if (!(hodb.getNum_linha().equalsIgnoreCase(hosw.getNum_linha()))) {

                                    updateHorarioOnibus(hosw);

                                } if (!(hodb.getVia().equalsIgnoreCase(hosw.getVia()))) {

                                    updateHorarioOnibus(hosw);

                                } if (!(hodb.getLoc_saida().equalsIgnoreCase(hosw.getLoc_saida()))) {

                                    updateHorarioOnibus(hosw);

                                } if (!(hodb.getLoc_chegada().equalsIgnoreCase(hosw.getLoc_chegada()))) {

                                    updateHorarioOnibus(hosw);

                                } if (!(hodb.getCidade().equalsIgnoreCase(hosw.getCidade()))) {

                                    updateHorarioOnibus(hosw);

                                } if (!(hodb.getEmpresa().equalsIgnoreCase(hosw.getEmpresa()))) {

                                    updateHorarioOnibus(hosw);

                                }if (!(hodb.getHora_saida().equalsIgnoreCase(hosw.getHora_saida()))) {

                                    updateHorarioOnibus(hosw);

                                }if (!(hodb.getStatus().equalsIgnoreCase(hosw.getStatus()))) {

                                    updateHorarioOnibus(hosw);
                                }
                        }
                    }


                    if (insert) {

                        insertHorarioOnibus(hosw);
                    }

                }
            } else {
                for (HorarioOnibus hodb : lHoDB) {
                    delete = true;
                    for (HorarioOnibus hosw : lHoSw) {
                        if (hosw.getCod().equals(hodb.getCod())) {

                            delete = false;
                        }
                    }
                    if (delete) {

                        deleteHorarioOnibus(hodb);
                    }

                }
            }
        }

    }


    public static void updateHorarioOnibus(HorarioOnibus ho) {


        String  ud ="UPDATE horario_onibus SET cod = '"+ ho.getCod()+ "'" +
                ", num_linha = '"+ho.getNum_linha()+ "'" +
                ", via = '"+ho.getVia()+ "'" +
                ", loc_saida = '"+ho.getLoc_saida()+ "'" +
                ", loc_chegada = '"+ho.getLoc_chegada()+ "'" +
                ", cidade = '"+ho.getCidade()+ "'" +
                ", empresa = '"+ho.getEmpresa()+ "'" +
                ", hora_saida = '"+ho.getHora_saida()+ "'" +
                ", status = '"+ho.getStatus()+ "'" +
                "  WHERE cod = '"+ho.getCod()+"' ;";

        String[] sql = {ud};
        Comandos.executeSQL(sql);
    }

    public static void insertHorarioOnibus(HorarioOnibus ho) {

        String ins = "INSERT INTO horario_onibus(cod, num_linha, via, loc_saida, loc_chegada, cidade, empresa, hora_saida, status) VALUES ('"+ ho.getCod() +"','"+ ho.getNum_linha() +"','"+ ho.getVia() +"','"+ ho.getLoc_saida() +"','"+ ho.getLoc_chegada() +"','"+ ho.getCidade() +"','"+ ho.getEmpresa() +"','"+ ho.getHora_saida() +"','"+ ho.getStatus() +"')";
        String[] sql = {ins};
        Comandos.executeSQL(sql);

    }
    public static void deleteHorarioOnibus(HorarioOnibus ho) {

        String del = "DELETE FROM horario_onibus  WHERE cod = '" + ho.getCod() + "' ;";
        String[] sql = {del};
        Comandos.executeSQL(sql);
    }

}
