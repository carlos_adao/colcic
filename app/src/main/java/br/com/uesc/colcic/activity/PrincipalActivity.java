package br.com.uesc.colcic.activity;

import android.os.Bundle;

import br.com.uesc.colcic.R;
import br.com.uesc.colcic.fragment.MeuHorarioTabFragment;
import br.com.uesc.colcic.fragment.OrientacaoMatriculaFragment;
import br.com.uesc.colcic.fragment.OrientacaoRequerimentosFragment;

public class PrincipalActivity extends BaseActivity {
    String tipo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        // Configura a Toolbar como a action bar

        setUpToolbar();

        tipo = getIntent().getStringExtra("texto");

       if (tipo.equalsIgnoreCase("Meu horário")) {

            iniciaTabFragmentoMeuHorario(savedInstanceState, tipo);
        } else if (tipo.equalsIgnoreCase("dia")) {

            setTitle("Selecione a data");
        } else if (tipo.equalsIgnoreCase("om")) {

            setTitle("Orientação de Matricula");
            iniciaFragmentoOrientacaoMatricula(savedInstanceState, "Orientação de Matricula");
        } else if (tipo.equalsIgnoreCase("or")) {

            setTitle(getString(R.string.titulo_frag_orien_requerimento));
            iniciaFragmentoOrientacaoRequerimento(savedInstanceState,"Orientação de Requerimento");
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }
    //Iniicia a tab fragment para o colcic
    public void iniciaTabFragmentoMeuHorario(Bundle savedInstanceState, String titulo) {
        // Título da toolbar e botão up navigation
        getSupportActionBar().setTitle(titulo);
        // Adiciona o fragment no layout
        if (savedInstanceState == null) {
            // Cria o fragment com o mesmo Bundle (args) da intent
            MeuHorarioTabFragment frag = new MeuHorarioTabFragment();
            frag.setArguments(getIntent().getExtras());
            // Adiciona o fragment no layout
            getSupportFragmentManager().beginTransaction().add(R.id.alternavelFragment, frag).commit();
        }
    }

    //Iniicia o fragmento com intruções de orientação a matricula
    public void iniciaFragmentoOrientacaoMatricula(Bundle savedInstanceState, String titulo) {
        // Título da toolbar e botão up navigation
        getSupportActionBar().setTitle(titulo);
        // Adiciona o fragment no layout
        if (savedInstanceState == null) {
            // Cria o fragment com o mesmo Bundle (args) da intent
            OrientacaoMatriculaFragment frag = new OrientacaoMatriculaFragment();
            frag.setArguments(getIntent().getExtras());
            // Adiciona o fragment no layout
            getSupportFragmentManager().beginTransaction().add(R.id.alternavelFragment, frag).commit();
        }
    }

    //Inicia o fragmento com intruções de orientação de requerimentos
    public void iniciaFragmentoOrientacaoRequerimento(Bundle savedInstanceState, String titulo) {
        // Título da toolbar e botão up navigation
        getSupportActionBar().setTitle(titulo);
        // Adiciona o fragment no layout
        if (savedInstanceState == null) {
            // Cria o fragment com o mesmo Bundle (args) da intent
            OrientacaoRequerimentosFragment frag = new OrientacaoRequerimentosFragment();
            frag.setArguments(getIntent().getExtras());
            // Adiciona o fragment no layout
            getSupportFragmentManager().beginTransaction().add(R.id.alternavelFragment, frag).commit();
        }
    }
}