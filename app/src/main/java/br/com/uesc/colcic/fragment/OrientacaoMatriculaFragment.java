package br.com.uesc.colcic.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.uesc.colcic.R;
import br.com.uesc.colcic.views.JustifiedTextView;

public class OrientacaoMatriculaFragment extends BaseFragment {
    JustifiedTextView tv_conteudo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_orientacao_matricula, container, false);

        tv_conteudo = (JustifiedTextView)view.findViewById(R.id.tv_conteudo);
        tv_conteudo.setText(getString(R.string.orientacaoMatricula_p1));
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }
}
