package br.com.uesc.colcic.ConexaoInternet.OperacoesSQL;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import br.com.uesc.colcic.ConexaoInternet.Objetos.Disciplina;
import br.com.uesc.colcic.ConexaoInternet.Objetos.Func;
import br.com.uesc.colcic.domain.Comandos;
import br.com.uesc.colcic.modelo.Funcionario;

/**
 * Created by estagio-nit on 26/09/17.
 */

public class AtualizaTabelaFuncionario {

    /*Metodos de atualização da tabela do banco de dados local*/
    //recebe uma lista de disciplinas vindas do banco de dados
    public static void run(ArrayList<Func> lFuncionarioSw, SQLiteDatabase bd) {
        ArrayList<Func> lFuncionarioDB = new ArrayList<>();
        Func funcionario = null;
        boolean insert;//caso a variavel permaneca falsa insere um novo professor coso contrario atualiza
        boolean delete;// caso a varial fique true deleta o professor


        Log.i("Atualizando","Tabela Funcionario");

        final String query = "SELECT * FROM funcionario";


        Cursor cursor = bd.rawQuery(query, null);

        while (cursor.moveToNext()) {

            int a = cursor.getColumnIndex("codigo");
            int b = cursor.getColumnIndex("nome");
            int c = cursor.getColumnIndex("email");
            int d = cursor.getColumnIndex("tel");
            int e = cursor.getColumnIndex("cargo");
            int f = cursor.getColumnIndex("hierarquia");
            int g = cursor.getColumnIndex("sexo");

            funcionario = new Func();

            funcionario.setCodigo(cursor.getString(a));
            funcionario.setNome(cursor.getString(b));
            funcionario.setEmail(cursor.getString(c));
            funcionario.setTel(cursor.getString(d));
            funcionario.setCargo(cursor.getString(e));
            funcionario.setHierarquia(cursor.getString(f));
            funcionario.setSexo(cursor.getString(g));


            lFuncionarioDB.add(funcionario);

        }

        if (lFuncionarioDB.size() == 0) {

            /*Caso não tenha dados na tabela insere os dados vindos do servidor*/
            for (Func fsw : lFuncionarioSw) {

                insertFuncionario(fsw);

            }

        }
        else {


            if (lFuncionarioSw.size() >= lFuncionarioDB.size()) {

                for (Func fsw : lFuncionarioSw) {
                    insert = true;
                    for (Func fdb : lFuncionarioDB) {

                        if ((fsw.getCodigo().equalsIgnoreCase(fdb.getCodigo()))) {

                            insert = false;
                            if (!(fdb.getNome().equalsIgnoreCase(fsw.getNome()))) {


                                updateFuncionario(fsw);

                            } else if (!(fdb.getEmail().equalsIgnoreCase(fsw.getEmail()))) {

                                updateFuncionario(fsw);

                            } else if (!(fdb.getTel().equalsIgnoreCase(fsw.getTel()))) {

                                updateFuncionario(fsw);

                            } else if (!(fdb.getCargo().equalsIgnoreCase(fsw.getCargo()))) {

                                updateFuncionario(fsw);

                            }else if (!(fdb.getHierarquia().equalsIgnoreCase(fsw.getHierarquia()))) {

                                updateFuncionario(fsw);

                            }else if (!(fdb.getSexo().equalsIgnoreCase(fsw.getSexo()))) {

                                updateFuncionario(fsw);
                            }
                        }
                    }
                    if (insert) {

                        insertFuncionario(fsw);

                    }
                }

            } else {


                for (Func fdb : lFuncionarioDB) {

                    delete = true;
                    for (Func fsw : lFuncionarioSw) {

                        if ((fsw.getCodigo().equalsIgnoreCase(fdb.getCodigo()))) {
                            delete = false;
                        }
                    }
                    if (delete) {

                        deleteFuncionario(fdb);
                    }

                }
            }
        }
    }

    public static void  updateFuncionario(Func ddh) {

        String ud = "UPDATE Funcionario SET nome = '" + ddh.getNome() + "'" +
                ", email = '" + ddh.getEmail() + "'" +
                ", tel = '" + ddh.getTel() + "'" +
                ", cargo = '" + ddh.getCargo() + "'" +
                ", hierarquia = '" + ddh.getHierarquia() + "'" +
                ", sexo = '" + ddh.getSexo() + "'" +
                "  WHERE codigo = '" + ddh.getCodigo() + "' ;";

        String[] sql = {ud};
        Comandos.executeSQL(sql);
    }

    public static void insertFuncionario(Func ddh) {

        String ins = "INSERT INTO Funcionario VALUES ('"+ ddh.getCodigo() +"','"+ ddh.getNome() +"','"+ ddh.getEmail() +"','"+ ddh.getTel() +"','"+ ddh.getCargo() +"','"+ ddh.getHierarquia() +"','"+ ddh.getSexo() +"')";
        String[] sql = {ins};
        Comandos.executeSQL(sql);

    }

    public static void deleteFuncionario(Func ddh) {

        String del = "DELETE FROM Funcionario  WHERE codigo = '" + ddh.getCodigo() + "';";
        String[] sql = {del};
        Comandos.executeSQL(sql);
    }
/************************************************************************************************************/

}
