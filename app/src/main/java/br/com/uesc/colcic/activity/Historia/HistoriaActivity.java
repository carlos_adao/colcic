package br.com.uesc.colcic.activity.Historia;

import android.content.Intent;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import br.com.uesc.colcic.R;
import br.com.uesc.colcic.views.JustifiedTextView;

public class HistoriaActivity extends AppCompatActivity {
    TextView tv_historia;
    JustifiedTextView tv_conteudo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historia);

        tv_historia = (TextView) findViewById(R.id.tv_historia);
        tv_historia.setText(getString(R.string.titulo_historia));
        tv_conteudo = (JustifiedTextView) findViewById(R.id.tv_conteudo);
        tv_conteudo.setText(getString(R.string.p1_historia) + getString(R.string.p2_historia) + getString(R.string.p3_historia) + getString(R.string.p4_historia));

        final Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        CollapsingToolbarLayout collapsingToolbar = findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle("História");

        loadBackdrop();
    }

    private void loadBackdrop() {
        final ImageView imageView = findViewById(R.id.backdrop);
        Glide.with(this).load(R.drawable.banner_historia).into(imageView);
    }

}
