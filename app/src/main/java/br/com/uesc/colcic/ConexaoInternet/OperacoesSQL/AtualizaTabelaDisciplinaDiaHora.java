package br.com.uesc.colcic.ConexaoInternet.OperacoesSQL;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import br.com.uesc.colcic.ConexaoInternet.Objetos.Disciplina;
import br.com.uesc.colcic.ConexaoInternet.Objetos.DisciplinaDiaHora;
import br.com.uesc.colcic.ConexaoInternet.Objetos.Professor;
import br.com.uesc.colcic.ConexaoInternet.Objetos.Sala;
import br.com.uesc.colcic.domain.Comandos;

/**
 * Created by estagio-nit on 26/09/17.
 */

public class AtualizaTabelaDisciplinaDiaHora {


    /*Metodos de atualização da tabela do banco de dados local*/
    //recebe uma lista de professores vinda do banco
    public static void run(ArrayList<DisciplinaDiaHora> lDisDiaHSw, SQLiteDatabase bd) {
        ArrayList<DisciplinaDiaHora> lDisDiaHDB = new ArrayList<>();
        DisciplinaDiaHora disDiaHora;
        boolean insert;//caso a variavel permaneca falsa insere um novo professor coso contrario atualiza
        boolean delete;// caso a varial fique true deleta o professor

        Log.i("Atualizando","Tabela Disciplina2 dia hora");

        final String query = "SELECT id, fk_codigo, fk_turma, fk_dia_hora, fk_sala FROM disciplina_dia_hora";

        Cursor cursor = bd.rawQuery(query, null);

        while (cursor.moveToNext()) {

            int e = cursor.getColumnIndex("id");
            int a = cursor.getColumnIndex("fk_codigo");
            int b = cursor.getColumnIndex("fk_turma");
            int c = cursor.getColumnIndex("fk_dia_hora");
            int d = cursor.getColumnIndex("fk_sala");


            disDiaHora = new DisciplinaDiaHora();
            disDiaHora.setId(Integer.parseInt(cursor.getString(e)));
            disDiaHora.setFk_codigo(cursor.getString(a));
            disDiaHora.setFk_turma(cursor.getString(b));
            disDiaHora.setFk_dia_hora(cursor.getString(c));
            disDiaHora.setFk_sala(cursor.getString(d));

            lDisDiaHDB.add(disDiaHora);


        }

        if (lDisDiaHDB.size() == 0) {


            /*Caso não tenha dados na tabela insere os dados vindos do servidor*/
            for (DisciplinaDiaHora dsw : lDisDiaHSw) {

                insertDisciplinaDiaHora(dsw);

            }

        } else {

            if (lDisDiaHSw.size() >= lDisDiaHDB.size()) {

                for (DisciplinaDiaHora ddhsw : lDisDiaHSw) {
                    insert = true;
                    for (DisciplinaDiaHora ddhdb : lDisDiaHDB) {

                        if ((ddhsw.getId() == ddhdb.getId())) {

                            insert = false;
                            if (!(ddhdb.getFk_codigo().equalsIgnoreCase(ddhsw.getFk_codigo()))) {

                                updateDisciplinaDiaHora(ddhsw);

                            } else if (!(ddhdb.getFk_turma().equalsIgnoreCase(ddhsw.getFk_turma()))) {

                                updateDisciplinaDiaHora(ddhsw);

                            } else if (!(ddhdb.getFk_sala().equalsIgnoreCase(ddhsw.getFk_sala()))) {

                                updateDisciplinaDiaHora(ddhsw);

                            } else if (!(ddhdb.getFk_dia_hora().equalsIgnoreCase(ddhsw.getFk_dia_hora()))) {

                                updateDisciplinaDiaHora(ddhsw);

                            }
                        }
                    }
                    if (insert) {

                        insertDisciplinaDiaHora(ddhsw);

                    }
                }

            } else {


                for (DisciplinaDiaHora ddhdb : lDisDiaHDB) {

                    delete = true;
                    for (DisciplinaDiaHora ddhsw : lDisDiaHSw) {

                        if ((ddhsw.getId() == ddhdb.getId())) {
                            delete = false;
                        }
                    }
                    if (delete) {

                        deleteDisciplinaDiaHora(ddhdb);
                    }

                }
            }
        }
    }


    public static void updateDisciplinaDiaHora(DisciplinaDiaHora ddh) {

        String ud = "UPDATE disciplina_dia_hora SET fk_codigo = '" + ddh.getFk_codigo() + "'" +
                ", fk_turma = '" + ddh.getFk_turma() + "'" +
                ", fk_dia_hora = '" + ddh.getFk_dia_hora() + "'" +
                ", fk_sala = '" + ddh.getFk_sala() + "'" +
                "  WHERE id = '" + ddh.getId() + "' ;";

        String[] sql = {ud};
        Comandos.executeSQL(sql);
    }

    public static void insertDisciplinaDiaHora(DisciplinaDiaHora ddh) {

        String ins = "INSERT INTO disciplina_dia_hora VALUES (" + ddh.getId() + ",'" + ddh.getFk_codigo() + "'" +
                ",'" + ddh.getFk_turma() + "','" + ddh.getFk_dia_hora() + "'," +
                "'" + ddh.getFk_sala() + "')";

        String[] sql = {ins};
        Comandos.executeSQL(sql);

    }

    public static void deleteDisciplinaDiaHora(DisciplinaDiaHora ddh) {

        String del = "DELETE FROM disciplina_dia_hora  WHERE id = '" + ddh.getId() + "';";
        String[] sql = {del};
        Comandos.executeSQL(sql);
    }
/************************************************************************************************************/

}
