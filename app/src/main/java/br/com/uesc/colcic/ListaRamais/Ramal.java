package br.com.uesc.colcic.ListaRamais;


public class Ramal {
    int id;
    String sigla;
    String nomeclatura;
    String ramal;

    public Ramal(int id, String sigla, String nomeclatura, String ramal){
        this.id = id;
        this.sigla = sigla;
        this.nomeclatura = nomeclatura;
        this.ramal = ramal;
    }

    public Ramal() {

    }

    public void setId(int id){
        this.id = id;
    }
    public void setSigla(String sigla){
        this.sigla = sigla;
    }
    public void setNomeclatura(String nomeclatura){
        this.nomeclatura = nomeclatura;
    }
    public void setRamal(String ramal){
        this.ramal = ramal;
    }

    public int getId(){
        return  this.id;
    }
    public String getSigla(){
        return this.sigla;
    }
    public String getNomeclatura(){
        return this.nomeclatura;
    }
    public String getRamal(){
        return this.ramal;
    }

}
