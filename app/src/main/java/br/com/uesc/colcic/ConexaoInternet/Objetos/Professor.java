package br.com.uesc.colcic.ConexaoInternet.Objetos;

/**
 * Created by estagio-nit on 21/09/17.
 */

public class Professor {

      String codigo, nome, email, tel, titulacao, classe, url_lattes;

    public Professor(String codigo, String nome, String email, String tel, String titulacao, String classe, String url_lattes) {
        this.codigo = codigo;
        this.nome = nome;
        this.email = email;
        this.tel = tel;
        this.titulacao = titulacao;
        this.classe = classe;
        this.url_lattes = url_lattes;
    }

    public Professor() {

    }


    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getTitulacao() {
        return titulacao;
    }

    public void setTitulacao(String titulacao) {
        this.titulacao = titulacao;
    }

    public String getClasse() {
        return classe;
    }

    public void setClasse(String classe) {
        this.classe = classe;
    }

    public String getUrl_lattes() {
        return url_lattes;
    }

    public void setUrl_lattes(String url_lattes) {
        this.url_lattes = url_lattes;
    }
}
