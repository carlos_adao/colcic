package br.com.uesc.colcic.ConexaoInternet.OperacoesSQL;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import br.com.uesc.colcic.ConexaoInternet.Objetos.Disciplina;
import br.com.uesc.colcic.ConexaoInternet.Objetos.DisciplinaDiaHora;
import br.com.uesc.colcic.domain.Comandos;

/**
 * Created by estagio-nit on 26/09/17.
 */

public class AtualizaTabelaDisciplina {

    /*Metodos de atualização da tabela do banco de dados local*/
    //recebe uma lista de disciplinas vindas do banco de dados
    public static void run(ArrayList<Disciplina> lDisciplinaSw, SQLiteDatabase bd) {
        ArrayList<Disciplina> lDisciplinaDB = new ArrayList<>();
        Disciplina disDiaHora;
        boolean insert;//caso a variavel permaneca falsa insere um novo professor coso contrario atualiza
        boolean delete;// caso a varial fique true deleta o professor


        Log.i("Atualizando","Tabela Disciplina");

        final String query = "SELECT codigo, nome, abreviacao,  carga_horaria, fk_semestre_virgente, semestre_diciplina, fk_curso, fk_professor, turma, CHS FROM disciplina ORDER BY semestre_diciplina";


        Cursor cursor = bd.rawQuery(query, null);

        while (cursor.moveToNext()) {

            int a = cursor.getColumnIndex("codigo");
            int b = cursor.getColumnIndex("nome");
            int ab = cursor.getColumnIndex("abreviacao");
            int c = cursor.getColumnIndex("carga_horaria");
            int d = cursor.getColumnIndex("fk_semestre_virgente");
            int e = cursor.getColumnIndex("semestre_diciplina");
            int f = cursor.getColumnIndex("fk_curso");
            int g = cursor.getColumnIndex("fk_professor");
            int h = cursor.getColumnIndex("turma");
            int i = cursor.getColumnIndex("CHS");

            Disciplina disc = new Disciplina ();
            disc.setCodigo(cursor.getString(a));
            disc.setNome(cursor.getString(b));
            disc.setAbreviacao(cursor.getString(ab));
            disc.setCarga_horaria(cursor.getString(c));
            disc.setFk_semestre_virgente(cursor.getString(d));
            disc.setSemestre_diciplina(cursor.getString(e));
            disc.setFk_curso(cursor.getString(f));
            disc.setFk_professor(cursor.getString(g));
            disc.setTurma(cursor.getString(h));
            disc.setCHS(cursor.getString(i));
            lDisciplinaDB.add(disc);



        }

        if (lDisciplinaDB.size() == 0) {


            /*Caso não tenha dados na tabela insere os dados vindos do servidor*/
            for (Disciplina dsw : lDisciplinaSw) {

                insertDisciplina(dsw);

            }

        }
        else {

            if (lDisciplinaSw.size() >= lDisciplinaDB.size()) {

                for (Disciplina dsw : lDisciplinaSw) {
                    insert = true;
                    for (Disciplina ddb : lDisciplinaDB) {

                        if ((dsw.getCodigo().equalsIgnoreCase(ddb.getCodigo()))  &&  (dsw.getTurma().equalsIgnoreCase(ddb.getTurma())) ) {

                            insert = false;
                            if (!(ddb.getNome().equalsIgnoreCase(dsw.getNome()))) {


                                updateDisciplina(dsw);

                            } else if (!(ddb.getAbreviacao().equalsIgnoreCase(dsw.getAbreviacao()))) {

                                updateDisciplina(dsw);
                            }
                            else if (!(ddb.getTurma().equalsIgnoreCase(dsw.getTurma()))) {

                                updateDisciplina(dsw);

                            } else if (!(ddb.getCarga_horaria().equalsIgnoreCase(dsw.getCarga_horaria()))) {

                                updateDisciplina(dsw);

                            } else if (!(ddb.getFk_semestre_virgente().equalsIgnoreCase(dsw.getFk_semestre_virgente()))) {

                                updateDisciplina(dsw);

                            }else if (!(ddb.getSemestre_diciplina().equalsIgnoreCase(dsw.getSemestre_diciplina()))) {

                                updateDisciplina(dsw);

                            }else if (!(ddb.getFk_curso().equalsIgnoreCase(dsw.getFk_curso()))) {

                                updateDisciplina(dsw);

                            }else if (!(ddb.getFk_professor().equalsIgnoreCase(dsw.getFk_professor()))) {

                                updateDisciplina(dsw);

                            }else if (!(ddb.getCHS().equalsIgnoreCase(dsw.getCHS()))) {

                                updateDisciplina(dsw);

                            }
                        }
                    }
                    if (insert) {

                        insertDisciplina(dsw);

                    }
                }

            } else {


                for (Disciplina ddb : lDisciplinaDB) {

                    delete = true;
                    for (Disciplina dsw : lDisciplinaSw) {

                        if ((dsw.getCodigo().equalsIgnoreCase(ddb.getCodigo()))  &&  (dsw.getTurma().equalsIgnoreCase(ddb.getTurma()))) {
                            delete = false;
                        }
                    }
                    if (delete) {

                        deleteDisciplina(ddb);
                    }

                }
            }
        }
    }

    public static void updateDisciplina(Disciplina ddh) {
        Log.i("Update", ddh.getNome());

        String ud = "UPDATE disciplina SET nome = '" + ddh.getNome() + "'" +
                ", abreviacao = '" + ddh.getAbreviacao() + "'" +
                ", carga_horaria = '" + ddh.getCarga_horaria() + "'" +
                ", fk_semestre_virgente = '" + ddh.getFk_semestre_virgente() + "'" +
                ", semestre_diciplina = '" + ddh.getSemestre_diciplina() + "'" +
                ", fk_curso = '" + ddh.getFk_curso() + "'" +
                ", fk_professor = '" + ddh.getFk_professor() + "'" +
                ", CHS = '" + ddh.getCHS() + "'" +
                "  WHERE nome = '" + ddh.getNome()                                                           + "' ;";

        String[] sql = {ud};
        Comandos.executeSQL(sql);
    }

    public static void insertDisciplina(Disciplina ddh) {
        Log.i("inserindo", ddh.getNome());

        String ins = "INSERT INTO disciplina VALUES ('"+ ddh.getCodigo() +"','"+ ddh.getNome() +"','"+ ddh.getAbreviacao() +"','"+ ddh.getCarga_horaria() +"',"+ ddh.getFk_semestre_virgente() +","+ ddh.getSemestre_diciplina() +",'"+ ddh.getFk_curso() +"','"+ ddh.getFk_professor() +"','"+ ddh.getTurma() +"','"+ ddh.getCHS() +"')";
        String[] sql = {ins};
        Comandos.executeSQL(sql);

    }

    public static void deleteDisciplina(Disciplina ddh) {
        Log.i("Deletando", ddh.getNome());

        String del = "DELETE FROM disciplina  WHERE codigo = '" + ddh.getCodigo() + "';";
        String[] sql = {del};
        Comandos.executeSQL(sql);
    }
/************************************************************************************************************/

}
