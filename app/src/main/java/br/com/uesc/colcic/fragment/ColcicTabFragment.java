package br.com.uesc.colcic.fragment;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.uesc.colcic.R;
import br.com.uesc.colcic.activity.CicActivity;
import br.com.uesc.colcic.activity.MeuHorarioActivity;
import br.com.uesc.colcic.adapter.TabsAdapter;

public class ColcicTabFragment extends BaseFragment {
    ViewPager viewPager;
    TabsAdapter ta;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_fragment_colcic, container, false);
        // ViewPager
        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        viewPager.setOffscreenPageLimit(3);
        ta = new TabsAdapter(getContext(), getChildFragmentManager());
        viewPager.setAdapter(ta);


        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tabLayout);

        tabLayout.setupWithViewPager(viewPager);
        int cor = ContextCompat.getColor(getContext(), R.color.white);

        tabLayout.setTabTextColors(cor, cor);
        return view;
    }

    public void comunicacaoColcicRecursosFragment(String pesq) {

        int i = viewPager.getCurrentItem();

        if (i == 1) {
            ColcicRecursosFragment fragment = (ColcicRecursosFragment) ta.getCRF();
            fragment.atualizaLista(pesq);

        }
    }

}