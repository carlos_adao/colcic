package br.com.uesc.colcic.fragment.MinhasTurmas;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import br.com.uesc.colcic.ConexaoInternet.Objetos.Disciplina;
import br.com.uesc.colcic.ConexaoInternet.OperacoesSQL.AtualizaCreditosDisciplina;
import br.com.uesc.colcic.R;
import br.com.uesc.colcic.activity.Meu_HorarioActivity;
import br.com.uesc.colcic.activity.MinhasTurmasActivity;
import br.com.uesc.colcic.adapter.ListaCreditoDisciplinaAdapter;
import br.com.uesc.colcic.domain.Comandos;
import br.com.uesc.colcic.domain.Tipo;
import br.com.uesc.colcic.modelo.Credito;


public class CreditosMinhaTurmaFragment extends Fragment {
    RecyclerView recyclerView;
    EditText ed_nomeMateria;
    private ArrayList<Credito> listasCreditos;
    private String nomeMateria;
    private TextView tv_media, tv_status, tv_situacao;

    /*Objetos*/
    private Disciplina disciplina;

    /*Listas*/
    private ArrayList<Credito> listaCreditoDisciplina = null;

    public CreditosMinhaTurmaFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_creditos_minha_turma, container, false);

        tv_media = (TextView) view.findViewById(R.id.tv_media);
        tv_status = (TextView) view.findViewById(R.id.tv_status);
        tv_situacao = (TextView) view.findViewById(R.id.tv_situacao);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar_credito_minha_turma);
        toolbar.setTitle(nomeMateria);
        toolbar.inflateMenu(R.menu.menu_credito_minhas_turmas);
        toolbar.setOnMenuItemClickListener(
                new Toolbar.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        int id = item.getItemId();

                        if (id == R.id.cancelar_se) {
                            fechaFragmento();
                        }
                        return true;
                    }
                });


        recyclerView = (RecyclerView) view.findViewById(R.id.rv_creditos_turmas);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);

        atualiza();

        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            // Lê o tipo dos argumentos.
            this.nomeMateria = getArguments().getString("nomeDisciplina");
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String retorno;

        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {

            retorno = data.getStringExtra("tipo");

            ed_nomeMateria.setText(String.valueOf(retorno));


        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }


    //fecha o menu de excluir ou salvar materia
    public void fechaFragmento() {

        ((MinhasTurmasActivity) getActivity()).fechaFragmento();
    }

    // metodo usado para click iv no adapter
    private ListaCreditoDisciplinaAdapter.ImageViewClick iconsClick() {
        return new ListaCreditoDisciplinaAdapter.ImageViewClick() {
            @Override
            public void onclick(View view, Credito credito, int comando, ImageView iv_salvar) {

                if (comando == 0) {

                    if (!AtualizaCreditosDisciplina.verificaNotaEmCredito(credito, Comandos.bd)) {
                        /*insert*/
                        AtualizaCreditosDisciplina.insertCredito(credito);
                        fechaTeclado(iv_salvar);
                        atualiza();
                    } else {

                        /*update*/
                        AtualizaCreditosDisciplina.updateCredito(credito);
                        fechaTeclado(iv_salvar);
                        atualiza();

                    }
                } else if (comando == 1) {
                    /*delete*/
                    AtualizaCreditosDisciplina.deleteCredito(credito);
                    atualiza();

                }
            }

        };
    }

    private void fechaTeclado(ImageView iv_salvar) {
        ((InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(iv_salvar.getWindowToken(), 0);

    }

    /*apos inserção deleção ou update atualiza*/
    public void atualiza() {

        disciplina = Comandos.retornaDisciplina(nomeMateria);
        listasCreditos = AtualizaCreditosDisciplina.selectCreditos(nomeMateria, Comandos.bd);
        recyclerView.setAdapter(new ListaCreditoDisciplinaAdapter(disciplina, listasCreditos, iconsClick()));


        if (AtualizaCreditosDisciplina.temCreditos(nomeMateria, Comandos.bd)) {

            float media = AtualizaCreditosDisciplina.returnMediaCreditos(nomeMateria, Comandos.bd);
            tv_media.setText(String.valueOf(media));

            int situacao = verificaSituacao(media);

            switch (situacao) {
                case 0:
                    tv_status.setText("Aprovado :)");
                    tv_status.setTextColor(R.color.green);
                    tv_situacao.setText("Parabéns");
                    break;

                case 1:
                    tv_status.setText("Reprovado :(");
                    tv_status.setTextColor(R.color.red);
                    tv_situacao.setText("");

                    break;
                case 2:
                    tv_status.setText("Em final");
                    tv_status.setTextColor(R.color.black);
                    float pf = quantoPrecisaFinal(media);
                    String leg =  " precisa de "+ String.valueOf(pf);
                    tv_situacao.setText(leg);
                    break;

                default:
                    break;
            }
        }else{

            tv_media.setText("");
            tv_situacao.setText("");
            tv_status.setText("");

        }
    }


    //Método para verificar situação do aluno
    public int verificaSituacao(float media) {

        NumberFormat formatter = NumberFormat.getInstance(Locale.US);
        formatter.setMaximumFractionDigits(2);
        formatter.setMinimumFractionDigits(2);
        formatter.setRoundingMode(RoundingMode.HALF_UP);
        media = new Float(formatter.format(media));

        float compara = (float) 1.6;
        if (media >= 7) {

            //aprovado
            return 0;

        } else if (media <= compara) {
            //reprovado

            return 1;
        } else if ((media >= 1.7) && (media < 7)) {
            //final
            return 2;

        }
        return 3;
    }

    /*Método que verifica quanto o aluno precisa na final*/
    public float quantoPrecisaFinal(float media) {

        float end;
        NumberFormat formatter = NumberFormat.getInstance(Locale.US);
        formatter.setMaximumFractionDigits(2);
        formatter.setMinimumFractionDigits(2);
        formatter.setRoundingMode(RoundingMode.HALF_UP);
        media = new Float(formatter.format(media));


        end = ((50 - media * 6) / 4);
        formatter = NumberFormat.getInstance(Locale.US);
        formatter.setMaximumFractionDigits(2);
        formatter.setMinimumFractionDigits(2);
        formatter.setRoundingMode(RoundingMode.HALF_UP);
        end = new Float(formatter.format(end));

        return end;
    }

}
