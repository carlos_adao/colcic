package br.com.uesc.colcic.activity;

import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import br.com.uesc.colcic.R;
import br.com.uesc.colcic.fragment.HorariosOnibus.IlheusSalobrinhoFragment;

public class HorarioOnibusActivity extends BaseActivity {

    private String titulo;
    int local;
    Bundle savedInstanceState;
    IlheusSalobrinhoFragment frag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_horario_onibus);
        this.savedInstanceState = savedInstanceState;

        setUpToolbar();

        titulo = getIntent().getStringExtra("titulo");
        local = getIntent().getIntExtra("local", local);

        if (local == 0) {

            iniciaFragmentoHorarioOnibus(savedInstanceState, titulo, local);


        } else if (local == 1) {

            iniciaFragmentoHorarioOnibus(savedInstanceState, titulo, local);

        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_horario_onibus, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (local == 0) {
            if (id == R.id.addProximos) {
                if (savedInstanceState == null) {

                    frag.setHorariosLocal(local, 0);

                }
            } else if (id == R.id.jaSairam) {

                if (savedInstanceState == null) {

                    frag.setHorariosLocal(local, 1);

                }

            } else if (id == R.id.todos) {

                if (savedInstanceState == null) {
                    frag.setHorariosLocal(local, 2);
                }
            }
        }else if(local == 1){
            if (id == R.id.addProximos) {
                if (savedInstanceState == null) {

                    frag.setHorariosLocal(local, 0);

                }
            } else if (id == R.id.jaSairam) {


                if (savedInstanceState == null) {

                    frag.setHorariosLocal(local, 1);
                }

            } else if (id == R.id.todos) {


                if (savedInstanceState == null) {

                    frag.setHorariosLocal(local, 2);
                }
            }
        }
        return super.onOptionsItemSelected(item);
    }


    //Iniicia o fragmento  que contem a historia
    public void iniciaFragmentoHorarioOnibus(Bundle savedInstanceState, String titulo, int local) {

        getSupportActionBar().setTitle(titulo);
        // Adiciona o fragment no layout
        if (savedInstanceState == null && local == 0) {

            Bundle args = new Bundle();
            args.putInt("local", 0);//define a cidade do intinerario
            args.putInt("tipo", 0);//define qual tipo de viagem
            IlheusSalobrinhoFragment f = new IlheusSalobrinhoFragment();
            f.setArguments(args);
            this.frag = f;
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.alternavelFragment_horario_onibus, this.frag);
            transaction.commit();

        } else if (savedInstanceState == null && local == 1) {

            Bundle args = new Bundle();
            args.putInt("local", 1);//local = 1 representa os onibus de itabuna
            args.putInt("tipo", 0);//define qual tipo de viagem
            IlheusSalobrinhoFragment f = new IlheusSalobrinhoFragment();
            f.setArguments(args);
            this.frag = f;
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.alternavelFragment_horario_onibus, this.frag);
            transaction.commit();

        }
    }

}
