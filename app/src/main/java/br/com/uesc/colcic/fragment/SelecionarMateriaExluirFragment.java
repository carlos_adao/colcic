package br.com.uesc.colcic.fragment;

import android.app.Activity;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

import br.com.uesc.colcic.ConexaoInternet.OperacoesSQL.AtualizaCreditosDisciplina;
import br.com.uesc.colcic.R;
import br.com.uesc.colcic.activity.MeuHorarioActivity;
import br.com.uesc.colcic.activity.Meu_HorarioActivity;
import br.com.uesc.colcic.adapter.SelecionarMateriaExcluirAdapter;
import br.com.uesc.colcic.domain.Comandos;
import br.com.uesc.colcic.domain.Tipo;
import br.com.uesc.colcic.modelo.Materia;


public class SelecionarMateriaExluirFragment extends Fragment {
    RecyclerView recyclerView;
    EditText ed_nomeMateria;
    private ArrayList<Tipo> listaTipos;
    private int tipo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_selecionar_mateia_excluir, container, false);

        //se o tipo for igual a zero abro o menu de exluir materias
        if (tipo == 0) {
            Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar_selecionarMateriaExcluir);
            toolbar.setTitle(getString(R.string.exc_mat));
            toolbar.inflateMenu(R.menu.menu_exluir_materia_horario);
            toolbar.setOnMenuItemClickListener(
                    new Toolbar.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            int id = item.getItemId();
                            String materias = null;
                            if (id == R.id.salvar_se) {
                                StringBuilder stringBuilder = new StringBuilder();
                                for (Tipo number : listaTipos) {
                                    if (number.isSelected()) {
                                        if (stringBuilder.length() > 0)
                                            stringBuilder.append(",");
                                        stringBuilder.append(number.getName());
                                        materias = String.valueOf(stringBuilder);
                                    }
                                }
                                if (stringBuilder.length() > 0) {
                                    String[] nomeMaterias = materias.split(",");
                                    for (String nome : nomeMaterias) {
                                        deletadisciplinas(nome);
                                        deletaCreditodisciplinas(nome);

                                    }
                                }
                                atualizaHorario();
                                fechaFragmento();
                                Toast.makeText(getActivity(), "Materias excluidas", Toast.LENGTH_LONG).show();
                            } else if (id == R.id.cancelar_se) {
                                fechaFragmento();

                            }
                            return true;
                        }
                    });
        } else if (tipo == 1) {
            Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar_selecionarMateriaExcluir);
            toolbar.setTitle(getString(R.string.add_mat));
            toolbar.inflateMenu(R.menu.menu_adicionar_materia_horario);
            toolbar.setOnMenuItemClickListener(
                    new Toolbar.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            int id = item.getItemId();
                            String materias = null;
                            if (id == R.id.salvar_se) {
                                StringBuilder stringBuilder = new StringBuilder();
                                for (Tipo number : listaTipos) {
                                    if (number.isSelected()) {
                                        if (stringBuilder.length() > 0)
                                            stringBuilder.append(",");
                                        stringBuilder.append(number.getName());
                                        materias = String.valueOf(stringBuilder);
                                    }
                                }
                                if (stringBuilder.length() > 0) {
                                    String[] nomeMaterias = materias.split(",");
                                    for (String nome : nomeMaterias) {
                                        String cod = Comandos.returnCodDisciplina(nome);
                                        String turma = Comandos.returnTurmaDisciplina(nome);
                                        Materia mat = Comandos.listaMateriaMeuHorario(nome);
                                        Comandos.insertIntoMeuHorario(cod,turma, mat.getHora_inicio(), mat.getHora_fim());

                                    }
                                    Toast.makeText(getContext(), "Matéria adicionada ao horário!", Toast.LENGTH_LONG).show();
                                }
                                atualizaHorario();
                                fechaFragmento();
                            } else if (id == R.id.cancelar_se) {
                                fechaFragmento();

                            }
                            return true;
                        }
                    });


        }
        recyclerView = (RecyclerView) view.findViewById(R.id.rv_selecionar_materia_exluir);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);

        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            // Lê o tipo dos argumentos.
            this.tipo = getArguments().getInt("tipo");
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String retorno;

        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {

            retorno = data.getStringExtra("tipo");

            ed_nomeMateria.setText(String.valueOf(retorno));


        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (tipo == 0) {
            ArrayList<String> listaNomeMaterias;
            setHasOptionsMenu(true);
            listaNomeMaterias = Comandos.listaNomeMateriasCursando();
            listaTipos = returnListTips(listaNomeMaterias);
            recyclerView.setAdapter(new SelecionarMateriaExcluirAdapter(listaTipos));
        } else if (tipo == 1) {
            ArrayList<String> listaNomeMaterias;
            setHasOptionsMenu(true);
            listaNomeMaterias = Comandos.listaNomeMaterias();

            listaTipos = returnListTips(listaNomeMaterias);
            recyclerView.setAdapter(new SelecionarMateriaExcluirAdapter(listaTipos));
        }

    }

    //metodo usado para retornar uma lista de tipos
    public ArrayList<Tipo> returnListTips(ArrayList<String> listaNomeMaterias) {
        TypedArray imgs;
        ArrayList<Tipo> listaTipos = new ArrayList<>();
        imgs = getResources().obtainTypedArray(R.array.img_materia);
        for (int i = 0; i < listaNomeMaterias.size(); i++) {
            listaTipos.add(new Tipo(listaNomeMaterias.get(i).toString(), imgs.getDrawable(0)));
        }
        return listaTipos;
    }

    //fecha o menu de excluir ou salvar materia
    public void fechaFragmento() {

        ((Meu_HorarioActivity) getActivity()).fechaFragmentoSelecionarMateria();
    }

    //atualiza o horariio
    public void atualizaHorario() {
        ((Meu_HorarioActivity) getActivity()).desenhaHorario(true);
    }

    public void deletadisciplinas(String nome) {
        Comandos.deleteMateriaCursando(nome);
    }
    public void deletaCreditodisciplinas(String nome) {
        Comandos.deleteMateriaCursando(nome);
        AtualizaCreditosDisciplina.deleteCredito(nome);
    }
}
