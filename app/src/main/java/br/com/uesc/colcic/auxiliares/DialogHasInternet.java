package br.com.uesc.colcic.auxiliares;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import br.com.uesc.colcic.R;

public class DialogHasInternet extends Activity {
    TextView tv_confirmacao;
    LinearLayout ll_confirmacao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_has_internet);

        tv_confirmacao = (TextView) findViewById(R.id.tv_confirmacao);
        ll_confirmacao = (LinearLayout) findViewById(R.id.ll_confirmacao);
        tv_confirmacao.setOnClickListener(clickConfimacao);
        ll_confirmacao.setOnClickListener(clickConfimacao);


    }

    //Método chamdo quando recebe um click no tv_confimacao ou no lyout_confirmacao
    private View.OnClickListener clickConfimacao = new View.OnClickListener() {
        public void onClick(View v) {

            switch (v.getId()) {
                /*clicando no icon home*/
                case R.id.tv_confirmacao:

                    finish();
                    break;
                case R.id.ll_confirmacao:

                    finish();
                    break;

                default:
                    break;
            }
        }
    };

}


