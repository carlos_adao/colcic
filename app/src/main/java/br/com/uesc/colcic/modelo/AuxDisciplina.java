package br.com.uesc.colcic.modelo;

/**
 * Created by carlos on 28/02/18.
 */

public class AuxDisciplina {
    String cod, turma, abreviacao;

    public AuxDisciplina(String cod, String turma, String abreviacao) {
        this.cod = cod;
        this.turma = turma;
        this.abreviacao = abreviacao;
    }

    public AuxDisciplina(String cod, String turma) {
        this.cod = cod;
        this.turma = turma;
    }

    public String getAbreviacao() {
        return abreviacao;
    }

    public void setAbreviacao(String abreviacao) {
        this.abreviacao = abreviacao;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public String getTurma() {
        return turma;
    }

    public void setTurma(String turma) {
        this.turma = turma;
    }

    @Override
    public String toString() {
        return "AuxDisciplina{" +
                "cod='" + cod + '\'' +
                ", turma='" + turma + '\'' +
                '}';
    }

}
