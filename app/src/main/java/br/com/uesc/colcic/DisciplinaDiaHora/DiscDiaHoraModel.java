package br.com.uesc.colcic.DisciplinaDiaHora;

import android.content.Context;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;

import br.com.uesc.colcic.R;

public class DiscDiaHoraModel {
    private String BASE_URL, DDH_URL;

    public JSONObject getJsonDDH(Context context) throws IOException, JSONException {
        BASE_URL = context.getResources().getString(R.string.URL);
        DDH_URL = context.getResources().getString(R.string.URL_dis_dia_hora);

        String url = BASE_URL + DDH_URL;
        int resposta = connectarDiscDiaHoras(url).getResponseCode();
        if (resposta == HttpURLConnection.HTTP_OK) {
            InputStream is = connectarDiscDiaHoras(url).getInputStream();
            JSONObject json = new JSONObject(bytesParaString(is));
            return json;
        }
        return null;
    }

    private HttpURLConnection connectarDiscDiaHoras(String urlArquivo) throws IOException {

        final int SEGUNDOS = 1000;
        URL url = new URL(urlArquivo.toString());
        HttpURLConnection conexao = (HttpURLConnection) url.openConnection();
        conexao.setReadTimeout(10 * SEGUNDOS);
        conexao.setConnectTimeout(15 * SEGUNDOS);
        conexao.setRequestMethod("GET");
        conexao.setDoInput(true);
        conexao.setDoOutput(false);
        conexao.connect();
        return conexao;
    }

    /*Utilizado para fazer a bufferização dos dados vindo pela rede*/
    private String bytesParaString(InputStream is) throws IOException {
        byte[] buffer = new byte[1024];
        // O bufferzao vai armazenar todos os bytes lidos
        ByteArrayOutputStream bufferzao = new ByteArrayOutputStream();
        // precisamos saber quantos bytes foram lidos
        int bytesLidos;
        // Vamos lendo de 1KB por vez...
        while ((bytesLidos = is.read(buffer)) != -1) {
            // copiando a quantidade de bytes lidos do buffer para o bufferzão
            bufferzao.write(buffer, 0, bytesLidos);
        }
        return new String(bufferzao.toByteArray(), "UTF-8");
    }
}

