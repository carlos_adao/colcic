package br.com.uesc.colcic.domain;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by carlos on 19/12/2016.
 */
public class SQLiteBanco extends SQLiteOpenHelper {
    private static final String CATEGORIA = "colcic";
    private String[] scriptSQLCreate;
    private String scriptSQLDelete;
    SQLiteDatabase bd;

    public SQLiteBanco(Context context, String nomeBanco, int versaoBanco, String[] scriptSQLCreate) {
        super(context, nomeBanco, null, versaoBanco);
        this.scriptSQLCreate = scriptSQLCreate;
    }

    //Cria um novo banco de dados
    @Override
    public void onCreate(SQLiteDatabase bd) {
        int qtdScripts = scriptSQLCreate.length;
        //Executa cada SQL passado como padrão
        for(int i = 0; i < qtdScripts;i++){
            String sql = scriptSQLCreate[i];
            Log.i(CATEGORIA, sql);
            //Cria um banco de dados usando script de criação
            bd.execSQL(sql);
        }
        this.bd = bd;
    }
    public boolean executaScript(String[] script){

        for(int i = 0; i < script.length; i++){
            String sql = script[i];
            bd.execSQL(sql);
        }
        return true;
    }

    //Mudou a versão
    @Override
    public void onUpgrade(SQLiteDatabase bd, int i, int i1) {
        //drop table and add new tables when version 2 released.

    }
}



