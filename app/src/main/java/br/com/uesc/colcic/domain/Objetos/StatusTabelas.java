package br.com.uesc.colcic.domain.Objetos;

/**
 * Created by estagio-nit on 18/10/17.
 */

public class StatusTabelas {

    public StatusTabelas(String nomeTabela, boolean estaPreenchida) {
        this.nomeTabela = nomeTabela;
        this.estaPreenchida = estaPreenchida;
    }

    String nomeTabela;
    boolean estaPreenchida;

    public String getNomeTabela() {
        return nomeTabela;
    }

    public void setNomeTabela(String nomeTabela) {
        this.nomeTabela = nomeTabela;
    }

    public boolean isEstaPreenchida() {
        return estaPreenchida;
    }

    public void setEstaPreenchida(boolean estaPreenchida) {
        this.estaPreenchida = estaPreenchida;
    }
}
