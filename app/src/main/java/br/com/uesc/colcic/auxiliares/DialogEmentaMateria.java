package br.com.uesc.colcic.auxiliares;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import br.com.uesc.colcic.R;
import br.com.uesc.colcic.domain.Comandos;
import br.com.uesc.colcic.modelo.Ementa;
import br.com.uesc.colcic.modelo.InformacoesMateria;

public class DialogEmentaMateria extends Activity {
    TextView tv_codMat, tv_nomeMat, tv_pre_req,tv_carg_horaria,tv_qtd_credito, tv_ementa,tv_objetivo,tv_metodologia,tv_avaliacao,tv_conteudo_programatico,tv_referencia;
    ImageView iv_emailProf;
    private String nome;
    private String emailProf;
    LinearLayout li;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog_ementa_materia);

        nome = getIntent().getStringExtra("tipo");

        tv_codMat = (TextView)findViewById(R.id.tv_codMat);
        tv_nomeMat = (TextView)findViewById(R.id.tv_nomeMat);
        tv_pre_req = (TextView)findViewById(R.id.tv_pre_req);
        tv_carg_horaria = (TextView)findViewById(R.id.tv_carg_horaria);
        tv_qtd_credito = (TextView)findViewById(R.id.tv_qtd_credito);
        tv_ementa = (TextView)findViewById(R.id.tv_ementa);
        tv_objetivo = (TextView)findViewById(R.id.tv_objetivo);
        tv_metodologia = (TextView)findViewById(R.id.tv_metodologia);
        tv_avaliacao = (TextView)findViewById(R.id.tv_avaliacao);
        tv_conteudo_programatico = (TextView)findViewById(R.id.tv_conteudo_programatico);
        tv_referencia = (TextView)findViewById(R.id.tv_referencia);

        exibeEmanta(verificaProgramaDisciplina(nome));
    }
    //verifica se a disciplina tem o programa para pegar o conteudo do banco de dados
    public Ementa verificaProgramaDisciplina(String nome){

        return Comandos.returnEmentaDisciplina(nome);
    }

    // caso a emanta seja diferente de null exibe a ementa
    public void exibeEmanta(Ementa ementa){

        if(ementa!=null){
            tv_codMat.setText(ementa.getCod());
            tv_nomeMat.setText(ementa.getNomeDisc());
            tv_pre_req.setText(ementa.getPreReq());
            tv_carg_horaria.setText(ementa.getCargaHoraria());
            tv_qtd_credito.setText(ementa.getQtdCreitos());
            tv_ementa.setText(ementa.getEmenta());
            tv_objetivo.setText(ementa.getObjetivo());
            tv_metodologia.setText(ementa.getMetodologia());
            tv_avaliacao.setText(ementa.getAvalicao());
            tv_conteudo_programatico.setText(ementa.getContProg());
            tv_referencia.setText(ementa.getReferencia());
        }
    }

}
