package br.com.uesc.colcic.ListaRamais;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import br.com.uesc.colcic.R;
import br.com.uesc.colcic.activity.BaseActivity;
import br.com.uesc.colcic.domain.Comandos;


public class ListaRamaisActivity extends BaseActivity {
        private static final String DEBUG_TAG = "AppCompatActivity";
    SearchView searchView = null;

        public static final String EXTRA_UPDATE = "update";
        public static final String EXTRA_DELETE = "delete";
        public static final String EXTRA_NAME = "name";
        public static final String EXTRA_RAMAL = "rm";
        public static final String EXTRA_COLOR = "color";
        public static final String EXTRA_INITIAL = "initial";

        public static final String TRANSITION_FAB = "fab_transition";
        public static final String TRANSITION_INITIAL = "initial_transition";
        public static final String TRANSITION_NAME = "name_transition";
        public static final String TRANSITION_DELETE_BUTTON = "delete_button_transition";

        private RecyclerView recyclerView;
        private SampleMaterialAdapter adapter;

        private int[] colors;
        private String[] names;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_lista_ramais);


            colors = getResources().getIntArray(R.array.initial_colors);



            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            if (toolbar != null) {
                setSupportActionBar(toolbar);
                toolbar.inflateMenu(R.menu._menu_icon_text_tabs);
            }
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Ramais UESC");

            recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

            atualizaLista("");

        }

        @Override
        protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);



        }


        private ArrayList<Card> initCards(String pesquisar) {
            //PEGA O RETORNO DA LISTA DE RAMAIS
            ArrayList<Card> cardsList = new ArrayList<>();
            ArrayList<Ramal> ListaRamais = Comandos.returnListaRamais(pesquisar);

            for (int i = 0; i < ListaRamais.size(); i++) {
                Ramal ramal = (Ramal) ListaRamais.get(i);
                Card card = new Card();
                card.setId((long) i);
                card.setName(ramal.getNomeclatura());
                card.setRamal(ramal.getRamal());
                card.setColorResource(colors[i]);
                cardsList.add(card);
            }

            return cardsList;
        }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu._menu_icon_text_tabs, menu);
        SearchManager searchManager = (SearchManager) getSystemService(this.SEARCH_SERVICE);
        MenuItem mSearchMenuItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();

        if (mSearchMenuItem != null) {
            searchView = (SearchView) mSearchMenuItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(this
                    .getComponentName()));
        }
        searchView.setIconifiedByDefault(true);
        MenuItemCompat.expandActionView(mSearchMenuItem);

        SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
            public boolean onQueryTextChange(String query) {

                atualizaLista(query);

                return false;

            }

            public boolean onQueryTextSubmit(String query) {
                // Here u can get the value "query" which is entered in the

                return false;

            }
        };
        searchView.setOnQueryTextListener(queryTextListener);

        return super.onCreateOptionsMenu(menu);
    }
    public void atualizaLista(String pesquisar){
        ArrayList<Card> cardsList = initCards(pesquisar);

        recyclerView.setAdapter(new SampleMaterialAdapter(this, cardsList));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }
}