package br.com.uesc.colcic.ConexaoInternet;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by estagio-nit on 21/09/17.
 */

public class ConexaoHttp {


    static URL url;

    /*retorna uma lista de dados no formato json*/
    public static JSONObject carregarArquivoJson(int inst, String URL) {
        HttpURLConnection conexao = null;
        try {

            switch (inst) {
                case 1:

                    //Estabelece a conexão com settigs

                    conexao = connectarFuncionario(URL);
                    break;

                case 2:
                    //Estabelece a conexão com professores
                    conexao = connectarProfessor(URL);
                    break;

                case 3:
                    //Estabelece a conexão com salas

                    conexao = connectarSalas(URL);
                    break;

                case 4:
                    //Estabelece a conexão com semestre virgente
                    conexao = connectarSemestreVirgente(URL);
                    break;

                case 5:
                    //Estabelece a conexão com disciplinas
                    conexao = connectarDisciplina(URL);
                    break;


                case 8:
                    //Estabelece a conexão disciplina dia hora
                    conexao = connectarDiscDiaHoras(URL);
                    break;

                case 9:
                    //Estabelece a conexão disciplina dia hora
                    conexao = connectarHorarioOnibus(URL);
                    break;


                default:
                    break;
            }

            int resposta = conexao.getResponseCode();
            if (resposta == HttpURLConnection.HTTP_OK) {
                InputStream is = conexao.getInputStream();
                JSONObject json = new JSONObject(bytesParaString(is));
                return json;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    /*Utilizado para fazer a bufferização dos dados vindo pela rede*/
    private static String bytesParaString(InputStream is) throws IOException {
        byte[] buffer = new byte[1024];
        // O bufferzao vai armazenar todos os bytes lidos
        ByteArrayOutputStream bufferzao = new ByteArrayOutputStream();
        // precisamos saber quantos bytes foram lidos
        int bytesLidos;
        // Vamos lendo de 1KB por vez...
        while ((bytesLidos = is.read(buffer)) != -1) {
            // copiando a quantidade de bytes lidos do buffer para o bufferzão
            bufferzao.write(buffer, 0, bytesLidos);
        }
        return new String(bufferzao.toByteArray(), "UTF-8");
    }

    private static HttpURLConnection connectarFuncionario(String urlArquivo) throws IOException {

        final int SEGUNDOS = 1000;
        URL url = new URL(urlArquivo.toString());
        HttpURLConnection conexao = (HttpURLConnection) url.openConnection();
        conexao.setReadTimeout(10 * SEGUNDOS);
        conexao.setConnectTimeout(15 * SEGUNDOS);
        conexao.setRequestMethod("GET");
        conexao.setDoInput(true);
        conexao.setDoOutput(false);
        conexao.connect();
        return conexao;
    }

    //metodo usado para iniciar com settings
    private static HttpURLConnection connectar(String urlArquivo) throws IOException {

        final int SEGUNDOS = 1000;
        url = new URL(urlArquivo.toString());
        HttpURLConnection conexao = (HttpURLConnection) url.openConnection();
        conexao.setReadTimeout(10 * SEGUNDOS);
        conexao.setConnectTimeout(15 * SEGUNDOS);
        conexao.setRequestMethod("GET");
        conexao.setDoInput(true);
        conexao.setDoOutput(false);
        conexao.connect();
        return conexao;
    }

    private static HttpURLConnection connectarProfessor(String urlArquivo) throws IOException {

        final int SEGUNDOS = 1000;
        URL url = new URL(urlArquivo.toString());
        HttpURLConnection conexao = (HttpURLConnection) url.openConnection();
        conexao.setReadTimeout(10 * SEGUNDOS);
        conexao.setConnectTimeout(15 * SEGUNDOS);
        conexao.setRequestMethod("GET");
        conexao.setDoInput(true);
        conexao.setDoOutput(false);
        conexao.connect();
        return conexao;
    }

    private static HttpURLConnection connectarSalas(String urlArquivo) throws IOException {

        final int SEGUNDOS = 1000;
        URL url = new URL(urlArquivo.toString());
        HttpURLConnection conexao = (HttpURLConnection) url.openConnection();
        conexao.setReadTimeout(10 * SEGUNDOS);
        conexao.setConnectTimeout(15 * SEGUNDOS);
        conexao.setRequestMethod("GET");
        conexao.setDoInput(true);
        conexao.setDoOutput(false);
        conexao.connect();
        return conexao;
    }

    private static HttpURLConnection connectarSemestreVirgente(String urlArquivo) throws IOException {

        final int SEGUNDOS = 1000;
        URL url = new URL(urlArquivo.toString());
        HttpURLConnection conexao = (HttpURLConnection) url.openConnection();
        conexao.setReadTimeout(10 * SEGUNDOS);
        conexao.setConnectTimeout(15 * SEGUNDOS);
        conexao.setRequestMethod("GET");
        conexao.setDoInput(true);
        conexao.setDoOutput(false);
        conexao.connect();
        return conexao;
    }
    private static HttpURLConnection connectarDisciplina(String urlArquivo) throws IOException {

        final int SEGUNDOS = 1000;
        URL url = new URL(urlArquivo.toString());
        HttpURLConnection conexao = (HttpURLConnection) url.openConnection();
        conexao.setReadTimeout(10 * SEGUNDOS);
        conexao.setConnectTimeout(15 * SEGUNDOS);
        conexao.setRequestMethod("GET");
        conexao.setDoInput(true);
        conexao.setDoOutput(false);
        conexao.connect();
        return conexao;
    }

    private static HttpURLConnection connectarDiscDiaHoras(String urlArquivo) throws IOException {

        final int SEGUNDOS = 1000;
        URL url = new URL(urlArquivo.toString());
        HttpURLConnection conexao = (HttpURLConnection) url.openConnection();
        conexao.setReadTimeout(10 * SEGUNDOS);
        conexao.setConnectTimeout(15 * SEGUNDOS);
        conexao.setRequestMethod("GET");
        conexao.setDoInput(true);
        conexao.setDoOutput(false);
        conexao.connect();
        return conexao;
    }

    private static HttpURLConnection connectarHorarioOnibus(String urlArquivo) throws IOException {

        final int SEGUNDOS = 1000;
        URL url = new URL(urlArquivo.toString());
        HttpURLConnection conexao = (HttpURLConnection) url.openConnection();
        conexao.setReadTimeout(10 * SEGUNDOS);
        conexao.setConnectTimeout(15 * SEGUNDOS);
        conexao.setRequestMethod("GET");
        conexao.setDoInput(true);
        conexao.setDoOutput(false);
        conexao.connect();
        return conexao;
    }




}
