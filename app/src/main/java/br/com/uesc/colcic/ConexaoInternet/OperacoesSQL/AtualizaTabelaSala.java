package br.com.uesc.colcic.ConexaoInternet.OperacoesSQL;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import br.com.uesc.colcic.ConexaoInternet.Objetos.Disciplina;
import br.com.uesc.colcic.ConexaoInternet.Objetos.Sala;
import br.com.uesc.colcic.domain.Comandos;

/**
 * Created by estagio-nit on 26/09/17.
 */

public class AtualizaTabelaSala {

    /*Metodos de atualização da tabela do banco de dados local*/
    //recebe uma lista de Salas vindas do banco de dados
    public static void run(ArrayList<Sala> lSalaSw, SQLiteDatabase bd) {
        ArrayList<Sala> lSalaDB = new ArrayList<>();
        Sala sala;
        boolean insert;//caso a variavel permaneca falsa insere um novo professor coso contrario atualiza
        boolean delete;// caso a varial fique true deleta o professor

        Log.i("Atualizando","Tabela Sala");


        final String query = "SELECT codigo, localizacao, caracteristica FROM sala";


        Cursor cursor = bd.rawQuery(query, null);

        while (cursor.moveToNext()) {

            int a = cursor.getColumnIndex("codigo");
            int b = cursor.getColumnIndex("localizacao");
            int c = cursor.getColumnIndex("caracteristica");


            sala = new Sala();
            sala.setCodigo(cursor.getString(a));
            sala.setLocalizacao(cursor.getString(b));
            sala.setCaracteristica(cursor.getString(c));
            lSalaDB.add(sala);
        }

        if (lSalaDB.size() == 0) {

            for (Sala dsw : lSalaSw) {

                insertSala(dsw);

            }

        }
        else {
            
            if (lSalaSw.size() >= lSalaDB.size()) {

                for (Sala ssw : lSalaSw) {
                    insert = true;
                    for (Sala sdb : lSalaDB) {

                        if ((ssw.getCodigo().equalsIgnoreCase(sdb.getCodigo()))) {

                            insert = false;
                            if (!(sdb.getLocalizacao().equalsIgnoreCase(ssw.getLocalizacao()))) {

                                updateSala(ssw);

                            } else if (!(sdb.getCaracteristica().equalsIgnoreCase(ssw.getCaracteristica()))) {

                                updateSala(ssw);

                            }
                        }
                    }
                    if (insert) {

                        insertSala(ssw);

                    }
                }

            } else {


                for (Sala sdb : lSalaDB) {

                    delete = true;
                    for (Sala ssw : lSalaSw) {

                        if ((ssw.getCodigo() == sdb.getCodigo())) {
                            delete = false;
                        }
                    }
                    if (delete) {

                        deleteSala(sdb);
                    }

                }
            }
        }
    }

    public static void updateSala(Sala ddh) {
        Log.i("Update", ddh.getCodigo());

        String ud = "UPDATE Sala SET localizacao = '" + ddh.getLocalizacao() + "'" +
                ", caracteristica = '" + ddh.getCaracteristica() + "'" +
                "  WHERE codigo = '" + ddh.getCodigo() + "' ;";

        String[] sql = {ud};
        Comandos.executeSQL(sql);
    }

    public static void insertSala(Sala ddh) {
        Log.i("inserindo", ddh.getCodigo());

        String ins = "INSERT INTO Sala VALUES ('"+ ddh.getCodigo() +"','"+ ddh.getLocalizacao() +"','"+ ddh.getCaracteristica() +"')";
        String[] sql = {ins};
        Comandos.executeSQL(sql);

    }

    public static void deleteSala(Sala ddh) {
        Log.i("Deletando", ddh.getCodigo());

        String del = "DELETE FROM Sala  WHERE codigo = '" + ddh.getCodigo() + "';";
        String[] sql = {del};
        Comandos.executeSQL(sql);
    }
/************************************************************************************************************/

}
