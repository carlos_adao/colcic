package br.com.uesc.colcic.fragment;

import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import br.com.uesc.colcic.R;
import br.com.uesc.colcic.adapter.MateriasAdapter;
import br.com.uesc.colcic.auxiliares.DialogInformacoesMaterias;
import br.com.uesc.colcic.domain.Acesso;
import br.com.uesc.colcic.domain.Comandos;
import br.com.uesc.colcic.modelo.Materia;


public class MeuHorarioFragment extends BaseFragment {
    protected RecyclerView recyclerView;
    protected ArrayList<Materia> listaMaterias = new ArrayList<>();
    private int tipo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_meu_horario, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);

        return view;
    }

    // Método para instanciar esse fragment pelo tipo.
    public static MeuHorarioFragment newInstance(int tipo) {
        Bundle args = new Bundle();
        args.putInt("tipo", tipo);
        MeuHorarioFragment f = new MeuHorarioFragment();
        f.setArguments(args);
        return f;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        leMateria();//procura as materias de acordo com o dia
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            // Lê o tipo dos argumentos.
            this.tipo = getArguments().getInt("tipo");
        }
    }

    private void leMateria() {

        Acesso a = new Acesso(getContext());

        a.criaBD(getContext());
        this.listaMaterias = Comandos.listaMateriaHorario(tipo);
        TypedArray cores = getResources().obtainTypedArray(R.array.cores);//pegar as cores das materias
        recyclerView.setAdapter(new MateriasAdapter(getContext(), listaMaterias, onClickMateria(),cores));

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private MateriasAdapter.MateriasOnClickListener onClickMateria() {
        return new MateriasAdapter.MateriasOnClickListener() {
            @Override
            public void onClickMaterias(View view, int idx) {
                Intent intent = new Intent(getContext(), DialogInformacoesMaterias.class);
                intent.putExtra("tipo", listaMaterias.get(idx).getNome());
                startActivityForResult(intent, 1);
            }
        };
    }
}
