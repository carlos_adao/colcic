package br.com.uesc.colcic.ListaRamais;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import br.com.uesc.colcic.R;


public class RamalLigacaoActivity extends AppCompatActivity {

    private TextView initialTextView;
    private TextView TextViewRamal;

    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ramal_ligacao);




        initialTextView = (TextView) findViewById(R.id.initial);
        TextViewRamal = (TextView) findViewById(R.id.ram);

        intent = getIntent();
        String nameExtra = intent.getStringExtra(ListaRamaisActivity.EXTRA_NAME);
        final String ramal = intent.getStringExtra(ListaRamaisActivity.EXTRA_RAMAL);
        String initialExtra = intent.getStringExtra(ListaRamaisActivity.EXTRA_INITIAL);
        int colorExtra = intent.getIntExtra(ListaRamaisActivity.EXTRA_COLOR, 0);


        TextViewRamal.setText("Ramal " + ramal);
        initialTextView.setText("UESC - Ilhéus\n\n" + nameExtra);
        initialTextView.setBackgroundColor(colorExtra);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.chamar);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String tel = "0733680"+ramal;
                intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", tel, null));
                startActivity(intent);
                }
            });

        }
    public void mensagem(String ex){

        Toast.makeText(this, "falha na ligação = "+ ex, Toast.LENGTH_SHORT).show();
    }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {

            int id = item.getItemId();

            if (id == android.R.id.home) {
                super.onBackPressed();
                return true;
            }

            return super.onOptionsItemSelected(item);
        }
    }
