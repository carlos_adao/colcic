package br.com.uesc.colcic.domain;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;


import br.com.uesc.colcic.ConexaoInternet.Objetos.DisciplinaDiaHora;
import br.com.uesc.colcic.ConexaoInternet.Objetos.Func;
import br.com.uesc.colcic.ConexaoInternet.Objetos.HorarioOnibus;
import br.com.uesc.colcic.ConexaoInternet.Objetos.Professor;
import br.com.uesc.colcic.ConexaoInternet.Objetos.Sala;
import br.com.uesc.colcic.ConexaoInternet.Objetos.SemestreVirgente;
import br.com.uesc.colcic.Settings.Settings;

import br.com.uesc.colcic.ConexaoInternet.OperacoesSQL.AtualizaTabelaDisciplina;
import br.com.uesc.colcic.ConexaoInternet.OperacoesSQL.AtualizaTabelaDisciplinaDiaHora;
import br.com.uesc.colcic.ConexaoInternet.OperacoesSQL.AtualizaTabelaFuncionario;
import br.com.uesc.colcic.ConexaoInternet.OperacoesSQL.AtualizaTabelaHorarioOnibus;
import br.com.uesc.colcic.ConexaoInternet.OperacoesSQL.AtualizaTabelaProfessores;
import br.com.uesc.colcic.ConexaoInternet.OperacoesSQL.AtualizaTabelaSala;
import br.com.uesc.colcic.ConexaoInternet.OperacoesSQL.AtualizaTabelaSemestreVirgente;
import br.com.uesc.colcic.ListaRamais.Ramal;
import br.com.uesc.colcic.domain.Objetos.InformacoesProfessor;
import br.com.uesc.colcic.domain.Objetos.StatusTabelas;
import br.com.uesc.colcic.fragment.QuadroDocente.Docente;
import br.com.uesc.colcic.ConexaoInternet.Objetos.Disciplina;
import br.com.uesc.colcic.modelo.AuxDisciplina;
import br.com.uesc.colcic.modelo.Ementa;
import br.com.uesc.colcic.modelo.Funcionario;
import br.com.uesc.colcic.modelo.InformacoesMateria;
import br.com.uesc.colcic.modelo.Materia;


/**
 * Created by carlos on 26/04/2017.
 */

public class Comandos {
    public static SQLiteDatabase bd;

    public void fechar() {
        //fecha o banco de dados
        if (bd != null) {
            bd.close();
        }
    }

    public static void matarBandoDados(Context context) {
        context.deleteDatabase("colcic");


    }

    public static boolean executeSQL(String[] script) {

        for (int i = 0; i < script.length; i++) {
            String sql = script[i];
            Log.i("SQL", sql);
            //Cria um banco de dados usando script de criação
            bd.execSQL(sql);
        }

        return true;
    }

    public static String[] createTable() {

        //************************CREATE TABLES******************************************//
        String funcionario = "CREATE TABLE IF NOT EXISTS funcionario(\n" +
                " codigo VARCHAR(10)PRIMARY KEY,\n" +
                " nome VARCHAR(50)NOT NULL UNIQUE,\n" +
                " email VARCHAR(50) NOT NULL,\n" +
                " tel VARCHAR(30) NOT NULL,\n" +
                " cargo VARCHAR(50) NOT NULL,\n" +
                " hierarquia VARCHAR(5) NOT NULL,\n" +
                " sexo VARCHAR(10)\n" +
                ")";

        String professor = "CREATE TABLE IF NOT EXISTS professor(\n" +
                " codigo VARCHAR(10)PRIMARY KEY,\n" +
                " nome VARCHAR(50)NOT NULL UNIQUE,\n" +
                " email VARCHAR(50) NOT NULL,\n" +
                " tel VARCHAR(30),\n" +
                " titulacao VARCHAR(100) NOT NULL,\n" +
                " classe VARCHAR(20) NOT NULL,\n" +
                " url_lates VARCHAR(100)\n" +
                ")";

        String curso = "CREATE TABLE IF NOT EXISTS curso(\n" +
                " codigo VARCHAR(10)PRIMARY KEY,\n" +
                " nome VARCHAR(50)NOT NULL UNIQUE,\n" +
                " cordenador VARCHAR(10)NOT NULL, \n" +
                " FOREIGN KEY(cordenador) REFERENCES professor (codigo)    \n" +
                ")";

        String sala = "CREATE TABLE IF NOT EXISTS sala(\n" +
                " codigo VARCHAR(10)PRIMARY KEY,\n" +
                " localizacao VARCHAR(100)NOT NULL,\n" +
                " caracteristica VARCHAR(50) NOT NULL\n" +
                ")";


        String hora_aula = "CREATE TABLE IF NOT EXISTS hora_aula(\n" +
                "   codigo INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                "   hora_inicio TIME NOT NULL,\n" +
                "   hora_fim TIME NOT NULL\n" +
                ")";

        String dia_semana = "CREATE TABLE IF NOT EXISTS dia_semana(\n" +
                "   codigo INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                "   nome VARCHAR(50)NOT NULL UNIQUE\n" +
                ")";

        String aluno = "CREATE TABLE IF NOT EXISTS aluno(\n" +
                " matricula VARCHAR(10)PRIMARY KEY,\n" +
                " nome VARCHAR(50)NOT NULL UNIQUE,\n" +
                " email VARCHAR(50),\n" +
                " cel VARCHAR(10)\n" +
                ")";

        String semestre_virgente = "CREATE TABLE IF NOT EXISTS semestre_virgente(\n" +
                "   codigo INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                "   ano INT NULL UNIQUE,\n" +
                "   periodo INT NULL\n" +
                ")";


        String disciplina = "CREATE TABLE IF NOT EXISTS disciplina(\n" +
                "   codigo VARCHAR(10),\n" +
                "   nome VARCHAR(50)NOT NULL UNIQUE,\n" +
                "   abreviacao VARCHAR(50)NOT NULL,\n" +
                "   carga_horaria INT NOT NULL,\n" +
                "   fk_semestre_virgente INT NOT NULL,   \n" +
                "   semestre_diciplina INT NOT NULL,  \n" +
                "   fk_curso VARCHAR(10)NOT NULL,\n" +
                "   fk_professor VARCHAR(10)NOT NULL,\n" +
                "   turma VARCHAR(10)NOT NULL,\n" +
                "   CHS  INT NOT NULL,\n" +
                "   PRIMARY KEY(codigo,turma), \n" +
                "   FOREIGN KEY(fk_semestre_virgente) REFERENCES semestre_virgente(codigo),   \n" +
                "   FOREIGN KEY(fk_curso) REFERENCES curso(codigo), \n" +
                "   FOREIGN KEY(fk_professor) REFERENCES professor(codigo)\n" +
                ")";


        String ementa = "CREATE TABLE IF NOT EXISTS ementa_disciplina(\n" +
                "   fk_disicplina VARCHAR(10) NOT NULL,\n" +
                "   fk_pre_requisito VARCHAR(10),\n" +
                "   ementa TEXT NOT NULL,\n" +
                "   objetivo TEXT NOT NULL,\n" +
                "   metodologia TEXT NOT NULL,\n" +
                "   avaliacao TEXT NOT NULL,\n" +
                "   conteudo_programatico TEXT NOT NULL,\n" +
                "   referencia TEXT NOT NULL,\n" +
                "   FOREIGN KEY(fk_disicplina) REFERENCES disciplina(codigo), \n" +
                "   FOREIGN KEY(fk_pre_requisito) REFERENCES pre_equisito(fk_disicplina)\n" +
                ")";


        String pre_requisito = "CREATE TABLE IF NOT EXISTS pre_requisito(\n" +
                "   fk_disicplina VARCHAR(10) NOT NULL,\n" +
                "   cod_disciplinas VARCHAR(10) NOT NULL\n" +
                ")";


        String aluno_disciplina = "CREATE TABLE IF NOT EXISTS aluno_disciplina(\n" +
                "   codigo INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                "   fk_aluno VARCHAR(10) NOT NULL,\n" +
                "   fk_disciplina VARCHAR(10) NOT NULL,\n" +
                "   FOREIGN KEY(fk_aluno) REFERENCES aluno(matricula) \n" +
                "   FOREIGN KEY(fk_disciplina) REFERENCES disciplina (codigo)  \n" +
                ")";


        String disciplina_dia_hora = "CREATE TABLE IF NOT EXISTS disciplina_dia_hora(\n" +
                "   id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "   fk_codigo VARCHAR(10) NOT NULL,\n" +
                "   fk_turma VARCHAR(10) NOT NULL,\n" +
                "   fk_dia_hora VARCHAR(10) NOT NULL,\n" +
                "   fk_sala VARCHAR(10)NOT NULL,\n" +
                "    \n" +
                "   FOREIGN KEY(fk_sala) REFERENCES sala(codigo), \n" +
                "   FOREIGN KEY(fk_dia_hora) REFERENCES dia_hora(codigo),\n" +
                "   FOREIGN KEY(fk_codigo, fk_turma) REFERENCES disciplina (codigo,turma)\n" +
                ")";

        String dia_hora = "CREATE TABLE IF NOT EXISTS dia_hora(\n" +
                "   codigo VARCHAR(10)PRIMARY KEY, \n" +
                "   fk_dia_semana INT NOT NULL,\n" +
                "   fk_hora_aula INT NOT NULL,\n" +
                "   FOREIGN KEY(fk_dia_semana) REFERENCES dia_semana(codigo),\n" +
                "   FOREIGN KEY(fk_hora_aula) REFERENCES hora_aula(codigo)\n" +
                ")";


        String meu_horario = "CREATE TABLE IF NOT EXISTS meu_horario(\n" +
                "   codigo VARCHAR(10)PRIMARY KEY, \n" +
                "   hora_inicio VARCHAR(10) NOT NULL, \n" +
                "   hora_fim VARCHAR(10) NOT NULL, \n" +
                "   fk_cod_disciplina VARCHAR(10) NOT NULL,\n" +
                "   fk_turma VARCHAR(10) NOT NULL,\n" +
                "   FOREIGN KEY(fk_cod_disciplina, fk_turma) REFERENCES disciplina (codigo,turma)\n" +
                ")";

        //tabela para criar o horarios dos busão
        String horario_onibus = "CREATE TABLE IF NOT EXISTS horario_onibus(\n" +
                "   cod VARCHAR(10)PRIMARY KEY, \n" +
                "   num_linha VARCHAR(10) NOT NULL, \n" +
                "   via VARCHAR(50) NOT NULL,\n" +
                "   loc_saida VARCHAR(50) NOT NULL,\n" +
                "   loc_chegada VARCHAR(50) NOT NULL,\n" +
                "   cidade VARCHAR(20) NOT NULL,\n" +
                "   empresa VARCHAR(20) NOT NULL,\n" +
                "   hora_saida VARCHAR(10) NOT NULL,\n" +
                "   status VARCHAR(10) NOT NULL\n" +
                ")";

        //tabela usada excusivamente para atualização de dados
        String settings = "CREATE TABLE IF NOT EXISTS settings(\n" +
                "   cod VARCHAR(10)PRIMARY KEY, \n" +
                "   att_funcionario VARCHAR(10) NOT NULL, \n" +
                "   att_professor VARCHAR(10) NOT NULL, \n" +
                "   att_sala VARCHAR(10) NOT NULL,\n" +
                "   att_semestre_virgente VARCHAR(10) NOT NULL,\n" +
                "   att_disciplina VARCHAR(10) NOT NULL,\n" +
                "   att_ementa VARCHAR(10) NOT NULL,\n" +
                "   att_pre_requisito VARCHAR(10) NOT NULL,\n" +
                "   att_disciplina_dia_hora VARCHAR(10) NOT NULL," +
                "   att_horario_onibus VARCHAR(10) NOT NULL" +
                ")";

        //tabela usada excusivamente para atualização de dados
        String creditos = "CREATE TABLE  IF NOT EXISTS creditos(\n" +
                "   id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "   num_credito VARCHAR(10) NOT NULL, \n" +
                "   nota_credito VARCHAR(10) NOT NULL, \n" +
                "   fk_nome_disciplina VARCHAR(10) NOT NULL," +
                "   FOREIGN KEY(fk_nome_disciplina) REFERENCES disciplina (nome)\n" +
                ")";

        //tabela usada excusivamente para atualização de dados
        String ramal = "CREATE TABLE  IF NOT EXISTS ramal(\n" +
                "   id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "   nome_setor VARCHAR(50) NOT NULL, \n" +
                "   sigla VARCHAR(10) NOT NULL, \n" +
                "   ramal VARCHAR(10) NOT NULL \n" +
                ")";


        String[] script = {funcionario, professor, curso, sala, hora_aula, dia_semana, aluno, disciplina, pre_requisito, ementa, semestre_virgente, aluno_disciplina, dia_hora, disciplina_dia_hora, meu_horario, settings, horario_onibus, creditos, ramal};

        return script;
    }

    public static void deleteMateriaCursando(String nome) {
        String cod = returnCodDisciplina(nome);
        String tur = returnTurmaDisciplina(nome);

        String D1 = "DELETE FROM meu_horario WHERE  fk_cod_disciplina = '" + cod + "' AND  fk_turma = '" + tur + "'";

        String[] script = {D1};
        executeSQL(script);

    }

    public static void dropTable() {
        String D1 = "DROP TABLE professor;";
        String D2 = "DROP TABLE curso;";
        String D3 = "DROP TABLE sala;";
        String D4 = "DROP TABLE hora_aula;";
        String D5 = "DROP TABLE dia_semana ";
        String D6 = "DROP TABLE aluno;";
        String D7 = "DROP TABLE semestre_virgente;";
        String D8 = "DROP TABLE disciplina;";
        String D9 = "DROP TABLE aluno_disciplina;";
        String D10 = "DROP TABLE dia_hora ";
        String D11 = "DROP TABLE disciplina_dia_hora";
        String D12 = "DROP TABLE meu_horario";
        String D13 = "DROP TABLE ementa_disciplina";
        String D14 = "DROP TABLE pre_requisito";
        String D15 = "DROP TABLE funcionario";
        String D18 = "DROP TABLE settings";
        String D19 = "DROP TABLE horario_onibus";
        String D20 = "DROP TABLE creditos";

        String[] script = {D1, D2, D3, D4, D5, D6, D7, D8, D9, D10, D11, D12, D14, D13, D15, D18, D19, D20};
        executeSQL(script);

    }

    public static void insertAllData() {
        // insertSettings();
        //insertFuncionarios();
        //insertProfessores();
        //insertSalas();
        insertHoraAula();
        insertDiaSemana();
        //insertDisciplina();
        insertDiaHora();
        insertEmenta();
    }


    //inserir disciplina no horario
    public static void insertIntoMeuHorario(String cod_disciplina, String turma, String ini, String fim) {

        String i1 = "INSERT INTO meu_horario (fk_cod_disciplina, fk_turma, hora_inicio, hora_fim)VALUES ('" + cod_disciplina + "','" + turma + "','" + ini + "','" + fim + "')";
        String[] insertIntoMeuHorario = {i1};
        executeSQL(insertIntoMeuHorario);

    }

    public static void insertSettings(Settings settingsRede) {
        Log.i("ho bus red", settingsRede.getAtt_horario_onibus());
        /*Insert para iniciar a tabela de configuranção*/
        String Es = "INSERT INTO settings (cod," +
                "att_funcionario," +
                "att_professor," +
                "att_sala," +
                "att_semestre_virgente," +
                "att_disciplina," +
                "att_ementa," +
                "att_pre_requisito," +
                "att_disciplina_dia_hora," +
                "att_horario_onibus)VALUES (" +
                "'" + settingsRede.getCod() + "'," +
                "'" + settingsRede.getAtt_funcionario() +
                "','" + settingsRede.getAtt_professor() + "'," +
                "'" + settingsRede.getAtt_sala() +
                "','" + settingsRede.getAtt_semestre_virgente() + "'," +
                "'" + settingsRede.getAtt_disciplina() +
                "','21-09-2017'," +
                "'21-09-2017'," +
                "'" + settingsRede.getAtt_disciplina_dia_hora() +
                "','" + settingsRede.getAtt_horario_onibus() + "')";
        String[] insertSettigns = {Es};
        executeSQL(insertSettigns);

    }

    public static void updateSettings(Settings settingsRede) {

        String ud = "UPDATE settings SET cod = '" + settingsRede.getCod() + "'" +
                ", att_funcionario = '" + settingsRede.getAtt_funcionario() + "'" +
                ", att_professor = '" + settingsRede.getAtt_professor() + "'" +
                ", att_sala = '" + settingsRede.getAtt_sala() + "'" +
                ", att_semestre_virgente = '" + settingsRede.getAtt_semestre_virgente() + "'" +
                ", att_disciplina = '" + settingsRede.getAtt_disciplina() + "'" +
                ", att_ementa = '" + settingsRede.getAtt_ementa() + "'" +
                ", att_pre_requisito = '" + settingsRede.getAtt_pre_requisito() + "'" +
                ", att_disciplina_dia_hora = '" + settingsRede.getAtt_disciplina_dia_hora() + "'" +
                ", att_horario_onibus = '" + settingsRede.getAtt_horario_onibus() + "'" +
                "  WHERE cod = '" + settingsRede.getCod() + "' ;";

        String[] sql = {ud};
        executeSQL(sql);
    }

    public static void deleteSettings() {

        /*deleta todos os dados da tabela de configuração*/
        String comando = "DELETE FROM settings";
        String[] deleteSettigns = {comando};
        executeSQL(deleteSettigns);

    }


    public static void insertHoraAula() {

        String i66 = "INSERT INTO hora_aula (hora_inicio , hora_fim) VALUES ('7:30', '8:20')";
        String i67 = "INSERT INTO hora_aula (hora_inicio , hora_fim) VALUES ('8:20', '9:10')";
        String i68 = "INSERT INTO hora_aula (hora_inicio , hora_fim) VALUES ('9:10', '10:00')";
        String i69 = "INSERT INTO hora_aula (hora_inicio , hora_fim) VALUES ('10:00', '10:50')";
        String i70 = "INSERT INTO hora_aula (hora_inicio , hora_fim) VALUES ('10:50', '11:40')";
        String i71 = "INSERT INTO hora_aula (hora_inicio , hora_fim) VALUES ('11:40', '12:30')";
        String i72 = "INSERT INTO hora_aula (hora_inicio , hora_fim) VALUES ('13:30', '14:20')";
        String i73 = "INSERT INTO hora_aula (hora_inicio , hora_fim) VALUES ('14:20', '15:10')";
        String i74 = "INSERT INTO hora_aula (hora_inicio , hora_fim) VALUES ('15:10', '16:00')";
        String i75 = "INSERT INTO hora_aula (hora_inicio , hora_fim) VALUES ('16:00', '16:50')";
        String i76 = "INSERT INTO hora_aula (hora_inicio , hora_fim) VALUES ('16:50', '17:40')";
        String i77 = "INSERT INTO hora_aula (hora_inicio , hora_fim) VALUES ('17:40', '18:30')";

        String[] hora = {i66, i67, i68, i69, i70, i71, i72, i73, i74, i75, i76, i77};
        executeSQL(hora);
    }

    public static void insertDiaSemana() {

        String i78 = "INSERT INTO dia_semana (nome) VALUES ('Dom')";
        String i79 = "INSERT INTO dia_semana (nome) VALUES ('Seg')";
        String i80 = "INSERT INTO dia_semana (nome) VALUES ('Ter')";
        String i81 = "INSERT INTO dia_semana (nome) VALUES ('Qua')";
        String i82 = "INSERT INTO dia_semana (nome) VALUES ('Qui')";
        String i83 = "INSERT INTO dia_semana (nome) VALUES ('Sex')";
        String i84 = "INSERT INTO dia_semana (nome) VALUES ('Sab')";

        String[] dia = {i78, i79, i80, i81, i82, i83, i84};
        executeSQL(dia);
    }


    public static void insertDiaHora() {

        /*Dia e hora*/
        String s1 = "INSERT INTO dia_hora VALUES ('21',2,1)";
        String s2 = "INSERT INTO dia_hora VALUES ('22',2,2)";
        String s3 = "INSERT INTO dia_hora VALUES ('23',2,3)";
        String s4 = "INSERT INTO dia_hora VALUES ('24',2,4)";
        String s5 = "INSERT INTO dia_hora VALUES ('25',2,5)";
        String s6 = "INSERT INTO dia_hora VALUES ('26',2,6)";
        String s7 = "INSERT INTO dia_hora VALUES ('27',2,7)";
        String s8 = "INSERT INTO dia_hora VALUES ('28',2,8)";
        String s9 = "INSERT INTO dia_hora VALUES ('29',2,9)";
        String s10 = "INSERT INTO dia_hora VALUES ('210',2,10)";
        String s11 = "INSERT INTO dia_hora VALUES ('211',2,11)";
        String s12 = "INSERT INTO dia_hora VALUES ('212',2,12)";

        String t1 = "INSERT INTO dia_hora VALUES ('31',3,1)";
        String t2 = "INSERT INTO dia_hora VALUES ('32',3,2)";
        String t3 = "INSERT INTO dia_hora VALUES ('33',3,3)";
        String t4 = "INSERT INTO dia_hora VALUES ('34',3,4)";
        String t5 = "INSERT INTO dia_hora VALUES ('35',3,5)";
        String t6 = "INSERT INTO dia_hora VALUES ('36',3,6)";
        String t7 = "INSERT INTO dia_hora VALUES ('37',3,7)";
        String t8 = "INSERT INTO dia_hora VALUES ('38',3,8)";
        String t9 = "INSERT INTO dia_hora VALUES ('39',3,9)";
        String t10 = "INSERT INTO dia_hora VALUES ('310',3,10)";
        String t11 = "INSERT INTO dia_hora VALUES ('311',3,11)";
        String t12 = "INSERT INTO dia_hora VALUES ('312',3,12)";

        String q1 = "INSERT INTO dia_hora VALUES ('41',4,1)";
        String q2 = "INSERT INTO dia_hora VALUES ('42',4,2)";
        String q3 = "INSERT INTO dia_hora VALUES ('43',4,3)";
        String q4 = "INSERT INTO dia_hora VALUES ('44',4,4)";
        String q5 = "INSERT INTO dia_hora VALUES ('45',4,5)";
        String q6 = "INSERT INTO dia_hora VALUES ('46',4,6)";
        String q7 = "INSERT INTO dia_hora VALUES ('47',4,7)";
        String q8 = "INSERT INTO dia_hora VALUES ('48',4,8)";
        String q9 = "INSERT INTO dia_hora VALUES ('49',4,9)";
        String q10 = "INSERT INTO dia_hora VALUES ('410',4,10)";
        String q11 = "INSERT INTO dia_hora VALUES ('411',4,11)";
        String q12 = "INSERT INTO dia_hora VALUES ('412',4,12)";

        String Q1 = "INSERT INTO dia_hora VALUES ('51',5,1)";
        String Q2 = "INSERT INTO dia_hora VALUES ('52',5,2)";
        String Q3 = "INSERT INTO dia_hora VALUES ('53',5,3)";
        String Q4 = "INSERT INTO dia_hora VALUES ('54',5,4)";
        String Q5 = "INSERT INTO dia_hora VALUES ('55',5,5)";
        String Q6 = "INSERT INTO dia_hora VALUES ('56',5,6)";
        String Q7 = "INSERT INTO dia_hora VALUES ('57',5,7)";
        String Q8 = "INSERT INTO dia_hora VALUES ('58',5,8)";
        String Q9 = "INSERT INTO dia_hora VALUES ('59',5,9)";
        String Q10 = "INSERT INTO dia_hora VALUES ('510',5,10)";
        String Q11 = "INSERT INTO dia_hora VALUES ('511',5,11)";
        String Q12 = "INSERT INTO dia_hora VALUES ('512',5,12)";

        String S1 = "INSERT INTO dia_hora VALUES ('61',6,1)";
        String S2 = "INSERT INTO dia_hora VALUES ('62',6,2)";
        String S3 = "INSERT INTO dia_hora VALUES ('63',6,3)";
        String S4 = "INSERT INTO dia_hora VALUES ('64',6,4)";
        String S5 = "INSERT INTO dia_hora VALUES ('65',6,5)";
        String S6 = "INSERT INTO dia_hora VALUES ('66',6,6)";
        String S7 = "INSERT INTO dia_hora VALUES ('67',6,7)";
        String S8 = "INSERT INTO dia_hora VALUES ('68',6,8)";
        String S9 = "INSERT INTO dia_hora VALUES ('69',6,9)";
        String S10 = "INSERT INTO dia_hora VALUES ('610',6,10)";
        String S11 = "INSERT INTO dia_hora VALUES ('611',6,11)";
        String S12 = "INSERT INTO dia_hora VALUES ('612',6,12)";

        String[] dia_hora = {s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, q1, q2, q3, q4, q5, q6, q7, q8, q9, q10, q11, q12, Q1, Q2, Q3, Q4, Q5, Q6, Q7, Q8, Q9, Q10, Q11, Q12, S1, S2, S3, S4, S5, S6, S7, S8, S9, S10, S11, S12};
        executeSQL(dia_hora);


    }

    public static void insertRamais() {

        String r = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('Colegiado de Med. Veterinária', '', '5140')";
        String r2 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('Colegiado de Ciências Sociais', '', '5526')";
        String r3 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('Colegiado de Educação Física', '', '5123')";
        String r4 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('ADUSC', '', '5065')";
        String r5 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('NBCGIB', '', '5213')";
        String r6 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('Restaurante Universitário', 'RU', '5310')";
        String r7 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('Guarita', '', '5081')";
        String r8 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('TV UESC', '', '5332')";
        String r9 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('Protocolo', '', '5502')";
        String r10 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('Univ. Aberta da  3º idade', 'UNATI', '5328')";
        String r11 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('Colegiado de LEA', '', '5072')";
        String r12 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('Colegiado de pedagogia', '', '5162')";
        String r13 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('LEA Junio Consutoria', '', '5133')";
        String r14 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('LEA Junio Consutoria', '', '5133')";
        String r15 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('Coleg. de Eng. Civil', '', '5544')";
        String r16 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('Coleg. de Eng. Mecânica', '', '5543')";
        String r17 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('Coleg. de Eng. Quimica', '', '5541')";
        String r18 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('Coleg. de Eng. Elétrica', '', '5542')";
        String r19 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('Coleg. de Ciência da Computação', 'Colcic', '5110')";
        String r20 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('Coleg. de Eng. P e Sistemas', '', '5201')";
        String r21 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('Coleg. de Física', '', '5549')";
        String r22 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('Coleg. de Matemática', '', '5547')";
        String r23 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('Coleg. de Química', '', '5548')";
        String r24 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('Tecno Jr', '', '5389')";
        String r25 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('Coleg. de Enfermagem', '', '5108')";
        String r26 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('Coleg. de Agromomia', '', '5113')";
        String r27 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('Coleg. de Geografia', '', '5141')";
        String r28 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('Coleg. de Biologia', '', '5265')";
        String r29 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('Coleg. de História', '', '5121')";
        String r30 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('Coleg. de Economia', '', '5218')";
        String r31 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('Coleg. de Administração', '', '5160')";
        String r32 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('ASSEST', '', '5452')";
        String r33 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('Telefonista', '', '5200')";
        String r34 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('Jovem Bom de Vida', '', '5130')";
        String r35 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('PROEX', '', '5405')";
        String r36 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('SECREGE', '', '5070')";
        String r37 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('PROGRAD', '', '5079')";
        String r38 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('SECREGE', '', '5071')";
        String r39 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('SEDOC', '', '5073')";
        String r40 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('SEGRAD', '', '5078')";
        String r41 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('GEFIN', '', '5064')";
        String r42 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('GERHU', '', '5061')";
        String r43 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('SEPAT', '', '5054')";
        String r44 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('SEMAT', '', '5061')";
        String r45 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('SUSAU', '', '5503')";
        String r46 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('Tesouraria', '', '5243')";
        String r47 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('EDITUS', '', '5170')";
        String r48 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('Revista UESC', '', '5386')";
        String r49 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('PROJUR', '', '5007')";
        String r50 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('Reitoria Gabinete', '', '5323')";
        String r51 = "INSERT INTO ramal (nome_setor, sigla, ramal) VALUES ('NIT', '', '5392')";

        String[] dia_hora = {r, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, r27, r28, r29, r30, r31, r32, r34, r35, r36, r37, r38, r39, r40, r45, r46, r47, r48, r49, r50, r51};
        executeSQL(dia_hora);

    }

    public static void insertEmenta() {

        /*EMENTA PRIMEIRO SEMESTRE*/
        String ementa = "Limite e continuidade de funções, deruvada, aplicações de derivada e Regra de L’Hopital";
        String objetivo = "Proporcionar aos estudantes os fundamentos de Cálculo diferencial, necessários para o  desenvolvimento de suas habilidades matemáticas.  Envolver os estudantes na pesquisa matemática utilizando os recursos tecnológicos como softwares, entre outros compatíveis para estudo de Cálculo.";
        String metodologia = "Apresentação teórica dos fundamentos de cálculo. Desenvolvimento de atividades individuais e/ou em grupo que explorem as aplicações práticas dos conhecimentos de Cálculo, visando a sedimentação e a justificação deste por parte dos alunos. Práticas laboratoriais a fim de exploração dos recursos tecnológicos. ";
        String avaliacao = "Resolução de problemas nas avaliações escritas (são previstas quatro avaliações escritas). Análise das atividades prática-laboratoriais.";
        String conteudo_programatico = "UNIDADE I - LIMITE E CONTINUIDADE DE FUNÇÕES  \n1.1	– Introdução ao conceito de limite  \n1.2	– Definição de limite  \n1.3	– Técnicas para a determinação de limites  \n1.4	– Limites laterais  \n1.5	– Limites no infinito  \n1.6	– Limites infinitos  \n1.7	– Funções contínuas.\n UNIDADE II – DERIVADA \n2.1	– Retas tangentes e taxas de variação \n2.2	– As equações das retas tangentes e normais  \n2.3	– Definição da derivada  \n2.4	– Diferenciabilidade e continuidade  \n2.5	– Técnicas de diferenciação  \n2.6	– Derivada de funções exponenciais e logarítmicas.\n UNIDADE III – APLICAÇÕES DA DERIVADA  \n3.1	– Razão de variação  \n3.2	– Taxas relacionadas  \n3.3	– Extremos das funções (esboço dos gráficos de funções)  \n3.4	– O teorema do valor intermediário  \n3.4	– O teorema do valor médio  \n3.5	– Funções crescentes e decrescentes e o teste da derivada primeira  \n3.6	– Derivadas de ordem superior  \n3.8	– Concavidade e o teste da derivada Segunda  \n3.9	– Formas indeterminadas: Regra de L’Hôpital. ";
        String referencia = "\nÁVILA Geraldo. Introdução às Funções e à Derivada. Editora – São Paulo.  \n\nLEITHOLD, Louis. O cálculo com Geometria Analítica. Editora HARBRA ltda. São Paulo, SP. Volume I.  \n\nMUNEN, Mustafa A, FOULIS, David J. Cálculo. Supervisão da tradução de Mário Ferreira Sobrinho. – 2a ed. – Vol. 1, pp. 605 – Rio de Janeiro: Ed. Guamabara Dois, 1983.  \n\nSWOKOWSKI, Earl W. Cálculo com Geometria Analítica. Tradução Alfredo Alves de Faria, com a colaboração dos professores Vera Regina L.F. Flores e Marcio Quintão Moreno, 2. ed. - - São Paulo: Makron Books, 1994. \n\nWILLIAM, E. Boyer, RICARDO, C. Diprima. Equações Diferenciais Elementares e Problemas de Valor de Contorno. Traduzido por Natonio C. C. Carvalho e Carlos A. A. Carvalho. - 2a ed. – pp. 587 – Rio de Janeiro: Ed. GUANABARA KOOGA S.A. – 1990. ";
        String e1 = "INSERT INTO ementa_disciplina VALUES ('CET632','','" + ementa + "','" + objetivo + "','" + metodologia + "','" + avaliacao + "','" + conteudo_programatico + "','" + referencia + "')";

        ementa = "Estrutura da matéria, física do estado sólido e introdução à mecânica quântica.";
        objetivo = "O aluno deverá compreender os conceitos da Física aplicáveis na Ciência da Computação.";
        metodologia = "O curso constará de aulas expositivas. ";
        avaliacao = "Avaliar-se-á através de testes, provas e relatórios.";
        conteudo_programatico = "\n1.	Introdução histórica a mecânica quântica \n1.1.	Radiação de corpo negro  \n\n1.2.	Efeito fotoelétrico  \n1.3.	Teoria dos fótons  \n2.	Modelos atômicos  \n3.	Princípios fundamentais da mecânica quântica  \n4.	Métodos da mecânica quântica  \n5.	Mecânica quântica de átomos  \n6.	Estrutura dos cristais e ligações nos sólidos  \n7.	Teoria dos eletros livres nos sólidos  \n8.	Teoria de banda nos sólidos  \n9.	Semicondutores  \n10.	Dispositivos semicondutores. ";
        referencia = "\nALVARENGA,Beatriz. Física, São Paulo: Ed. Bernardo Álvares, 1975.  Volume 01.  \nALVES FILHO, Avelino, et al. Física. São Paulo: Ed. Ática 1984.  Volume 01.  \nAMALDI, Ugo. Imagens da Física. São Paulo: Ed.Scipione ,1997. 534 p.  \nHALLIDAY, D.; RESNICK, R; Krane, K. Física. Rio de Janeiro: L.T.C., 1973. Volume 01.  \nRAMALHO, Francisco et al. Os Fundamentos da Física. São Paulo: Ed. Moderna 1988. Volume 01.  \nNARCISO, Garcia, DAMASK, Arthur, SCHWARZ, Steven, Physics for Computer Science Students, Second Edition. Springer. ";
        String e2 = "INSERT INTO ementa_disciplina VALUES ('CET633','','" + ementa + "','" + objetivo + "','" + metodologia + "','" + avaliacao + "','" + conteudo_programatico + "','" + referencia + "')";

        ementa = "Estudos de diferentes textos da área através das várias estratégias de leitura, visando a aquisição de informações, a compreensão, análise, reflexão, discussão e interpretação de cada parágrafo; identificação das idéias gerais e específicas, e apreensão das variadas formas e funções da linguagem apresentadas no texto.";
        objetivo = "Capacitar os estudantes, através de diferentes estratégias, à leitura de textos autênticos da área nos vários níveis de compreensão, analisando e discutindo as idéias principais e específicas contidas em cada parágrafo. Utilizar as referências vividas e o conhecimento prévio (sistemático e asistemático) para a compreensão e discussão dos textos propostos.";
        metodologia = "O curso possibilitará oportunidades de leitura individual e/ou em grupo de vários textos da área bem como, trabalhos de pesquisa, discussões, debates e apresentações.O curso possibilitará oportunidades de leitura individual e/ou em grupo de vários textos da área bem como, trabalhos de pesquisa, discussões, debates e apresentações.";
        avaliacao = "Através do interesse, participação e desempenho nas atividades propostas.";
        conteudo_programatico = "\n1.	Estratégias de leitura. Leitura de textos para busca de informações gerais. Leitura de textos para busca de informações específicas.  \n2.	Cognatos e falsos cognatos.  \n3.	Descoberta do signficado através do contexto. Palavras-chave.  \n4.	Detalhes tipográficos: pontuação, figuras, tabelas, gráficos, fonte e tipo de letras, lay-out, letras maiúsculas e itálicas.  \n5.	Tipos de textos: intruções, descrições, relatórios de pesquisa ou experimentos, resumos, sinopses, artigos, etc.  \n6.	Estrutura do texto: título, subtítulos, parágrafos introdutórios e conclusão.  \n7.	Uso do dicionário.  \n8.	Texto e contexto.  \n9.	Tradução e interpretação de textos em Ciência da Computação.  \n10.	Gramática: números, artigos, pronomes, nomes, adjetivos, advérbios conjunções, tempos verbais, etc.";
        referencia = "\nDIAS, Reinildes. Reading Critically in English. Belo Horizonte: Ed. UFMG, 2002. 232 p.  \nHOLMES, John. Text tipology and the Preparation of Materials. Projeto Nacional do Ensino de Inglês Instrumental. São Paulo: Working Paper nº 10, 1984.  \nTORRES, Wilson. Gramática do Inglês Descomplicado. São Paulo: Ed. Moderna, 1987.  \nARAÚJO, A. D.; SILVA, S. M. S. Inglês Instrumental – Caminhos para a leitura. Alínea Publicações Editora. 2002.  \nBURNS, L. D.; BRYANT, N. O. The Business of Fashion: Designing, Manufacturing and Marketing. Fairchild Books & Visuals. 1997  \nMUNHOZ, R. Inglês instrumental: Estratégias de leitura - módulo I. São Paulo: Editora Texto Novo, 2001.  \nMUNHOZ, R. Inglês instrumental: Estratégias de leitura - módulo II. São Paulo: Editora Texto Novo, 2001.";
        String e3 = "INSERT INTO ementa_disciplina VALUES ('LTA332','','" + ementa + "','" + objetivo + "','" + metodologia + "','" + avaliacao + "','" + conteudo_programatico + "','" + referencia + "')";

        ementa = "História da Computação. Visão geral de um sistema computacional. Sistemas numéricos: decimais, octal, hexa e binário. Representação e armazenamento da informação. Arquitetura de Von Neumann: memória, processador, unidades de entrada e saída, barramentos, gargalo de Von Neumann. Introdução ao Sistema Operacional. Introdução a rede de computadores. ";
        objetivo = "Apresentar o mundo da computação ao aluno na forma mais abrangente possível, com a intenção de não apenas construir uma base para as futuras disciplinas, mas também de permitir que acompanhe e compreenda as novidades que ocorrem diariamente;";
        metodologia = "O conteúdo da disciplina será desenvolvido através de aulas expositivas da teoria, de seminários temáticos e de trabalhos individuais e em trabalhos em grupo.";
        avaliacao = "Avaliar-se-á através da realização de quatro créditos teóricos, onde poderão ser utilizados provas, avaliação de seminários, trabalhos individuais e em grupo.";
        conteudo_programatico = "\n1.	História da computação \n1.1.	Ábaco \n1.2.	Os grandes nomes da história da computação e suas invenções \n1.3.	Allan M. Turing \n1.4.	von Newman \n1.5.	Evolução da física dos elementos sólidos \n2.	•Sistemas numéricos \n2.1.	Decimal, binário, octal e hexadecimal \n2.2.	Conversão entre as diferentes bases \n2.3.	Aritmética binária – 4 operações \n3.	Ponto Flutuante (decimal e binário) \n3.1.	Conversão entre as diferentes bases \n3.2.	Aritmética de ponto flutuante (+ - * / ) \n4.	Representação e armazenamento da informação \n4.1.	O que é dado e o que é informação \n4.2.	Tipos de dados e suas características \n5.	Unidades componentes de hardware \n5.1.	CPU \n5.1.1.	UC \n5.1.2.	ULA \n5.1.3.	Cache \n5.1.4.	Registradores \n5.2.	Barramentos \n5.2.1. Internos a CPU \n \n5.2.2. Comunicação entre os componentes da CPU 5.2.3. Externos a CPU \n5.3. Memória \n \n5.3.1. Operações básicas: leitura e gravação 5.3.2. Modificação e exclusão 5.3.3. Hierarquia \n \n5.3.4. Tipos de memórias 5.3.5. Tempo de acesso \n5.3.6. Tecnologias: RAM, ROM, ótica, eletromagnética, magnética, etc. 5.4. Periféricos \n \n5.4.1. Funções básicas 5.4.2. Dispositivos de Entrada 5.4.3. Dispositivos de saída 5.4.4. Aspectos de velocidade: interfaces \n \n5.4.5. Protocolo de comunicação: ACK/NAK 5.4.6. Formas de comunicação: serial e paralela \n5.4.7. Formas de transmissão: simplex, half-duplex e full-duplex \n6.	Introdução a sistemas operacionais \n6.1.	O que é, função, características, etc. \n\n\n6.2.	Processo de inicialização do SO (boot) \n6.3.	Fase de testes e carga do SO \n7.	Introdução a Redes de Computadores";
        referencia = "\nTANENBAUM, Andrew S. Organização Estruturada de Computadores. L.T.C., 1999. 460 p.  \nBIGGS, Norman L. Introduction to Computing With Pascal ( Oxford Science Publications)  \nWHITE, Ron. Como Funciona o Computador. Quark do Brasil LTDA, 1995. 218 p.  \nHelp Informática - Ed. Globo.";
        String e4 = "INSERT INTO ementa_disciplina VALUES ('CET634','','" + ementa + "','" + objetivo + "','" + metodologia + "','" + avaliacao + "','" + conteudo_programatico + "','" + referencia + "')";

        ementa = "Conceitos básicos de algoritmos. Construção de algoritmos: estrutura de um programa, tipos de dados escalares e estruturados , estruturas de controle.  Prática em construção de algoritmos: transcrição para uma linguagem de programação, depuração e documentação.";
        objetivo = "Desenvolver o raciocínio lógico e a capacidade de abstração de maneira intuitiva, tornando o aluno apto a propor soluções algorítmicas.";
        metodologia = "Aulas teóricas e práticas, iniciando com portugol e introduzindo paralelamente uma linguagem de programação. ";
        avaliacao = "Avaliação escrita e trabalho computacional.";
        conteudo_programatico = "\n1.	CONCEITOS BÁSICOS DE ALGORITMOS \n.	Introdução; \n \n.	O que é programa; \n \n.	Algoritmo; \n \n.	Linguagem de Programação; \n \n.	Portugol e fluxograma; \n \n2.	CONSTRUÇÃO DE ALGORITMOS \n.	Sintaxe x Semântica \n \n.	Elementos de uma Linguagem de Programação; \n. \n.	Tipos simples de dados: inteiro, caracter, real, lógico; \n \n.	Variáveis; \n \n.	Estrutura básica de um algoritmo em portugol; \n \n.	Atribuição, leitura e escrita; \n \n.	Operadores aritméticos, lógicos e relacionais; \n \n.	Documentação; \n \n3.	INTRODUÇÃO A UMA LINGUAGEM DE PROGRAMAÇÃO \n \n.	Linguagem de programação x Portugol; \n \n.	Depuração; \n \n.	Estruturas de controle condicional e de repetição na linguagem de programação; \n \n.	Tipos estruturados: \n \n.	Unidimensional; \n \n.	Cadeia de caracteres; \n \n.	Multidimensional; \n \n.	Heterogêneo (registro);";
        referencia = "\nFARRER, Harry et al. Algoritmos Estruturados. 3 ed. Rio de Janeiro: LTC, 1999. 260 p. \n \nSCHILDT, Herbert. C Completo e Total. São Paulo: Makron, McGraw-Hill, 1990. 889 p \n \nGUIMARÃES, A. de M; LAGES, N. A. de C. Algoritmos e Estruturas de Dados.L.T.C, 1994. 216 p. \n \nTREMBLAY, Jean-Paul; BUNT, R.B. Ciência dos Computadores: uma Abordagem Algorítmica. Markon, 1997. 384 p. \n \nSALVETTI, D. D.; BARBOSA, L. M. Algoritmos. Markon, 1997. 274 pg.";
        String e5 = "INSERT INTO ementa_disciplina VALUES ('CET635','','" + ementa + "','" + objetivo + "','" + metodologia + "','" + avaliacao + "','" + conteudo_programatico + "','" + referencia + "')";

        ementa = "Análise lógica da linguagem cotidiana. Sentido lógico-matemático convencional dos conectivos. Simbologia de sentenças da linguagem cotidiana. Lógica Proposicional. Tabelas-verdade. Lógica de 1ª Ordem. Teoria e sistema formal.";
        objetivo = "Fornecer aos discentes um embasamento teórico formal das Lógicas Proposicional e de Predicados. Desenvolver o pensamento crítico e a argumentação lógica.";
        metodologia = "Aulas expositivas e exercícios.";
        avaliacao = "Quatro provas escritas.";
        conteudo_programatico = "\n1.	Introdução à Lógica \n \n2.	Lógica proposicional \n.	Sintaxe (simbologia) \n \n.	Semântica (interpretação) \n \n.	Método de prova: tabela-verdadeSatisfatibilidade \n \n.	Conseqüência lógica \n \n.	Formas normais: conjuntiva e disjuntiva \n \n3.	Lógica de 1ª Ordem ou Lógica de predicados \n\n.	Quantificadores (existencial e universal) \n \n.	Sintaxe (simbologia) \n \n.	Semântica (interpretação): conjunto universo e predicados \n \n4.	Teoria de 1ª Ordem \n.	Decidibilidade \n \n.	Teoria axiomatizável \n \n5.	Sistema formal axiomático \n.	Linguagem \n \n	Axiomas \n \n	Regras de inferência: modus ponens, modus tollens, etc.";
        referencia = "\nCOPI, M. Introdução à Lógica. Rio de Janeiro: Mestre Jou, 1978. 488 p. FILHO, A. Iniciação à Lógica Matemática. São Paulo:Nobel, 1986. 202 p. \n \nCHANG, C. & LEE, R. Symbolic Logic and Mechanical Theorem Proving. Academic Press, 1973. \n \nGERSTING, J. L. Fundamentos Matemáticos para a Ciência da Computação. LTC editora, 2001. ";
        String e6 = "INSERT INTO ementa_disciplina VALUES ('CET636','','" + ementa + "','" + objetivo + "','" + metodologia + "','" + avaliacao + "','" + conteudo_programatico + "','" + referencia + "')";

        ementa = "Pesquisa científica. Planejamento da pesquisa. Elementos dos textos científicos. Elaboração de um texto científico. ";
        objetivo = "Fornecer aos discentes um embasamento teórico formal das Lógicas Proposicional e de Predicados. Desenvolver o pensamento crítico e a argumentação lógica.";
        metodologia = "Apresentar a formatação e implementação de textos científicos, através da análise e produção de textos.";
        avaliacao = "Provas objetivas e subjetivas, avaliação de textos produzidos pelos alunos.";
        conteudo_programatico = "\n?	1.Ciência e conhecimento científico \n \n?	2.A pesquisa científica \n \n?	3.O ciclo de vida da pesquisa científica \n \n?	4.O papel da pesquisa no processo de aprendizagem \n \n?	5.Tipos de pesquisa \n \n?	6.Estudos exploratórios e referencial teórico \n \n?	7.Planejamento da pesquisa \n \n?	8.Desenvolvimento da argumentação \n \n?	9.Hipóteses e evidências \n \n?	10.Análise de dados \n \n?	11.Elementos presentes nos textos científicos \no	11.1.Escrita de introduções e conclusões \no	11.2.Escrita com clareza, precisão, fluidez, objetividade \n \n?	12.Artigos, Relatórios, Monografias, Dissertações, Teses \n \n?	13.Elaboração de um texto científico.";
        referencia = "\nCERVO, AL. L; BERVIAN, P., A Metodologia Científica. São Paulo: McGraw-Hill, 1983. FAZENDA, I. , Metodologia da Pesquisa. São Paulo: Cortez, 1997. \n \nGIL, A. C., Métodos e Técnicas de Pesquisa Social. 5. ed. São Paulo: Atlas, 1999. \n \nLAKATOS, E. M. e MARCONI M. de A., Fundamentos de Metodologia Científica. 4. ed. rev. e amp,l. São Paulo: Atlas, 2001";
        String e7 = "INSERT INTO ementa_disciplina VALUES ('FCH310','','" + ementa + "','" + objetivo + "','" + metodologia + "','" + avaliacao + "','" + conteudo_programatico + "','" + referencia + "')";

        /*       segundo semestre       */
        /* ALGEBRA E GEOMETRIA ANALÍTICA*/
        ementa = "Álgebra vetorial. O Espaço Vetorial Rn. Estudo da reta no plano e no espaço. Distância Euclidiana. Coordenadas Polares. Translação e rotação de eixos. Matrizes, Sistemas Lineares e Determinantes.";
        objetivo = "Apresentar os conceitos básicos de geometria analítica no plano e no espaço, criando subsídios para aplicar os conhecimentos em outras disciplinas.";
        metodologia = "Aulas expositivas. Uso de software para mostrar os conceitos.";
        avaliacao = "Provas teóricas.";
        conteudo_programatico = "\n1.\tÁlgebra Vetorial: O conceito de Vetor. Operações com Vetores: adição, multiplicação por escalar, produto escalar, produto vetorial, produto misto. Dependência e Independência Linear. Bases ortogonais e ortonormais. Ângulo entre vetores. \n \n2.\tRetas e Planos: Coordenadas Cartesianas. Equações do Plano. Ângulo entre Dois Planos. Equações de uma Reta no Espaço. Ângulo entre Duas Retas. Distâncias: de um ponto a um plano, de um ponto a uma reta, entre duas retas. Interseção de planos. \n \n3.\tTranslação e rotação de eixos.  Sistemas de coordenadas cartesiano e polar. \n \n4.\tMatrizes: Definição. Operações Matriciais: adição, multiplicação, multiplicação por escalar, transposta. Propriedades das Operações Matriciais. \n \n5.\tSistemas de Equação Lineares: Matrizes Escalonadas. O processo de Eliminação de Gauss - Jordan. Sistemas Homogêneos. \n \n6.\tInversa de uma matriz: definição e cálculo. \n \n7.\tDeterminantes: Definição por cofatores. Propriedades. Regra de Cramer.";
        referencia = "\nFARRER, Harry et al. Algoritmos Estruturados. 3 ed. Rio de Janeiro: LTC, 1999. 260 p. \n \nSCHILDT, Herbert. C Completo e Total. São Paulo: Makron, McGraw-Hill, 1990. 889 p \n \nGUIMARÃES, A. de M; LAGES, N. A. de C. Algoritmos e Estruturas de Dados.L.T.C, 1994. 216 p. \n \nTREMBLAY, Jean-Paul; BUNT, R.B. Ciência dos Computadores: uma Abordagem Algorítmica. Markon, 1997. 384 p. \n \nSALVETTI, D. D.; BARBOSA, L. M. Algoritmos. Markon, 1997. 274 pg.  ";
        String e8 = "INSERT INTO ementa_disciplina VALUES ('CET638','','" + ementa + "','" + objetivo + "','" + metodologia + "','" + avaliacao + "','" + conteudo_programatico + "','" + referencia + "')";

        /*CÁLCULO APLICADO II*/
        ementa = "Integração, integral definida, aplicações de integral defina, técnicas de integração, sequências e séries.";
        objetivo = "Proporcionar aos estudantes a continuidade de seus conhecimentos adquiridos em Cálculo I, necessários para o desenvolvimento de suas habilidades matemáticas. Envolver os estudantes na pesquisa matemática utilizando os recursos tecnológicos como softwares, entre outros compatíveis para estudo de Cálculo.";
        metodologia = "Discussão teórica dos conteúdos de Cálculo, de acordo com a ementa, desenvolvimento de atividades individuais e/ou em grupo, aulas teóricas e práticas laboratoriais a fim de exploração dos recursos tecnológicos e aplicação dos princípios do cálculo a problemas reais.";
        avaliacao = "Resolução de problemas nas avaliações escritas (são previstas quatro avaliações escritas). Análise das atividades prática-laboratoriais.";
        conteudo_programatico = "\nUNIDADE I \n \n1.1 – ANTIDIFERENCIAÇÃO \n•\tRegras básicas para antidiferenciação \n \n•\tMudança de variáveis em integrais indefinidas \n \n1.2 – INTERGRAL DEFINIDA \n \n•\tNotação Sigma e suas propriedades \n•\tÁrea como limite de soma \n•\tA integral definida \n•\tPropriedades da integral definida \n•\tTeorema do valor médio para integrais \n•\tTeorema fundamental de Cálculo \n•\tAplicações \n \nUNIDADE II \n \n2.1 - APLICAÇÕES DA INTEGRAL DEFINIDA \n•\tÁrea \n \n•\tVolume de sólido de revolução \n \n•\tComprimento de arco e superfícies de revolução \n \n•\tÁrea e comprimento de arco em coordenadas polares \n \n•\tMomentos e centros de massa \n \n•\tOutras aplicações \n \nUNIDADE III \n \n3.1 - TECNICAS DE INTEGRAÇÃO \n \n•\tIntegração por parte \n \n•\tSubstituição trigonométrica \n \n•\tIntegrais de funções racionais por frações parciais \n \nUNIDADE IV \n \n4.1 SEQÜÊNCIAS \n \n•\tLimite de uma seqüência \n \n•\tPropriedades dos limites de seqüência \n \n•\tTeoremas \n \n•\tAplicações \n \n4.2 SÉRIES NUMÉRICAS \n \n•\tConvergência e divergência \n \n•\tSéries de termos positivos \n \n•\tOs testes da razão e da raiz \n \n•\tTeste do n-ésimo termo \n \n•\tSéries de Maclaurin e de Taylor \n \n•\tAplicações dos polinômios de Taylor";
        referencia = "\n\nLEITHOLD, Louis. O Cálculo com Geometria Analítica, São Paulo, Harbra. Vol. 1.  \n\nMUNEM, Mustafa A. ; Foulis David J. Cálculo. Rio de Janeiro, Guanabara Dois. Vol. 1  \n\nSWOKOWSKI, Earl William. O Cálculo com Geometria Analítica. São Paulo, McGraw-Hill. Vol 1  \n\nTHOMAS Júnior; GEORGE, B.; FINNEY, Ross L. Cálculo e Geometria Analítica. Rio de Janeiro. Livros Técnicos e Científicos Ltda. Vols 1, 2 e 3.  \n\nTHOMAS Júnior, George B. Cálculo. Rio de Janeiro. Livros Técnicos e Científicos Ltda. Vols 1, 2 e 3.";
        String e9 = "INSERT INTO ementa_disciplina VALUES ('CET639','CÁLCULO APLICADO I','" + ementa + "','" + objetivo + "','" + metodologia + "','" + avaliacao + "','" + conteudo_programatico + "','" + referencia + "')";

         /*Eletrônica*/
        ementa = "Princípios básicos de eletrônica. Teoria dos circuitos. Diodos. Teoria da realimentação. Amplificadores Operacionais. Circuitos osciladores e conversores.";
        objetivo = "\nApresentar os principais componentes eletrônicos, suas características e comportamentos;  \nCompreender as bases físicas do funcionamento dos circuitos eletrônicos;  \nProjetar e analisar circuitos eletrônicos básicos utilizados em equipamentos profissionais;  \nMontar circuitos eletrônicos;  \nOperar diversos equipamentos eletrônicos, tais como: multimetro, gerador de função, osciloscópio, frequencímetro e outros.";
        metodologia = "Exposição interativa do conteúdo, práticas de laboratório; seminários e discussão de resultados experimentais";
        avaliacao = "\n•\tProvas teóricas \n•\tProvas práticas \n•\tRelatórios \n•\tSeminários";
        conteudo_programatico = "\n1.\tCircuitos de corrente contínua (DC) e Circuitos de Corrente Alternada (AC) 1.1.1. Conceitos de corrente; voltagem e resistência; \n1.1.2. Leis de Ohm; Joule; Kirchhoff; \n1.1.3. Teoremas de Thévenin e Norton; \n1.1.4. Circuitos seriais e paralelos. \n \n2.\tAnálise de circuito AC e DC. \n2.1.1.1.\tConceito de impedância e impedância complexa; \n2.1.1.2.\tCircuitos RLC; \n2.1.1.3.\tCircuitos em ponte; \n \n2.1.1.4.\tTransformadores. \n \n3.\tCircuitos com diodos \n3.1.1. Circuitos retificadores; \n3.1.2. Filtros; \n3.1.3. Reguladores de voltagem. \n \n4.\tCircuitos amplificadores \n4.1.1. Amplificadors de voltagem \n \n5.\tAmplificadores operacionais \n5.1.1. Amplificador com realimentação; \n5.1.2. Circuitos com amplificador operacional \n \n6.\tTecnologia de manufatura de circuitos integrais \n \n7.\tOsciladores \n7.1.1. Conceito de ralimentação \n7.1.2. Osciladores RC; \n7.1.3. Oscilador RC \n7.1.4. Osciladores a cristal; \n7.1.5. Geradores de formas de onda \n \n8.\tConversores A/D e D/A \n8.1. Métodos de conversão de Digital para Analógico \n8.2. Métodos de conversão de Analógico para Digital \n8.3. Características e parâmetros da conversão A/D e D/A \n8.4. Aplicações de conversão A/D e D/A";
        referencia = "\nBROPHY, James J. Basic Eletronics for Scientists. 5ª edição. Editora Mcgraw-Hill, 1989. \n \nCAPUANO, Francisco Gabriel ; IDOETA, Ivan V. Elementos de Eletrônica Digital. \n30ª edição. Editora Érica, 2000. \n \nMALVINO, Albert Paul. Eletrônica. 4ª edição. Ed. Makron Books , 1997. Vol.1. \n \nMALVINO, Albert Paul. Eletrônica. 4ª edição. Ed. Makron Books,1997. Vol.2. \n \nTUCCI ; BRANDASSI. Circuitos Básicos em Eletricidade e Eletrônica. 4ª edição. \nEd Nobel,1979.";
        String e10 = "INSERT INTO ementa_disciplina VALUES ('CET637','Física para Computação\nIntrodução à Ciência da Computação','" + ementa + "','" + objetivo + "','" + metodologia + "','" + avaliacao + "','" + conteudo_programatico + "','" + referencia + "')";

         /*FUNDAMENTOS MATEMÁTICOS PARA COMPUTAÇÃO*/
        ementa = "Introdução aos métodos de prova. Recursão e Recorrência. Teorema de Herbrand. Resolução. Grafos e árvores. Representação de um programa usando Lógica.";
        objetivo = "Apresentação dos fundamentos de Matemática Discreta, bem como desenvolver no aluno o senso crítico com relação à importância desta disciplina dentro da Ciência da Computação.";
        metodologia = "Aulas expositivas e listas de exercício.";
        avaliacao = "Três provas escritas (três créditos). \nListas de exercício (um crédito). \nProva final, se necessário.";
        conteudo_programatico = "\n1.\tMétodos de prova \n•Construção \n•Contradição \n•Indução \n \n2.\tTeorema de Herbrand \n \n4.\tRepresentação Clausal de Fórmulas \n \n•O Universo de Herbrand •Interpretação de Herbrand •Árvores Semânticas \n \n5.\tResolução \n \n7.\tO Princípio da Resolução (Lógica Proposicional) \n•Unificação \n•O Princípio da Resolução (Lógica de 1ª Ordem) \n \n8.\tRecursão e Recorrência \n \n11. Definição de Programas usando Lógica \n \n13.\tProblema do Término do Programa \n \n•Problema da Resposta •Problema da Corretude •Problema da Equivalência \n \n15.\tGrafos e Árvore \n \n•Definição e Terminologia de um Grafo •Aplicações de Grafos \n \n•Definição e Terminologia de uma Árvore •Aplicações de Árvores \n \n•Algoritmos de Percurso: busca em profundidade e largura ";
        referencia = "\nGERSTING, J. L. Fundamentos Matemáticos para a Ciência da Computação. LTC Editora, 2001. \n \nCHANG, C.; LEE, R. Symbolic Logic and Mechanical Theorem Proving. Academic Press, 1973.";
        String e11 = "INSERT INTO ementa_disciplina VALUES ('CET640','CET636','" + ementa + "','" + objetivo + "','" + metodologia + "','" + avaliacao + "','" + conteudo_programatico + "','" + referencia + "')";

        /*LINGUAGEM DE PROGRAMAÇÃO II*/
        ementa = "Construção de programas: modularização (função, procedimento e bibliotecas), passagem de parâmetros, tipos de dados dinâmicos, recursividade e arquivos. Uso de uma linguagem de programação.";
        objetivo = "Desenvolver o raciocínio lógico e a capacidade de abstração de maneira intuitiva, tornando o aluno apto a propor soluções algorítmicas.";
        metodologia = "Aulas teóricas e práticas utilizando uma linguagem de programação.";
        avaliacao = "Avaliação escrita e trabalho computacional.";
        conteudo_programatico = "\n1.\tTIPOS DE DADOS DINÂMICOS \n \n2.\tMODULARIZAÇÃO \n•\tIntrodução \n \n•\tFunção x Procedimento \n \n•\tPassagem de parâmetro \n \n•\tRecursividade \n \n•\tCriação de Bibliotecas \n \n3.\tARQUIVOS \n•\tIntrodução \n \n•\tDefinição \n \n•\tTipos de arquivos \n \n•\tComandos para se trabalhar com arquivos";
        referencia = "\nFARRER, Harry et al. Algoritmos Estruturados. 3 ed. Rio de Janeiro: LTC, 1999. 260 p. \n \nSCHILDT, Herbert. C Completo e Total. São Paulo: Makron, McGraw-Hill, 1990. 889 p \n \nGUIMARÃES, A. de M; LAGES, N. A. de C. Algoritmos e Estruturas de Dados.L.T.C, 1994. 216 p. \n \nTREMBLAY, Jean-Paul; BUNT, R.B. Ciência dos Computadores: uma Abordagem Algorítmica. Markon, 1997. 384 p. \n \nSALVETTI, D. D.; BARBOSA, L. M. Algoritmos. Markon, 1997. 274 pg.";
        String e12 = "INSERT INTO ementa_disciplina VALUES ('CET641','Linguagem de Programação I ','" + ementa + "','" + objetivo + "','" + metodologia + "','" + avaliacao + "','" + conteudo_programatico + "','" + referencia + "')";

        /*Lógica Digital I*/
        ementa = "Sistemas de Numeração; Códigos; Algebra de Boole; Circuitos Combinatórios, Dispositivos Programáveis.";
        objetivo = "Fornecer aos discentes um embasamento teórico formal da Lógica Digital, permitindo a compreensão e construção de sistemas eletrônicos digitais.";
        metodologia = "Aulas expositivas; seminários temáticos; trabalhos individuais e em grupos voltados à resolução de problemas padrões e cotidianos; utilização de meios computacionais para subsidiar informações e processos necessários às resoluções dos problemas propostos. Experiências em laboratório com kit digital e instrumentos de medição e controle.";
        avaliacao = "Avaliações escritas individuais, de conteúdo cumulativo, em número igual ao número de créditos da disciplina mais um, desprezando-se a menor nota, efetuando-se a média aritmética das demais; trabalhos escritos e orais, individuais e coletivos, cujas notas irão compor a média ponderada com a média das avaliações escritas individuais, fornecendo a avaliação global final.";
        conteudo_programatico = "\n1.\tSISTEMAS DE NUMERAÇÃO 1.1 Histórico \n1.2 Decimal \n1.3 Binário \n1.4 Hexadecimal \n1.5 Conversão entre os sistemas \n \n1.6 Representação computacional dos conjuntos numéricos \n2.\tCÓDIGOS \n2.1 BCD \n2.2 ASCII \n \n2.3 Gray \n2.4 Outros códigos \n \n3.\tÁLGEBRA DE BOOLE 3.1 Operação NÃO \n3.2 Operação E  \n3.3 Operação OU \n3.4 Operação OU EXCLUSIVO \n3.5 Operação NÃO E \n3.6 Operação NÃO OU \n3.7 Teorema de De Morgan \n3.8 Propriedades \n3.9 Simplificação de funções \n3.10 Formas normalizadas \n3.11 Mintermos e maxtermos \n3.12 Mapa de Karnaugh \n3.13 Síntese usando portas NÃO E  e  NÃO OU \n3.14 Funções especificadas incompletamente \n \n4.\tTECNOLOGIA DE CIRCUITOS INTEGRADOS \n4.1 TTL \n4.2 MOS \n4.3 CMOS \n5.\tPORTAS LÓGICAS \n5.1 Portas NOT, AND, OR, NAND, NOR, XOR \n \n6.\tCIRCUITOS COMBINATÓRIOS \n6.1. Codificador \n6.2. Multiplexer \n6.3. Demultiplexer \n6.4. Somador \n6.5. Subtractor \n6.6. ALUs \n6.7. Comparadores, circuitos de paridade \n \n6.8. PLDs combinatorios";
        referencia = "WAKERLY, John F. Digital Design principles & practices. 3ra Edição. Prentice Hall, 2001 , 945 pág. \n \nMANO, M. Morris; KIME, Charles R. Logic and Computer Design Fundamentals. 3. \ned. Pearson Prentice Hall, 2004, 645p. \n \nIDOETA, Ivan V.; GAPUANO, Francisco G. Elementos de Eletrônica digital. 35a  ed. \nErica Editores, 2003, 524p. \n \nERCEGOVAC,\tM.;\tLANG,\tT.;\tMORENO,\tJ.\tintroducao\taos\tsitemas\tdigitais. \nBookmanl, 1999, 453p.  ";
        String e13 = "INSERT INTO ementa_disciplina VALUES ('CET642','CET 636 – Lógica para Computação','" + ementa + "','" + objetivo + "','" + metodologia + "','" + avaliacao + "','" + conteudo_programatico + "','" + referencia + "')";

        /*       terceiro semestre       */
        /*ALGEBRA ABSTRATA*/
        ementa = "Estruturas algébricas. Princípios de contagem. Indução matemática. Álgebra universal.";
        objetivo = "Capacitar o aluno a manipular e provar propriedades sobre estruturas indutivas e não-indutivas e apresentar os principais conceitos de álgebra abstrata.";
        metodologia = "Aulas expositivas.";
        avaliacao = "Provas teóricas.  ";
        conteudo_programatico = "\n1.\tEstruturas algébricas Axiomas e teoremas. Métodos de prova. \n \nConjuntos, relações e funções. Relação de equivalência. \n \nTipos de funções: parcial, total, inversa, injetora, sobrejetora, bijetora. Grupo e semigrupo. \nPropriedades dos grupos. \nAnéis: propriedades e homomorfismos. \nReticulados: definição, isomorfismo. Reticulados algébricos. \n \n2.\tPrincípios de contagem \n \nCardinalidade de conjunto e de união de conjuntos. Enumeração de conjunto. \nDiagonalização. \n \n3.\tIndução Matemática Princípio de indução. Provas por indução. \n \nErros comuns em indução. Definições indutivas. \nResolução de equações recorrentes. \n \nPrimeiro Princípio de Indução (Indução Fraca). Segundo Princípio de Indução (Indução Forte). \n \n4.\tÁlgebra universal Definição e exemplos. Álgebras isomórficas. Homomorfismos e isomorfismos.";
        referencia = "MENEZES, Paulo Blauth. Matemática Discreta para Computação e Informática. \n \nEditora SAGRA, 2004. \n \nSCHEINERMAN, Edward R. Matemática Discreta. Editora: Thomson Pioneira, 2002. \n \nGRAHAM, R.; KNUTH, D.; PATASHNIK, O . Concrete Mathematics: A Foundation for Computer Science. Editora Addison-Wesley, 1994. \n \nROSEN, H. Kenneth. Discrete Mathematics and Its Applications. Editora McGraw-Hill, 2003. \n \nGERSTING, Judith L. Fundamentos matemáticos para a Ciência da Computação. \nEditora LTC, 2001. \n \nGERSTING, Judith L. Mathematical Structures for Computer Science : A Modern Treatment of Discrete Mathematics. Editora W. H. Freeman, 2002. \n \nBURRIS, S.; SANKAPPANAVAR. A course in Universal Algebra. Springer-Verlag, New York, 1981.";
        String e14 = "INSERT INTO ementa_disciplina VALUES ('CET074','CET 638 – ALGEBRA E GEOMETRIA ANALÍTICA ','" + ementa + "','" + objetivo + "','" + metodologia + "','" + avaliacao + "','" + conteudo_programatico + "','" + referencia + "')";

        /*CÁLCULO APLICADO III*/
        ementa = "Funções de várias variáveis, integrais múltiplas.";
        objetivo = "Proporcionar aos estudantes a continuidade de seus conhecimentos adquiridos em Cálculo II, necessários para o desenvolvimento de suas habilidades matemáticas. Envolver os estudantes na pesquisa matemática utilizando os recursos tecnológicos como softwares, entre outros compatíveis para estudo de Cálculo.";
        metodologia = "Proporcionar aos estudantes a continuidade de seus conhecimentos adquiridos em Cálculo II, necessários para o desenvolvimento de suas habilidades matemáticas. Envolver os estudantes na pesquisa matemática utilizando os recursos tecnológicos como softwares, entre outros compatíveis para estudo de Cálculo.Discussão teórica dos conteúdos de Cálculo, desenvolvimento de atividades individuais e/ou em grupo, aulas teóricas e práticas laboratoriais a fim de exploração dos recursos tecnológicos.";
        avaliacao = "Resolução de problemas nas avaliações escritas (são previstas quatro avaliações escritas). Análise das atividades prática-laboratoriais.";
        conteudo_programatico = "\nUNIDADE I: DIFERENCIAÇÃO PARCIAL \n \n•\tFunções de várias variáveis \n•\tLimites e continuidade \n•\tDerivadas parciais \n•\tIncrementos e diferenciais \n•\tDerivadas parciais de ordem superior \n•\tExtremos de funções de diversas variáveis \n•\tMultiplicadores de Lagrange \n \nUNIDADE II: INTEGRAIS MULTIPLAS \n \n•\tIntegrais \n•\tÁrea e volume";
        referencia = "ÁVILA, G.. Cálculo 3: funções de várias variáveis. Rio de Janeiro: Livros Técnicos e Científicos Editora, 1983. \n \nKAPLAN, W e LEWIS, D.J.. Cálculo e Álgebra Linear. Rio de Janeiro: Livros Técnicos e Científicos S.A., 1973. Vol. 4. \n \nLEITHOLD, Louis. O Cálculo com Geometria Analítica, São Paulo: Harbra. Vol. .2 \n \nMUNEM, Mustafa A. e Foulis David J. Cálculo. Rio de Janeiro: Guanabara Dois. Vol. 1 e 2 \n \nSPIEGEL, M.R.. Cálculo Avançado: resumo da teoria. São Paulo: McGraw-Hill do Brasil, 1972. \n \nSWOKOWSKI, Earl William. O cálculo com Geometria Analítica. São Paulo: McGraw-Hill. Vol 1 e 2 \n \nTHOMAS Júnior, George B. e FINNEY, Ross L. Cálculo e Geometria Analítica. Rio de Janeiro: Livros Técnicos e Científicos Ltda. Vols 1, 2 e 3. \n \nTHOMAS Júnior, George B. Cálculo. Rio de Janeiro: Livros Técnicos e Científicos Ltda. \nVols 1, 2 e 3.";
        String e15 = "INSERT INTO ementa_disciplina VALUES ('CET075','CET 639 – CÁLCULO APLICADO II','" + ementa + "','" + objetivo + "','" + metodologia + "','" + avaliacao + "','" + conteudo_programatico + "','" + referencia + "')";

        /*Estrutura de dados*/
        ementa = "Representação de dados. Estruturas lineares: vetor, lista, pilha e fila. Recursão. Árvores binárias. Árvores de busca. Árvores balanceadas. Algoritmos para manipulação de estruturas: inserção, remoção, busca e percurso. Ordenação de dados. Heaps. Filas com prioridades. Noções de complexidade dos algoritmos utilizados.";
        objetivo = "Capacitar o aluno a manipular estruturas de dados e ter noção da complexidade de algoritmos.";
        metodologia = "Aulas expositivas.";
        avaliacao = "Provas teóricas e práticas. Trabalhos de implementação.";
        conteudo_programatico = "\n1. Tipos abstrados de dados. Revisão de Recursão. \n2. Para cada estrutura de dados: apresentar a definição e algoritmos para \ninserção, remoção e busca de dados e algoritmos de percurso. Apresentar \nalgoritmos recursivos e iterativos. Para cada algoritmo, apresentar uma noção \nde sua complexidade. \no Vetores. \no Pilhas. \no Filas. \no Listas encadeadas. \no Listas duplamente encadeadas. \no Árvores binárias (de busca). \no Árvores balanceadas. \n3. Utilização de pilha para simulação da recursão. \n4. Ordenação de dados: apresentar algoritmos com complexidade: linear, n log n, \nn2. \n5. \"Heaps\": definição e implementação em vetores. Filas com prioridades: \ndefinição e implementação com heaps.";
        referencia = "LEISERNON, Charles E. et all. Algoritmos - Trad. 2ª Ed. Americana. Editora \nCampus, 2002. \nPREISS, Bruno. Estruturas de Dados e Algoritmos. Editora Campus, 2001. \nDROZDEK, Adam. Estruturas de Dados e Algoritmos em C++. Thomson Pioneira,2001. \nLAFORE, Robert. Aprenda em 24 Horas Estruturas de Dados e Algoritmos. \nCampus, 1999.";
        String e16 = "INSERT INTO ementa_disciplina VALUES ('CET077','CET 641 – Linguagem de Programação II','" + ementa + "','" + objetivo + "','" + metodologia + "','" + avaliacao + "','" + conteudo_programatico + "','" + referencia + "')";

        /*FUNDAMENTOS DE ECONOMIA*/
        ementa = "A Ciência Econômica. Evolução histórica e doutrinas econômicas. Organização da atividade econômica. Sistema de mercados: oferta x demanda. Teoria Elementar da demanda e da oferta. Mercados competitivos e mercados imperfeitos. Teoria da Produção: fatores e funções de produção. Custos e estrutura dos custos de produção. Teoria dos Jogos.";
        objetivo = "Desenvolver instrumentos e fundamentos analíticos baseados em referenciais teóricos básicos de fácil entendimento na área de conhecimento da disciplina, que possibilitem ao aluno a compreensão e análise dos principais fenômenos e comportamentos dos agentes econômicos.";
        metodologia = "A disciplina será conduzida a partir de aulas expositivas, discussões e ilustrações sobre cada assunto. Leitura coletiva e apresentações orais e escritas também serão estimuladas em classe.";
        avaliacao = "Participação, leituras, apresentação oral de textos específicos, nível de freqüência, provas individuais e trabalhos em grupo, entre outros, constituem a base do processo de avaliação.";
        conteudo_programatico = "\n1. A ciência econômica \n1.1 Definição e objeto \n1.2 Inter-relações e aspectos metodológicos \n1.3 Evolução histórica das escolas e doutrinas econômicas: caracterização das principais linhas do pensamento econômico – Adam Smith, David Ricado,John Stuat Mill, Malthus, entre outros \n2. Problemas econômicos \n2.1 Problemas centrais da economia, necessidades, escassez, pleno emprego, \neficácia e eficiência, fatores de produção \n2.2 Curva de possibilidades de produção e custo de oportunidade \n3. Organização econômica \n3.1 Funcionamento do modelo simplificado de mercado: fluxos real e monetário \n3.2 Mercado de fatores de produção e mercado de produtos \n3.3 Classificação de bens e setores econômicos \n4. Os sistemas econômicos contemporâneos \n4.1 A livre iniciativa, os sistemas mistos e as economias planificadas \n5. Teorias explicativas do valor \n5.1 Teorias objetiva e subjetiva do valor \n6. Formação do preço \n6.1 Leis e estruturação das curvas de procura e oferta \n6.2 Equilíbrio e mudança de equilíbrio \n6.3 Elasticidade: tipos, cálculo, interpretação e aplicação de conceitos \n7. Fundamentos teóricos da produção \n7.1 Fundamentos teóricos da produção \n7.2 Custos e rendimentos \n7.3 Análise e interpretação das curvas \n8. Mercados e concorrência \n8.1 Classificação e conceitos \n8.2 Funcionamentos das diferentes estruturas de mercados: monopólio, oligopólio, concorrência monopolista, monopsônio, oligopsônio, dentre outros \n8.3 Estabelecimento das condições de otimização sob diferentes estruturas";
        referencia = "ROSSETI, José Paschoal. Introdução à economia. São Paulo: Atlas. 1997. PINHO, Diva Benevides (org.). Manual de Economia da USP. 4. ed. São Paulo: Ed. Saraiva, 2003. 606 p. \n\nPINDYCK, Robert S.; Rubinfeld, Daniel L. Microeconomia. 4. ed. São Paulo: Makron Books Ltda. 1999. \n\nARAÚJO, Carlos Roberto Vieira. História do pensamento econômico: uma abordagem introdutória. São Paulo: Atlas, 1988, 158 p. \n\nCARVALHO, Luiz Carlos P. Microeconomia introdutória para cursos de administração e contabilidade. Com questões e soluções. Segunda edição. São Paulo: Atlas. 2000. \n\nGILSON, de Lima Garófalo; LUIZ Carlos P. Carvalho. Teoria Microeconômica. 3. ed. São Paulo: Atlas. \n\nLAGES, Beatriz Helena Gelas; MILONE, Paulo Cezar. Economia do turismo. 7. ed. São Paulo: Atlas, 2001, 226 pág. Capítulo I – página 17 - 34. \n\nPASSOS, Carlos Alberto Martins; NOGAMI, Otto. Princípios de economia. São Paulo: Pioneira, 3. Ed. 2001, 475 p. \n\nPASSOS, Carlos Alberto Martins; NOGAMI, Otto. Lista de exercícios. São Paulo: Pioneira, 3. Ed. 2001. \n\nREIS, Ricardo Pereira. Introdução à teoria econômica. Lavras: UFLA – FAEPE, 1998, 108 p. \n\nRIANI, Flávio. Economia: Princípios básicos e introdução à microeconomia. São Paulo: Pioneira, 1998, 178 p. \n\nSAMUELSON, Paul; NORDHAUS, William D. Economia. 16 ed. Rio de Janeiro: McGrawHill, 1999, 779 p. \n\nVARIAN, Hall R. Microeconomia: princípios básicos. Tradução da Segunda edição original. Rio de Janeiro: Campus, 1994. 710 p.";
        String e17 = "INSERT INTO ementa_disciplina VALUES ('CAE015','','" + ementa + "','" + objetivo + "','" + metodologia + "','" + avaliacao + "','" + conteudo_programatico + "','" + referencia + "')";

        /*LINGUAGEM DE PROGRAMAÇÃO III*/
        ementa = "Programação orientada a objetos. Conceitos e terminologia: objetos, classes, métodos, mensagens, herança, polimorfismo, encapsulamento. Utilização de uma linguagem orientada a objetos.";
        objetivo = "Possibilitar que o aluno modele problemas de acordo com a filosofia de orientação a objetos e implemente-os através de uma linguagem de programação apropriada.";
        metodologia = "Aulas teóricas e práticas utilizando uma linguagem de programação orientada a objetos.";
        avaliacao = "Avaliação escrita e trabalho computacional.";
        conteudo_programatico = "\n1. INTRODUÇÃO AO PARADIGMA DE OBJETOS \n\uF0A7 Introdução; \n\uF0A7 Mundo real x objetos ; \n2. O MODELO DE OBJETOS \n\uF0A7 Objetos e classes; \n\uF0A7 Instâncias; \n\uF0A7 Atributos; \n\uF0A7 Métodos; \n\uF0A7 Encapsulamento e ocultamento da informação; \n\uF0A7 Agregação; \n\uF0A7 Generalização/especialização e Herança; \n\uF0A7 Polimorfismo;";
        referencia = "SANTOS, Rafael. Introdução à Programação Orientada a Objetos Usando Java. Rio de Janeiro: Campus, 2003. 352 p. Tutorial de Java da Sun. Disponível em: www.java.sun.br. Acessado em: 28 de outubro de 2004. \n\nDEITEL, H. M.; DEITEL, P. J. Java como programar. 3. ed. Porto Alegre: Bookman, 2001. 1201p. \n\nCOAD, P.; YOURDON, E. Análise baseada em objetos. 2. ed. rev. Rio de Janeiro: Campus, 1991. 225p. \n\nCOAD, P; YOURDON, E. Projeto baseado em objetos. Rio de janeiro: Campus, c1993. 195p. \n\nMEYER, B. Object-oriented software construction. 2ª ed. Upper Saddle River, NJ: Prenteice-Hall PTR, c1997. 1254p.";
        String e18 = "INSERT INTO ementa_disciplina VALUES ('CET078','CET 641 – Linguagem de Programação II','" + ementa + "','" + objetivo + "','" + metodologia + "','" + avaliacao + "','" + conteudo_programatico + "','" + referencia + "')";

        /*Lógica Digital II*/
        ementa = "Circuitos Seqüenciais; Memórias; Dispositivos Programáveis.";
        objetivo = "Fornecer aos discentes um embasamento teórico formal da Lógica Digital, permitindo a compreensão e construção de sistemas eletrônicos digitais.";
        metodologia = "Aulas expositivas; seminários temáticos; trabalhos individuais e em grupos voltados à resolução de problemas padrões e cotidianos; utilização de meios computacionais para subsidiar informações e processos necessários às resoluções dos problemas propostos. Experiências em laboratório com kit digital e instrumentos de medição e controle.";
        avaliacao = "Avaliações escritas individuais, de conteúdo cumulativo, em número igual ao número de créditos da disciplina mais um, desprezando-se a menor nota, efetuando-se a média aritmética das demais; trabalhos escritos e orais, individuais e coletivos, cujas notas irão compor a média ponderada com a média das avaliações escritas individuais, fornecendo a avaliação global final.";
        conteudo_programatico = "1. CIRCUITOS SEQÜENCIAIS \n1.1 Latch RS \n1.2. Flip-flops Mestre-escravo \n1.3. Flip-flops de transição \n1.4. Flip-flops tipo JK, D, T \n1.5. Registradores \n1.6. Contadores assíncronos e síncronos \n1.7. Registradores de deslocamento \n2. MEMÓRIAS \n2.1 Tipos de Memórias \n2.2 ROM \n2.3 RAM \n2.4 RAM estatica e dinâmica \n2.5 outros tipos de RAM \n3. DISPOSITIVOS PROGRAMÁVEIS \n3.1 CPLD da Xilinx \n3.2 FPGAs da Xilinx";
        referencia = "WAKERLY, John F. Digital Design principles & practices. 3ra Edição. Prentice Hall, 2001 , 945 pág. \n\nMANO, M. Morris; KIME, Charles R. Logic and Computer Design Fundamentals. 3. ed. Pearson Prentice Hall, 2004, 645p. \n\nIDOETA, Ivan V.; GAPUANO, Francisco G. Elementos de Eletrônica digital. 35a ed. Erica Editores, 2003, 524p. \n\nERCEGOVAC, M.; LANG, T.; MORENO, J. Introducao aos sitemas digitais. Bookmanl, 1999, 453p.";
        String e19 = "INSERT INTO ementa_disciplina VALUES ('CET065','CET 642 – Lógica Digital I \nCET 637 - Eletrônica','" + ementa + "','" + objetivo + "','" + metodologia + "','" + avaliacao + "','" + conteudo_programatico + "','" + referencia + "')";

        /*materias do 4 semestre*/
        /* ANÁLISE DOS SISTEMAS DE INFORMAÇÃO*/
        ementa = "Análise de sistemas, histórico da análise, principais diagramas: DFD, MER e outros, análise orientada a objetos, UML: requisitos, casos de uso, diagramas de seqüência, colaboração, conceitual e de classes de projeto.";
        objetivo = "Dotar o aluno da capacidade de eliciar requisitos e responsabilidades de um sistema computacional a ser construído ou modificado e definir as estratégias para o desenvolvimento de forma que esta tarefa seja exeqüível contemplando economia de esforços e orientada para a efetiva resolução do problema a que se propõe.";
        metodologia = "Estudo sistemático de uma metodologia de modelagem de sistemas, UML, com acompanhamento de uma aplicação prática que será desenvolvida segundo a metodologia e ao longo do curso. Um estudo da história e dos princípios que regem a tarefa de análise de sistemas para dar ao aluno a necessária visão crítica do processo como um todo.";
        avaliacao = "Os alunos deverão desenvolver, em equipe, toda uma modelagem de sistema no decorrer do processo, seguindo passo a passo o que vai sendo apresentado. No final do curso eles deverão dispor, pois, de um modelo de um sistema computacional implementável e que será alvo de avaliação de seu desempenho. No decorrer, duas provas escritas também serão apresentadas com o objetivo de aferir a fixação dos conhecimentos teóricos que lhe são apresentados.";
        conteudo_programatico = "\n1. Introdução – O que é e porque se faz análise de sistemas \n2. Histórico da Modelagem \n2.1.Programação e Abstração \n2.2.Decomposição funcional \n2.3.Análise Estruturada \n2.3.1. O Diagrama de Fluxo de Dados (DFD) \n2.4.Engenharia da Informação \n2.4.1. O Diagrama (ou Modelo) de Entidades e Relacionamentos (MER ouDER) \n2.5.Análise Estruturada Moderna \n2.5.1. Outras ferramentas: DHF, DTE. \n2.6.Orientação Objetos \n3. Princípios de Administração da Complexidade \n4. UML \n4.1.Levantamento de Requisitos \n4.2.Casos de Uso \n4.2.1. Atores \n4.2.2. Tipos de Casos de Uso \n4.2.3. Seqüências típicas e alternativas de eventos \n4.2.4. Diagramas de Casos de Uso \n4.2.5. Casos de uso reais e essenciais \n4.3.Modelo Conceitual \n4.3.1. Identificar conceitos \n4.3.2. Identificar associações \n4.3.2.1. Notações em UML \n4.3.3. Identificar Atributos \n4.4.Glossário \n4.5.Comportamento do Sistema \n4.5.1. Diagramas de Seqüência \n4.5.1.1. Eventos e Operações \n4.5.1.2. Como construir diagramas de seqüência \n4.5.2. Contratos \n4.5.3. Diagramas de Colaboração \n4.5.3.1. Interações \n4.5.3.2. Como construir diagramas de colaboração \n4.5.3.3. Notações \n4.6.Diagramas de classe de projeto \n4.7.Mapeando o projeto para o código \n4.7.1. Criar classes a partir de diagramas de classes de projeto \n4.7.2. Criar métodos a partir de diagramas de colaboração ou de seqüência \n5. Conclusões e considerações sobre a tarefa de modelagem de sistemas";
        referencia = "LARMAN, Greg, Utilizando UML e Padrões Ed. Bookman, 2000. \n\nCOAD, Peter; YOURDON, Edward. Análise Baseada em Objetos, Ed. Campus, 1992 \n\nCOAD, Peter; YOURDON, Edward. Projeto Baseado em Objetos, Ed. Campus, 1992 \n\nYOURDON, E. Análise Estruturada Moderna Ed. Campus, 1990.";
        String e20 = "INSERT INTO ementa_disciplina VALUES ('CET079','CET 078 – LINGUAGEM DE PROGRAMAÇÃO III','" + ementa + "','" + objetivo + "','" + metodologia + "','" + avaliacao + "','" + conteudo_programatico + "','" + referencia + "')";

         /*Computação Gráfica*/
        ementa = "Conceitos básicos de processamento de imagens e computação gráfica; computação gráfica bidimensional: modelagem, visualização, fundamentos matemáticos; computação gráfica tridimensional: modelagem, visualização, técnicas básicas para síntese de imagens, realismo, animação.";
        objetivo = "Apresentar o mundo da Computação Gráfica ao Aluno. Capacitar o aluno a fazer a síntese de estruturas geométricas básicas. Compreender a Base do Realismo e Animação.";
        metodologia = "Aulas expositivas; Exercícios teóricos; Exercícios práticos em opengl; Trabalhos em grupo.";
        avaliacao = "2 avaliações teóricas e 1 trabalho prático.";
        conteudo_programatico = "1. Introdução \nDefinição \nProcessamento de Imagens \nHistórico \nAplicações \n\n2. Imagem \nDefinição \nContínuo x Discreto \nImagem Digital \nDiscretização \nPixel \nQualidade \nRepresentação matricial x vetorial \nRasterização \nReconstrução \n\n3. Dispositivos Gráficos \n\n4. Cor \nIntrodução \nCor na Computação Gráfica \nSistemas de Cores \n\n5. Objetos Gráficos \nPrimitivas Gráficas \nProcesso de Visualização \nMapeamento \nAlgoritmos de Rasterização \nDesenho de retas, círculos e outros \nRecorte \n\n6. Transformações Geométricas e Projeções \nIntrodução \nTranslação, Escala e Rotação \nProjeções paralelas e ortográficas \nOpengl \n\n7. Iluminação \nIntrodução \nModelos de Iluminação \nSombreamento \nTransparência \nExercícios em opengl \n\n8. Renderização \nIntrodução \nFaces ocultas \nSerrilhamento \nTextura \nRaytracing \n\n9. Animação \nIntrodução \nTécnicas de animação \n\n10.Modelagem \nIntrodução \nRepresentação \nTécnicas de modelagem";
        referencia = "AZEVEDO, Eduardo; CONCI, Aura. Computação Gráfica - Teoria e Prática. Rio de Janeiro: Elsevier, 2003. 353 p. \n\nSHREINER, Dave, et al. OpenGL Programming Guide. 4 ed. Boston: Addison-Wesley, 2004. 759 p. \n\nHEARN, Donald; BAKER, M. Pauline. Computer Graphics, C version. 2 ed. New Jersey: Prentice Hall, 1997. 652 p. \n\nGomes, J.; Velho, L. Computação gráfica, vol. 1. Rio de Janeiro, IMPA, 1998. \n\nJ. GOMES e L. VELHO, Conceitos Básicos de Computação Gráfica. VII Escola de Computação, São Paulo, 1990.";
        String e22 = "INSERT INTO ementa_disciplina VALUES ('CET080','CET 078 - Linguagem de Programação III \nCET 638 – Álgebra e Geometria Analítica','" + ementa + "','" + objetivo + "','" + metodologia + "','" + avaliacao + "','" + conteudo_programatico + "','" + referencia + "')";

        /*Direito e Legislação*/
        ementa = "Noções Básicas, fontes princípios de Direito; Direito do trabalho; Direito Autoral e propriedade industrial; Defesa do consumidor; Proteção Legal do hardware e software; Regulamentação do trabalho do profissional de informática; Informática e privacidade; Ética profissional.";
        objetivo = "Identificar as noções básicas sobre o Direito, suas fontes e princípios, bem como os aspectos jurídicos na informática desenvolvendo no aluno uma postura crítica-analítica que possibilite a aplicação prática dos conhecimentos adquiridos.";
        metodologia = "Contemplará exposições dialogadas, estudos de textos teóricos e discussões de análise crítica.";
        avaliacao = "De forma continuada, através de produção de trabalhos de pesquisa, provas escritas e orais, estudos em grupos e individuais.";
        conteudo_programatico = "I I UNIDADE \n1. Introdução ao estudo do Direito \n1.1Direito: conceito, etimologia, importância; \n1.2Fontes do Direito: conceito, classificação \n1.3Princípios gerais do Direito: distinções, funções; \n1.4Ramos do Direito \n2. Direito do Trabalho \n2.1Histórico, conceito, fontes e princípios; \n2.2 Empregado e empregador. \n\nII UNIDADE \n3. Código de Defesa do Consumidor \n3.1Conceito de fornecedor e consumidor; \n3.2Responsabilidade por vício do produto e serviço. \n3.3Práticas abusivas e comerciais \n\nIII UNIDADE \n4. Direito autoral e propriedade industrial; \n5. Informática e privacidade; \n6. Proteção legal do hardware e software; \n\nIV UNIDADE \n7. Regulamentação do trabalho do profissional de informática; \n\n8. Ética profissional.";
        referencia = "BRANCATO, Ricardo Teixeira. Instituições de Direito Público e Privado \n\nMARTINS, Sérgio Pinto. Instituições de Direito Público e Privado. Ed. Atlas \n\nMONTORO, André Franco. Introdução à Ciência do Direito. \n\nDONATO, Maria Antonieta Zanardo. Proteção ao consumidor. Ed. RT";
        String e23 = "INSERT INTO ementa_disciplina VALUES ('CIJ098','','" + ementa + "','" + objetivo + "','" + metodologia + "','" + avaliacao + "','" + conteudo_programatico + "','" + referencia + "')";

        /*Organização e Arquitetura dos Computadores*/
        ementa = "Sistemas de computadores, estrutura dos dispositivos de hardware, funcionamento do computador nos diversos níveis de abstração, arquiteturas avançadas.";
        objetivo = "- Compreender o funcionamento básico do computador e a organização do hardware.  \n\n- Fornecer subsídios para o desenvolvimento de software mais aprimorado e eficiente, através dos conhecimentos de desempenho e funcionamento do hardware.";
        metodologia = "Aulas expositivas, utilização de simuladores.";
        avaliacao = "Provas e monografias.";
        conteudo_programatico = "1. Introdução \n1.1 Histórico dos computadores: Arquitetura de Von Neuman \n1.2 Máquinas multinível \n\n2. Organização de sistemas de computadores \n2.1 Processadores: execução de instruções, CPU, paralelismo \n2.2 Memória interna: bits, bytes, endereços, hierarquia, cache \n2.3 Memória externa: disco rígido, discos ópticos \n2.4 Entrada/saída: periféricos, modems, caracteres \n2.5 Microprocessadores: Intel, Motorola, AMD \n2.6 Barramentos: síncronos, assíncronos, interrupções, PCI, AGP, USB \n\n3. Microprogramação \n3.1 Multiplexadores e decodificadores \n3.2 ALUs e deslocadores \n3.3 Microarquiteturas \n3.4 Temporização das microinstruções \n3.5 Microprogramação horizontal e vertical \n3.6 Nanoprogramação \n3.7 Pipelining \n\n4. Nível de máquina \n4.1 Formato das instruções \n4.2 Endereçamento: imediato, direto (DMA), indireto, de pilha \n4.3 Tipos de instruções: diádicas, monádicas, comparações, desvios, loop \n4.4 Fluxo de controle: procedimentos, interrupções \n\n5. Nível de linguagem de montagem \n5.1 Formato do comando \n5.2 Processo de montagem \n5.3 Ligação \n\n6. Arquiteturas avançadas \n6.1 RISC, CISC, Paralelas";
        referencia = "Tanenbaum, A. S, Organização Estruturada de Computadores. Editora Prentice/Hall do Brasil, 1992. \n\nStallings, W., Computer Organization and Architecture. Editora Prentice Hall. \n\nWeber, R. F., Arquitetura de Computadores Pessoais, Coleção Livros Didáticos. Editora Sagra-Luzzatto, 2000. \n\nHayes, J. P., Computer Architecture and Organization, McGraw-Hill Series in Electrical and Computer Engineering, ISBN: 0070273553.";
        String e24 = "INSERT INTO ementa_disciplina VALUES ('CET081','CET 065 - Lógica Digital II','" + ementa + "','" + objetivo + "','" + metodologia + "','" + avaliacao + "','" + conteudo_programatico + "','" + referencia + "')";

        /*Organização e Recuperação da Informação*/
        ementa = "Dispositivos externos de armazenamento. Representação e manipulação de dados em memória secundária. Organização e estrutura de arquivos. Classificação externa. Técnicas de compressão de dados. Introdução à criptografia. Noções de complexidade dos algoritmos estudados.";
        objetivo = "Apresentar as principais técnicas para armazenamento, recuperação, ordenação e garantia de segurança de dados em memória secundária.";
        metodologia = "Aulas teóricas e práticas, com utilização das ferramentas de programação disponíveis. Trabalhos práticos de implementação das diferentes estruturas e seus algoritmos de manipulação.";
        avaliacao = "Provas teóricas e trabalho computacional.";
        conteudo_programatico = "1. Introdução \n\uF02D Dispositivos de armazenamento secundário. \n\uF02D Introdução à complexidade de algoritmos. \n\n2. Arquivos seqüenciais. \n\n3. Arquivos com estruturas baseadas em árvores: arquivos seqüenciais \nindexados, árvores binárias de busca, AVL, B e variações (B#,B+), Tries, \nPatricia, árvores de busca digital. \n\n4. Busca por chaves secundárias: arquivos invertidos, multilista e árvores K-d. \n\n5. Arquivos de acesso direto. \n\n6. Ordenação externa: por intercalação, intercalação balanceada de vários caminhos, intercalação em disco e intercalação em cascata. \n\n7. Compressão de arquivos: método de Huffman e outros. \n\n8. Introdução à criptografia. \n\n9. RAID";
        referencia = "ZIVIANI, N. Projeto de algoritmos com implementação em Pascal e C. 2 ed. Thomson Pioneira, 2004. 572 p. \n\nGARCIA-MOLINA, H. et al. Implementação de Sistemas de Banco de Dados. Tradução de Vandenberg D. de Souza. Rio de Janeiro: Campus, 2001. \n\nTENENBAUM, A. M., LANGSAM, Y., AUGENSTEIN M. J., Estruturas de Dados Usando C. 1 ed. Makron Books, 1995. 904 p. \n\nKNUTH, D. E. The art of computer programming: sorting and searching. 2.ed. vol 3. Hardcover, 1998. \n\nTHARP, A. L. File Organization and Processing. John Willey & Sons, Inc. 1988.";
        String e25 = "INSERT INTO ementa_disciplina VALUES ('CET082','CET 077 - Estrutura de Dados','" + ementa + "','" + objetivo + "','" + metodologia + "','" + avaliacao + "','" + conteudo_programatico + "','" + referencia + "')";

        /*PROBABILIDADE E ESTATÍSTICA*/
        ementa = "Introdução à estatística; Introdução à Amostragem; Análise exploratória dos dados; Introdução à Probabilidade; Variável aleatória; Distribuições amostrais; Distribuições de probabilidades de variáveis aleatórias contínuas (N, Z, t, X2 e F) e suas aplicações na inferência estatística; Estimação de parâmetros e Correlação linear simples.";
        objetivo = "Capacitar os estudantes interpretar e realizar análises estatísticas básicas, tanto de natureza exploratória quanto inferencial.";
        metodologia = "Aulas teóricas e práticas, trabalhos individuais e em grupo.";
        avaliacao = "Provas e trabalhos práticos.";
        conteudo_programatico = "Conceitos básicos de estatística \na. Conceituação da estatística \nb. Estatística descritiva vs. inferencial \nc. População vs. amostra \nd. Terminologias: parâmetro, dedução, indução, etc. \n\n2. Introdução à amostragem \na. Métodos probabilísticos \nb. Métodos não probabilísticos \n\n3. Análise exploratória dos dados \na. Apresentação dos dados em tabelas, gráficos e assemelhados \nb. Medidas de tendência central \nc. Medidas de posição ou separatrizes \nd. Medidas de dispersão \ne. Medidas de assimetria e curtose \n\n4. Introdução à Probabilidade \n\n5. Variáveis aleatórias \n\n6. Distribuições de variáveis aleatórias e suas aplicações na inferência estatística \na. Normal \nb. Normal reduzida \nc. t \nd. \ne. F \n\n7. Distribuições amostrais \n\n8. Teste de hipóteses \n\n9. Intervalos de confiança para média e variância \n\n10.Correlação linear simples.";
        referencia = "Bunchaft G. & Kellner, S.R.O. Estatítica Sem Mistérios. Petrópolis, Vozes, 759p. 1998. Fonseca, J.S & Martins, G.A. Curso de Estatística. 3a. ed, São Paulo, Atlas, 320p. 1996.";
        String e26 = "INSERT INTO ementa_disciplina VALUES ('CET083','CET 075 – CÁLCULO APLICADO III','" + ementa + "','" + objetivo + "','" + metodologia + "','" + avaliacao + "','" + conteudo_programatico + "','" + referencia + "')";

        /*PPROJETO E ANÁLISE DE ALGORITMOS*/
        ementa = "Conceito de algoritmo. Papel dos algoritmos em computação. Corretude e eficiência. Complexidade assintótica no pior e melhor casos e no caso médio. Padrões de algoritmos: força bruta, gulosos (greedy), retrocesso (backtrack), divisão e conquista. Programação dinâmica. Grafos. Problemas geométricos. Problemas intratáveis.";
        objetivo = "Desenvolver habilidades para usar o computador na solução de problemas e apresentar técnicas de desenvolvimento algoritmos para novos problemas";
        metodologia = "Aulas expositivas e práticas (programação).";
        avaliacao = "Provas teóricas e práticas. Trabalhos em grupo.";
        conteudo_programatico = "1. Introdução: conceito de algoritmo e seu papel em computação. Indução finita e sua relação com recursão. \n\n2. Corretude e eficiência de algoritmos. \n\n3. Noções de complexidade assintótica de algoritmos: melhor caso; pior caso; caso médio; cotas inferiores. \n\n4. Padrões de algoritmos: Força Bruta, Gulosos (Greedy), Retrocesso (Backtrack), Divisão e Conquista. \n\n5. Programação Dinâmica. \n\n6. Algoritmos para problemas geométricos no plano. Envoltória convexa. \n\n7. Grafos: definição e implementação (matriz e listas de adjacências). Algoritmos para problemas de grafos: percursos, conexidade, árvore geradora mínima, caminho mais curto, emparelhamento, planaridade, etc. \n\n8. Problemas intratáveis. Classes de problemas: P, NP, NP-difícil e NP-completo.";
        referencia = "LEISERSON, Charles E.; STEIN, Clifford; RIVEST, Ronald L.; CORMEN,Thomas H. Algoritmos - Trad. 2ª Ed. Americana, Editora Campus, 2002. \n\nPREISS, Bruno. Estruturas de Dados e Algoritmos Editora Campus, 2001. \n\nDROZDEK, Adam. Estruturas de Dados e Algoritmos em C++. Thomson Pioneira, 2001. \n\nSKIENA, Steven S. The Algorithm Design Manual Springer-Verlag, 1997. Online: http://www2.toki.or.id/book/AlgDesignManual/ \n\nSKIENA, Steven S.; REVILLA, Miguel A. Programming Challenges Springer, 2003";
        String e27 = "INSERT INTO ementa_disciplina VALUES ('CET084','CET 077 – ESTRUTURA DE DADOS','" + ementa + "','" + objetivo + "','" + metodologia + "','" + avaliacao + "','" + conteudo_programatico + "','" + referencia + "')";

        /*materias do 5º semestre*/
        /*Análise Numérica*/
        ementa = "Análise de erros. Série de Potências. Raízes de Equações. Matrizes e Sistemas de Equações Lineares. Ajuste de funções. Interpolação. Derivação e Integração Numérica. Solução Numérica de Equações Diferenciais Ordinárias.";
        objetivo = "Apresentar técnicas computacionais básicas aplicadas à solução numérica de problemas científicos.";
        metodologia = "Aulas expositivas. Utilização de software para mostrar os conceitos.";
        avaliacao = "Provas teóricas e trabalhos práticos.";
        conteudo_programatico = "1. Representação binária de números reais. Análise de soluções numéricas: instabilidade, critérios de parada, propagação de erros. Tipos e fontes de erros. Métodos exatos e aproximados. \n\n2. Raízes de equações. Análise de equações polinomiais e trancendentais. Métodos aplicados a raiz única. \n\n3. Matrizes e sistemas de equações lineares. Métodos de solução numérica de sistemas lineares e de cálculo de matriz inversa. \n\n4. Sistemas de equações não-lineares. \n\n5. Interpolação: polinomial, linear e multidimensional. Erros na interpolação. \n\n6. Ajuste de funções. Método dos mínimos quadrados. Regressão linear. \n\n7. Derivação numérica: interpolação polinomial, diferenças finitas, iterações de primeira ordem, convergência. \n\n8. Integração numérica: método dos trapézios, de Simpson. Quadratura Gaussiana. \n\n9. Equações diferenciais ordinárias: métodos de passo simples e de passos múltiplos. Sistemas de primeira ordem e de ordem superior.";
        referencia = "CLÁUDIO, D. M. e Marins, J. M., Cálculo Numérico Computacional - Teoria e Prática, Atlas (1994). \n\nSTARK, P. A., Introdução aos Métodos Numéricos. Interciência, 1979. \n\nATKINSON, Kendall E., \"An Introduction to Numerical Analysis\", John Wiley & Sons, 2a edição, 1989. \n\nBARROSO, L. C.; Barroso, M. M. de A; Campos Filho, F. F.; Carvalho, M. L. B. de; Maia, M. L., Cálculo Numérico (com aplicações), 2ª Edição. Editora Harbra (1987) \n\nDORN, William S. e Daniel D. McCracken, \"Cálculo Numérico comEstudos de Casos em \n\nFORTRAN IV\", Editora Campus, Universidade de São Paulo, 2a reimpressão, 1989. \n\nHUMES, Ana Flora P. de Castro et alli, \"Noções de Cálculo Numérico\", Editora McGraw-Hill do Brasil Ltda, 1984. \n\nPRESS, William H., et alli, \"Numerical Recipes in C - The Art of Scientific Computing\", 2a edição, Press Syndicate of the University of Cambridge, 1994.";
        String e28 = "INSERT INTO ementa_disciplina VALUES ('CET086','CET 075 - Calculo Aplicado III \nCET 078 - Linguagem de Programação III','" + ementa + "','" + objetivo + "','" + metodologia + "','" + avaliacao + "','" + conteudo_programatico + "','" + referencia + "')";

        /*BANCO DE DADOS I*/
        ementa = "Banco de Dados – Conceitos Básicos- Arquitetura de um Sistema de Banco de DadosModelagem de dados- Modelos de Dados Relacional Hierárquicos e de Redes. Linguagens de Definição e manipulação de Dados. Projetos de Banco de Dados Relacional- Transações ACID: recuperação de falhas, concorrência, segurança e integridade. Conceitos de Banco de Dados Distribuídos.";
        objetivo = "Fornecer conceitos básicos e mecânicos para uso de sistemas de gerenciamento de banco de dados para o desenvolvimento de aplicações e/ou suporte de banco de dados";
        metodologia = ". Aulas expositivas \n. Trabalhos em grupo \n. Seminários \n. Exercícios em Laboratório";
        avaliacao = "Provas escritas, trabalhos individuais e em grupo.";
        conteudo_programatico = "1. Introdução \n2. Visão geral de Banco de Dados \n2.1.Arquitetura de Sistemas de Banco de Dados \n3. Modelos de Dados \n3.1.Hierárquico \n3.2.Redes \n3.3.Relacional \n3.4.Orientado a Objetos \n3.5.Outros \n4. Introdução aos Sistemas Relacionais \n5. Modelagem de Dados \n6. Linguagens de Consulta para Bancos de Dados Relacionais \n6.1.SQL \n6.2.Outras (Quel, QBE) \n6.3.Comandos de definição de dados (DDL) em SQL \n6.3.1. Tabelas e colunas \n6.3.2. Restrições \n6.3.3. Relacionamentos \n7. Cálculo a Álgebra Relacional \n8. Projeto de Banco de Dados Relacional \n8.1.Entidades ou Classes-Entidades \n8.2.Relacionamentos ou associações \n8.2.1. Cardinalidade \n8.2.2. Opcionalidade \n8.3.Generalizações \n9. Transações ACID \n9.1.Commit e Rollback \n9.2.Recuperação de Falhas \n9.3.Concorrência \n9.3.1. Bloqueio Perpétuo \n9.4.Segurança \n9.5.Otimização \n10.Conceitos de bancos de dados distribuídos \n10.1. Distribuição de dados \n10.1.1. Horizontal \n10.1.2. Vertical \n10.2. Commit em duas fases";
        referencia = "DATE, C. J. Introdução a Sistemas de Banco de Dados. Tradução da 7ª. Edição Americana. Editora Campus. \n\nDATE, C.J. Bancos de Dados, Tópicos Avançados, Editora Campus \n\nKORTH, H. F., SILBERSCHATZ, A. Sistema de Banco de Dados, Makron Books \n\nGARCIA-MOLINA, H. ULLMAN, J.D., WIDOM, J. Implementação de Sistemas de Bancos de Dados. Editora Campus";
        String e29 = "INSERT INTO ementa_disciplina VALUES ('CET090','CET 082 – ORGANIZAÇÃO E RECUPERAÇÃO DA INFORMAÇÃO \nCET 079 – ANÁLISE DOS SISTEMAS DE INFORMAÇÃO','" + ementa + "','" + objetivo + "','" + metodologia + "','" + avaliacao + "','" + conteudo_programatico + "','" + referencia + "')";

        /*Conceitos de Linguagens de Programação*/
        ementa = "Caracterização das linguagens de programação de computadores e seus diferentes paradigmas de programação (lógico, procedimental, funcional, orientação a objetos, orientação a aspectos, etc.). Ambiente. Estudo dos conceitos básicos de cada paradigma. Modelos computacionais. Semântica. Outros paradigmas de programação.";
        objetivo = "Proporcionar ao aluno uma visão geral dos conceitos envolvidos no projeto e no uso dos diversos paradigmas de linguagens de programação.";
        metodologia = "";
        avaliacao = "Provas teóricas e práticas. Trabalhos em grupo";
        conteudo_programatico = "1. Características e objetivos das Linguagens de Programação. Abstração de Dados. Polimorfismo. Ambiente e escopo de variáveis. \n\n2. Tipos. Linguagens fortemente e fracamente tipadas. Linguagens não-tipadas. Inferência de tipos. Linguagens com tipo estático (Haskell) e dinâmico (Scheme). \n\n3. Semântica: conceitos das semânticas operacional, denotacional, axiomática, algébrica e de ações \n\n4. Linguagens Imperativas: \n. variáveis e atribuição \n. efeitos colaterais \n. desvios e execução sequencial de comandos \n. programação estruturada \n\n5. Linguagens Lógicas: \n. cláusulas de Horn, \n. implicação lógica \n. sistema de inferência lógica \n. variáveis lógicas \n. diferença entre Prolog e programação lógica \n\n6. Linguagens Funcionais: \n. cálculo lambda \n. ordem de avaliação de expressões: cálculo preguiçoso (lazyevaluation) e cálculo guloso (eager evaluation) \n. avaliação parcial \n. casamento de padrões \n. transparência referencial \n. transformação de programas \n\n7. Linguagens Orientadas a Objetos: tipos abstratos de dados, encapsulamento de dados e operações, herança e outros conceitos de orientação a objetos. \n\n8. Outros paradigmas: programação concorrente, modelo de dados relacional (SQL), Predes de Petri, orientação a aspectos, etc.";
        referencia = "SEBESTA, Robert W. Conceitos de Linguagens de Programação";
        String e30 = "INSERT INTO ementa_disciplina VALUES ('CET087','CET 084 – Projeto e Análise de Algoritmos','" + ementa + "','" + objetivo + "','" + metodologia + "','" + avaliacao + "','" + conteudo_programatico + "','" + referencia + "')";

        /*INTELIGÊNCIA ARTIFICIAL*/
        ementa = "Caracterização das linguagens de programação de computadores e seus diferentes paradigmas de programação (lógico, procedimental, funcional, orientação a objetos, orientação a aspectos, etc.). Ambiente. Estudo dos conceitos básicos de cada paradigma. Modelos computacionais. Semântica. Outros paradigmas de programação.";
        objetivo = "Apresentação dos fundamentos, métodos e aplicações de Inteligência Artificial, bem como desenvolver no aluno o senso crítico com relação à importância desta disciplina dentro da Ciência da Computação.";
        metodologia = "Aulas expositivas, seminários ministrados pelos alunos e listas de exercício.";
        avaliacao = "Duas provas escritas (dois créditos). \nUm seminário (um crédito). \nListas de exercício (um crédito). \nProva final, se necessário.";
        conteudo_programatico = "•Introdução à Inteligência Artificial \n•Conceitos e Características \n•Fundamentos \n•História \n•Domínios de Aplicação \n•Resolução de Problemas \n•Espaço de Problemas \n•Busca Cega e Heurística \n•Representação de Conhecimento \n•Representação Procedural \n•Representação por Objetos Estruturados \n•Regras de Produção \n•Redes Semânticas \n•Representação Lógica \n•Prolog \n•Tópicos Avançados \n•Redes Neurais \n•Sistemas Especialistas \n•Robótica \n•Algoritmos Genéticos \n•Descoberta Científica \n•Processamento de Linguagem Natural \n•Aprendizagem de Máquina \n•Agentes Inteligentes \n•Raciocínio Baseado em Casos";
        referencia = "RUSSELL, S.; NORVIG, P. Artificial Intelligence: A Modern Approach. Prentice Hall, 1995. \n\nRICH, E.; KNIGHT, K. Inteligência Artificial. Makron Books, 1993. \n\nBRATKO, I. Prolog: Programming for Artificial Intelligence. Addison-Wesley Publish Company, 1990.";
        String e31 = "INSERT INTO ementa_disciplina VALUES ('CET068','CET 640 – Fundamentos Matemáticos para Computação','" + ementa + "','" + objetivo + "','" + metodologia + "','" + avaliacao + "','" + conteudo_programatico + "','" + referencia + "')";

        /*SISTEMAS OPERACIONAIS*/
        ementa = "Introdução: conceitos básicos e histórico. Processos: sincronização, escalonamento de processos, região crítica, exclusão mútua, IPC, deadlocks. Gerenciamento de memória: paginação, segmentação. Sistemas de arquivos. Entrada e saída: interrupções, device drivers, discos, relógios.";
        objetivo = "Suprir os alunos com os conceitos de Sistemas Operacionais, apresentando, de uma forma geral, os diferentes tipos de S.Os., assim como os quatro componentes básicos que formam a maioria desses sistemas: gerenciamento de processos, gerenciamento de memória, gerenciamento de entrada/saída e sistemas de arquivos.";
        metodologia = "Aulas expositivas com utilização de simuladores";
        avaliacao = "";
        conteudo_programatico = "1 Introdução \n1.1 Definição de SO \n1.2 Componentes básicos \n1.3 Estruturas de SO \n1.4 Histórico \n1.5 Linux \n2 Processos \n2.1 Conceito de processo e thread \n2.2 Operações sobre processos e threads \n2.3 Multiprogramação \n2.4 Estados de um processo \n2.5 Escalonamento \n2.6 Proteção entre processos \n2.7 Comunicação entre processos/threads \n2.8 Concorrência \n2.8.1 Condições de corrida \n2.8.2 Exclusão mútua \n2.8.3 Problemas clássicos \n2.9 Deadlocks \n3 Gerenciamento de memória \n3.1 Espaços de endereçamento \n3.2 Alocação, relocação, proteção \n3.3 Fragmentação \n3.4 Memória virtual \n3.5 Paginação \n3.5.1 Definição \n3.5.2 Algoritmos de substituição de páginas \n3.5.3 Paginação por demanda \n3.6 Segmentação \n3.7 Memória compartilhada \n3.8 Arquivos mapeados em memória \n4 Sistemas de Arquivos \n4.1 Arquivos e diretórios \n4.2 Alocação de blocos \n4.3 Gerência do espaço livre \n4.4 Desempenho \n4.5 Integridade \n4.6 Sistemas de arquivos estruturados em log \n4.7 Interface VFS (Virtual File System) \n4.8 Proteção \n5 Entrada e Saída \n5.1 Função de E/S \n5.2 Estruturação do suporte a E/S \n5.2.1 Software independente de dispositivo \n5.2.2 Device drivers \n5.3 Bufferização \n5.4 Escalonamento de disco \n5.5 Temporizadores e relógios";
        referencia = "STALLINGS, William. Operating Systems. 3ª Edição- Prentice Hall \n\nTANENBAUM, Andrew S. Modern operating systems. \n\nMACHADO, Francis B.; Maia, Luiz Paulo. Arquitetura de sistemas operacionais. \n\nSOLOMON, David A. Desvendando o windows NT";
        String e32 = "INSERT INTO ementa_disciplina VALUES ('CET101','CET 081 - ORGANIZAÇÃO E ARQUITETURA DE COMPUTADORES','" + ementa + "','" + objetivo + "','" + metodologia + "','" + avaliacao + "','" + conteudo_programatico + "','" + referencia + "')";

        /*Software Básico*/
        ementa = "Endereçamento de memória, montagem, conjunto de instruções, operações, interrupções.";
        objetivo = ". Analisar os fundamentos de linguagens de montagens e relacioná-los aos elementos da estrutura de um computador. \n\n.Desenvolver algoritmos e programas em uma ou mais linguagens nativas de programação.";
        metodologia = "Aulas teóricas, aulas práticas em laboratório.";
        avaliacao = "Provas, trabalhos práticos, listas de exercícios.";
        conteudo_programatico = "1. Introdução ao computador pessoal. \n1.1 Representação de números e códigos. \n1.2 Processador, memória e segmentos. \n\n2. Requisitos de programação do PC. \n2.1 Características do sistema operacional. \n2.2 Pilha e endereçamento. \n\n3. Requisitos de linguagem de montagem. \n3.1 Identificadores, diretivas e declarações. \n3.2 Definição de dados. \n\n4. Montagem, ligação e execução de programas. \n4.1 Montagem de um programa fonte. \n4.2 Ligação de objetos. \n4.3 Execução \n\n5. Instruções de controle, lógica de programação e endereçamento. \n5.1 Conjunto de instruções. \n5.2 Modos de endereçamento e tipos de instruções. \n5.3 Chamada de procedimento. \n5.4 Instruções lógico-aritméticas. \n\n6. Processamento de E/S por vídeo e teclado. \n6.1 Funções de manipulação de entrada pelo teclado. \n6.2 Funções de manipulação do vídeo. \n\n7. Operações em cadeias. \n7.1 Prefixo de repetição. \n7.2 Instruções de manipulação de cadeias. \n\n8. Operações aritméticas. \n8.1 Adição e subtração. \n8.2 Multiplicação e divisão. \n\n9. Processamento de Tabelas. \n9.1 Definição de tabelas. \n9.2 Manipulação de tabelas. \n\n10. Interrupções";
        referencia = "HYDE, R. - The Art of Assembly Language Programming, edição pública, outubro 1996, \"http://webster.cs.ucr.edu/Page_asm/ArtofAssembly/pdf/AoAPDF.html\", visitado em 25/11/2004. \n\nABEL, P. - IBM PC Assembly Language and Programming, quarta edição, 1998, Prentice Hall. \n\nPAPPAS, C.H.; MURRAY, W. N., 80386/80286 Assembly Language Programming, McGraw-Hill.";
        String e33 = "INSERT INTO ementa_disciplina VALUES ('CET088','CET 081 – Organização e Arquitetura de Computadores','" + ementa + "','" + objetivo + "','" + metodologia + "','" + avaliacao + "','" + conteudo_programatico + "','" + referencia + "')";

        /* TEORIA DA COMPUTAÇÃO*/
        ementa = "Introdução à Teoria da Computação. Notações Matemáticas. Linguagens Regulares. Autômatos Finitos Determinísticos e Não-determinísticos. Expressões Regulares. Lema do Bombeamento. Linguagens Livres do Contexto. Gramáticas. Autômatos de Pilha. Máquinas de Turing Determinísticas e Não-determinísticas. Problema da Parada. Decidibilidade.";
        objetivo = "Apresentação dos fundamentos de computabilidade e linguagens formais, bem como desenvolver no aluno o senso crítico com relação à importância desta disciplina dentro da Ciência da Computação.";
        metodologia = "Aulas expositivas, seminários ministrados pelos alunos e listas de exercício.";
        avaliacao = ". Três provas escritas e três listas de exercício (três créditos). \n\n. Um seminário (um crédito). \n\n. Prova final, se necessário.";
        conteudo_programatico = "\n•Introdução à Teoria da Computação \n•Autômatos, Computabilidade e Complexidade \n•Noções e Notações Matemáticas (conjuntos, seqüências ou tuplas, funções e relações, cadeias e linguagens). \n•Linguagens Regulares \n•Autômatos Finitos \n•Não-determinismo \n•Expressões Regulares \n•Lema do Bombeamento \n•Linguagens Livres de Contexto \n•Gramáticas \n•Forma Normal de Chomsky \n•Autômatos de Pilha \n•Lema do Bombeamento para Linguagens Livres do Contexto \n•Máquinas de Turing e Tese de Church-Turing \n•Máquinas de Turing Determinísticas e Não-determinísticas \n•Definição de Algoritmo \n•Problema da Parada \n•Decidibilidade";
        referencia = "SIPSER, M. Introduction to the Theory of Computation. PWS Publishing Company, 1997. \n\nHOPCROFT, E.; Ullman, J. D. Introduction to Automata Theory, Languages and Computation. Addison-Wesley Publishing Company, 1979. \n\nCARROLL, J.; Long, D. Theory of Finite Automata – with an Introduction to Formal Languages. Prentice-Hall International, Inc., 1989. \n\nGERSTING, J. L. Fundamentos Matemáticos para a Ciência da Computação. LTC Editora, 2001.";
        String e34 = "INSERT INTO ementa_disciplina VALUES ('CET089','CET 084 – PROJETO E ANÁLISE DE ALGORITMOS \nCET 640 - FUNDAMENTOS MATEMÁTICOS PARA A COMPUTAÇÃO \nCET 074 – ALGEBRA ABSTRATA','" + ementa + "','" + objetivo + "','" + metodologia + "','" + avaliacao + "','" + conteudo_programatico + "','" + referencia + "')";

        /*materias do 6º semestre*/
        /* ADMINISTRAÇÃO PARA INFORMÁTICA*/
        ementa = "Administração Geral. Organizações e Empresas. Conhecimentos empresariais: tomada de decisão, planejamento, controle. O papel da tecnologia. Liderança nas organizações.";
        objetivo = "Conhecer os elementos presentes na administração geral e estabelecer, quando possível, uma relação com o profissional de informática nesse contexto.";
        metodologia = "Aulas expositivas com utilização de slides de Power Point, discussão em sala de aula, pesquisas direcionadas, mostra de filmes relacionados, grupos de estudos para realizar visita a empresas e verificar na prática os conceitos apresentados em sala de aula.";
        avaliacao = "Avaliação escrita, trabalho em grupo.";
        conteudo_programatico = "\n1. Conceitos básicos da administração \n2. Organizações \n1. 2.1 Teoria dos Sistemas \n3. Empresas \n4. Ambiente Externo \n5. Planejamento e Controle \n1. 5.1 Gráfico de Gantt \n6. Tomada de Decisão \n1. 6.1 Ponto de equilíbrio \n7. Tecnologia \n8. Recursos Humanos \n9. Equipes \n10. Cultura Organizacional \n11. Motivação e Recompensas \n12. Liderança";
        referencia = "CHIAVENATO, I. Teoria Geral da Administração. 5ª ed. Atualizada. Rio de Janeiro: Campus, 1999. Vol. 1. 695 p. \n\nROBBINS, S. P., Administração: Mudanças e Perspectivas, 1a edição, editora Saraiva, 2003. \n\nMOTA, Fernando C. Prestes. Teoria Geral da Administração: uma introdução. 6.ª ed. São Paulo:Livraria Pioneira, 1977. \n\nA. L. Cautela e E.G.F. Pollon. Sistemas de Informação na Administração de Empresas. Editora Atlas, 1976. \n\nD. A. Rezende e A. F. de Abreu. Tecnologia da Informação Aplicada a sistemas de Informações Empresariais. 2a ed. Editora Atlas, 2000 \n\nSTONER, James. Administração. 5.ed. Rio de Janeiro: LTC, 1999. ";
        String e35 = "INSERT INTO ementa_disciplina VALUES ('CET085','CET 079 – ANÁLISE DOS SISTEMAS DE INFORMAÇÃO','" + ementa + "','" + objetivo + "','" + metodologia + "','" + avaliacao + "','" + conteudo_programatico + "','" + referencia + "')";

        /* Banco de Dados II*/
        ementa = "Especificação de um sistema de dados. Modelagem de dados. Dependência Funcional, Chaves, Normalização. Mapeamento para o modelo Relacional. Especificação dos módulos para Manipulação de Dados. Usuário de Banco de Dados: segurança. Visões e Integração de Visões. Projeto de interface com o Usuário. Implantação do sistema: integração de módulos e validação do usuário. Considerações adicionais de Administração: Performance e disponibilidade do Sistema Gerenciador de Bancos de Dados";
        objetivo = "Dotar o aluno da convivência prática com SGBDS relacionais, munindo-o do ferramental teórico e prático necessário para projetar e desenvolver Sistemas Suportados nesta tecnologia.";
        metodologia = "Aulas teóricas e práticas com o auxílio de algum(s) SGBD(s) Relacional(is).";
        avaliacao = "Duas provas aplicadas e um projeto num SGBD Relacional";
        conteudo_programatico = "Especificação dos Requisitos de um Sistema de Dados 1.1.UML \n\n2. Modelagem de Estruturas de Dados \n\n3. Mapeamento Modelo de Classes – Modelo Relacional \n\n4. Dependência Funcional \n\n5. Chaves \n\n6. Normalização \n\n6.1.1ª Forma Normal \n\n6.2.2ª Forma Normal \n\n6.3.3ª Forma Normal \n\n6.4.Formas Normais adicionais: Boyce Codd, 4ª, 5ª, 6ª e 7ª Formas \n\n7. Especificação dos Módulos para Manipulação dos Dados \n\n7.1.Mapeamento Métodos – Procedimentos Armazenados \n\n7.1.1. Procedures e Functions \n\n7.1.2. Triggers \n\n7.2.Cursores \n\n8. Usuário de banco de dados \n\n8.1.Permissões \n\n8.2.Grupos ou Rules \n\n9. Visões \n\n9.1.Integração de visões \n\n10.Projeto de Interface com o Usuário \n\n10.1. Implementação de Classes de Fronteira \n\n11.Implantação do Sistema \n\n11.1. Ciclos de Vida \n\n11.1.1. Cascata \n\n11.1.2. Cascata com FeedBack \n\n11.1.3. Espiral \n\n12.Integração de Módulos \n\n13.Validação do usuário \n\n14.Administração de Bancos de Dados \n\n14.1. Performance \n\n14.1.1. Índices \n\n14.1.2. Otimização de Consultas \n\n14.2. Disponibilidade \n\n14.2.1. Registros de Log \n\n14.2.2. Backup e Recuperação";
        referencia = "KORTH, H. F., SILBERSCHATZ, A. Sistema de Banco de Dados, Makron Books \n\nDATE, C. J. Introdução a Sistemas de Banco de Dados. Tradução da 7ª. Edição Americana. Editora Campus. \n\nDATE, C.J. Bancos de Dados, Tópicos Avançados, Editora Campus \n\nGARCIA-MOLINA, H. ULLMAN, J.D., WIDOM, J. Implementação de Sistemas de Bancos de Dados. Editora Campus Manuais Técnicos e Livros voltados para Bancos de Dados específicos.";
        String e36 = "INSERT INTO ementa_disciplina VALUES ('CET091','CET 090 – Banco de Dados I','" + ementa + "','" + objetivo + "','" + metodologia + "','" + avaliacao + "','" + conteudo_programatico + "','" + referencia + "')";

        /*Compiladores*/
        ementa = "Introdução ao estudo dos compiladores. Linguagens de programação, tradutores e compiladores. Análise léxica. Análise sintática. Geração de código intermediário. Otimização de código. Gerência de memória. Geração de código objeto.";
        objetivo = "Apresentação dos fundamentos de compiladores, bem como desenvolver no aluno o senso crítico com relação à importância desta disciplina dentro da Ciência da Computação.";
        metodologia = "Aulas expositivas, listas de exercício e projetos realizados pelos alunos.";
        avaliacao = "Três provas escritas e listas de exercício (três créditos). \n\nUm projeto (um crédito). \n\nProva final, se necessário.";
        conteudo_programatico = "•Linguagens de Programação \n\n•Evolução das linguagens de programação \n\n•Tradutores e compiladores \n\n•Análise Léxica \n\n•Gramáticas e linguagens regulares (revisão) \n\n•Tokens \n\n•Especificação, implementação e tabela de símbolos \n\n•Análise Sintática \n\n•Gramáticas livres-do-contexto (revisão) \n\n•Análise descendente (top-down) \n\n•Análise Redutiva (bottom-up) \n\n•Recuperação de erros \n\n•Tradução dirigida por sintaxe \n\n•Geração de Código Intermediário \n\n•Linguagens intermediárias \n\n•Construção da tabela de símbolos \n\n•Geração de códigos para comando de atribuição, expressões lógicas, \n\ncomandos de controle e Backpatching \n\n•Otimização de Código \n\n•Otimização de código intermediário \n\n•Otimização de código para expressões aritméticas \n\n•Gerência de Memória \n\n•Geração de Código Objeto";
        referencia = "PRICE, Ana Maria de Alencar;TOSCANI, Simão Sirineo. Implementação de Linguagens de Programação: Compiladores. Editora Sagra Luzzatto, 2001. \n\nAHO, Alfred V. Compiladores – Princípios, Técnicas e Ferramentas. LTC Editora. \n\nGERSTING, J. L. Fundamentos Matemáticos para a Ciência da Computação. LTC Editora, 2001. \n\nGHEZZI, C.; JAZAYERI, M. Conceitos de Linguagens de Programação. Editora Campus, 1985.";
        String e37 = "INSERT INTO ementa_disciplina VALUES ('CET058','CET 089 – Teoria da Computação \n CET 088 – Software Básico','" + ementa + "','" + objetivo + "','" + metodologia + "','" + avaliacao + "','" + conteudo_programatico + "','" + referencia + "')";

        /*Interface Homem Máquina*/
        ementa = "Psicologia do Usuário: aspectos perceptivos e cognitivos. Projeto do Diálogo Homemmáquina. Implementação: hardware e software Interface; Usabilidade e Avaliação. Impactos sociais e individuais.";
        objetivo = "Apresentar os princípios utilizados na implementação de interfaces homem-máquina, assim como a metodologia para avaliar a usabilidade de uma interface. Verificar a importância dos projetos centrados no usuário.";
        metodologia = "Aulas teóricas e práticas, com utilização das ferramentas de programação disponíveis.";
        avaliacao = "Provas teóricas e trabalho computacional.";
        conteudo_programatico = "1. Introdução \n\uF02D Evolução das interfaces x Evolução do hardware e software; \n\uF02D Elementos da diversidade humana; \n\n2. Interfaces: Conceitos e Importância \n\uF02D Definição geral e no contexto da Engenharia de Software; \n\uF02D Interface amigável; \n\uF02D Ergonomia; \n\uF02D Estilos de Interação; \n\n3. Projeto de interface \n\uF02D Protótipos; \n\uF02D Projetos centrados no usuário; \n\uF02D Testes e avaliação; \n\n4. Avaliação de projetos de interface \n\uF02D Definição de usabilidade; \n\uF02D Atributos de usabilidade; \n\uF02D Formas de avaliação de usabilidade; \n\uF02D Heurísticas de usabilidade; \n\n5. Impactos sociais e novas tecnologias \n\uF02D Organizações e mudanças nas organizações; \n\uF02D Impactos na estrutura formal; \n\uF02D Impactos sobre o indivíduo e o emprego; \n\uF02D Tecnologia apropriada; \n\uF02D Novas tecnologias de informação e comunicação; \n\uF0B7 Reconhecimento de Voz \n\uF0B7 Reconhecimento de Face \n\uF0B7 E outras.";
        referencia = "ROCHA, H. V. da, BARANAUSKAS, M. C. C. Design e Avaliação de Interfaces HumanoComputador. Núcleo de Informática Aplicada à Educação – Nied, 2003. \n\nSILVA, E. J Sistemas Interativos. Ouro Preto/ MG: DCC/UFOP. Apostila de Interface Homem-Máquina, 1998, 169p. \n\nSZETO, G.et al. Interatividade na Web. São Paulo: Berkeley Brasil, 1997, 494p. \n\nPREECE, J. et al. Human Computer Interaction. Addison Wesley, 1994. \n\nHIX, D.; HARTSON, H. R. Developing user interface: ensuring usability through product and process. John Wiley and Sons, 1993, 329p. \n\nDISX, A.; FINALY, J.; ABOWD, G.; BEALE, R. Human Computer Interaction. 2º ed. Prentice Hall Europe: London, 1998. 572 p.";
        String e38 = "INSERT INTO ementa_disciplina VALUES ('CET096','CET 079 - Análise dos Sistemas de Informação','" + ementa + "','" + objetivo + "','" + metodologia + "','" + avaliacao + "','" + conteudo_programatico + "','" + referencia + "')";

        /*REDE DE COMPUTADORES I*/
        ementa = "Características Gerais e Aplicações: Estruturas Topologias e Meios de Transmissão; Protocolos de Comunicação; Departamento dos Níveis do Modelo OSI da ISSO; Análise de Algumas Redes do Ponto de Vista do Modelo OSI; Sistemas Operacionais para Redes: Desempenho, Custos e Segurança em Redes de Computadores: Aspectos de LAN’S, MAN’S e WAN’S; Estudos de Casos. Redes Novell. Redes TCP/IP. Redes ATM. Gerenciamento de Redes de Computadores: Aspectos da Gerência de Redes. Segurança em Redes de Computadores. Ferramentas para Gerenciamento de Redes.";
        objetivo = "Apresentar a idéia de conectividade e seus principais recursos e vantagens utilizando rede de computadores e descrever, em linhas gerais, as considerações envolvidas na implementação de Estruturas, Topologias e Meios de Transmissão, Protocolos de Comunicação de LANs, MANs e WANs.";
        metodologia = "Aulas expositivas com utilização de slides de power point, aulas praticas, pesquisas direcionadas e grupos de estudos.";
        avaliacao = "Provas objetivas e subjetivas, explanação de temáticas via seminários.";
        conteudo_programatico = "Características Gerais e Aplicações: Estruturas Topologias e Meios de Transmissão; \n\nProtocolos de Comunicação; \n\nModelo OSI da ISSO; \n\nAnálise de Algumas Redes do Ponto de Vista do Modelo OSI; Sistemas \n\nOperacionais para Redes: Desempenho, Custos e Segurança em Redes de Computadores; \n\nAspectos de LAN’S, MAN’S e WAN’S; \n\nEstudos de Casos. \n\nGerenciamento de Redes de Computadores: Aspectos da Gerência de Redes.";
        referencia = "TANENBAUM, Andrew S. COMPUTER NETWORKS. 3 ed. Prentice Hall, 1996. 813 pág. \n\nSOARES, Luiz F. G.; LEMOS, Guido; COLCHER, Sérgio. REDES DE COMPUTADORES Das \n\nLANs, MANs e WANs às Redes ATM. 2 ed. Editora CAMPUS, 1995. 705 pág. \n\nSTALLINGS, William. DATA AND COMPUTER COMMUNICATIONS. 5 ed. Prentice Hall, 1996. 798 pág.";
        String e39 = "INSERT INTO ementa_disciplina VALUES ('CET098','CET 101 – SISTEMAS OPERACIONAIS','" + ementa + "','" + objetivo + "','" + metodologia + "','" + avaliacao + "','" + conteudo_programatico + "','" + referencia + "')";


        String[] emen = {e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, e22, e23, e24, e25, e26, e27, e28, e29, e30, e31, e32, e33, e34, e35, e36, e37, e38, e39};
        executeSQL(emen);

    }


    //tem como entrada o nome da meteria e retorna seu codigo e turma
    public static String returnCodTurmaDisciplina(String nomeDisciplina) {
        Cursor cursor;
        String codigoMateria = null;

        final String query = "SELECT distinct disciplina.codigo AS cod, disciplina.turma AS turma FROM disciplina WHERE disciplina.nome = '" + nomeDisciplina + "'";

        cursor = bd.rawQuery(query, null);

        while (cursor.moveToNext()) {

            /*CODIGO DA MATERIA*/
            int cod = cursor.getColumnIndex("cod");
            int turma = cursor.getColumnIndex("turma");

            codigoMateria = cursor.getString(cod) + ":" + cursor.getString(turma);

        }

        return codigoMateria;
    }

    //tem como entrada o nome da meteria e retorna seu codigo
    public static String returnCodDisciplina(String nomeDisciplina) {
        Cursor cursor;
        String codigoMateria = null;

        final String query = "SELECT distinct disciplina.codigo AS cod FROM disciplina WHERE disciplina.nome = '" + nomeDisciplina + "'";

        cursor = bd.rawQuery(query, null);

        while (cursor.moveToNext()) {

            /*CODIGO DA MATERIA*/
            int cod = cursor.getColumnIndex("cod");


            codigoMateria = cursor.getString(cod);

        }

        return codigoMateria;
    }

    //tem como entrada o nome da meteria e retorna a turma
    public static String returnTurmaDisciplina(String nomeDisciplina) {
        Cursor cursor;
        String turmaMateria = null;

        final String query = "SELECT distinct disciplina.turma AS turma FROM disciplina WHERE disciplina.nome = '" + nomeDisciplina + "'";

        cursor = bd.rawQuery(query, null);

        while (cursor.moveToNext()) {

            /*turma DA MATERIA*/
            int turma = cursor.getColumnIndex("turma");


            turmaMateria = cursor.getString(turma);

        }

        return turmaMateria;
    }

    //RETORNA UM ARRAY COM O NOME DE TODAS MATERIAS
    public static ArrayList<String> listaNomeMaterias() {
        Cursor cursor;

        ArrayList<String> listaNomeMateria = new ArrayList<>();

        final String query = "SELECT DISTINCT disciplina.nome AS nome FROM disciplina\n" +
                "INNER JOIN disciplina_dia_hora ON (disciplina.codigo = disciplina_dia_hora.fk_codigo AND disciplina.turma =  disciplina_dia_hora.fk_turma)";

        cursor = bd.rawQuery(query, null);

        while (cursor.moveToNext()) {
            //nome das disciplinas
            int nome = cursor.getColumnIndex("nome");
            String name = cursor.getString(nome);

            listaNomeMateria.add(name);
        }


        return listaNomeMateria;
    }

    //RETORNA UM ARRAY COM O NOME DAS DISCIPLINAS QUE ESTOU CURSANDO
    public static ArrayList<String> listaNomeMateriasCursando() {
        Cursor cursor;

        ArrayList<String> listaNomeMateria = new ArrayList<>();

        final String query = "SELECT disciplina.nome AS nome FROM disciplina\n" +
                "INNER JOIN meu_horario ON (disciplina.codigo = meu_horario.fk_cod_disciplina AND disciplina.turma =  meu_horario.fk_turma)" +
                "ORDER BY disciplina.nome";

        cursor = bd.rawQuery(query, null);

        while (cursor.moveToNext()) {
            //nome das disciplinas
            int nome = cursor.getColumnIndex("nome");
            String name = cursor.getString(nome);

            listaNomeMateria.add(name);
        }


        return listaNomeMateria;
    }


    public static ArrayList<Materia> listaMateriaHorario(int cod_dia) {
        Cursor cursor;
        ArrayList<Materia> listaMateria = new ArrayList<>();

        final String query = "SELECT distinct disciplina.codigo AS cod,disciplina.nome AS nome, dia_semana.nome AS dia, hora_aula.hora_inicio AS ini, hora_aula.hora_fim AS fim,  professor.nome AS prof\n" +
                "FROM disciplina INNER JOIN disciplina_dia_hora ON (disciplina.codigo = disciplina_dia_hora.fk_codigo AND disciplina.turma =  disciplina_dia_hora.fk_turma)\n" +
                "INNER JOIN dia_hora ON disciplina_dia_hora.fk_dia_hora = dia_hora.codigo \n" +
                "INNER JOIN dia_semana ON dia_hora.fk_dia_semana = dia_semana.codigo\n" +
                "INNER JOIN hora_aula ON dia_hora.fk_hora_aula = hora_aula.codigo\n" +
                "INNER JOIN professor ON professor.codigo = disciplina.fk_professor\n" +
                "INNER JOIN meu_horario ON (disciplina.codigo = meu_horario.fk_cod_disciplina AND disciplina.turma =  meu_horario.fk_turma)\n" +
                "WHERE dia_semana.codigo = " + cod_dia + "\n" +
                "ORDER BY meu_horario.hora_fim";

        cursor = bd.rawQuery(query, null);

        while (cursor.moveToNext()) {

            /*MATERIA*/
            int cod = cursor.getColumnIndex("cod");
            int nome = cursor.getColumnIndex("nome");
            int dia = cursor.getColumnIndex("dia");
            int inic = cursor.getColumnIndex("ini");
            int fim = cursor.getColumnIndex("fim");
            int prof = cursor.getColumnIndex("prof");

            Materia mat = new Materia();
            mat.setCodigo(cursor.getString(cod));
            mat.setNome(cursor.getString(nome));
            mat.setDia_semana(cursor.getString(dia));
            mat.setHora_inicio(cursor.getString(inic));
            mat.setHora_fim(cursor.getString(fim));
            mat.setProfessor(cursor.getString(prof));
            listaMateria.add(mat);
        }

        return listaMateria;
    }

    //Retorna uma materia para adcionar ao horario contendo codigo turma hora inicio e hora fim

    public static InformacoesMateria returnInformacoesDisciplina(String name) {
        Cursor cursor;
        InformacoesMateria inf = new InformacoesMateria();

        final String query = "SELECT disciplina.nome AS nome, disciplina.CHS AS qtdCr, sala.localizacao AS loc, sala.codigo AS sala, professor.nome AS nomeProf, professor.email AS emailProf \n" +
                "FROM disciplina INNER JOIN disciplina_dia_hora ON (disciplina.codigo = disciplina_dia_hora.fk_codigo AND disciplina.turma =  disciplina_dia_hora.fk_turma) \n" +
                "INNER JOIN sala ON  disciplina_dia_hora.fk_sala = sala.codigo \n" +
                "INNER JOIN professor ON disciplina.fk_professor = professor.codigo \n" +
                "WHERE disciplina.nome = '" + name + "'";

        cursor = bd.rawQuery(query, null);

        while (cursor.moveToNext()) {

            /*INFORMAÇÕES*/
            int nomeMat_i = cursor.getColumnIndex("nome");
            int qtdCr_i = cursor.getColumnIndex("qtdCr");
            int loc_i = cursor.getColumnIndex("loc");
            int sala_i = cursor.getColumnIndex("sala");
            int nomeProf_i = cursor.getColumnIndex("nomeProf");
            int emailProf_i = cursor.getColumnIndex("emailProf");


            inf.setNomeMat(cursor.getString(nomeMat_i));
            inf.setQtdCod(cursor.getInt(qtdCr_i));
            inf.setLocalizacao(cursor.getString(loc_i));
            inf.setSala(cursor.getString(sala_i));
            inf.setProf(cursor.getString(nomeProf_i));
            inf.setEmailProf(cursor.getString(emailProf_i));
        }

        return inf;
    }

    public static Materia listaMateriaMeuHorario(String name) {
        Cursor cursor;

        boolean flag = false;//flag utilizada para verificar se determinada materia já está no horario
        ArrayList<Materia> listaMateria = new ArrayList<>();

        final String query = "SELECT distinct disciplina.codigo AS cod,disciplina.nome AS nome, dia_semana.nome AS dia, hora_aula.hora_inicio AS ini, hora_aula.hora_fim AS fim,  professor.nome AS prof\n" +
                "FROM disciplina INNER JOIN disciplina_dia_hora ON (disciplina.codigo = disciplina_dia_hora.fk_codigo AND disciplina.turma =  disciplina_dia_hora.fk_turma)\n" +
                "INNER JOIN dia_hora ON disciplina_dia_hora.fk_dia_hora = dia_hora.codigo \n" +
                "INNER JOIN dia_semana ON dia_hora.fk_dia_semana = dia_semana.codigo\n" +
                "INNER JOIN hora_aula ON dia_hora.fk_hora_aula = hora_aula.codigo\n" +
                "INNER JOIN professor ON professor.codigo = disciplina.fk_professor\n" +
                "WHERE disciplina.nome = '" + name + "'";

        cursor = bd.rawQuery(query, null);

        if (cursor.moveToFirst()) {


            /*MATERIA*/
            int cod = cursor.getColumnIndex("cod");
            int nome = cursor.getColumnIndex("nome");
            int dia = cursor.getColumnIndex("dia");
            int inic = cursor.getColumnIndex("ini");
            int fim = cursor.getColumnIndex("fim");
            int prof = cursor.getColumnIndex("prof");

            //loop percor
            do {
                flag = false;

                Materia mat = new Materia();
                mat.setCodigo(cursor.getString(cod));
                mat.setNome(cursor.getString(nome));
                mat.setDia_semana(cursor.getString(dia));
                mat.setHora_inicio(cursor.getString(inic));
                mat.setHora_fim(cursor.getString(fim));
                mat.setProfessor(cursor.getString(prof));
                if (listaMateria.size() == 0) {
                    listaMateria.add(mat);
                } else {
                    for (int i = 0; i < listaMateria.size(); i++) {
                        Materia m = (Materia) listaMateria.get(i);


                        if (m.getCodigo().equalsIgnoreCase(mat.getCodigo()) && m.getDia_semana().equalsIgnoreCase(mat.getDia_semana())) {
                            int hi = mat.getHora_inicio().compareTo(m.getHora_inicio());
                            int hf = m.getHora_fim().compareTo(mat.getHora_fim());
                            flag = true;
                            if (hi < 0) {
                                m.setHora_inicio(mat.getHora_inicio());
                            }
                            if (hf < 0) {
                                m.setHora_fim(mat.getHora_fim());
                            }
                        }
                    }
                    if (!flag) {

                        listaMateria.add(mat);
                    }
                }

            } while (cursor.moveToNext());
        }
        return listaMateria.get(0);
    }

    /*retorna uma lista com todas as horas que iram ter aulas*/
    public static ArrayList<String> returnListaHorasAulas(int semestre) {

        Cursor cursor;
        ArrayList<String> listHoraAulas = new ArrayList<>();
        final String query = "SELECT DISTINCT hora_aula.codigo AS cod, hora_aula.hora_inicio as hi\n" +
                "FROM disciplina INNER JOIN disciplina_dia_hora ON (disciplina.codigo = disciplina_dia_hora.fk_codigo AND disciplina.turma =  disciplina_dia_hora.fk_turma)\n" +
                "INNER JOIN dia_hora ON dia_hora.codigo = disciplina_dia_hora.fk_dia_hora\n" +
                "INNER JOIN hora_aula ON hora_aula.codigo = dia_hora.fk_hora_aula\n" +
                "WHERE disciplina.semestre_diciplina = '" + semestre + "'\n" +
                "ORDER BY hora_aula.codigo";

        cursor = bd.rawQuery(query, null);

        while (cursor.moveToNext()) {

            int hi_i = cursor.getColumnIndex("hi");
            String hi_s = cursor.getString(hi_i);

            listHoraAulas.add(hi_s);
        }

        return listHoraAulas;
    }

    /*Tem como entrada a hora e o dia e semestre  retorna a lista de aulas aula*/
    public static ArrayList<AuxDisciplina> returnListAula(String hora, int dia, int semestre) {

        Cursor cursor;
        AuxDisciplina auxDisc;
        ArrayList<AuxDisciplina> listDisciplinaHorario = new ArrayList<>();
        final String query = "SELECT disciplina.codigo, disciplina.turma, disciplina.abreviacao \n" +
                "FROM disciplina INNER JOIN disciplina_dia_hora ON (disciplina.codigo = disciplina_dia_hora.fk_codigo AND disciplina.turma =  disciplina_dia_hora.fk_turma)\n" +
                "INNER JOIN dia_hora ON dia_hora.codigo = disciplina_dia_hora.fk_dia_hora\n" +
                "INNER JOIN hora_aula ON hora_aula.codigo = dia_hora.fk_hora_aula\n" +
                "INNER JOIN dia_semana ON dia_semana.codigo = dia_hora.fk_dia_semana\n" +
                "WHERE disciplina.semestre_diciplina = '" + String.valueOf(semestre) + "' AND hora_aula.hora_inicio ='" + hora + "' AND dia_semana.codigo = " + dia + "";

        cursor = bd.rawQuery(query, null);

        while (cursor.moveToNext()) {
            int cod_i = cursor.getColumnIndex("codigo");
            int turma_i = cursor.getColumnIndex("turma");
            int abv_i = cursor.getColumnIndex("abreviacao");
            String cod_s = cursor.getString(cod_i);
            String turma_s = cursor.getString(turma_i);
            String abv_s = cursor.getString(abv_i);
            auxDisc = new AuxDisciplina(cod_s, turma_s, abv_s);
            listDisciplinaHorario.add(auxDisc);
        }

        return listDisciplinaHorario;
    }

    /*retorna uma lista com todas as horas do meu horario*/
    public static ArrayList<String> returnListaHorasAulasMeuHorario() {

        Cursor cursor;
        ArrayList<String> listHoraAulas = new ArrayList<>();
        final String query = "SELECT DISTINCT hora_aula.hora_inicio as hi FROM meu_horario\n" +
                "INNER JOIN disciplina ON (disciplina.codigo = meu_horario.fk_cod_disciplina AND disciplina.turma =  meu_horario.fk_turma)\n" +
                "INNER JOIN disciplina_dia_hora ON (disciplina.codigo = disciplina_dia_hora.fk_codigo AND disciplina.turma =  disciplina_dia_hora.fk_turma)\n" +
                "INNER JOIN dia_hora ON dia_hora.codigo = disciplina_dia_hora.fk_dia_hora\n" +
                "INNER JOIN hora_aula ON hora_aula.codigo = dia_hora.fk_hora_aula " +
                "ORDER BY hora_aula.codigo";

        cursor = bd.rawQuery(query, null);

        while (cursor.moveToNext()) {

            int hi_i = cursor.getColumnIndex("hi");
            String hi_s = cursor.getString(hi_i);

            listHoraAulas.add(hi_s);
        }
        return listHoraAulas;
    }

    /*Tem como entrada a hora e o dia retorna a lista de aulas aula*/
    public static ArrayList<AuxDisciplina> returnListAulaMeuHorario(String hora, int dia) {

        Cursor cursor;
        AuxDisciplina auxDisc;
        ArrayList<AuxDisciplina> listDisciplinaHorario = new ArrayList<>();
        final String query = "SELECT meu_horario.fk_cod_disciplina as codigo, meu_horario.fk_turma as turma , disciplina.abreviacao FROM meu_horario\n" +
                "INNER JOIN disciplina ON (disciplina.codigo = meu_horario.fk_cod_disciplina AND disciplina.turma =  meu_horario.fk_turma)\n" +
                "INNER JOIN disciplina_dia_hora ON (disciplina_dia_hora.fk_codigo = disciplina.codigo AND disciplina.turma = disciplina_dia_hora.fk_turma)\n" +
                "INNER JOIN dia_hora ON dia_hora.codigo = disciplina_dia_hora.fk_dia_hora\n" +
                "INNER JOIN dia_semana ON dia_semana.codigo = dia_hora.fk_dia_semana\n" +
                "INNER JOIN hora_aula ON hora_aula.codigo = dia_hora.fk_hora_aula\n" +
                "WHERE dia_semana.codigo = " + dia + " AND hora_aula.hora_inicio = '" + hora + "'";

        cursor = bd.rawQuery(query, null);

        while (cursor.moveToNext()) {
            int cod_i = cursor.getColumnIndex("codigo");
            int turma_i = cursor.getColumnIndex("turma");
            int abv_i = cursor.getColumnIndex("abreviacao");
            String cod_s = cursor.getString(cod_i);
            String turma_s = cursor.getString(turma_i);
            String abv_s = cursor.getString(abv_i);

            auxDisc = new AuxDisciplina(cod_s, turma_s, abv_s);

            listDisciplinaHorario.add(auxDisc);
        }

        return listDisciplinaHorario;
    }


    /*recebe o codigo e a tuma de uma disciplina e retorna seu nome*/
    public static String getNameDisciplina(String cod, String turma) {
        Cursor cursor;
        String nome = null;

        final String query = "SELECT disciplina.nome FROM disciplina \tWHERE codigo = '" + cod + "' AND turma = '" + turma + "' ";

        cursor = bd.rawQuery(query, null);

        while (cursor.moveToNext()) {
            int nomeMat_i = cursor.getColumnIndex("nome");
            nome = cursor.getString(nomeMat_i);
            return nome;
        }

        return nome;
    }


    //Retorna todas as horas possiveis
    public static String[] returnDisciplinaLinhas(String hora, int semestre, String turma) {
        Cursor cursor;
        String[] listNomeMateria = new String[7];
        final String query = "SELECT dia_semana.codigo AS dia, hora_aula.hora_inicio AS hora ,disciplina.codigo AS cod, disciplina.nome As nome \n" +
                "FROM disciplina INNER JOIN disciplina_dia_hora ON (disciplina.codigo = disciplina_dia_hora.fk_codigo AND disciplina.turma =  disciplina_dia_hora.fk_turma)\n" +
                "INNER JOIN dia_hora ON disciplina_dia_hora.fk_dia_hora = dia_hora.codigo \n" +
                "INNER JOIN dia_semana ON dia_hora.fk_dia_semana = dia_semana.codigo\n" +
                "INNER JOIN hora_aula ON dia_hora.fk_hora_aula = hora_aula.codigo \n" +
                "WHERE hora_aula.hora_inicio = '" + hora + "' AND disciplina.semestre_diciplina = " + semestre + " ";

        cursor = bd.rawQuery(query, null);

        while (cursor.moveToNext()) {

            /*INFORMAÇÕES*/

            int codMat_i = cursor.getColumnIndex("cod");
            int nomeMat_i = cursor.getColumnIndex("nome");
            int dia = cursor.getColumnIndex("dia");
            int hr = cursor.getColumnIndex("hora");
            String hr_s = cursor.getString(hr);
            String dia_s = cursor.getString(dia);
            String cod = cursor.getString(codMat_i);
            String nome = cursor.getString(nomeMat_i);

            if (dia_s.equalsIgnoreCase(String.valueOf(2))) {
                listNomeMateria[0] = hr_s;
                listNomeMateria[1] = cod + ";" + nome;

            } else if (dia_s.equalsIgnoreCase(String.valueOf(3))) {
                listNomeMateria[0] = hr_s;
                listNomeMateria[2] = cod + ";" + nome;

            } else if (dia_s.equalsIgnoreCase(String.valueOf(4))) {
                listNomeMateria[0] = hr_s;
                listNomeMateria[3] = cod + ";" + nome;

            } else if (dia_s.equalsIgnoreCase(String.valueOf(5))) {
                listNomeMateria[0] = hr_s;
                listNomeMateria[4] = cod + ";" + nome;

            } else if (dia_s.equalsIgnoreCase(String.valueOf(6))) {
                listNomeMateria[0] = hr_s;
                listNomeMateria[5] = cod + ";" + nome;

            }

        }

        return listNomeMateria;
    }


    //Retorna todas as disciplina por hora do meu horario
    public static String[] returnDisciplinaLinhasMyHorario(String hora) {
        Cursor cursor;
        String[] listNomeMateria = new String[7];
        final String query = "SELECT dia_semana.codigo AS dia, hora_aula.hora_inicio AS hora ,disciplina.codigo AS cod, disciplina.nome As nome \n" +
                "FROM disciplina INNER JOIN disciplina_dia_hora ON (disciplina.codigo = disciplina_dia_hora.fk_codigo AND disciplina.turma =  disciplina_dia_hora.fk_turma)\n" +
                "INNER JOIN dia_hora ON disciplina_dia_hora.fk_dia_hora = dia_hora.codigo \n" +
                "INNER JOIN dia_semana ON dia_hora.fk_dia_semana = dia_semana.codigo\n" +
                "INNER JOIN hora_aula ON dia_hora.fk_hora_aula = hora_aula.codigo\n" +
                "INNER JOIN professor ON professor.codigo = disciplina.fk_professor\n" +
                "INNER JOIN meu_horario ON (disciplina.codigo = meu_horario.fk_cod_disciplina AND disciplina.turma =  meu_horario.fk_turma)\n" +
                "WHERE hora_aula.hora_inicio = '" + hora + "' ";

        cursor = bd.rawQuery(query, null);

        while (cursor.moveToNext()) {

            /*INFORMAÇÕES*/

            int codMat_i = cursor.getColumnIndex("cod");
            int nomeMat_i = cursor.getColumnIndex("nome");
            int dia = cursor.getColumnIndex("dia");
            int hr = cursor.getColumnIndex("hora");
            String hr_s = cursor.getString(hr);
            String dia_s = cursor.getString(dia);
            String cod = cursor.getString(codMat_i);
            String nome = cursor.getString(nomeMat_i);

            if (dia_s.equalsIgnoreCase(String.valueOf(2))) {
                listNomeMateria[0] = hr_s;
                listNomeMateria[1] = cod + ";" + nome;

            } else if (dia_s.equalsIgnoreCase(String.valueOf(3))) {
                listNomeMateria[0] = hr_s;
                listNomeMateria[2] = cod + ";" + nome;

            } else if (dia_s.equalsIgnoreCase(String.valueOf(4))) {
                listNomeMateria[0] = hr_s;
                listNomeMateria[3] = cod + ";" + nome;

            } else if (dia_s.equalsIgnoreCase(String.valueOf(5))) {
                listNomeMateria[0] = hr_s;
                listNomeMateria[4] = cod + ";" + nome;

            } else if (dia_s.equalsIgnoreCase(String.valueOf(6))) {
                listNomeMateria[0] = hr_s;
                listNomeMateria[5] = cod + ";" + nome;

            }

        }

        return listNomeMateria;
    }

    //usado na activity do horarios para mostrar as materias por semestre
    public static ArrayList<Materia> listaMateriaHorarioCursando() {
        Cursor cursor;
        boolean flag = false;//flag utilizada para verificar se determinada materia já está no horario
        ArrayList<Materia> listaMateria = new ArrayList<>();

        final String query = "SELECT distinct disciplina.codigo AS cod,disciplina.nome AS nome, disciplina.turma AS turma, dia_semana.nome AS dia, hora_aula.hora_inicio AS ini, hora_aula.hora_fim AS fim,  professor.nome AS prof\n" +
                "FROM disciplina INNER JOIN disciplina_dia_hora ON (disciplina.codigo = disciplina_dia_hora.fk_codigo AND disciplina.turma =  disciplina_dia_hora.fk_turma)\n" +
                "INNER JOIN dia_hora ON disciplina_dia_hora.fk_dia_hora = dia_hora.codigo \n" +
                "INNER JOIN dia_semana ON dia_hora.fk_dia_semana = dia_semana.codigo\n" +
                "INNER JOIN hora_aula ON dia_hora.fk_hora_aula = hora_aula.codigo\n" +
                "INNER JOIN professor ON professor.codigo = disciplina.fk_professor\n" +
                "INNER JOIN meu_horario ON (disciplina.codigo = meu_horario.fk_cod_disciplina AND disciplina.turma =  meu_horario.fk_turma)";

        cursor = bd.rawQuery(query, null);

        if (cursor.moveToFirst()) {

            while (cursor.moveToNext()) {
                flag = false;
                /*MATERIA*/
                int cod = cursor.getColumnIndex("cod");
                int nome = cursor.getColumnIndex("nome");
                int turma = cursor.getColumnIndex("turma");
                int dia = cursor.getColumnIndex("dia");
                int inic = cursor.getColumnIndex("ini");
                int fim = cursor.getColumnIndex("fim");
                int prof = cursor.getColumnIndex("prof");

                Materia mat = new Materia();
                mat.setCodigo(cursor.getString(cod));
                mat.setNome(cursor.getString(nome));
                mat.setTurma(cursor.getString(turma));
                mat.setDia_semana(cursor.getString(dia));
                mat.setHora_inicio(cursor.getString(inic));
                mat.setHora_fim(cursor.getString(fim));
                mat.setProfessor(cursor.getString(prof));


                if (listaMateria.isEmpty()) {
                    listaMateria.add(mat);
                } else {


                    for (Materia m : listaMateria) {

                        if (mat.getNome().equals(m.getNome())) {
                            int hora_inicio = mat.getHora_inicio().compareTo(m.getHora_inicio());
                            int hora_fim = m.getHora_fim().compareTo(mat.getHora_fim());
                            flag = true;
                            if (hora_inicio < 0) {
                                m.setHora_inicio(mat.getHora_inicio());
                            }
                            if (hora_fim < 0) {
                                m.setHora_fim(mat.getHora_fim());
                            }
                        }
                    }

                    if (!flag) {
                        listaMateria.add(mat);
                    }

                }
            }
        }
        return listaMateria;
    }


    //Retorna uma materia para adcionar ao horario contendo codigo turma hora inicio e hora fim
    public static ArrayList<String> returnTodasHorasAulas() {
        Cursor cursor;
        ArrayList<String> listHorasAula = new ArrayList<>();
        final String query = "SELECT hora_aula.hora_inicio AS ini FROM hora_aula";

        cursor = bd.rawQuery(query, null);
        while (cursor.moveToNext()) {

            /*HORA DE AULAS*/
            int ini = cursor.getColumnIndex("ini");
            String hora_ini = cursor.getString(ini);
            listHorasAula.add(hora_ini);
        }
        return listHorasAula;
    }

    public static ArrayList<String[]> agrupaNomeMateriasLinhasColunas(int sem, String turma) {
        int semestre = sem;
        ArrayList<String[]> tabelaHoraAula = new ArrayList<>();
        ArrayList<String> listHoras = returnTodasHorasAulas();
        for (String hora : listHoras) {

            String[] linha = returnDisciplinaLinhas(hora, semestre, turma);
            tabelaHoraAula.add(linha);
        }
        return tabelaHoraAula;
    }

    //usado na activity do horarios para mostrar as materias por semestre
    public static ArrayList<Materia> listaMateriaHorarioPorSemestre(int semestre) {
        Cursor cursor;
        boolean flag = false;//flag utilizada para verificar se determinada materia já está no horario
        ArrayList<Materia> listaMateria = new ArrayList<>();

        final String query = "SELECT distinct disciplina.codigo AS cod,disciplina.nome AS nome, disciplina.turma AS turma, dia_semana.nome AS dia, hora_aula.hora_inicio AS ini, hora_aula.hora_fim AS fim,  professor.nome AS prof\n" +
                "FROM disciplina INNER JOIN disciplina_dia_hora ON (disciplina.codigo = disciplina_dia_hora.fk_codigo AND disciplina.turma =  disciplina_dia_hora.fk_turma)\n" +
                "INNER JOIN dia_hora ON disciplina_dia_hora.fk_dia_hora = dia_hora.codigo \n" +
                "INNER JOIN dia_semana ON dia_hora.fk_dia_semana = dia_semana.codigo\n" +
                "INNER JOIN hora_aula ON dia_hora.fk_hora_aula = hora_aula.codigo\n" +
                "INNER JOIN professor ON professor.codigo = disciplina.fk_professor\n" +
                "WHERE disciplina.semestre_diciplina = '" + semestre + "'";

        cursor = bd.rawQuery(query, null);

        if (cursor.moveToFirst()) {

            while (cursor.moveToNext()) {
                flag = false;
                /*MATERIA*/
                int cod = cursor.getColumnIndex("cod");
                int nome = cursor.getColumnIndex("nome");
                int turma = cursor.getColumnIndex("turma");
                int dia = cursor.getColumnIndex("dia");
                int inic = cursor.getColumnIndex("ini");
                int fim = cursor.getColumnIndex("fim");
                int prof = cursor.getColumnIndex("prof");

                Materia mat = new Materia();
                mat.setCodigo(cursor.getString(cod));
                mat.setNome(cursor.getString(nome));
                mat.setTurma(cursor.getString(turma));
                mat.setDia_semana(cursor.getString(dia));
                mat.setHora_inicio(cursor.getString(inic));
                mat.setHora_fim(cursor.getString(fim));
                mat.setProfessor(cursor.getString(prof));


                if (listaMateria.isEmpty()) {
                    listaMateria.add(mat);
                } else {


                    for (Materia m : listaMateria) {

                        if (mat.getNome().equals(m.getNome())) {
                            int hora_inicio = mat.getHora_inicio().compareTo(m.getHora_inicio());
                            int hora_fim = m.getHora_fim().compareTo(mat.getHora_fim());
                            flag = true;
                            if (hora_inicio < 0) {
                                m.setHora_inicio(mat.getHora_inicio());
                            }
                            if (hora_fim < 0) {
                                m.setHora_fim(mat.getHora_fim());
                            }
                        }
                    }

                    if (!flag) {
                        listaMateria.add(mat);
                    }

                }
            }
        }
        return listaMateria;
    }

    //retorna uma lista contendo todos os professores do curso // usado no fragmento colcid recursos
    public static ArrayList<Docente> returnListProfessores() {
        Cursor cursor;
        ArrayList<Docente> listDocentes = new ArrayList<>();
        int cont = 0;
        final String query = "SELECT DISTINCT professor.codigo, professor.nome, professor.email, professor.tel, professor.titulacao,\n" +
                "professor.classe, professor.url_lates FROM professor\n" +
                "INNER JOIN disciplina ON disciplina.fk_professor = professor.codigo\n" +
                "INNER JOIN disciplina_dia_hora ON (disciplina.codigo = disciplina_dia_hora.fk_codigo AND disciplina.turma =  disciplina_dia_hora.fk_turma)\n" +
                "ORDER BY professor.nome";

        cursor = bd.rawQuery(query, null);


        while (cursor.moveToNext()) {

            int cd = cursor.getColumnIndex("codigo");
            int n = cursor.getColumnIndex("nome");
            int e = cursor.getColumnIndex("email");
            int t = cursor.getColumnIndex("tel");
            int tt = cursor.getColumnIndex("titulacao");
            int c = cursor.getColumnIndex("classe");
            int ul = cursor.getColumnIndex("url_lates");

            String co = cursor.getString(cd);
            String no = cursor.getString(n);
            String ma = cursor.getString(e);
            String te = cursor.getString(t);
            String ti = cursor.getString(tt);
            String cl = cursor.getString(c);
            String ur = cursor.getString(ul);

            cont++;
            Log.i("nome", no);
            Docente docente = new Docente(co, no, ma, ti, cl);
            listDocentes.add(docente);
        }


        return listDocentes;
    }

    //LIKEretorna uma lista contendo todos os professores do curso // usado no fragmento colcid recursos
    public static ArrayList<Docente> returnListProfessoresLike(String consulta) {
        Cursor cursor;
        ArrayList<Docente> listDocentes = new ArrayList<>();
        String query;

        if (consulta.isEmpty()) {

            query = "SELECT professor.codigo AS cod, professor.nome AS nome, professor.email AS mail, professor.titulacao AS tit, " +
                    "professor.classe AS cla FROM professor order by nome";

        } else {

            query = "SELECT professor.codigo AS cod, professor.nome AS nome, professor.email AS mail, professor.titulacao AS tit, " +
                    "professor.classe AS cla FROM professor where professor.nome like '%" + consulta + "%' collate utf8_general_ci  order by nome";
        }

        cursor = bd.rawQuery(query, null);


        while (cursor.moveToNext()) {

            int cod = cursor.getColumnIndex("cod");
            int nome = cursor.getColumnIndex("nome");
            int mail = cursor.getColumnIndex("mail");
            int tit = cursor.getColumnIndex("tit");
            int cla = cursor.getColumnIndex("cla");
            String c = cursor.getString(cod);
            String n = cursor.getString(nome);
            String m = cursor.getString(mail);
            String t = cursor.getString(tit);
            String cl = cursor.getString(cla);


            Docente docente = new Docente(c, n, m, t, cl);
            listDocentes.add(docente);
        }

        return listDocentes;
    }

    /*retorna as infomações de um professor de acordo com o nome passado "InformacoesProfessor
    ord 1- somente informacoes professores 2 - informações professores e disciplinas que eles ministram*/
    public static InformacoesProfessor returnInformacoesProfessor(String name) {
        Cursor cursor;
        InformacoesProfessor informacoesProfessor = null;

        final String query = "SELECT professor.nome AS nome, " +
                "professor.email AS email," +
                "professor.tel AS tel," +
                "professor.url_lates AS url " +
                "FROM professor " +
                "WHERE professor.nome = '" + name + "' ";

        cursor = bd.rawQuery(query, null);
        cursor.moveToNext();


        int nome = cursor.getColumnIndex("nome");
        int email = cursor.getColumnIndex("email");
        int tel = cursor.getColumnIndex("tel");
        int urlLates = cursor.getColumnIndex("url");


        informacoesProfessor = new InformacoesProfessor(cursor.getString(nome), cursor.getString(email), cursor.getString(tel), cursor.getString(urlLates));


        return informacoesProfessor;
    }

    /*Obem uma lista de disciplinas que o professor está ministrando no semestre*/
    public static ArrayList<String> returnListDisciplinasMinistradasSem(String nameProfessor) {
        Cursor cursor;
        InformacoesProfessor informacoesProfessor = null;
        ArrayList<String> listNomeDisciplina = new ArrayList<>();
        final String query = "SELECT DISTINCT disciplina.nome AS nomeDisciplina, professor.nome AS p \n" +
                "FROM professor INNER JOIN disciplina ON professor.codigo = disciplina.fk_professor \n" +
                "INNER JOIN disciplina_dia_hora ON (disciplina.codigo = disciplina_dia_hora.fk_codigo AND disciplina.turma =  disciplina_dia_hora.fk_turma)\n" +
                "WHERE professor.nome = '" + nameProfessor + "'";

        cursor = bd.rawQuery(query, null);


        while (cursor.moveToNext()) {

            int nomeDisciplina = cursor.getColumnIndex("nomeDisciplina");
            int dis = cursor.getColumnIndex("p");
            String nd = cursor.getString(nomeDisciplina);
            listNomeDisciplina.add(nd);
        }

        return listNomeDisciplina;
    }


    //usado para colocar cor no na materia returna um indice com o numero de id materia
    public static int returnIdMateria(String name) {
        Cursor cursor;
        int cont = 0;
        final String query = "SELECT disciplina.nome AS nome FROM disciplina";

        cursor = bd.rawQuery(query, null);

        if (cursor.moveToFirst()) {

            while (cursor.moveToNext()) {

                int nome = cursor.getColumnIndex("nome");
                String n = cursor.getString(nome);
                if (name.equalsIgnoreCase(n)) {
                    return cont;
                }
                cont++;
            }
        }
        return cont;
    }

    //usado para colocar cor no na materia returna um indice com o numero de id materia
    public static int returnIdMateria(String codigo, String turma) {
        Cursor cursor;
        int cont = 0;
        final String query = "SELECT disciplina.codigo AS cod, disciplina.turma AS tur FROM disciplina";
        Log.i("Entrada", "cod: " + codigo + " " + turma);
        cursor = bd.rawQuery(query, null);


        while (cursor.moveToNext()) {

            int cod_i = cursor.getColumnIndex("cod");
            String cod_s = cursor.getString(cod_i);
            int turma_i = cursor.getColumnIndex("tur");
            String turma_s = cursor.getString(turma_i);

            if (cod_s.equalsIgnoreCase(codigo) && turma_s.equalsIgnoreCase(turma)) {
                if (codigo.equalsIgnoreCase("CET636") && turma.equalsIgnoreCase("T1")) {
                    Log.i("ID PAA:", String.valueOf(cont));

                }

                return cont;
            }
            cont++;

        }

        return cont;
    }


    //retorna a ementa de uma dicipliina para exibir as informações
    public static Ementa returnEmentaDisciplina(String nome) {
        Cursor cursor;
        Ementa ementa = new Ementa();

        String query = "SELECT disciplina.codigo AS cod " +
                "FROM disciplina " +
                "WHERE disciplina.nome  = '" + nome + "'";

        cursor = bd.rawQuery(query, null);
        int codigo = 0;
        if (cursor.moveToFirst()) {

            codigo = cursor.getColumnIndex("cod");
        }

        String codi = cursor.getString(codigo);

        query = "SELECT disciplina.codigo AS cod ,disciplina.nome AS nome, disciplina.CHS AS ch, disciplina.carga_horaria AS cr, " +
                "fk_pre_requisito AS pre_req, ementa AS emen, objetivo AS obj, metodologia AS met," +
                "avaliacao AS ava, conteudo_programatico AS cont, referencia AS ref " +
                "FROM ementa_disciplina INNER JOIN disciplina ON disciplina.codigo = ementa_disciplina.fk_disicplina " +
                "WHERE disciplina.codigo  = '" + codi + "'";

        cursor = bd.rawQuery(query, null);

        if (cursor.moveToFirst()) {

            int c = cursor.getColumnIndex("cod");
            int n = cursor.getColumnIndex("nome");
            int pr = cursor.getColumnIndex("pre_req");
            int ch = cursor.getColumnIndex("ch");
            int cr = cursor.getColumnIndex("cr");

            int e = cursor.getColumnIndex("emen");
            int o = cursor.getColumnIndex("obj");
            int m = cursor.getColumnIndex("met");
            int a = cursor.getColumnIndex("ava");
            int con = cursor.getColumnIndex("cont");
            int ref = cursor.getColumnIndex("ref");

            ementa.setCod(cursor.getString(c));
            ementa.setNomeDisc(cursor.getString(n));
            ementa.setPreReq(cursor.getString(pr));
            ementa.setQtdCreitos(cursor.getString(ch));
            ementa.setCargaHoraria(cursor.getString(cr));
            ementa.setEmenta(cursor.getString(e));
            ementa.setObjetivo(cursor.getString(o));
            ementa.setMetodologia(cursor.getString(m));
            ementa.setAvalicao(cursor.getString(a));
            ementa.setContProg(cursor.getString(con));
            ementa.setReferencia(cursor.getString(ref));

        }
        return ementa;
    }

    //retorna lista de ramais
    public static ArrayList<Ramal> returnListaRamais(String nome) {
        Cursor cursor;
        String query;
        Ramal ramal;
        ArrayList<Ramal> listaRamais = new ArrayList<Ramal>();

        if (nome.isEmpty()) {

            query = "SELECT * FROM ramal order by nome_setor";

        } else {

            query = "SELECT * FROM ramal where  ramal.nome_setor like '%" + nome + "%' or ramal.sigla like '%" + nome + "%' order by nome_setor";
        }

        cursor = bd.rawQuery(query, null);

        while (cursor.moveToNext()) {

            int a = cursor.getColumnIndex("id");
            int b = cursor.getColumnIndex("nome_setor");
            int c = cursor.getColumnIndex("sigla");
            int d = cursor.getColumnIndex("ramal");

            ramal = new Ramal();
            ramal.setId(cursor.getInt(a));
            ramal.setNomeclatura(cursor.getString(b));
            ramal.setSigla(cursor.getString(c));
            ramal.setRamal(cursor.getString(d));
            listaRamais.add(ramal);
        }
        return listaRamais;
    }


    /*Metodo usado para retornar uma string que informa o semestre atual, é atualizada pelo servidor web*/
    public static SemestreVirgente returnSemestreVirgente() {
        SemestreVirgente svdb = null;


        String query = "SELECT * FROM semestre_virgente";

        Cursor cursor = bd.rawQuery(query, null);

        while (cursor.moveToNext()) {

            int a = cursor.getColumnIndex("codigo");
            int b = cursor.getColumnIndex("ano");
            int c = cursor.getColumnIndex("periodo");


            svdb = new SemestreVirgente();
            svdb.setCodigo(cursor.getString(a));
            svdb.setAno(cursor.getString(b));
            svdb.setPeriodo(cursor.getString(c));

        }

        return svdb;
    }

    //Metodo para retornar todos os funcionarios
    public static ArrayList<Funcionario> retornaListaFuncionarios() {
        Funcionario funcionario = null;
        ArrayList<Funcionario> listaFuncionarios = new ArrayList<>();

        final String query = "SELECT nome, email, tel, cargo, sexo FROM funcionario";

        Cursor cursor = bd.rawQuery(query, null);


        while (cursor.moveToNext()) {


            int i = cursor.getColumnIndex("codigo");
            int n = cursor.getColumnIndex("nome");
            int e = cursor.getColumnIndex("email");
            int t = cursor.getColumnIndex("tel");
            int c = cursor.getColumnIndex("cargo");
            int s = cursor.getColumnIndex("sexo");

            funcionario = new Funcionario();
            funcionario.setNome(cursor.getString(n));
            funcionario.setEmail(cursor.getString(e));
            funcionario.setCargo(cursor.getString(c));
            funcionario.setTel(cursor.getString(t));
            funcionario.setSexo(cursor.getString(s));
            listaFuncionarios.add(funcionario);
        }

        return listaFuncionarios;
    }

    //verifica se as tabelas estão preenchidas
    public static ArrayList<StatusTabelas> verificaTabelasEstaoPreenchidas() {
        String query;
        StatusTabelas statusTabelas;
        ArrayList<StatusTabelas> listStatusTabelas = new ArrayList<>();

        Cursor cursor = null;
        String[] tabelas = {"funcionario", "professor", "sala", "hora_aula", "dia_semana", "disciplina", "ementa_disciplina", "disciplina_dia_hora", "dia_hora", "horario_onibus", "settings", "ramal"};

        for (int i = 0; i < tabelas.length; i++) {

            query = "SELECT * FROM " + tabelas[i] + "";
            cursor = bd.rawQuery(query, null);

            if (!cursor.moveToNext()) {

                statusTabelas = new StatusTabelas(tabelas[i], false);
                listStatusTabelas.add(statusTabelas);
            }
        }
        return listStatusTabelas;
    }

    //retorna uma lista contendo todos horios dos onibus tem como parametros de entrada a query(cidade, hora, tipoDeViagem)
    public static ArrayList<HorarioOnibus> retornaListaHorarioOnibus(String query) {
        ArrayList<HorarioOnibus> lHoDB = new ArrayList<>();

        Cursor cursor = bd.rawQuery(query, null);
        while (cursor.moveToNext()) {

            lHoDB.add(setHorarioOnibus(cursor));
        }

        return lHoDB;
    }


    public static Settings retornaTabelasDataAtualizacoes() {
        final String query = "SELECT cod, att_funcionario, att_professor,att_sala,att_semestre_virgente,att_disciplina,att_ementa,att_pre_requisito,att_disciplina_dia_hora, att_horario_onibus" +
                " FROM settings";

        Cursor cursor = bd.rawQuery(query, null);
        while (cursor.moveToNext()) {
            return setSettings(cursor);
        }
        cursor.close();
        return null;
    }


    public static void atualizaTabelaFuncionario(ArrayList<Func> lFuncSw) {
        AtualizaTabelaFuncionario.run(lFuncSw, bd);
    }

    public static void atualizaTabelaProfessores(ArrayList<Professor> lprofSw) {
        AtualizaTabelaProfessores.run(lprofSw, bd);
    }

    public static void atualizaTabelaSalas(ArrayList<Sala> lSalasSw) {
        AtualizaTabelaSala.run(lSalasSw, bd);
    }

    public static void atualizaTabelaDisciplinaDiaHora(ArrayList<DisciplinaDiaHora> lDisDiaHSw) {

        AtualizaTabelaDisciplinaDiaHora.run(lDisDiaHSw, bd);
    }

    public static void atualizaTabelaHorarioOnibus(ArrayList<HorarioOnibus> lHoSw) {

        AtualizaTabelaHorarioOnibus.run(lHoSw, bd);
    }

    /*Chama a classe que atualiza a tabela de disciplinas*/
    public static void atualizaTabelaDisciplina(ArrayList<Disciplina> lDisciplinas) {

        AtualizaTabelaDisciplina.run(lDisciplinas, bd);
    }

    /*Chama a classe que atualiza a tabela do periodo do semestre*/
    public static void atualizaTabelaSemestreVirgente(SemestreVirgente svsw) {

        AtualizaTabelaSemestreVirgente.run(svsw, bd);
    }

    public static Disciplina retornaDisciplina(String nome) {
        Disciplina disc = null;

        final String query = "SELECT codigo, nome, carga_horaria, fk_semestre_virgente, semestre_diciplina, fk_curso, fk_professor, turma, CHS FROM disciplina where nome = '" + nome + "' ";

        Cursor cursor = bd.rawQuery(query, null);

        String j2;
        while (cursor.moveToNext()) {

            int a = cursor.getColumnIndex("codigo");
            int b = cursor.getColumnIndex("nome");
            int c = cursor.getColumnIndex("carga_horaria");
            int d = cursor.getColumnIndex("fk_semestre_virgente");
            int e = cursor.getColumnIndex("semestre_diciplina");
            int f = cursor.getColumnIndex("fk_curso");
            int g = cursor.getColumnIndex("fk_professor");
            int h = cursor.getColumnIndex("turma");
            int i = cursor.getColumnIndex("CHS");

            disc = new Disciplina();
            disc.setCodigo(cursor.getString(a));
            disc.setNome(cursor.getString(b));
            disc.setCarga_horaria(cursor.getString(c));
            disc.setFk_semestre_virgente(cursor.getString(d));
            disc.setSemestre_diciplina(cursor.getString(e));
            disc.setFk_curso(cursor.getString(f));
            disc.setFk_professor(cursor.getString(g));
            disc.setTurma(cursor.getString(h));
            disc.setCHS(cursor.getString(i));
        }

        return disc;
    }

    /*pega os dados da tabela de configuração local*/
    public static Settings selectSetting() {

        final String query = "SELECT cod, att_funcionario, att_professor, att_sala, att_semestre_virgente, att_disciplina, att_ementa, att_pre_requisito, att_disciplina_dia_hora, att_horario_onibus FROM settings";

        Cursor cursor = bd.rawQuery(query, null);
        while (cursor.moveToNext()) {
            return setSettings(cursor);
        }
        return null;
    }

    /*Cria um objeto do tipo setting*/
    public static Settings setSettings(Cursor cursor) {
        Settings settings = new Settings();

        int c = cursor.getColumnIndex("cod");
        int f = cursor.getColumnIndex("att_funcionario");
        int p = cursor.getColumnIndex("att_professor");
        int s = cursor.getColumnIndex("att_sala");
        int se = cursor.getColumnIndex("att_semestre_virgente");
        int d = cursor.getColumnIndex("att_disciplina");
        int e = cursor.getColumnIndex("att_ementa");
        int pr = cursor.getColumnIndex("att_pre_requisito");
        int ddh = cursor.getColumnIndex("att_disciplina_dia_hora");
        int ho = cursor.getColumnIndex("att_horario_onibus");

        settings.setCod(cursor.getString(c));
        settings.setAtt_funcionario(cursor.getString(f));
        settings.setAtt_professor(cursor.getString(p));
        settings.setAtt_sala(cursor.getString(s));
        settings.setAtt_semestre_virgente(cursor.getString(se));
        settings.setAtt_disciplina(cursor.getString(d));
        settings.setAtt_ementa(cursor.getString(e));
        settings.setAtt_pre_requisito(cursor.getString(pr));
        settings.setAtt_disciplina_dia_hora(cursor.getString(ddh));
        settings.setAtt_horario_onibus(cursor.getString(ho));

        return settings;
    }

    public static HorarioOnibus setHorarioOnibus(Cursor cursor){
        HorarioOnibus ho = new HorarioOnibus();

        int a = cursor.getColumnIndex("cod");
        int b = cursor.getColumnIndex("num_linha");
        int c = cursor.getColumnIndex("via");
        int d = cursor.getColumnIndex("loc_saida");
        int e = cursor.getColumnIndex("loc_chegada");
        int f = cursor.getColumnIndex("cidade");
        int g = cursor.getColumnIndex("empresa");
        int h = cursor.getColumnIndex("hora_saida");
        int i = cursor.getColumnIndex("status");

        ho = new HorarioOnibus();
        ho.setCod(cursor.getString(a));
        ho.setNum_linha(cursor.getString(b));
        ho.setVia(cursor.getString(c));
        ho.setLoc_saida(cursor.getString(d));
        ho.setLoc_chegada(cursor.getString(e));
        ho.setCidade(cursor.getString(f));
        ho.setEmpresa(cursor.getString(g));
        ho.setHora_saida(cursor.getString(h));
        ho.setStatus(cursor.getString(i));

        return ho;
    }


}
