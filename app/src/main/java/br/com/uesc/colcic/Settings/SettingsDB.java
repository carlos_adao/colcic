package br.com.uesc.colcic.Settings;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import br.com.uesc.colcic.ConexaoInternet.AtualizaTabelas;
import br.com.uesc.colcic.ConexaoInternet.Objetos.DisciplinaDiaHora;
import br.com.uesc.colcic.DisciplinaDiaHora.DiscDiaHoraController;
import br.com.uesc.colcic.DisciplinaDiaHora.DiscDiaHoraDB;
import br.com.uesc.colcic.domain.Comandos;

public class SettingsDB {
    SQLiteDatabase db;
    Context context;
    DiscDiaHoraController ddhController;

    public SettingsDB(Context context, SQLiteDatabase db) {
        this.db = db;
        this.context = context;
    }

    /*Recebe um objeto setting do servidor e insere no banco de dados local*/
    public void insertSettings(Settings settingsRede) {
        Log.i("ho bus red", settingsRede.getAtt_horario_onibus());
        /*Configuração inicial da tabela settings*/
        String Es = "INSERT INTO settings (cod," +
                "att_funcionario," +
                "att_professor," +
                "att_sala," +
                "att_semestre_virgente," +
                "att_disciplina," +
                "att_ementa," +
                "att_pre_requisito," +
                "att_disciplina_dia_hora," +
                "att_horario_onibus)VALUES (" +
                "'" + settingsRede.getCod() + "'," +
                "'" + settingsRede.getAtt_funcionario() +
                "','" + settingsRede.getAtt_professor() + "'," +
                "'" + settingsRede.getAtt_sala() +
                "','" + settingsRede.getAtt_semestre_virgente() + "'," +
                "'" + settingsRede.getAtt_disciplina() +
                "','21-09-2017'," +
                "'21-09-2017'," +
                "'" + settingsRede.getAtt_disciplina_dia_hora() +
                "','" + settingsRede.getAtt_horario_onibus() + "')";
        String[] insertSettigns = {Es};
        Comandos.executeSQL(insertSettigns);
    }

    /*Atualização da tabela de Settings*/
    public void updateSettings(Settings settingsRede) {

        String ud = "UPDATE settings SET cod = '" + settingsRede.getCod() + "'" +
                ", att_funcionario = '" + settingsRede.getAtt_funcionario() + "'" +
                ", att_professor = '" + settingsRede.getAtt_professor() + "'" +
                ", att_sala = '" + settingsRede.getAtt_sala() + "'" +
                ", att_semestre_virgente = '" + settingsRede.getAtt_semestre_virgente() + "'" +
                ", att_disciplina = '" + settingsRede.getAtt_disciplina() + "'" +
                ", att_ementa = '" + settingsRede.getAtt_ementa() + "'" +
                ", att_pre_requisito = '" + settingsRede.getAtt_pre_requisito() + "'" +
                ", att_disciplina_dia_hora = '" + settingsRede.getAtt_disciplina_dia_hora() + "'" +
                ", att_horario_onibus = '" + settingsRede.getAtt_horario_onibus() + "'" +
                "  WHERE cod = '" + settingsRede.getCod() + "' ;";
        String[] sql = {ud};
        Comandos.executeSQL(sql);
    }

    /*Exclusão da tabela de Settings*/
    public void deleteSettings() {
        /*deleta todos os dados da tabela de configuração*/
        String comando = "DELETE FROM settings";
        String[] deleteSettigns = {comando};
        Comandos.executeSQL(deleteSettigns);
    }

    /*Verifica se a tabela settings está preenchida*/
    public Boolean settingsEstaPreenchida() {
        if (retornaTabelaAtualizacoes() != null) {
            return true;
        }
        return false;
    }

    /*Retorna a tabela de conficurações*/
    public Settings retornaTabelaAtualizacoes() {
        String query = "SELECT cod, att_funcionario, att_professor,att_sala,att_semestre_virgente,att_disciplina,att_ementa,att_pre_requisito,att_disciplina_dia_hora, att_horario_onibus" +
                " FROM settings";

        Cursor cursor = db.rawQuery(query, null);
        while (cursor.moveToNext()) {
            return setSettings(cursor);
        }
        cursor.close();
        return null;
    }

    /*Cria um objeto do tipo setting*/
    public Settings setSettings(Cursor cursor) {
        String cod, att_f, att_p, att_s, att_sv, att_d, att_e, att_pr, att_ddh, att_ho;

        int c = cursor.getColumnIndex("cod");
        int f = cursor.getColumnIndex("att_funcionario");
        int p = cursor.getColumnIndex("att_professor");
        int s = cursor.getColumnIndex("att_sala");
        int se = cursor.getColumnIndex("att_semestre_virgente");
        int d = cursor.getColumnIndex("att_disciplina");
        int e = cursor.getColumnIndex("att_ementa");
        int pr = cursor.getColumnIndex("att_pre_requisito");
        int ddh = cursor.getColumnIndex("att_disciplina_dia_hora");
        int ho = cursor.getColumnIndex("att_horario_onibus");

        cod = cursor.getString(c);
        att_f = cursor.getString(f);
        att_p = cursor.getString(p);
        att_s = cursor.getString(s);
        att_sv = cursor.getString(se);
        att_d = cursor.getString(d);
        att_e = cursor.getString(e);
        att_pr = cursor.getString(pr);
        att_ddh = cursor.getString(ddh);
        att_ho = cursor.getString(ho);

        return new Settings(cod, att_f, att_p, att_s, att_sv, att_d, att_e, att_pr, att_ddh, att_ho);
    }


    /*Metodos de atualização da tabela discplina dia hora do banco de dados local*/
    //recebe duas listas de DDH uma do WS e outra do BD e verifica se estão atualizadas entre si
    public void atualizaSettings(Settings settingsSW, Settings settingsDB) {

        int contInsert = 0;
        int contUpdate = 0;
        int contDelete = 0;


        AtualizaTabelas att;

        if (!settingsEstaPreenchida()) {

            Log.i("LOG", "A tabela settings não está preenchida!!!");

            att = new AtualizaTabelas();
            att.setTipo(1);
            att.execute();
            att = new AtualizaTabelas();
            att.setTipo(2);
            att.execute();
            att = new AtualizaTabelas();
            att.setTipo(3);
            att.execute();
            att = new AtualizaTabelas();
            att.setTipo(4);
            att.execute();
            att = new AtualizaTabelas();
            att.setTipo(5);
            att.execute();
            att = new AtualizaTabelas();
            att.setTipo(8);
            att.execute();
            att = new AtualizaTabelas();
            att.setTipo(9);
            att.execute();

            Comandos.insertSettings(settingsSW);

        } else {

            Log.i("LOG", "A tabela settings está preenchida!!!");
            Comandos.retornaTabelasDataAtualizacoes();

            if (!(settingsSW.getAtt_funcionario().equalsIgnoreCase(settingsDB.getAtt_funcionario()))) {
                att = new AtualizaTabelas();
                att.setTipo(1);
                att.execute();
                Comandos.updateSettings(settingsSW);


            }

            if (!(settingsSW.getAtt_professor().equalsIgnoreCase(settingsDB.getAtt_professor()))) {
                att = new AtualizaTabelas();
                att.setTipo(2);
                att.execute();
                Comandos.updateSettings(settingsSW);

            }

            if (!(settingsSW.getAtt_sala().equalsIgnoreCase(settingsDB.getAtt_sala()))) {
                att = new AtualizaTabelas();
                att.setTipo(3);
                att.execute();
                Comandos.updateSettings(settingsSW);

            }

            if (!(settingsSW.getAtt_semestre_virgente().equalsIgnoreCase(settingsDB.getAtt_semestre_virgente()))) {
                att = new AtualizaTabelas();
                att.setTipo(4);
                att.execute();
                Comandos.updateSettings(settingsSW);
            }

            if (!(settingsSW.getAtt_disciplina().equalsIgnoreCase(settingsDB.getAtt_disciplina()))) {
                att = new AtualizaTabelas();
                att.setTipo(5);
                att.execute();

                Comandos.updateSettings(settingsSW);
            }

            if (!(settingsSW.getAtt_ementa().equalsIgnoreCase(settingsDB.getAtt_ementa()))) {


            }

            if (!(settingsSW.getAtt_pre_requisito().equalsIgnoreCase(settingsDB.getAtt_pre_requisito()))) {


            }

            if (!(settingsSW.getAtt_disciplina_dia_hora().equalsIgnoreCase(settingsDB.getAtt_disciplina_dia_hora()))) {

                /*Instanciando a classe DDH server*/
                ddhController = new DiscDiaHoraController();
                DiscDiaHoraDB DDHDB = new DiscDiaHoraDB(db);
                DDHDB.deleteAllDisciplinaDiaHora();
                ddhController.setContext(context, DDHDB);
                ddhController.execute();
                Comandos.updateSettings(settingsSW);
            }

            if (!(settingsSW.getAtt_horario_onibus().equalsIgnoreCase(settingsDB.getAtt_horario_onibus()))) {
                att = new AtualizaTabelas();
                att.setTipo(9);
                att.execute();
                Comandos.updateSettings(settingsSW);

            }
        }
    }
}
