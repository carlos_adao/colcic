package br.com.uesc.colcic.ListaRamais;


import android.animation.Animator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import br.com.uesc.colcic.R;


public class SampleMaterialAdapter extends RecyclerView.Adapter<SampleMaterialAdapter.ViewHolder> {
    private static final String DEBUG_TAG = "SampleMaterialAdapter";

    public Context context;
    public ArrayList<Card> cardsList;

    public SampleMaterialAdapter(Context context, ArrayList<Card> cardsList) {
        this.context = context;
        this.cardsList = cardsList;
    }

    /*AQUI ACONTECE O PREENCHIMENTO DO MEU CARD COM AS INFORMAÇÕES*/
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        String name = cardsList.get(position).getName();
        String ramal = cardsList.get(position).getRamal();
        int color = cardsList.get(position).getColorResource();
        TextView initial = viewHolder.nomeclatura;
        TextView ramalTextView = viewHolder.ramal;
        ramalTextView.setText(ramal);
        initial.setBackgroundColor(color);

        initial.setText(name);
    }

    @Override
    public void onViewDetachedFromWindow(ViewHolder viewHolder) {
        super.onViewDetachedFromWindow(viewHolder);
        viewHolder.itemView.clearAnimation();
    }

    @Override
    public void onViewAttachedToWindow(ViewHolder viewHolder) {
        super.onViewAttachedToWindow(viewHolder);
        animateCircularReveal(viewHolder.itemView);
    }

    public void animateCircularReveal(View view) {
        int centerX = 0;
        int centerY = 0;
        int startRadius = 0;
        int endRadius = Math.max(view.getWidth(), view.getHeight());
        Animator animation = ViewAnimationUtils.createCircularReveal(view, centerX, centerY, startRadius, endRadius);
        view.setVisibility(View.VISIBLE);
        animation.start();
    }

    @Override
    public int getItemCount() {
        if (cardsList.isEmpty()) {
            return 0;
        } else {
            return cardsList.size();
        }
    }

    @Override
    public long getItemId(int position) {
        return cardsList.get(position).getId();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        LayoutInflater li = LayoutInflater.from(viewGroup.getContext());
        View v = li.inflate(R.layout.card_view_holder, viewGroup, false);
        return new ViewHolder(v);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView nomeclatura;
        private TextView ramal;


        public ViewHolder(View v) {
            super(v);
            nomeclatura = (TextView) v.findViewById(R.id.initial);
            ramal = (TextView) v.findViewById(R.id.ramal);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Pair<View, String> p1 = Pair.create((View) nomeclatura, ListaRamaisActivity.TRANSITION_INITIAL);

                    ActivityOptionsCompat options;
                    Activity act = (AppCompatActivity) context;
                    options = ActivityOptionsCompat.makeSceneTransitionAnimation(act, p1);//p3 p2

                    int requestCode = getAdapterPosition();
                    String name = cardsList.get(requestCode).getName();
                    String ramal = cardsList.get(requestCode).getRamal();
                    int color = cardsList.get(requestCode).getColorResource();



                    Intent transitionIntent = new Intent(context, RamalLigacaoActivity.class);
                    transitionIntent.putExtra(ListaRamaisActivity.EXTRA_NAME, name);
                    transitionIntent.putExtra(ListaRamaisActivity.EXTRA_RAMAL, ramal);
                    transitionIntent.putExtra(ListaRamaisActivity.EXTRA_INITIAL, Character.toString(name.charAt(0)));
                    transitionIntent.putExtra(ListaRamaisActivity.EXTRA_COLOR, color);
                    transitionIntent.putExtra(ListaRamaisActivity.EXTRA_UPDATE, false);
                    transitionIntent.putExtra(ListaRamaisActivity.EXTRA_DELETE, false);
                    ((AppCompatActivity) context).startActivityForResult(transitionIntent, requestCode, options.toBundle());
                }
            });
        }
    }
}
