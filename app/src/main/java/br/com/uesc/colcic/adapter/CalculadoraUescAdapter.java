package br.com.uesc.colcic.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import br.com.uesc.colcic.R;
import br.com.uesc.colcic.activity.CalculadoraUescActivity;
import br.com.uesc.colcic.auxiliares.DecimalDigitsInputFilter;
import br.com.uesc.colcic.auxiliares.MyDigitsKeyListener;
import br.com.uesc.colcic.fragment.ResultadoCalculadoraUescFragment;


/**
 * Created by ian on 11/08/17.
 */

public class CalculadoraUescAdapter extends RecyclerView.Adapter<CalculadoraUescAdapter.MyViewHolder> {

    private CalculadoraUescActivity mContext;
    private ArrayList<String> lista;



    public class MyViewHolder extends RecyclerView.ViewHolder {
        public EditText ed_credito;
        public ImageView icon_btn_menos;
        LinearLayout ll_iv_mais_calculadora_uesc;

        public MyViewHolder(View v) {
            super(v);

            icon_btn_menos = (ImageView) v.findViewById(R.id.icon_btn_menos);
            ed_credito = (EditText) v.findViewById(R.id.ed_credito);
            ll_iv_mais_calculadora_uesc = (LinearLayout) v.findViewById(R.id.ll_iv_mais_calculadora_uesc);
        }
    }

    public CalculadoraUescAdapter(CalculadoraUescActivity mContext, ArrayList<String> lista) {
        this.mContext = mContext;
        this.lista = lista;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.linha_calculadora_uesc, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        holder.ed_credito.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(2, 2)});
        holder.ed_credito.setKeyListener( new MyDigitsKeyListener( false, true ) );
        holder.ed_credito.setHint((position+2)+"º Credito");
        holder.ed_credito.setText(lista.get(position));
        holder.ed_credito.setOnFocusChangeListener(focoTexto);
        holder.ed_credito.addTextChangedListener(new TextWatcher() {
            boolean isUpdating;
            public void afterTextChanged(Editable s) {

            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {

            }

            @Override
            public void onTextChanged(
                    CharSequence s, int start, int before, int after) {

                if (isUpdating) {
                    isUpdating = false;
                    return;
                }
                boolean hasMask = s.toString().indexOf('.') > -1 || s.toString().indexOf('-') > -1 || s.toString().indexOf(',') > -1;

                // Remove o '.' e '-' da String
                String str = s.toString().replaceAll("[.]", "").replaceAll("[-]", "").replaceAll("[,]", "");

                if (after > before) {

                    if (str.equals("100")) {//verifica se a nota digita é dez
                        str = str.substring(0, 2) + '.' + str.substring(2);
                        isUpdating = true;
                        holder.ed_credito.setText(str);
                        holder.ed_credito.setSelection(Math.max(0, Math.min(hasMask ? start - (before) : start, str.length())));
                    } else if (str.length() > 0) {

                        str = str.substring(0, 1) + '.' + str.substring(1);
                    }
                    // Seta a flag pra evitar chamada infinita
                    isUpdating = true;
                    // seta o novo texto
                    holder.ed_credito.setText(str);
                    // seta a posição do cursor
                    holder.ed_credito.setSelection(holder.ed_credito.getText().length());

                } else {
                    if (str.length() == 2) {
                        str = str.substring(0, 1) + '.' + str.substring(1);
                        isUpdating = true;
                        holder.ed_credito.setText(str);
                        holder.ed_credito.setSelection(Math.max(0, Math.min(hasMask ? start - (before - 1) : start, str.length())));
                    } else {
                        isUpdating = true;
                        holder.ed_credito.setText(str);

                        holder.ed_credito.setSelection(Math.max(0, Math.min(hasMask ? start - (before) : start, str.length())));
                    }
                }
                //reposiciona o ponto para que á nota dez fique correta
                if (s.toString().equals("1.00")) {
                    CalculadoraUescActivity.ltest.set(position, "10.0");
                } else {
                    CalculadoraUescActivity.ltest.set(position, s.toString());
                }
            }
        });

        holder.icon_btn_menos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                CalculadoraUescActivity.ltest.remove(position);
                mContext.desenhaLinhas();
                if (CalculadoraUescActivity.fg_resultado_aberto) {
                    mContext.fechaFragmentoResultado();
                    CalculadoraUescActivity.fechaResultado();
                }
            }
        });
        holder.ll_iv_mais_calculadora_uesc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                CalculadoraUescActivity.ltest.remove(position);
                mContext.desenhaLinhas();
                if (CalculadoraUescActivity.fg_resultado_aberto) {
                    mContext.fechaFragmentoResultado();
                    CalculadoraUescActivity.fechaResultado();
                }
            }
        });

        holder.ed_credito.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (keyEvent != null && keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    if (keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                        mContext.desenhaLinhasBranco();
                        return false;
                    }

                }
                return false;
            }
        });

        holder.ed_credito.requestFocus();

    }

    @Override
    public int getItemCount() {
        return lista.size();
    }


    //Método usuado para evento de FOCO nos campo de texto
    private View.OnFocusChangeListener focoTexto = new View.OnFocusChangeListener()

    {

        @Override
        public void onFocusChange(View view, boolean hasFocus) {

            switch (view.getId()) {
                /*clicando no icon btn reset*/
                case R.id.ed_credito:
                    if (hasFocus) {

                        if (CalculadoraUescActivity.fg_resultado_aberto) {
                            CalculadoraUescActivity.fechaResultado();
                            mContext.fechaFragmentoResultado();

                        }
                    } else {

                        if (CalculadoraUescActivity.fg_resultado_aberto) {
                            // CalculadoraUescActivity.fechaResultado();
                        }
                    }

                    break;

                default:
                    break;


            }
        }
    };

}
